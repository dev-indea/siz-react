# Step 1

FROM node:12-alpine as build-step

RUN mkdir /app

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . /app

RUN npm run build


# Stage 2

FROM nginx:1.17.1-alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*

COPY --from=build-step /app/build /usr/share/nginx/html

RUN  touch /var/run/nginx.pid && \
     chown -R nginx:nginx /var/cache/nginx /var/run/nginx.pid /usr/share/nginx/html /etc/nginx

RUN chmod -R 777 /etc/nginx/conf.d /var/cache/nginx /etc/nginx/nginx.conf /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]