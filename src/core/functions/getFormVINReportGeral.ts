import { FormVINReportGeral } from '../../sdk/@types/FormVINReportGeral';
import * as XLSX from 'xlsx';
import moment from 'moment';

const getReportData = async (formVINs: FormVINReportGeral.Detailed[]) => {
  let data: any[] = [];
  let formVINArray: any[];

  const headers = [
    [
      'Número Form IN',
      'Número SISBRAVET',
      'Protocolo SISBRAVET',

      'Número Inspeção',
      'Data Inspeção',
      'Data de Cadastro',
      'Tipo Estabelecimento',
      'Nome Estabelecimento',
      'Código Estabelecimento',
      'Código Exploração',
      'Nome Proprietario',

      'Latitude - grau',
      'Latitude - min',
      'Latitude - seg',
      'Longitude - grau',
      'Longitude - min',
      'Longitude - seg',

      'Ausência de animais doentes',
      'Observações',
      'Nome Representante',
      'Funções no estabelecimento',
      'Vínculo Epidemiológico',
      'Espécie',
      'Tipo de Agrupamento',
      'Total de Agrupamentos existentes',
      'Total de Animais existentes',
      'Total de Agrupamentos vistoriados',
      'Total de Animais vistoriados',
      'Total de Agrupamentos examinados',
      'Total de Animais examinados',
      'Novo Estabelecimento para Investigação',
      'Novo Estabelecimento - Vínculo Epidemiológico',
    ],
  ];

  if (formVINs) {
    formVINs.forEach((formVIN) => {
      formVINArray = [];

      formVINArray.push(formVIN.numero);
      formVINArray.push(formVIN.numero_form_in_sisbravet);
      formVINArray.push(formVIN.numero_protocolo_sisbravet);

      formVINArray.push(formVIN.numero_inspecao);
      formVINArray.push(
        moment(formVIN.data_inspecao).format('DD/MM/YYYY').toString()
      );
      formVINArray.push(
        moment(formVIN.data_cadastro).format('DD/MM/YYYY').toString()
      );
      formVINArray.push(formVIN.tipo_estabelecimento);
      formVINArray.push(formVIN.nome_estabelecimento);

      switch (formVIN.tipo_estabelecimento) {
        case 'PROPRIEDADE':
          formVINArray.push(
            '51' + formVIN.estabelecimento_id?.toString().padStart(9, '0')
          );
          break;
        case 'ABATEDOURO':
          formVINArray.push(
            '51' + formVIN.estabelecimento_id?.toString().padStart(8, '0')
          );
          break;
        case 'RECINTO':
          formVINArray.push(
            '51' + formVIN.estabelecimento_id?.toString().padStart(8, '0')
          );
          break;
      }

      formVINArray.push(
        formVIN.exploracao_id !== null
          ? '51' + formVIN.exploracao_id?.toString().padStart(8, '0')
          : ''
      );

      formVINArray.push(formVIN.nome_proprietario);

      formVINArray.push(formVIN.coordenadaGeografica?.latGrau);
      formVINArray.push(formVIN.coordenadaGeografica?.latMin);
      formVINArray.push(formVIN.coordenadaGeografica?.latSeg);
      formVINArray.push(formVIN.coordenadaGeografica?.longGrau);
      formVINArray.push(formVIN.coordenadaGeografica?.longMin);
      formVINArray.push(formVIN.coordenadaGeografica?.longSeg);

      formVINArray.push(formVIN.ausencia_de_animais_doentes);
      formVINArray.push(formVIN.observacoes);

      formVINArray.push(formVIN.nome);
      formVINArray.push(formVIN.representante_funcoes);
      formVINArray.push(formVIN.vinculo_epidemiologico);
      formVINArray.push(formVIN.especie);
      formVINArray.push(formVIN.tipo_de_agrupamento);
      formVINArray.push(formVIN.total_agrup_existentes);
      formVINArray.push(formVIN.total_animais_existentes);
      formVINArray.push(formVIN.total_agrup_vistoriados);
      formVINArray.push(formVIN.total_animais_vistoriados);
      formVINArray.push(formVIN.total_agrup_examinados);
      formVINArray.push(formVIN.total_animais_examinados);
      formVINArray.push(formVIN.nome_novo_estabelecimento);
      formVINArray.push(formVIN.novo_estab_vinc_epidem);

      data.push(formVINArray);
    });
  }

  return { headers, data };
};

export default async function getFormVINReportGeral(
  visitas: FormVINReportGeral.Detailed[]
) {
  const reportData = await getReportData(visitas);

  let worksheet = XLSX.utils.json_to_sheet(reportData.data);

  XLSX.utils.sheet_add_aoa(worksheet, reportData.headers, {
    origin: 'A1',
  });

  let workBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workBook, worksheet, 'FormVINs SIZ v3');

  XLSX.writeFile(workBook, `formVINs.xlsx`);
}
