import MunicipioIDBService from '../../sdk/services/indexeddb/MunicipioIDB.service';

export async function offlinedataSaved(): Promise<boolean> {
  return await MunicipioIDBService.getAll()
    .then((list) => {
      if (list && list.length > 0) {
        return true;
      }
      return false;
    })
    .catch(() => {
      return Promise.resolve(false);
    });
}
