import localforage from 'localforage';
import {
  ServiceIDBPayload,
  ServiceIDBPayloadInput,
} from '../../sdk/services/indexeddb/ServiceIDB';
import FVVIDBService from '../../sdk/services/indexeddb/FVVIDB.service';
import FVERIDBService from '../../sdk/services/indexeddb/FVERIDB.service';
import Service from '../../sdk/services/SIZ-API/Service';
import AuthorizationService from '../auth/Authorization.service';
import { store } from '../store';

export default async function syncFVERs(manually?: boolean): Promise<string> {
  let errors = 0;

  const promise = new Promise<string>(async (resolve, reject) => {
    return await FVERIDBService.getAllFromInputTable()
      .then(async (listFVERsOffline) => {
        const saveFVERAsync = async () => {
          for (const fverOffline of listFVERsOffline) {
            if (
              fverOffline.status === 'SINCRONIZADO' ||
              fverOffline.status === 'NAO FINALIZADO'
            ) {
              //@TODO preguiça de negar a condição. Arrumar depois
            } else {
              await localforage.getItem('token').then(async (token) => {
                let access_token;
                if (token) access_token = token;
                else access_token = AuthorizationService.getAccessToken();

                if (!access_token) {
                  throw new Error('Sessão expirada');
                }

                await fetch(`${Service.BASE_URL}/visitas`, {
                  method: 'POST',
                  body: JSON.stringify(fverOffline.payload),
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${access_token}`,
                  },
                })
                  .then(async (response) => {
                    if (response.status === 401) {
                      throw new Error('Sessão expirada');
                    }

                    if (response.ok) {
                      return await response
                        .json()
                        .then(async (fverResponse) => {
                          //update local FVER
                          let newFVER: ServiceIDBPayloadInput = {
                            ...fverOffline,
                            payload: {
                              ...fverOffline.payload,
                              id: fverResponse.id,
                              numero: fverResponse.numero,
                            },
                            status: 'SINCRONIZADO',
                          };

                          await FVERIDBService.update(
                            //@ts-ignore
                            fverOffline.id,
                            newFVER
                          );

                          //update FVV
                          await FVVIDBService.getByCodigoVerificador(
                            fverResponse.codigoVerificador
                          )
                            .then(async (fvv) => {
                              let newFVV: ServiceIDBPayload = {
                                ...fvv,
                                payload: {
                                  ...fvv.payload,
                                  visitaPropriedadeRural: fverResponse,
                                },
                              };

                              if (fvv) {
                                await FVVIDBService.update(
                                  //@ts-ignore
                                  fvv.id,
                                  newFVV
                                );
                              }
                            })
                            .catch((e) => {});
                        });
                    } else {
                      return await response
                        .json()
                        .then(async (fverResponse) => {
                          let motivoErro = fverResponse.detail;

                          errors = errors + 1;
                          let newFVER: ServiceIDBPayloadInput = {
                            ...fverOffline,
                            status: 'ERRO',
                            motivoErro,
                          };

                          return await FVERIDBService.update(
                            //@ts-ignore
                            fverOffline.id,
                            newFVER
                          );
                        });
                    }
                  })
                  .catch((e) => {
                    throw new Error(
                      'Não conseguimos conectar com o servidor. Motivo: ' + e
                    );
                  });
              });
            }
          }
        };
        await saveFVERAsync();
      })
      .then(async () => {
        if (errors === 0) {
          return resolve('FVERs sincronizados com sucesso');
        } else {
          return reject(
            'A sincronização dos FVERs foi finalizada com erros. Alguns FVERs não puderam ser sincronizadas. Revise-os e tente novamente'
          );
        }
      })
      .catch((e) => {
        return reject(e.message);
      });
  });

  return await promise;
}
