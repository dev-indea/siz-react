import hash from 'object-hash';

export default function generateHash(obj: any) {
  return hash(obj);
}
