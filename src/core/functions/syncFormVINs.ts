import localforage from 'localforage';
import FormVINIDBService from '../../sdk/services/indexeddb/FormVINIDB.service';
import Service from '../../sdk/services/SIZ-API/Service';
import AuthorizationService from '../auth/Authorization.service';
import { store } from '../store';

export default async function syncFormVINs(
  manually?: boolean
): Promise<string> {
  const promise = new Promise<string>(async (resolve, reject) => {
    let errors = 0;

    await FormVINIDBService.getAll()
      .then(async (formVINs) => {
        return await Promise.all(
          formVINs.map(async (formVIN) => {
            return localforage.getItem('token').then(async (value) => {
              let access_token;
              if (value) access_token = value;
              else access_token = AuthorizationService.getAccessToken();

              if (!access_token) {
                return Promise.reject(
                  'A sua sessão expirou e não foi possível sincronizar automaticamente.'
                );
              }

              return await fetch(`${Service.BASE_URL}/form-vins`, {
                method: 'POST',
                body: JSON.stringify(formVIN.payload),
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: `Bearer ${access_token}`,
                },
              }).then(async (response) => {
                if (response.status === 401) {
                  throw new Error('Sessão expirada');
                }

                if (response.ok) {
                  //@ts-ignore
                  return await FormVINIDBService.delete(formVIN.id);
                } else {
                  errors = errors + 1;
                  formVIN.payload.status = 'ERRO';

                  await FormVINIDBService.update(
                    //@ts-ignore
                    formVIN.id,
                    formVIN
                  );
                }
              });
            });
          })
        ).catch((e) => {
          return reject(
            'Algumas formVINs não puderam ser sincronizadas. Revise-as e tente novamente'
          );
        });
      })
      .then(async () => {
        if (errors === 0) {
          return resolve(
            'Conexão de internet foi detectada e os dados foram sincronizados com sucesso'
          );
        } else {
          return reject(
            'Conexão de internet foi detectada, porém a sincronização foi finalizada com erros. Alguns Form VINs não puderam ser sincronizados. Revise-os e tente novamente'
          );
        }
      })
      .catch((e) => {
        return reject(
          'Algumas formVINs não puderam ser sincronizadas. Revise-as e tente novamente'
        );
      });
  });

  return await promise;
}
