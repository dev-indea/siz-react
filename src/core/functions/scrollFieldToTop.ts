export default function scrollFieldToTop(id: string) {
  /* document
    .querySelector(`#${id}`)
    ?.scrollIntoView({ block: 'center', behavior: 'smooth' }); */
  const element = document.querySelector(`#${id}`);
  //@ts-ignore
  const y = element?.getBoundingClientRect().top + window.scrollY - 130;
  window.scrollTo({ top: y, behavior: 'smooth' });

  // You can do this way
  // const elId = "#id" + id;
  // const element = document.querySelector(elId);
  // element.scrollIntoView({ behavior: "smooth" });
}
