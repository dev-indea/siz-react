import localforage from 'localforage';
import { Moment } from 'moment';
import { FVV } from '../../sdk/@types';
import { ServiceIDBPayloadInput } from '../../sdk/services/indexeddb/ServiceIDB';
import FVVIDBService from '../../sdk/services/indexeddb/FVVIDB.service';
import Service from '../../sdk/services/SIZ-API/Service';
import AuthorizationService from '../auth/Authorization.service';
import { store } from '../store';

export default async function syncFVVs(manually?: boolean): Promise<string> {
  const promise = new Promise<string>(async (resolve, reject) => {
    let errors = 0;

    return await FVVIDBService.getAll()
      .then(async (listaFVVsOffline) => {
        const saveFVVAsync = async () => {
          for (const fvvOffline of listaFVVsOffline) {
            if (
              fvvOffline.status === 'SINCRONIZADO' ||
              fvvOffline.status === 'NAO FINALIZADO'
            ) {
              //@TODO preguiça de negar a condição. Arrumar depois
            } else if (fvvOffline.payload.visitaPropriedadeRural.id !== -1) {
              let fccDTO: FVV.Input;
              const data = fvvOffline.payload.dataVigilancia;
              const dataMoment = data as Moment;

              fccDTO = {
                ...fvvOffline.payload,
                visitaPropriedadeRural: {
                  ...fvvOffline.payload.visitaPropriedadeRural,
                  dataDaVisita: undefined,
                },
              };

              await localforage.getItem('token').then(async (token) => {
                let access_token;
                if (token) access_token = token;
                else access_token = AuthorizationService.getAccessToken();

                if (!access_token) {
                  return Promise.reject(
                    'A sua sessão expirou e não foi possível sincronizar automaticamente.'
                  );
                }

                await fetch(`${Service.BASE_URL}/vigilancias-veterinarias`, {
                  method: 'POST',
                  body: JSON.stringify(fccDTO),
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${access_token}`,
                  },
                })
                  .then(async (response) => {
                    if (response.status === 401) {
                      throw new Error('Sessão expirada');
                    }

                    if (response.ok) {
                      return await response.json().then(async (fvvResponse) => {
                        let newFVV: ServiceIDBPayloadInput = {
                          ...fvvOffline,
                          payload: {
                            ...fvvOffline.payload,
                            id: fvvResponse.id,
                            numero: fvvResponse.numero,
                          },
                          status: 'SINCRONIZADO',
                        };

                        await FVVIDBService.update(
                          //@ts-ignore
                          fvvOffline.id,
                          newFVV
                        );
                      });
                    } else {
                      return await response.json().then(async (fvvResponse) => {
                        let motivoErro = fvvResponse.detail;

                        errors = errors + 1;
                        let newFVV: ServiceIDBPayloadInput = {
                          ...fvvOffline,
                          status: 'ERRO',
                          motivoErro,
                        };

                        return await FVVIDBService.update(
                          //@ts-ignore
                          fvvOffline.id,
                          newFVV
                        );
                      });
                    }
                  })
                  .catch((e) => {
                    throw new Error(
                      'Não conseguimos conectar com o servidor. Tente novamente mais tarde'
                    );
                  });
              });
            }
          }
        };

        await saveFVVAsync();
      })
      .then(async () => {
        if (errors === 0) {
          return resolve('FVVs sincronizados com sucesso com sucesso');
        } else {
          return reject(
            'A sincronização dos FVVs foi finalizada com erros. Alguns FVVs não puderam ser sincronizadas. Revise-os e tente novamente'
          );
        }
      })
      .catch((e) => {
        return reject(e.message);
      });
  });

  return await promise;
}
