import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import * as SyncingStateActions from '../store/SyncingState.slice';

export default function useSyncingState() {
  const dispatch = useDispatch();

  const fver = useSelector((state: RootState) => state.syncing.fver);
  const fvv = useSelector((state: RootState) => state.syncing.fvv);
  const formVin = useSelector((state: RootState) => state.syncing.formVin);

  const setFver = useCallback(
    (logingIn: boolean) => {
      dispatch(SyncingStateActions.setFver(logingIn));
    },
    [dispatch]
  );

  const setFvv = useCallback(
    (logingIn: boolean) => {
      dispatch(SyncingStateActions.setFvv(logingIn));
    },
    [dispatch]
  );

  const setFormVin = useCallback(
    (logingIn: boolean) => {
      dispatch(SyncingStateActions.setFormVin(logingIn));
    },
    [dispatch]
  );

  return {
    fver,
    fvv,
    formVin,
    setFver,
    setFvv,
    setFormVin,
  };
}
