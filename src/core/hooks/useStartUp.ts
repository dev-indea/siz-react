import useBadgeState from './useBadgeState';
import NotificacaoSistemaService from '../../sdk/services/SIZ-API/NotificacaoSistema.service';
import AuthorizationService from '../auth/Authorization.service';

export default function useStartUp() {
  const { setNotificacoes } = useBadgeState();

  const init = () => {
    if (window.navigator.onLine)
      if (AuthorizationService.getUsuarioSIZ())
        NotificacaoSistemaService.getByUsuario(
          //@ts-ignore
          AuthorizationService.getUsuarioSIZ()?.userName
        )
          .then((notificacoesSistema) => {
            if (notificacoesSistema && notificacoesSistema.length > 0)
              setNotificacoes(notificacoesSistema.length);
          })
          .catch(() => {});
  };

  return {
    init,
  };
}
