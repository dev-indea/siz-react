import { useCallback, useState } from 'react';
import { Aglomeracao } from '../../sdk/@types/Aglomeracao';
import AglomeracaoService from '../../sdk/services/SIZ-API/Aglomeracao.service';

export default function useAglomeracao() {
  const [aglomeracao, setAglomeracao] = useState<Aglomeracao.Summary>();
  const [fetching, setFetching] = useState<boolean>(false);

  const fetchAglomeracaoByCodigo = useCallback((aglomeracaoCodigo: string) => {
    try {
      setFetching(true);
      AglomeracaoService.getByCodigo(aglomeracaoCodigo).then(setAglomeracao);
    } finally {
      setFetching(false);
    }
  }, []);

  return {
    aglomeracao,
    fetching,
    fetchAglomeracaoByCodigo,
  };
}
