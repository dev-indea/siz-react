import { useCallback, useState } from 'react';
import { Veterinario } from '../../sdk/@types/Veterinario';
import VeterinarioIDBService from '../../sdk/services/indexeddb/VeterinarioIDB.service';
import VeterinarioService from '../../sdk/services/SIZ-API/Veterinario.service';

export default function useVeterinario() {
  const [veterinario, setVeterinario] = useState<Veterinario.Summary>();
  const [veterinarios, setVeterinarios] = useState<Veterinario.Summary[]>();
  const [fetchingVeterinario, setFetchingVeterinario] =
    useState<boolean>(false);
  const [query, setQuery] = useState<Veterinario.Query>({
    profissionalOficial: true,
  });

  const fetchVeterinarios = useCallback(() => {
    VeterinarioService.getAll(query).then(setVeterinarios);
  }, [query]);

  const fetchExistingVeterinarioByCRMV = useCallback(
    async (crmv: string, fromApi?: boolean) => {
      try {
        setFetchingVeterinario(true);

        if (!fromApi) {
          return await VeterinarioIDBService.getByCRMV(crmv).then(
            async (payload) => {
              if (payload) setVeterinario(payload.payload);
              else if (window.navigator.onLine) {
                return await VeterinarioService.getByCRMV(crmv).then(
                  setVeterinario
                );
              }
            }
          );
        } else if (window.navigator.onLine)
          return await VeterinarioService.getByCRMV(crmv).then(setVeterinario);
      } catch (err) {
        throw err;
      } finally {
        setFetchingVeterinario(false);
      }
    },
    []
  );

  const fetchExistingVeterinarioByCPF = useCallback(
    async (cpf: string, fromApi?: boolean) => {
      try {
        setFetchingVeterinario(true);

        if (!fromApi) {
          return await VeterinarioIDBService.getByCpf(cpf).then(
            async (payload) => {
              if (payload) setVeterinario(payload.payload);
              else if (window.navigator.onLine) {
                return await VeterinarioService.getByCRMV(cpf).then(
                  setVeterinario
                );
              }
            }
          );
        } else if (window.navigator.onLine)
          return await VeterinarioService.getByCRMV(cpf).then(setVeterinario);
      } catch (err) {
        throw err;
      } finally {
        setFetchingVeterinario(false);
      }
    },
    []
  );

  return {
    fetchingVeterinario,
    fetchExistingVeterinarioByCPF,
    fetchExistingVeterinarioByCRMV,
    fetchVeterinarios,
    veterinarios,
    query,
    setQuery,
    veterinario,
  };
}
