import { useEffect } from 'react';

export default function usePageTitle(title: string) {
  const BASE_TITLE = 'SIZ';

  useEffect(() => {
    document.title = `${title} - ${BASE_TITLE}`;
  }, [title]);
}
