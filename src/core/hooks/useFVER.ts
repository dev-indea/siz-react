import localforage from 'localforage';
import { useCallback, useState } from 'react';
import { FVER } from '../../sdk/@types';
import { SYNC_VISITA_NEEDED } from '../../sdk/@types/ServiceWorker.types';
import FVERIDBService from '../../sdk/services/indexeddb/FVERIDB.service';
import FVERService from '../../sdk/services/SIZ-API/FVER.service';
import AuthorizationService from '../auth/Authorization.service';
import generateHash from '../functions/generateHash';
import {
  ServiceIDBPayload,
  ServiceIDBPayloadInput,
} from '../../sdk/services/indexeddb/ServiceIDB';
import FVVIDBService from '../../sdk/services/indexeddb/FVVIDB.service';

export default function useFVER() {
  const [fver, setFVER] = useState<FVER.Input | null>();
  const [fvers, setFVERs] = useState<FVER.Paginated>();
  const [visitas2, setVisitas2] = useState<FVER.Summary[]>();
  const [fetching, setFetching] = useState<boolean>(false);
  const [query, setQuery] = useState<FVER.Query>({
    page: 0,
    size: 10,
  });

  const fetchFVERs = useCallback(() => {
    return FVERService.getAll(query).then(setFVERs);
  }, [query]);

  const fetchFVERById = useCallback((id: number) => {
    return FVERService.getById(id).then(setFVER);
  }, []);

  const modifyQuery = useCallback(
    (modifiedQuery: FVER.Query) => {
      setQuery({
        ...query,
        ...modifiedQuery,
      });
    },
    [query]
  );

  const fetchByNumero = useCallback(
    async (numero: string, fromApi?: boolean) => {
      try {
        setFetching(true);

        if (!fromApi) {
          return await FVERIDBService.getByNumero(numero).then(
            async (payload) => {
              if (payload) {
                setFVER(payload?.payload);
              } else if (window.navigator.onLine) {
                return await FVERService.getByNumero(numero).then(setFVER);
              } else setFVER(null);
            }
          );
        } else {
          return await FVERService.getByNumero(numero).then(setFVER);
        }
      } catch (err) {
        throw err;
      } finally {
        setFetching(false);
      }
    },
    []
  );

  /* const fetchVisitaByNumero = useCallback((id: number) => {
    return VisitaPropriedadeRuralService.findByNumero(id).then(setVisita);
  }, []); */

  const fetchByMunicipioEData = useCallback(
    (codgIBGE: string, dataInicio: string, dataFim: string) => {
      return FVERService.getByMunicipio(codgIBGE, dataInicio, dataFim).then(
        setVisitas2
      );
    },
    []
  );

  const insert = useCallback((visitaInput: FVER.Input) => {
    visitaInput.codigoVerificador = generateHash(visitaInput);
    visitaInput.status = 'NOVO';

    if (window.navigator.onLine) {
      return FVERService.add(visitaInput);
    } else {
      if (navigator.serviceWorker)
        navigator.serviceWorker.ready.then((swRegistration) => {
          //@ts-ignore
          if (swRegistration.sync) {
            localforage.setItem('token', AuthorizationService.getAccessToken());
            navigator.serviceWorker.ready.then((registration) => {
              //@ts-ignore
              registration.sync.register(SYNC_VISITA_NEEDED);
            });
          } else {
          }
        });
      return FVERIDBService.insert(visitaInput);
    }
  }, []);

  const updateFVEROffline = useCallback(
    async (id: number, visita: FVER.Input) => {
      const oldCodigoVerificador = visita.codigoVerificador;
      const newCodigoVerificador = generateHash(visita);
      let newFVER: ServiceIDBPayloadInput = {
        id: id,
        payload: {
          ...visita,
          codigoVerificador: newCodigoVerificador,
        },
        status: 'NOVO',
        date: new Date(),
      };

      return await FVERIDBService.update(id, newFVER).then(async (idVisita) => {
        return await FVVIDBService.getByCodigoVerificador(
          oldCodigoVerificador
        ).then(async (fvv) => {
          if (fvv) {
            let newFVV: ServiceIDBPayload = {
              ...fvv,
              payload: {
                ...fvv.payload,
                visitaPropriedadeRural: {
                  ...visita,
                  codigoVerificador: newCodigoVerificador,
                  id: -1,
                },
              },
            };

            return await FVVIDBService.update(
              //@ts-ignore
              fvv.id,
              newFVV
            );
          }
        });
      });
    },
    []
  );

  return {
    fetchByMunicipioEData,
    fetchFVERs,
    fetchFVERById,
    fetchByNumero,
    insert,
    query,
    modifyQuery,
    setQuery,
    updateFVEROffline,
    fver,
    fvers,
    visitas2,
  };
}
