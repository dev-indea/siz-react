import { useCallback, useState } from 'react';
import { Servidor } from '../../sdk/@types/Servidor';
import ServidorIDBService from '../../sdk/services/indexeddb/ServidorIDB.service';
import ServidorService from '../../sdk/services/SIZ-API/Servidor.service';

export default function useServidor() {
  const [servidor, setServidor] = useState<Servidor.Summary | null>();
  const [fetchingServidor, setFetchingServidor] = useState<boolean>(false);

  const fetchExistingServidorByCpf = useCallback(
    async (cpf: string, fromApi?: boolean) => {
      try {
        setFetchingServidor(true);

        if (!fromApi) {
          return await ServidorIDBService.getByCpf(cpf).then(
            async (payload) => {
              if (payload) {
                setServidor(payload?.payload);
              } else if (window.navigator.onLine) {
                return await ServidorService.getByCpf(cpf).then(setServidor);
              } else setServidor(null);
            }
          );
        } else return await ServidorService.getByCpf(cpf).then(setServidor);
      } catch (err) {
        throw err;
      } finally {
        setFetchingServidor(false);
      }
    },
    []
  );

  const fetchExistingServidorByMatricula = useCallback(
    async (matricula: string, fromApi?: boolean) => {
      try {
        setFetchingServidor(true);

        if (!fromApi) {
          return await ServidorIDBService.getByMatricula(matricula).then(
            async (payload) => {
              if (payload) {
                setServidor(payload?.payload);
              } else if (window.navigator.onLine) {
                return await ServidorService.getByMatricula(matricula).then(
                  setServidor
                );
              } else setServidor(null);
            }
          );
        } else
          return await ServidorService.getByMatricula(matricula).then(
            setServidor
          );
      } catch (err) {
        throw err;
      } finally {
        setFetchingServidor(false);
      }
    },
    []
  );

  return {
    servidor,
    fetchingServidor,
    fetchExistingServidorByCpf,
    fetchExistingServidorByMatricula,
  };
}
