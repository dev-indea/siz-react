import jwtDecode from 'jwt-decode';
import { withRouter } from 'react-router-dom';
import AuthorizationService from './Authorization.service';

const parseJwt = (token) => {
  try {
    return jwtDecode(token);
  } catch (e) {
    return null;
  }
};

const AuthorizationVerify = (props) => {
  props.history.listen(() => {
    if (window.location.pathname === '/auth') return;

    if (window.location.pathname.startsWith('/visualizarFVER')) return;

    if (!window.navigator.onLine) return;
    if (window.location.pathname === '/logout') return;

    if (!AuthorizationService.isAuthenticated()) {
      AuthorizationService.logout();
      return;
    }
    const user = AuthorizationService.getUsuarioSIZ;
    const accessToken = AuthorizationService.getAccessToken();
    const refreshToken = AuthorizationService.getRefreshToken();

    if (!accessToken) {
      return;
    }

    if (!refreshToken) {
      return;
    }

    if (user) {
      const decodedAccesTokenJwt = parseJwt(
        AuthorizationService.getAccessToken()
      );

      if (!decodedAccesTokenJwt) return;
      if (new Date(decodedAccesTokenJwt.exp * 1000) < Date.now()) {
        return;
      } else {
        const decodedRefreshTokenJwt = parseJwt(
          AuthorizationService.getRefreshToken()
        );

        if (new Date(decodedRefreshTokenJwt.exp * 1000) < Date.now()) {
          return;
        }
      }
    }
  });

  return <div></div>;
};

export default withRouter(AuthorizationVerify);
