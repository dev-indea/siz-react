export enum FonteDaNotificacao {
  PROPRIEDADE = 'Propriedade',
  TERCEIRO = 'Terceiros',
  VIGILANCIA_SVO = 'Vigilância pelo SVO',
  MEDICO_VETERINARIO_PRIVADO = 'Médico veterinário privado',
  FRIGORIFICO = 'Frigorífico',
  CENTRO_DE_CONTROLE_ZOONOZES = 'Centro de controle de zoonozes',
}

export namespace FonteDaNotificacao {
  export function keys(): string[] {
    return Object.keys(FonteDaNotificacao).filter(
      //precisa filtrar as chaves que são nomes de métodos
      (key) => key !== 'keys' && key !== 'valueOf' && key !== 'keyOf'
    );
  }

  export function valueOf(key: string) {
    return FonteDaNotificacao[key as keyof typeof FonteDaNotificacao];
  }
  export function keyOf(value: string) {
    const indexOfS = Object.values(FonteDaNotificacao).indexOf(
      value as unknown as FonteDaNotificacao
    );

    const key = Object.keys(FonteDaNotificacao)[indexOfS];

    return key;
  }
}
