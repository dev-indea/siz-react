export enum UsoCamaDeAviario {
  ALIMENTACAO_DE_ANIMAIS = 'Alimentação de animais',
  ADUBACAO_DE_PASTAGEM = 'Adubação de pastagem',
  ADUBACAO_DE_HORTA = 'Adubação de horta',
}
export namespace UsoCamaDeAviario {
  export function keys(): string[] {
    return Object.keys(UsoCamaDeAviario).filter(
      //precisa filtrar as chaves que são nomes de métodos
      (key) => key !== 'keys' && key !== 'valueOf' && key !== 'keyOf'
    );
  }

  export function valueOf(key: string) {
    return UsoCamaDeAviario[key as keyof typeof UsoCamaDeAviario];
  }

  export function keyOf(value: string) {
    const indexOfS = Object.values(UsoCamaDeAviario).indexOf(
      value as unknown as UsoCamaDeAviario
    );

    const key = Object.keys(UsoCamaDeAviario)[indexOfS];

    return key;
  }
}
