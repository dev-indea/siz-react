import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface SyncingState {
  fver: boolean;
  fvv: boolean;
  formVin: boolean;
}

const initialState: SyncingState = {
  fver: false,
  fvv: false,
  formVin: false,
};

const SyncingStateSlice = createSlice({
  initialState,
  name: 'syncingState',
  reducers: {
    setFver(state, action: PayloadAction<boolean>) {
      state.fver = action.payload;
    },
    setFvv(state, action: PayloadAction<boolean>) {
      state.fvv = action.payload;
    },
    setFormVin(state, action: PayloadAction<boolean>) {
      state.formVin = action.payload;
    },
  },
});

export const { setFver, setFvv, setFormVin } = SyncingStateSlice.actions;

const SyncingStateReducer = SyncingStateSlice.reducer;
export default SyncingStateReducer;
