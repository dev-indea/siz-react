import { configureStore } from '@reduxjs/toolkit';
import AssinaturaBase64ImageReducer from './AssinaturaBase64Image.slice';
import LoadingPageReducer from './LoadingPage.slice';
import ServiceWorkerControllerReducer from './ServiceWorkerController.slice';
import SyncingStateReducer from './SyncingState.slice';
import FVERReducer from './FVER.slice';
import BadgeStateReducer from './BadgeState.slice';
import OfflinePersistStateReducer from './OfflinePersistState.slice';
import FVVReducer from './FVV.slice';

export const store = configureStore({
  reducer: {
    assinaturaBase64Image: AssinaturaBase64ImageReducer,
    badgeState: BadgeStateReducer,
    loadingPage: LoadingPageReducer,
    offlinePersistState: OfflinePersistStateReducer,
    serviceWorkerController: ServiceWorkerControllerReducer,
    syncing: SyncingStateReducer,
    fver: FVERReducer,
    fvv: FVVReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
