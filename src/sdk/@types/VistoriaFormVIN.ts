import { SIZ } from '.';

export namespace VistoriaFormVIN {
  export type Input = SIZ.components['schemas']['VistoriaFormVINInput'];
}
