import { SIZ } from '.';

export namespace Aglomeracao {
  export type Summary = SIZ.components['schemas']['AglomeracaoSummary'];
}
