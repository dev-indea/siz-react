import { SIZ } from '.';

export namespace Abatedouro {
  export type Summary = SIZ.components['schemas']['AbatedouroSummary'];
}
