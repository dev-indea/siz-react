import { SIZ } from './SIZ';

export namespace VigilanciaAves {
  export type Input = SIZ.components['schemas']['VigilanciaAvesInput'];
  export type Detailed = SIZ.components['schemas']['VigilanciaAvesDetailed'];
}
