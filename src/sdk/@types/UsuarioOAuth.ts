export type UsuarioOAuth = {
  sub: string;
  email_verified: boolean;
  cpf: string;
  name: string;
  preferred_username: string;
  given_name: string;
  dataNascimento: Date;
  family_name: string;
  email: string;
  nomeMae: string;
};
