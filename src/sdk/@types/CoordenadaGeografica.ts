import { SIZ } from '.';

export namespace CoordenadaGeografica {
  export type Detailed =
    SIZ.components['schemas']['CoordenadaGeograficaDetailed'];
}
