import { SIZ } from '.';

export namespace Propriedade {
  export type Summary = SIZ.components['schemas']['PropriedadeSummary'];
}
