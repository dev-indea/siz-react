import { SIZ } from '.';

export namespace FormIN {
  export type Summary = SIZ.components['schemas']['FormINSummary'];

  export type Query = {
    dataIni?: string;
    dataFim?: string;
  };
}
