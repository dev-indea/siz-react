import { SIZ } from '.';

export namespace Produtor {
  export type Summary = SIZ.components['schemas']['ProdutorSummary'];
  export type Input = SIZ.components['schemas']['ProdutorRequest'];
}
