import { SIZ } from '.';

export namespace Setor {
  export type Summary = SIZ.components['schemas']['SetorSummary'];
}
