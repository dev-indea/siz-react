import { SIZ } from '.';

export namespace ChavePrincipalFVER {
  export type Input =
    SIZ.components['schemas']['ChavePrincipalVisitaPropriedadeRuralInput'];
}
