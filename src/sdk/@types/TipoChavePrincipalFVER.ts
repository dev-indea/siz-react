import { SIZ } from '.';

export namespace TipoChavePrincipalFVER {
  export type Summary =
    SIZ.components['schemas']['TipoChavePrincipalVisitaPropriedadeRuralSummary'];
}
