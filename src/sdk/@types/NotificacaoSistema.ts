import { SIZ } from '.';

export namespace NotificacaoSistema {
  export type Response =
    SIZ.components['schemas']['NotificacaoSistemaResponse'];
  export type Request = SIZ.components['schemas']['NotificacaoSistemaRequest'];
}
