import { SIZ } from '.';

export namespace Municipio {
  export type Detailed = SIZ.components['schemas']['MunicipioDetailed'];
}
