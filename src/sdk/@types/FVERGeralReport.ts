import { SIZ } from '.';

export namespace FVERGeralReport {
  export type Summary =
    SIZ.components['schemas']['VisitaPropriedadeRuralGeralV3Response'];
}
