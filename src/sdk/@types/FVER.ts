import { SIZ } from './SIZ';

export namespace FVER {
  export type Paginated =
    SIZ.components['schemas']['Page«VisitaPropriedadeRuralSummary»'];
  export type Summary =
    SIZ.components['schemas']['VisitaPropriedadeRuralSummary'];
  export type Input = SIZ.components['schemas']['VisitaPropriedadeRuralInput'];
  export type Detailed =
    SIZ.components['schemas']['VisitaPropriedadeRuralDetailed'];

  export type Query = {
    page: number;
    size?: number;
    sort?: [keyof Summary, 'asc' | 'desc'];
    codigoMunicipio?: string;
    numero?: number;
    dataVisita?: string;
    dataInicio?: string;
    dataFim?: string;
    tipoEstabelecimento?: string;
    nomeEstabelecimento?: string;
    codigoEstabelecimento?: string;
    nomeServidor?: string;
    codigoVerificador?: string;
  };
}
