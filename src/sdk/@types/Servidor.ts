import { SIZ } from '.';

export namespace Servidor {
  export type Summary = SIZ.components['schemas']['ServidorSummary'];
}
