import { SIZ } from '.';

export namespace Exploracao {
  export type Summary = SIZ.components['schemas']['ExploracaoSummary'];
}
