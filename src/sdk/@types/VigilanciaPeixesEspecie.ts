import { SIZ } from './SIZ';

export namespace VigilanciaPeixesEspecie {
  export type Input = SIZ.components['schemas']['VigilanciaPeixesEspecieInput'];
}
