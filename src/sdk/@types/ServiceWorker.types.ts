export const SYNC_VISITA_NEEDED = 'SYNC_VISITA_NEEDED';
export const SYNC_VISITA_DONE = 'SYNC_VISITA_DONE';
export const SYNC_FORM_VIN_NEEDED = 'SYNC_FORM_VIN_NEEDED';
export const SYNC_FORM_VIN_DONE = 'SYNC_FORM_VIN_DONE';
export const SYNC_VIGILANCIA_NEEDED = 'SYNC_VIGILANCIA_NEEDED';
export const SYNC_VIGILANCIA_DONE = 'SYNC_VIGILANCIA_DONE';
export const SYNC_MUNICIPIO_DONE = 'SYNC_MUNICIPIO_DONE';
export const SYNC_MUNICIPIO_DONE_WITH_ERROR = 'SYNC_MUNICIPIO_DONE_WITH_ERROR';
