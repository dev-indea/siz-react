import { SIZ } from '.';

export namespace Recinto {
  export type Summary = SIZ.components['schemas']['RecintoSummary'];
}
