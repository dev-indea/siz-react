import { SIZ } from './SIZ';

export namespace FVV {
  export type Paginated =
    SIZ.components['schemas']['Page«VigilanciaVeterinariaSummary»'];
  export type Summary =
    SIZ.components['schemas']['VigilanciaVeterinariaSummary'];
  export type Input = SIZ.components['schemas']['VigilanciaVeterinariaInput'];
  export type Detailed =
    SIZ.components['schemas']['VigilanciaVeterinariaDetailed'];

  export type Query = {
    page: number;
    size?: number;
    sort?: [keyof Summary, 'asc' | 'desc'];
    codigoMunicipio?: number;
    numero?: number;
    dataVisita?: string;
    nomeEstabelecimento?: string;
    codigoVerificador?: string;
    numeroVisita?: string;
    estabelecimentoId?: number;
    nomeVeterinario?: string;
  };
}
