import { SIZ } from '.';

export namespace Usuario {
  export type Detailed = SIZ.components['schemas']['UsuarioDetailed'];
  export type Input = SIZ.components['schemas']['UsuarioInput'];
  export type Summary = SIZ.components['schemas']['UsuarioSummary'];

  export type Query = {
    nome: string;
    userName: string;
    cpf: string;
    tipoUsuario: string;
    ativoInativo: string;
  };
}
