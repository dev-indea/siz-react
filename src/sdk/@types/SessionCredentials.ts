import { SIZ } from '.';

export namespace SessionCredentials {
  export type Input = SIZ.components['schemas']['SessionCredentials'];
}
