import { SIZ } from './SIZ';

export namespace Mensagem {
  export type Body = SIZ.components['schemas']['Mensagem'];
}
