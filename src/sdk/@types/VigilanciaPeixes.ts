import { SIZ } from './SIZ';

export namespace VigilanciaPeixes {
  export type Input = SIZ.components['schemas']['VigilanciaPeixesInput'];
}
