import { SIZ } from './SIZ';

export namespace VigilanciaAvesEspecie {
  export type Input = SIZ.components['schemas']['VigilanciaAvesEspecieInput'];
}
