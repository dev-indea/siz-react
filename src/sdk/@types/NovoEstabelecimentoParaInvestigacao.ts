import { SIZ } from './SIZ';

export namespace NovoEstabelecimentoParaInvestigacao {
  export type Input =
    SIZ.components['schemas']['NovoEstabelecimentoParaInvestigacaoInput'];
}
