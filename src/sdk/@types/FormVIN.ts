import { SIZ } from '.';

export namespace FormVIN {
  export type Input = SIZ.components['schemas']['FormVINInput'];
  export type Detailed = SIZ.components['schemas']['FormVINDetailed'];
  export type Summary = SIZ.components['schemas']['FormVINSummary'];
  export type Paginated = SIZ.components['schemas']['Page«FormVINSummary»'];

  export type Query = {
    page: number;
    size?: number;
    sort?: [keyof Summary, 'asc' | 'desc'];
  };
}
