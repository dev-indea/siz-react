import { SIZ } from '.';

export namespace Veterinario {
  export type Summary = SIZ.components['schemas']['VeterinarioSummary'];

  export type Query = {
    profissionalOficial?: boolean;
  };
}
