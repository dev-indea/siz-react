import { SIZ } from '.';

export namespace FormVINReportGeral {
  export type Detailed =
    SIZ.components['schemas']['FormVINGeneralReportDetailed'];
}
