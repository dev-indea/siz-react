import { SIZ } from '.';

export namespace TipoChaveSecundariaFVER {
  export type Summary =
    SIZ.components['schemas']['TipoChaveSecundariaVisitaPropriedadeRuralSummary'];
}
