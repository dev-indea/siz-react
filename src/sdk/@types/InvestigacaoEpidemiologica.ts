import { SIZ } from '.';

export namespace InvestigacaoEpidemiologica {
  export type Input =
    SIZ.components['schemas']['InvestigacaoEpidemiologicaInput'];
  export type Detailed =
    SIZ.components['schemas']['InvestigacaoEpidemiologicaDetailed'];
}
