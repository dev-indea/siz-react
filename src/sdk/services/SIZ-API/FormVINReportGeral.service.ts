import { FormVINReportGeral } from '../../@types/FormVINReportGeral';
import Service from './Service';

class FormVINReportGeralService extends Service {
  static get(): Promise<FormVINReportGeral.Detailed[]> {
    return this.Http.get<FormVINReportGeral.Detailed[]>(
      '/form-vins/relatorio-geral'
    ).then(this.getData);
  }
}

export default FormVINReportGeralService;
