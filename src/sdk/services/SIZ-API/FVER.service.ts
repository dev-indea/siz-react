import { FVER } from '../../@types';
import generateQueryString from '../../utils/generateQueryString';
import Service from './Service';

class FVERService extends Service {
  static getAll(query: FVER.Query): Promise<FVER.Paginated> {
    const queryString = generateQueryString(query);
    return this.Http.get<FVER.Paginated>('/visitas'.concat(queryString)).then(
      this.getData
    );
  }

  static add(fver: FVER.Input) {
    return this.Http.post<FVER.Input>('/visitas', fver).then(this.getData);
  }

  static remove(id: number) {
    return this.Http.delete<boolean>(`/visitas/${id}`, {
      data: { id: id },
    }).then(this.getData);
  }

  static getById(id: number) {
    return this.Http.get<FVER.Input>(`/visitas/${id}`).then(this.getData);
  }

  static getByNumero(numero: string) {
    return this.Http.get<FVER.Input>(`/visitas/numero/${numero}`).then(
      this.getData
    );
  }

  static getByMunicipio(codgIBGE: string, dataIni: string, dataFim: string) {
    const queryString = generateQueryString({
      dataIni,
      dataFim,
    });
    return this.Http.get<FVER.Summary[]>(
      `/visitas/municipio/${codgIBGE}`.concat(queryString)
    ).then(this.getData);
  }

  static getByCodigoVerificador(codigoVerificador: string) {
    const queryString = generateQueryString({
      codigoVerificador,
    });
    return this.Http.get<FVER.Paginated>(`/visitas`.concat(queryString)).then(
      this.getData
    );
  }

  static getByCodigoVerificador_External(codigoVerificador: string) {
    return this.Http.get<FVER.Input>(
      `/visitas/codigo/${codigoVerificador}`
    ).then(this.getData);
  }

  static getByDataCadastro(dataCadastro: string) {
    const queryString = generateQueryString({
      dataCadastro,
    });
    return this.Http.get<FVER.Detailed[]>(
      `/visitas/data-cadastro`.concat(queryString)
    ).then(this.getData);
  }
}

export default FVERService;
