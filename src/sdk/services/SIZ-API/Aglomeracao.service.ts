import { Aglomeracao } from '../../@types';
import Service from './Service';

class AglomeracaoService extends Service {
  static getByCodigo(aglomeracaoCodigo: string) {
    return this.Http.get<Aglomeracao.Summary>(
      `/aglomeracoes/codigo/${aglomeracaoCodigo}`
    ).then(this.getData);
  }
}

export default AglomeracaoService;
