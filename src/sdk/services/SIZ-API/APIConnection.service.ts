import { VERSION } from '../../../app/EnvironmentConfig';
import Service from './Service';
import UsuarioService from './Usuario.service';

class APIConnectionService extends Service {
  static async isOnline(): Promise<boolean> {
    return await UsuarioService.checkIfExists(VERSION);
  }
}

export default APIConnectionService;
