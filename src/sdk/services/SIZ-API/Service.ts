import { notification } from 'antd';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

const Http = axios.create();

type SIZ_APIError = {
  status: number;
  title: string;
  detail: string;
  timestamp: string;
  type: string;
};

Http.interceptors.response.use(undefined, async (error) => {
  if (error.message?.includes('Network Error')) {
    notification.error({
      message: 'Não conseguimos conectar com o servidor',
      description: 'Tente novamente mais tarde',
    });
  }

  const erroAPI: SIZ_APIError = error.response.data;

  if (error?.response?.status === 502) {
    notification.error({
      message:
        'Ocorreu um erro tentando comunicar com o servidor. Contate o administrador do sistema.',
    });
  }

  return Promise.reject(error);
});

class Service {
  static BASE_URL = 'https://web.indea.mt.gov.br/siz-api';
  static NETWORK_ERROR = 'Network Error';

  protected static Http = Http;
  protected static getData = getData;

  public static setRequestInterceptor(
    onFulfilled: (
      request: AxiosRequestConfig
    ) => AxiosRequestConfig | Promise<AxiosRequestConfig>,
    onRejected?: (error: any) => any
  ) {
    Http.interceptors.request.use(onFulfilled, onRejected);
  }

  public static setResponseInterceptor(
    onFulfilled: (
      request: AxiosResponse
    ) => AxiosResponse | Promise<AxiosResponse>,
    onRejected?: (error: any) => any
  ) {
    Http.interceptors.response.use(onFulfilled, onRejected);
  }
}

function getDataThrowable<T>(res: AxiosResponse<T>) {
  return res.data;
}

function getData<T>(res: AxiosResponse<T>) {
  if (res instanceof Error) {
    //@ts-ignore
    if (!res.response) {
      throw new Error(Service.NETWORK_ERROR);
    } else {
      //@ts-ignore
      throw new Error((res.response.data as SIZ_APIError).detail);
    }
  } else {
    return getDataThrowable(res);
  }
}

Http.defaults.baseURL = Service.BASE_URL;

export default Service;
