import AbatedouroIDBService from './AbatedouroIDB.service';
import DBConfig from './DB.config';
import FormVINIDBService from './FormVINIDB.service';
import MunicipioIDBService from './MunicipioIDB.service';
import ProdutorIDBService from './ProdutorIDB.service';
import PropriedadeIDBService from './PropriedadeIDB.service';
import RecintoIDBService from './RecintoIDB.service';
import ServidorIDBService from './ServidorIDB.service';
import SetorIDBService from './SetorIDB.service';
import TipoChavePrincipalFVERIDBService from './TipoChavePrincipalFVERIDB.service';
import TipoChaveSecundariaFVERIDBService from './TipoChaveSecundariaFVERIDB.service';
import VeterinarioIDBService from './VeterinarioIDB.service';
import FVERIDBService from './FVERIDB.service';
import ServidorService from '../../services/SIZ-API/Servidor.service';
import TipoChavePrincipalFVERService from '../SIZ-API/TipoChavePrincipalFVER.service';
import TipoChaveSecundariaFVERService from '../SIZ-API/TipoChaveSecundariaFVER.service';
import VeterinarioService from '../../services/SIZ-API/Veterinario.service';
import ServiceIDB, { ServiceIDBPayload } from './ServiceIDB';
import FormINIDBService from './FormINIDB.service';
import { store } from '../../../core/store/index';
import { setDataBaseStatus } from '../../../core/store/LoadingPage.slice';
import FVVIDBService from './FVVIDB.service';

export enum InitializerStatus {
  CREATED = 'CREATED',
  ERROR = 'ERROR',
  STILL_VALID = 'STILL_VALID',
  UPDATED = 'UPDATED',
}

export async function initializeIndexedDB_v1() {
  let db: IDBDatabase;
  const request = indexedDB.open(DBConfig.DB_NAME, DBConfig.DB_VERSION);

  request.onupgradeneeded = (e) => {
    console.log('New Version', e.newVersion);
    db = request.result;

    const objectStoreNames = [
      AbatedouroIDBService.DB_STORE_ABATEDOURO,
      FormINIDBService.DB_STORE_FORM_IN,
      FormVINIDBService.DB_STORE_FORM_VIN,
      MunicipioIDBService.DB_STORE_MUNICIPIO,
      ProdutorIDBService.DB_STORE_PRODUTOR,
      PropriedadeIDBService.DB_STORE_PROPRIEDADE,
      RecintoIDBService.DB_STORE_RECINTO,
      ServidorIDBService.DB_STORE_SERVIDOR,
      SetorIDBService.DB_STORE_SETOR,
      TipoChavePrincipalFVERIDBService.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL,
      TipoChaveSecundariaFVERIDBService.DB_STORE_TIPO_CHAVE_SECUNDARIA_VISITA_PROPRIEDADE_RURAL,
      VeterinarioIDBService.DB_STORE_VETERINARIO,
      FVERIDBService.DB_STORE_FVER_LIST,
      FVERIDBService.DB_STORE_FVER_INPUT,
      FVVIDBService.DB_STORE_FVV,
    ];

    try {
      objectStoreNames.forEach((name) => {
        db.deleteObjectStore(name);
      });
    } catch (e) {
      console.log('PRIMEIRO ACESSO');
    }

    createObjects(db);
  };
}

function createObjects(db: IDBDatabase) {
  createStore(MunicipioIDBService.DB_STORE_MUNICIPIO, db).createIndex(
    MunicipioIDBService.DB_STORE_MUNICIPIO_INDEX_NAME,
    MunicipioIDBService.DB_STORE_MUNICIPIO_INDEX_KEY,
    { unique: true }
  );

  createStore(
    TipoChavePrincipalFVERIDBService.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL,
    db
  ).createIndex(
    TipoChavePrincipalFVERIDBService.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL_INDEX_NAME,
    TipoChavePrincipalFVERIDBService.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL_INDEX_KEY,
    { unique: true }
  );

  createStore(
    TipoChaveSecundariaFVERIDBService.DB_STORE_TIPO_CHAVE_SECUNDARIA_VISITA_PROPRIEDADE_RURAL,
    db
  ).createIndex(
    TipoChaveSecundariaFVERIDBService.DB_STORE_TIPO_CHAVE_SECUNDARIA_VISITA_PROPRIEDADE_RURAL_INDEX_NAME,
    TipoChaveSecundariaFVERIDBService.DB_STORE_TIPO_CHAVE_SECUNDARIA_VISITA_PROPRIEDADE_RURAL_INDEX_KEY,
    { unique: true }
  );

  const propriedadeStore = createStore(
    PropriedadeIDBService.DB_STORE_PROPRIEDADE,
    db
  );

  propriedadeStore.createIndex(
    PropriedadeIDBService.DB_STORE_PROPRIEDADE_INDEX_ID_NAME,
    PropriedadeIDBService.DB_STORE_PROPRIEDADE_INDEX_ID_KEY,
    {
      unique: true,
    }
  );

  propriedadeStore.createIndex(
    PropriedadeIDBService.DB_STORE_PROPRIEDADE_INDEX_MUNICIPIO_NAME,
    PropriedadeIDBService.DB_STORE_PROPRIEDADE_INDEX_MUNICIPIO_KEY
  );

  const produtorStore = createStore(ProdutorIDBService.DB_STORE_PRODUTOR, db);

  produtorStore.createIndex(
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_ID_NAME,
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_ID_KEY,
    {
      unique: true,
    }
  );

  produtorStore.createIndex(
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_CPF_NAME,
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_CPF_KEY,
    {
      multiEntry: true,
    }
  );

  produtorStore.createIndex(
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_NOME_NAME,
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_NOME_KEY,
    {
      multiEntry: true,
    }
  );

  produtorStore.createIndex(
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_MUNICIPIO_NAME,
    ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_MUNICIPIO_KEY
  );

  createStore(SetorIDBService.DB_STORE_SETOR, db).createIndex(
    SetorIDBService.DB_STORE_SETOR_INDEX_MUNICIPIO_NAME,
    SetorIDBService.DB_STORE_SETOR_INDEX_MUNICIPIO_KEY,
    {
      multiEntry: true,
    }
  );

  const abatedouroStore = createStore(
    AbatedouroIDBService.DB_STORE_ABATEDOURO,
    db
  );

  abatedouroStore.createIndex(
    AbatedouroIDBService.DB_STORE_ABATEDOURO_INDEX_CPF_NAME,
    AbatedouroIDBService.DB_STORE_ABATEDOURO_INDEX_CPF_KEY,
    {
      multiEntry: true,
    }
  );

  abatedouroStore.createIndex(
    AbatedouroIDBService.DB_STORE_ABATEDOURO_INDEX_MUNICIPIO_NAME,
    AbatedouroIDBService.DB_STORE_ABATEDOURO_INDEX_MUNICIPIO_KEY
  );

  const recintoStore = createStore(RecintoIDBService.DB_STORE_RECINTO, db);

  recintoStore.createIndex(
    RecintoIDBService.DB_STORE_RECINTO_INDEX_CODIGO_NAME,
    RecintoIDBService.DB_STORE_RECINTO_INDEX_CODIGO_KEY,
    {
      unique: true,
    }
  );

  recintoStore.createIndex(
    RecintoIDBService.DB_STORE_RECINTO_INDEX_MUNICIPIO_NAME,
    RecintoIDBService.DB_STORE_RECINTO_INDEX_MUNICIPIO_KEY
  );

  const servidorStore = createStore(ServidorIDBService.DB_STORE_SERVIDOR, db);

  servidorStore.createIndex(
    ServidorIDBService.DB_STORE_SERVIDOR_INDEX_CPF_NAME,
    ServidorIDBService.DB_STORE_SERVIDOR_INDEX_CPF_KEY,
    {
      unique: true,
    }
  );

  servidorStore.createIndex(
    ServidorIDBService.DB_STORE_SERVIDOR_INDEX_MATRICULA_NAME,
    ServidorIDBService.DB_STORE_SERVIDOR_INDEX_MATRICULA_KEY
  );

  const vetrinarioStore = createStore(
    VeterinarioIDBService.DB_STORE_VETERINARIO,
    db
  );

  vetrinarioStore.createIndex(
    VeterinarioIDBService.DB_STORE_VETERINARIO_INDEX_CPF_NAME,
    VeterinarioIDBService.DB_STORE_VETERINARIO_INDEX_CPF_KEY,
    {
      unique: true,
    }
  );

  vetrinarioStore.createIndex(
    VeterinarioIDBService.DB_STORE_VETERINARIO_INDEX_CRMV_NAME,
    VeterinarioIDBService.DB_STORE_VETERINARIO_INDEX_CRMV_KEY
  );

  createStore(FVERIDBService.DB_STORE_FVER_INPUT, db);

  const visitaPropriedadeRuralStore = createStore(
    FVERIDBService.DB_STORE_FVER_LIST,
    db
  );

  visitaPropriedadeRuralStore.createIndex(
    FVERIDBService.DB_STORE_FVER_LIST_INDEX_NUMERO_NAME,
    FVERIDBService.DB_STORE_FVER_LIST_INDEX_NUMERO_KEY,
    {
      unique: true,
    }
  );

  visitaPropriedadeRuralStore.createIndex(
    FVERIDBService.DB_STORE_FVER_LIST_INDEX_CODG_IBGE_NAME,
    FVERIDBService.DB_STORE_FVER_LIST_INDEX_CODG_IBGE_KEY
  );

  const vigilanciaVeterinariaStore = createStore(
    FVVIDBService.DB_STORE_FVV,
    db
  );
  vigilanciaVeterinariaStore.createIndex(
    FVVIDBService.DB_STORE_VIGILANCIA_VETERINARIA_INDEX_NAME,
    FVVIDBService.DB_STORE_VIGILANCIA_VETERINARIA_INDEX_KEY
  );

  createStore(FormVINIDBService.DB_STORE_FORM_VIN, db);

  const formINStore = createStore(FormINIDBService.DB_STORE_FORM_IN, db);

  formINStore.createIndex(
    FormINIDBService.DB_STORE_FORM_IN_INDEX_NUMERO_NAME,
    FormINIDBService.DB_STORE_FORM_IN_INDEX_NUMERO_KEY,
    {
      unique: true,
    }
  );

  formINStore.createIndex(
    FormINIDBService.DB_STORE_FORM_IN_INDEX_MUNICIPIO_NAME,
    FormINIDBService.DB_STORE_FORM_IN_INDEX_MUNICIPIO_KEY
  );
}

export async function persistOfflineGlobalData(
  forceUpdate?: boolean
): Promise<InitializerStatus[]> {
  console.log('INICIO SINCRONIZAÇÃO DADOS GLOBAIS');
  store.dispatch(setDataBaseStatus('SYNCING'));

  return await Promise.all([
    await persistTipoChavePrincipal(forceUpdate),
    await persistTipoChaveSecundaria(forceUpdate),
    await persistServidor(forceUpdate),
    await persistVeterinario(forceUpdate),
  ])
    .then((e): Promise<InitializerStatus[]> => {
      store.dispatch(setDataBaseStatus('FINISHED'));
      if (e.includes(InitializerStatus.ERROR)) {
        console.log('ERRO NA SINCRONIZAÇÃO DADOS GLOBAIS');
        throw Promise.resolve(InitializerStatus.ERROR);
      } else if (
        e.includes(InitializerStatus.CREATED) ||
        e.includes(InitializerStatus.UPDATED)
      ) {
        console.log('FINALIZAÇÃO SINCRONIZAÇÃO DADOS GLOBAIS');
        return Promise.resolve([InitializerStatus.UPDATED]);
      } else {
        console.log('FINALIZAÇÃO SINCRONIZAÇÃO DADOS GLOBAIS');
        return Promise.resolve([InitializerStatus.STILL_VALID]);
      }
    })
    .catch((e) => {
      console.log('ERRO NA SINCRONIZAÇÃO DADOS GLOBAIS');
      store.dispatch(setDataBaseStatus('FINISHED'));
      return Promise.resolve([InitializerStatus.ERROR]);
    })
    .finally(() => {
      store.dispatch(setDataBaseStatus('FINISHED'));
    });
}

function createStore(storeName: string, db: IDBDatabase) {
  const oS: IDBObjectStore = db.createObjectStore(storeName, {
    keyPath: 'id',
    autoIncrement: true,
  });

  return oS;
}

export async function persistTipoChavePrincipal(
  forceUpdate?: boolean
): Promise<InitializerStatus> {
  let listLocalTipoChavePrincipal: ServiceIDBPayload[];

  return await TipoChavePrincipalFVERIDBService.getAll()
    .then(async (result) => {
      let type: InitializerStatus;

      listLocalTipoChavePrincipal = result;

      if (forceUpdate) {
        type = InitializerStatus.UPDATED;

        await TipoChavePrincipalFVERIDBService.deleteAll();
      } else if (
        listLocalTipoChavePrincipal &&
        listLocalTipoChavePrincipal.length > 0
      ) {
        if (ServiceIDB.isDataExpired(listLocalTipoChavePrincipal[0].date)) {
          type = InitializerStatus.UPDATED;
          await TipoChavePrincipalFVERIDBService.deleteAll();
        } else {
          type = InitializerStatus.STILL_VALID;
        }
      } else type = InitializerStatus.CREATED;

      if (type !== InitializerStatus.STILL_VALID) {
        let listApiTipoChavePrincipal;
        await TipoChavePrincipalFVERService.getAllActive()
          .then(async (result) => {
            listApiTipoChavePrincipal = result;

            await TipoChavePrincipalFVERIDBService.addAllTipoChavePrincipalVisitaPropriedadeRural(
              listApiTipoChavePrincipal
            );
          })
          .catch((e) => {
            return Promise.resolve(InitializerStatus.ERROR);
          });
      }

      return Promise.resolve(type);
    })
    .catch((e) => {
      return Promise.resolve(InitializerStatus.ERROR);
    });
}

export async function persistTipoChaveSecundaria(
  forceUpdate?: boolean
): Promise<InitializerStatus> {
  let listLocalTipoChaveSecundaria: any;
  return await TipoChaveSecundariaFVERIDBService.getAll()
    .then(async (result) => {
      let type: InitializerStatus;

      listLocalTipoChaveSecundaria = result;

      if (forceUpdate) {
        type = InitializerStatus.UPDATED;
        await TipoChaveSecundariaFVERIDBService.deleteAll();
      } else if (
        listLocalTipoChaveSecundaria &&
        listLocalTipoChaveSecundaria.length > 0
      ) {
        if (ServiceIDB.isDataExpired(listLocalTipoChaveSecundaria[0].date)) {
          type = InitializerStatus.UPDATED;
          await TipoChaveSecundariaFVERIDBService.deleteAll();
        } else {
          type = InitializerStatus.STILL_VALID;
        }
      } else type = InitializerStatus.CREATED;

      if (type !== InitializerStatus.STILL_VALID) {
        let listApiTipoChaveSecundaria;
        await TipoChaveSecundariaFVERService.getAllActive().then(
          async (result) => {
            listApiTipoChaveSecundaria = result;

            await TipoChaveSecundariaFVERIDBService.addAllTipoChaveSecundariaVisitaPropriedadeRural(
              listApiTipoChaveSecundaria
            );
          }
        );
      }
      return Promise.resolve(type);
    })
    .catch((e) => {
      return Promise.resolve(InitializerStatus.ERROR);
    });
}

export async function persistServidor(
  forceUpdate?: boolean
): Promise<InitializerStatus> {
  let listLocalServidor: any;
  return await ServidorIDBService.getAll()
    .then(async (result) => {
      let type: InitializerStatus;

      listLocalServidor = result;

      if (forceUpdate) {
        type = InitializerStatus.UPDATED;
        await ServidorIDBService.deleteAll();
      } else if (listLocalServidor && listLocalServidor.length > 0) {
        if (ServiceIDB.isDataExpired(listLocalServidor[0].date)) {
          type = InitializerStatus.UPDATED;
          await ServidorIDBService.deleteAll();
        } else {
          type = InitializerStatus.STILL_VALID;
        }
      } else type = InitializerStatus.CREATED;

      if (type !== InitializerStatus.STILL_VALID) {
        let listApiServidor;
        await ServidorService.getAllActive().then(async (result) => {
          listApiServidor = result;

          await ServidorIDBService.addAllServidor(listApiServidor);
        });
      }
      return Promise.resolve(type);
    })
    .catch((e) => {
      return Promise.resolve(InitializerStatus.ERROR);
    });
}

export async function persistVeterinario(
  forceUpdate?: boolean
): Promise<InitializerStatus> {
  let listLocalVeterinario: any;
  return await VeterinarioIDBService.getAll()
    .then(async (result) => {
      let type: InitializerStatus;

      listLocalVeterinario = result;

      if (forceUpdate) {
        type = InitializerStatus.UPDATED;
        await VeterinarioIDBService.deleteAll();
      } else if (listLocalVeterinario && listLocalVeterinario.length > 0) {
        if (ServiceIDB.isDataExpired(listLocalVeterinario[0].date)) {
          type = InitializerStatus.UPDATED;
          await VeterinarioIDBService.deleteAll();
        } else {
          type = InitializerStatus.STILL_VALID;
        }
      } else type = InitializerStatus.CREATED;

      if (type !== InitializerStatus.STILL_VALID) {
        let listApiVeterinario;
        const query = { profissionalOficial: true };
        await VeterinarioService.getAll(query).then(async (result) => {
          listApiVeterinario = result;

          await VeterinarioIDBService.addAllVeterinario(listApiVeterinario);
        });
      }
      return Promise.resolve(type);
    })
    .catch((e) => {
      return Promise.resolve(InitializerStatus.ERROR);
    });
}
