import DBConfig from './DB.config';

class DB {
  static db: IDBDatabase;

  static openDatabase = (databaseName: string) => {
    const request = indexedDB.open(databaseName);

    request.onsuccess = () => {
      DB.db = request.result;
    };

    request.onupgradeneeded = () => {
      DB.db = request.result;
      createDBStructure(DB.db);
    };

    request.onerror = (e: Event) => {
      throw new Error(
        'Não foi possível abrir o banco de dados local',
        //@ts-ignore
        e.target.errorCode
      );
    };
  };
  /*
  createDBStructure = (db: IDBDatabase) => {
    const oS = db.createObjectStore(
      DBConfig.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL,
      {
        keyPath:
          DBConfig.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL_KEY,
      }
    );

    oS.createIndex(
      DBConfig.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL_KEY,
      DBConfig.DB_STORE_TIPO_CHAVE_PRINCIPAL_VISITA_PROPRIEDADE_RURAL_KEY,
      { unique: true }
    );
  };*/

  static getObjectStore = (
    store_name: string,
    db: IDBDatabase,
    mode?: IDBTransactionMode
  ): IDBObjectStore => {
    return db.transaction(store_name, mode).objectStore(store_name);
  };

  static clearObjectStore(store_name: string, db: IDBDatabase) {
    const store = this.getObjectStore(store_name, db, 'readwrite');
    const request = store.clear();
    request.onerror = (e) => {
      throw new Error(
        'Não foi limpar a store',
        //@ts-ignore
        e.target.errorCode
      );
    };
  }
}

export default DB;

function createDBStructure(db: IDBDatabase) {
  throw new Error('Function not implemented.');
}
