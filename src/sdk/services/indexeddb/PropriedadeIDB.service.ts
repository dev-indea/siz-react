import { Propriedade } from '../../@types';
import { openDB } from 'idb';
import DBConfig from './DB.config';
import ServiceIDB, { ServiceIDBPayload } from './ServiceIDB';
import ProdutorIDBService from './ProdutorIDB.service';

class PropriedadeIDBService extends ServiceIDB {
  static DB_STORE_PROPRIEDADE = 'propriedade';
  static DB_STORE_PROPRIEDADE_INDEX_ID_NAME = 'propriedade_index_id';
  static DB_STORE_PROPRIEDADE_INDEX_ID_KEY = 'payload.id';
  static DB_STORE_PROPRIEDADE_INDEX_MUNICIPIO_NAME =
    'propriedade_index_municipio';
  static DB_STORE_PROPRIEDADE_INDEX_MUNICIPIO_KEY =
    'payload.municipio.codgIBGE';

  static async getFirst() {
    const db = await openDB(DBConfig.DB_NAME);
    let cursor = await db
      .transaction(PropriedadeIDBService.DB_STORE_PROPRIEDADE)
      .store.openCursor();

    const first = cursor?.value;
    return first;
  }

  static async getAll() {
    const db = await openDB(DBConfig.DB_NAME);
    const list = await db.getAll(PropriedadeIDBService.DB_STORE_PROPRIEDADE);
    return list;
  }

  static async getAllPayload(): Promise<Propriedade.Summary[]> {
    let list: Propriedade.Summary[] = [];

    await this.getAll().then((result) => {
      list = result.map((row: any) => {
        return row.payload;
      });
    });

    return list;
  }

  static add(propriedade: Propriedade.Summary) {
    return openDB(DBConfig.DB_NAME).then((db) => {
      /* if (propriedade.id === 593584)
        throw new Error('Erro adicionando propriedade 593584'); */

      const date = new Date();
      const obj = {
        date: date,
        payload: propriedade,
      };

      db.add(this.DB_STORE_PROPRIEDADE, obj);

      propriedade.exploracaos?.forEach((exp) => {
        exp.produtores.forEach((produtor) => {
          db.add(ProdutorIDBService.DB_STORE_PRODUTOR, {
            produtor: {
              cpf: produtor.produtor.cpf,
              nome: produtor.produtor.nome,
            },
            propriedade: {
              id: propriedade.id,
              municipio: propriedade.municipio,
            },
          });
        });
      });
    });
  }

  static async addAllPropriedade(lista: Propriedade.Summary[]) {
    const db = await openDB(DBConfig.DB_NAME);
    lista.forEach((propriedade) => {
      const date = new Date();
      const obj = {
        date: date,
        payload: propriedade,
      };

      db.add(this.DB_STORE_PROPRIEDADE, obj);

      propriedade.exploracaos?.forEach((exp) => {
        exp.produtores.forEach((produtor) => {
          db.add(ProdutorIDBService.DB_STORE_PRODUTOR, {
            produtor: {
              cpf: produtor.produtor.cpf,
              nome: produtor.produtor.nome,
            },
            propriedade: {
              id: propriedade.id,
              municipio: propriedade.municipio,
            },
          });
        });
      });
    });
  }

  static async getByProdutorCpf(cpf: string): Promise<Propriedade.Summary[]> {
    let listaPropriedade: Propriedade.Summary[] = [];
    return await super
      ._getAllFromIndex(
        ProdutorIDBService.DB_STORE_PRODUTOR,
        ProdutorIDBService.DB_STORE_PRODUTOR_INDEX_CPF_NAME,
        cpf
      )
      .then((codigos: any[]) => {
        return Promise.all(
          codigos.map(async (element) => {
            return await PropriedadeIDBService.getById(
              element.propriedade.id
            ).then((e) => {
              listaPropriedade = listaPropriedade.concat(e.payload);
              return e.payload;
            });
          })
        );
      });
  }

  static async getById(id: number): Promise<ServiceIDBPayload> {
    return await super._getFromIndex(
      this.DB_STORE_PROPRIEDADE,
      this.DB_STORE_PROPRIEDADE_INDEX_ID_NAME,
      id
    );
  }

  static async deleteAll() {
    const db = await openDB(DBConfig.DB_NAME);
    return db.clear(this.DB_STORE_PROPRIEDADE);
  }

  static async deleteAllByMunicipio(codg_ibge: string) {
    return await super
      ._getAllFromIndex(
        this.DB_STORE_PROPRIEDADE,
        this.DB_STORE_PROPRIEDADE_INDEX_MUNICIPIO_NAME,
        codg_ibge
      )
      .then(async (propriedades: any[]) => {
        return await Promise.all(
          propriedades.map((propriedade) => {
            return super._delete(this.DB_STORE_PROPRIEDADE, propriedade.id);
          })
        );
      });
  }
}

export default PropriedadeIDBService;
