import { AxiosError } from 'axios';
import { notification } from 'antd';

export default function handleAxiosResponseError(error: AxiosError) {
  const { response } = error;

  console.log('ERRO AXIOS:', error);

  if (error.message?.includes('Network Error')) {
    throw new Error(
      'Não conseguimos conectar com o servidor. Tente novamente mais tarde'
    );
  }

  if (response?.status === 500) {
    throw new Error(
      'Ocorreu um erro ao buscar o registro. Contate o administrador do sistema'
    );
  }

  if (response?.status === 502) {
    notification.error({
      message: 'A conexão com o servidor foi perdida',
      description: 'Tente novamente em alguns minutos',
    });
    throw new Error(
      'Não conseguimos conectar com o servidor. Tente novamente mais tarde'
    );
  }

  if (response?.status === 400) {
    throw new Error(
      'Ocorreu um erro ao enviar os dados. Contate o administrador do sistema.'
    );
  }

  throw error;
}
