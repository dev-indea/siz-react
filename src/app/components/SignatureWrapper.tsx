import { HighlightOutlined } from '@ant-design/icons';
import { Button, Col, notification, Row, Space, Typography } from 'antd';
import { useCallback, useEffect, useRef, useState } from 'react';
import SignatureCanvas from 'react-signature-canvas';
import useAssinaturaBase64Image from '../../core/hooks/useAssinaturaBase64Image';
//@ts-ignore
import trimCanvas from 'trim-canvas';
import { CustomModal } from './CustomModal';

export default function SignatureWrapper(props: {
  onSaveSignature: () => any;
  onClearSignature: () => any;
  disabled: boolean;
}) {
  const [showModal, setShowModal] = useState<boolean>(false);

  const { assinaturaBase64Image, setAssinaturaBase64Image } =
    useAssinaturaBase64Image();

  const [isLandscape, setIsLandscape] = useState<boolean>(false);
  let sigCanvas = useRef<SignatureCanvas>(null);

  window.screen.orientation.onchange = (e) => {
    toogleLandscape();
  };

  useEffect(() => {
    toogleLandscape();
  }, []);

  const toogleLandscape = useCallback(() => {
    const orientation: typeof window.screen.orientation.type =
      window.screen.orientation.type;
    if (
      orientation === 'landscape-primary' ||
      orientation === 'landscape-secondary'
    )
      setIsLandscape(true);
    else setIsLandscape(false);
  }, []);

  const clearInput = () => {
    sigCanvas.current?.clear();
    setAssinaturaBase64Image(undefined);
    props.onClearSignature();
  };

  const save = async () => {
    if (sigCanvas.current?.isEmpty()) return;

    setAssinaturaBase64Image(
      trimCanvas(sigCanvas.current?.getTrimmedCanvas()).toDataURL('image/png')
    );
  };

  return (
    <>
      <Space
        direction={'vertical'}
        style={{ maxWidth: '100%', width: '100%' }}
        size={8}
      >
        <Row>
          <Col xs={24}>
            {assinaturaBase64Image ? (
              <img
                src={assinaturaBase64Image}
                alt='my signature'
                style={{
                  display: 'block',
                  margin: '0 auto',
                  border: '1px solid black',
                  minHeight: '60px',
                  maxHeight: '100px',
                }}
              />
            ) : null}
          </Col>
        </Row>
        <Row>
          <Col xs={24} lg={8}>
            <Button
              icon={<HighlightOutlined />}
              style={{ width: '100%' }}
              type={'primary'}
              onClick={() => {
                toogleLandscape();
                setShowModal(true);
              }}
              disabled={props.disabled}
            >
              Coletar assinatura
            </Button>
          </Col>
        </Row>
      </Space>

      <CustomModal
        centered
        title={'Assinatura'}
        open={showModal}
        onCancel={() => {
          setShowModal(false);
        }}
        footer={null}
        maskClosable={false}
        width={'100vw'}
        bodyStyle={{ height: '80vh' }}

        /* style={{
          width: '100vw',
        }} */
      >
        {!isLandscape && (
          <Typography.Title
            level={3}
            style={{ justifyContent: 'center', textAlign: 'center' }}
          >
            Por favor, use o dispositivo no modo PAISAGEM para assinar
          </Typography.Title>
        )}
        {isLandscape && (
          <>
            <div style={{ height: '80%' }}>
              <div style={{ border: '1px solid', height: '100%' }}>
                <SignatureCanvas
                  canvasProps={{
                    className: 'sigCanvas',
                  }}
                  ref={sigCanvas}
                />
              </div>
              <br />

              <Row align={'middle'} justify={'center'} gutter={24}>
                <Col span={4}>
                  <Button
                    style={{ width: '100%' }}
                    onClick={() => {
                      setShowModal(false);
                    }}
                  >
                    Cancelar
                  </Button>
                </Col>
                <Col span={4}>
                  <Button
                    style={{ width: '100%' }}
                    onClick={clearInput}
                    type={'primary'}
                    danger
                  >
                    Limpar
                  </Button>
                </Col>
                <Col span={4}>
                  <Button
                    style={{ width: '100%' }}
                    onClick={async () => {
                      await save();
                      setShowModal(false);
                      notification.success({
                        message: 'Assinatura gravada com sucesso',
                      });
                      props.onSaveSignature();
                    }}
                    type={'primary'}
                  >
                    Salvar
                  </Button>
                </Col>
              </Row>
            </div>
          </>
        )}
      </CustomModal>
    </>
  );
}
