import {
  CheckCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import {
  Button,
  Col,
  Descriptions,
  Divider,
  Form,
  FormInstance,
  Input,
  Modal,
  notification,
  Popconfirm,
  Row,
  Table,
  Tooltip,
  Typography,
} from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useCallback, useEffect, useState } from 'react';
import scrollFieldToTop from '../../core/functions/scrollFieldToTop';
import useLoadingPage from '../../core/hooks/useLoadingPage';
import useServidor from '../../core/hooks/useServidor';
import { Servidor } from '../../sdk/@types';
import { CustomModal } from './CustomModal';

interface ServidoresProps {
  form: FormInstance;
  formDisabled: boolean;
  listaServidores: Servidor.Summary[];
  setListaServidores: (listaServidores: Servidor.Summary[]) => void;
}

export default function ServidoresPanel(props: ServidoresProps) {
  const { xs } = useBreakpoint();
  const { mode, setLoading } = useLoadingPage();
  const [showModalServidores, setShowModalServidores] =
    useState<boolean>(false);

  const {
    servidor,
    fetchExistingServidorByCpf,
    fetchExistingServidorByMatricula,
    fetchingServidor,
  } = useServidor();
  const [servidorSelecionado, setServidorSelecionado] =
    useState<boolean>(false);

  useEffect(() => {
    //se o servidor for undefined, quer dizer que é o primeiro uso do useServidor
    if (servidor === undefined) return;

    if (!fetchingServidor) {
      if (!servidor) {
        notification.info({
          message: `Não foi possível encontrar nenhum servidor`,
        });
      } else {
        props.form.setFieldsValue({
          servidorBusca: servidor,
        });

        setServidorSelecionado(true);

        notification.success({
          message: `Servidor ${servidor.pessoa.nome} encontrado`,
        });
      }
    }

    setLoading(fetchingServidor);
    ///////////////////////////////
  }, [fetchingServidor, setLoading]);

  const cleanModalServidor = useCallback(() => {
    props.form.setFieldsValue({
      servidorBusca: undefined,
    });
    setServidorSelecionado(false);
  }, [props.form]);

  const handleAddServidor = useCallback(() => {
    let servidorBusca = props.form.getFieldValue(['servidorBusca']);
    servidorBusca.id = null;

    const arrayDeChavesIguaisAChaveQueEstaSendoInserida =
      props.listaServidores.filter((servidor) => {
        return servidor.matricula === servidorBusca.matricula;
      });

    if (arrayDeChavesIguaisAChaveQueEstaSendoInserida?.length > 0) {
      notification.warn({
        message: 'Este servidor já foi incluído',
        description: 'Selecione outro servidor',
      });
      return;
    }

    const novoServidor: typeof props.listaServidores = [servidorBusca];

    props.setListaServidores(props.listaServidores.concat(novoServidor));

    setShowModalServidores(false);
    cleanModalServidor();
    props.form.validateFields(['tableServidores']);
    notification.success({
      message: `Servidor ${servidorBusca.pessoa.nome} incluído com sucesso`,
    });
  }, [props.form, props.listaServidores, cleanModalServidor]);

  const servidorTableActions = (servidor: Servidor.Summary) => {
    return (
      <Row align={'middle'} justify={xs ? 'start' : 'center'} gutter={12}>
        <Popconfirm
          title={'Deseja remover o servidor?'}
          disabled={props.formDisabled}
          onConfirm={() => {
            props.setListaServidores(
              props.listaServidores.filter(
                (servidorFilter) =>
                  servidorFilter.matricula !== servidor.matricula
              )
            );
            notification.success({
              message: `Servidor ${servidor.pessoa.nome} removido`,
            });
          }}
        >
          <Button
            icon={<DeleteOutlined />}
            danger
            disabled={props.formDisabled}
            type={'ghost'}
            title={'Remover servidor'}
          />
        </Popconfirm>
      </Row>
    );
  };

  const removeServidor = () => {
    props.form.setFieldsValue({ servidorBusca: undefined });
    setServidorSelecionado(false);
  };

  const fetchServidorByCpf = useCallback(
    async (cpf: string) => {
      props.form.validateFields([['servidorBusca', 'pessoa', 'cpfCnpj']]);
      if (cpf) {
        await fetchExistingServidorByCpf(cpf);
      } else return;
    },
    [fetchExistingServidorByCpf, props.form]
  );

  const fetchServidorByMatricula = useCallback(
    async (matricula: string) => {
      props.form.validateFields([['servidorBusca', 'matricula']]);
      if (matricula) {
        await fetchExistingServidorByMatricula(matricula);
      } else return;
    },
    [fetchExistingServidorByMatricula, props.form]
  );

  return (
    <>
      <Col xs={24} lg={24}>
        <Form.Item
          validateTrigger={'onSubmit'}
          name='tableServidores'
          rules={[
            {
              validator(_, value) {
                if (props.listaServidores.length < 1)
                  return Promise.reject(
                    new Error('Deve ser informado pelo menos um servidor')
                  );
                return Promise.resolve();
              },
            },
          ]}
        >
          <Table<Servidor.Summary>
            dataSource={props.listaServidores}
            size={'small'}
            rowKey={(record) => record.matricula}
            title={() => {
              return (
                <Row justify={'space-between'}>
                  <Typography.Title level={5} style={{ color: 'white' }}>
                    Servidores
                  </Typography.Title>
                  <Button
                    icon={<PlusOutlined />}
                    type={'primary'}
                    onClick={() => {
                      setShowModalServidores(true);

                      scrollFieldToTop('tableServidores');
                    }}
                    disabled={props.formDisabled}
                  />
                </Row>
              );
            }}
            columns={[
              {
                dataIndex: 'matricula',
                responsive: ['xs'],
                render(_, servidor) {
                  return (
                    <Descriptions size={'small'} column={1} bordered>
                      <Descriptions.Item label={'Nome'}>
                        {servidor.pessoa.nome}
                      </Descriptions.Item>
                      <Descriptions.Item label={'CPF'}>
                        {servidor.pessoa.cpfCnpj}
                      </Descriptions.Item>
                      <Descriptions.Item label={'Matrícula'}>
                        {servidor.matricula}
                      </Descriptions.Item>
                      <Descriptions.Item label='Ações'>
                        {servidorTableActions(servidor)}
                      </Descriptions.Item>
                    </Descriptions>
                  );
                },
              },

              {
                dataIndex: ['pessoa', 'nome'],
                title: 'Nome',
                responsive: ['sm'],
                width: 450,
              },
              {
                dataIndex: ['pessoa', 'cpfCnpj'],
                responsive: ['sm'],
                title: 'CPF',
              },
              {
                dataIndex: 'matricula',
                responsive: ['sm'],
                title: 'Matrícula',
              },
              {
                dataIndex: 'id',
                title: 'Ações',
                responsive: ['sm'],
                width: 60,
                render(_, servidor) {
                  return servidorTableActions(servidor);
                },
              },
            ]}
          />
        </Form.Item>
      </Col>

      <CustomModal
        centered
        open={showModalServidores}
        title={'Cadastro de servidores'}
        onCancel={() => {
          setShowModalServidores(false);
          cleanModalServidor();
        }}
        width={800}
        footer={
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            <Button
              style={{ width: '50%' }}
              danger
              icon={<CloseOutlined />}
              onClick={() => {
                setShowModalServidores(false);
                cleanModalServidor();
              }}
            >
              Cancelar
            </Button>
            <Button
              style={{ width: '50%' }}
              type={'primary'}
              icon={<CheckCircleOutlined />}
              loading={fetchingServidor}
              onClick={() => {
                props.form.validateFields([
                  ['servidorBusca', 'pessoa', 'cpfCnpj'],
                  ['servidorBusca', 'pessoa', 'nome'],
                  ['servidorBusca', 'matricula'],
                  ['servidorBusca', 'id'],
                ]);

                const cpf = props.form.getFieldValue([
                  'servidorBusca',
                  'pessoa',
                  'cpfCnpj',
                ]);
                const matricula = props.form.getFieldValue([
                  'servidorBusca',
                  'matricula',
                ]);

                const nome = props.form.getFieldValue([
                  'servidorBusca',
                  'pessoa',
                  'nome',
                ]);

                if (!cpf || !matricula || !nome) {
                  notification.warn({
                    message: 'Não foi possível salvar o servidor',
                    description: 'Nenhum servidor selecionado',
                  });
                } else handleAddServidor();
              }}
            >
              Salvar
            </Button>
          </div>
        }
        maskClosable={false}
        destroyOnClose
      >
        <Form layout={'vertical'} size={!xs ? 'small' : 'large'}>
          <Row gutter={24}>
            <Col xs={24} lg={12}>
              <Form.Item
                label={'CPF'}
                name={['servidorBusca', 'pessoa', 'cpfCnpj']}
                rules={[
                  {
                    required: true,
                    message: 'O campo é obrigatório',
                  },
                ]}
              >
                <Input.Search
                  width={'100%'}
                  placeholder={'Informe cpf do servidor'}
                  style={{ width: '100%' }}
                  loading={fetchingServidor}
                  allowClear={false}
                  disabled={servidorSelecionado}
                  inputMode='numeric'
                  addonAfter={
                    <Button
                      disabled={props.formDisabled}
                      icon={<DeleteOutlined />}
                      danger
                      onClick={removeServidor}
                    />
                  }
                  onSearch={(cpf) => {
                    if (!cpf) removeServidor();
                    else fetchServidorByCpf(cpf);
                  }}
                  onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                      e.preventDefault();
                    }
                  }}
                  onKeyPress={(event: any) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                />
              </Form.Item>
            </Col>

            <Col xs={24} lg={12}>
              <Form.Item
                label={'Matrícula'}
                name={['servidorBusca', 'matricula']}
                rules={[
                  {
                    required: true,
                    message: 'O campo é obrigatório',
                  },
                ]}
              >
                <Input.Search
                  width={'100%'}
                  placeholder={'Informe a matrícula do servidor'}
                  style={{ width: '100%' }}
                  loading={fetchingServidor}
                  allowClear={false}
                  disabled={servidorSelecionado}
                  inputMode='numeric'
                  addonAfter={
                    <Button
                      disabled={props.formDisabled}
                      icon={<DeleteOutlined />}
                      danger
                      onClick={removeServidor}
                    />
                  }
                  onSearch={(matricula) => {
                    if (!matricula) removeServidor();
                    else fetchServidorByMatricula(matricula);
                  }}
                  onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                      e.preventDefault();
                    }
                  }}
                  onKeyPress={(event: any) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                />
              </Form.Item>
            </Col>

            <Col xs={24} lg={24}>
              <Form.Item
                label={'Nome'}
                name={['servidorBusca', 'pessoa', 'nome']}
                rules={[{ required: true, message: 'O campo é obrigatório' }]}
              >
                <Input disabled />
              </Form.Item>
            </Col>

            <Col xs={0} lg={0}>
              <Form.Item
                label={''}
                name={['servidorBusca', 'matricula']}
                hidden
              >
                <Input hidden />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </CustomModal>
    </>
  );
}
