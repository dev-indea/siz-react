import { Typography } from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { Link } from 'react-router-dom';
import PageNotFound from '../../assets/404.jpg';

export default function NotFoundPage() {
  const { xs } = useBreakpoint();
  return (
    <>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        {!xs && (
          <img
            src={PageNotFound}
            alt={'PageNotFound'}
            height={512}
            width={512}
          />
        )}

        {xs && <img src={PageNotFound} alt={'PageNotFound'} width={'100%'} />}
        <br />
        <Link to='/'>
          <Typography.Title level={4}>Ir para Home</Typography.Title>
        </Link>
      </div>
    </>
  );
}
