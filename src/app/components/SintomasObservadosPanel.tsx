import { Checkbox, Col, Form, FormInstance, Row } from 'antd';
import { SintomasObservados } from '../../core/enums/SintomasObservados';

type SintomasObservadosPanelProps = {
  form: FormInstance;
  formDisabled?: boolean;
  listaSintomasObservados: string[];
  pathToCoordenada: string;
};

export default function SintomasObservadosPanel(
  props: SintomasObservadosPanelProps
) {
  return (
    <>
      <Col xs={24} lg={24}>
        <Form.Item
          label={'Sintomas Observados'}
          name={[props.pathToCoordenada, 'sinaisSintomasObservados']}
          valuePropName={'checked'}
        >
          <Checkbox.Group
            style={{ width: '100%' }}
            defaultValue={props.form.getFieldValue([
              props.pathToCoordenada,
              'sinaisSintomasObservados',
            ])}
            onChange={(e) => {
              let listSintomas: any[] = [];

              e.forEach((name) => {
                listSintomas = listSintomas.concat([
                  SintomasObservados.valueOf(name.toString()),
                ]);
              });

              props.form.setFieldsValue({
                [props.pathToCoordenada]: {
                  sinaisSintomasObservados: e,
                },
              });
            }}
          >
            <Row>
              {props.listaSintomasObservados?.map((sintoma: any) => (
                <Col xs={24} lg={8}>
                  <Checkbox key={sintoma} value={sintoma}>
                    {SintomasObservados.valueOf(sintoma)}
                  </Checkbox>
                </Col>
              ))}
            </Row>
          </Checkbox.Group>
        </Form.Item>
      </Col>
    </>
  );
}
