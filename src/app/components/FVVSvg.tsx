export default function FVVSvg() {
  return (
    <svg width='16' height='16' xmlns='http://www.w3.org/2000/svg'>
      <g>
        <title>Criar FVV</title>
        <text
          transform='matrix(11.0184 0 0 10.4574 -1142.1 -1792.86)'
          stroke='#000'
          fontStyle='normal'
          fontWeight='normal'
          xmlSpace='preserve'
          textAnchor='start'
          fontFamily="'Calistoga'"
          fontSize='24'
          id='svg_1'
          y='16'
          x='114'
          strokeWidth='0'
          fill='#000000'
        >
          FVV
        </text>
        <text
          transform='matrix(0.979709 0 0 1.32524 -1.39221 -0.746276)'
          stroke='#000'
          fontWeight='bold'
          xmlSpace='preserve'
          textAnchor='start'
          fontFamily='monospace'
          fontSize='10'
          id='svg_2'
          y='10.21239'
          x='0.82301'
          strokeWidth='0'
          fill='#000000'
        >
          FVV
        </text>
      </g>
    </svg>
  );
}
