import { SyncOutlined } from '@ant-design/icons';
import {
  Alert,
  Button,
  Card,
  Col,
  Form,
  Input,
  message,
  Modal,
  notification,
  Row,
  Spin,
  Typography,
} from 'antd';
import { useEffect, useState } from 'react';
import { useGeolocated } from 'react-geolocated';
import NumberFormat from 'react-number-format';
import { CoordenadaGeografica } from '../../sdk/@types';
interface CoordenadasPanelProps {
  formDisabled: boolean;
  props?: CoordenadaGeografica.Detailed;
  pathToCoordenada: string[];
  onAcceptCoords: (coords: GeolocationCoordinates) => any;
}

export default function CoordenadasPanel(props: CoordenadasPanelProps) {
  const [fetching, setFetching] = useState(false);

  const {
    coords,
    getPosition,
    isGeolocationAvailable,
    isGeolocationEnabled,
    positionError,
  } = useGeolocated({
    positionOptions: {
      enableHighAccuracy: false,
    },
    userDecisionTimeout: undefined,
  });

  useEffect(() => {}, [fetching]);

  useEffect(() => {}, [isGeolocationAvailable, isGeolocationEnabled]);

  const onClickButtonUpdateCoords = () => {
    if ('geolocation' in navigator) {
      getPosition();

      if (coords) {
        props.onAcceptCoords(coords);
        message.success('Coordenadas atualizadas');
      } else {
        let message;
        if (positionError?.code === 1) {
          if (positionError.message.includes('secure')) {
            message =
              'A conexão do SIZ não é mais segura, contate o administrador do sistema';
          } else {
            message =
              'O SIZ não possui permissão para acessar a sua localização';
          }
        } else if (positionError?.code === 2) {
          message =
            'O seu dispositivo não capacidade de localização ou ela está desativada';
        } else {
          message = 'O seu dispositivo está demorando para responder';
        }

        notification.error({
          message: 'Não foi possível obter a localização',
          description: message,
        });
      }
    } else {
      Modal.error({
        title: 'Não foi possível obter a localização',
        content: (
          <>
            <Typography.Paragraph style={{ textAlign: 'justify' }}>
              Este dispositivo não é capaz de obter a sua localização.
            </Typography.Paragraph>
          </>
        ),
      });
    }
  };

  return (
    <>
      <Spin
        size='large'
        spinning={fetching}
        style={{
          position: 'fixed',
          top: '20%',
          zIndex: '1',
        }}
      >
        {!isGeolocationAvailable && (
          <Alert
            style={{ marginBottom: '5px' }}
            message={'Este dispositivo não possui suporte à geolocalização'}
            type='error'
            showIcon
          />
        )}

        {!isGeolocationEnabled && (
          <Alert
            style={{ marginBottom: '5px' }}
            message={'O SIZ não possui permissão para usar a geolocalização'}
            type='warning'
            showIcon
          />
        )}
        <Row align={'bottom'} gutter={24}>
          <Col xs={24} md={12}>
            <Card size='small' title='Latitude' bordered={false}>
              <Row gutter={12}>
                <Col xs={8} lg={8}>
                  <Form.Item
                    hidden
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'id',
                    ]}
                  >
                    <Input hidden />
                  </Form.Item>

                  <Form.Item
                    hidden
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'pessoa',
                      'id',
                    ]}
                  >
                    <Input hidden />
                  </Form.Item>

                  <Form.Item
                    hidden
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'orientacaoLatitude',
                    ]}
                  >
                    <Input hidden />
                  </Form.Item>

                  <Form.Item
                    hidden
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'orientacaoLongitude',
                    ]}
                  >
                    <Input hidden />
                  </Form.Item>

                  <Form.Item
                    label='Grau'
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'latGrau',
                    ]}
                    rules={[
                      {
                        required: true,
                        message: 'O campo é obrigatório',
                      },
                    ]}
                  >
                    <Input
                      maxLength={2}
                      inputMode='numeric'
                      autoComplete='off'
                      disabled
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={8} lg={8}>
                  <Form.Item
                    label={'Minuto'}
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'latMin',
                    ]}
                    rules={[
                      {
                        required: true,
                        message: 'O campo é obrigatório',
                      },
                    ]}
                  >
                    <Input
                      maxLength={2}
                      inputMode='numeric'
                      autoComplete='off'
                      disabled
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={8} lg={8}>
                  <Form.Item
                    label={'Segundo'}
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'latSeg',
                    ]}
                    rules={[
                      {
                        required: true,
                        message: 'O campo é obrigatório',
                      },
                    ]}
                  >
                    <NumberFormat
                      className='ant-col ant-form-item-control ant-form-item-control-input ant-input '
                      style={{ maxWidth: '100%', height: '24px' }}
                      value={12345}
                      displayType='input'
                      type={'text'}
                      decimalSeparator=','
                      decimalScale={1}
                      max={180}
                      maxLength={4}
                      min={-180}
                      autoComplete={'off'}
                      disabled
                      onKeyPress={(event: any) => {
                        let { value } = event.target;

                        if (!/[0-9]/.test(event.key) || !/./.test(event.key)) {
                          event.preventDefault();
                        }

                        value = value.replace(/(\d{2})/, '$1,');
                        value = value.replace(',,', ',');

                        event.currentTarget.value = value;
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Card>
          </Col>

          <Col xs={24} md={12}>
            <Card size='small' title='Longitude' bordered={false}>
              <Row gutter={12}>
                <Col xs={8} lg={8}>
                  <Form.Item
                    label={'Grau'}
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'longGrau',
                    ]}
                    rules={[
                      {
                        required: true,
                        message: 'O campo é obrigatório',
                      },
                    ]}
                  >
                    <Input
                      maxLength={3}
                      inputMode='numeric'
                      autoComplete='off'
                      disabled
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={8} lg={8}>
                  <Form.Item
                    label={'Minuto'}
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'longMin',
                    ]}
                    rules={[
                      {
                        required: true,
                        message: 'O campo é obrigatório',
                      },
                    ]}
                  >
                    <Input
                      maxLength={3}
                      inputMode='numeric'
                      autoComplete='off'
                      disabled
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={8} lg={8}>
                  <Form.Item
                    label={'Segundo'}
                    name={[
                      ...props.pathToCoordenada,
                      'coordenadaGeografica',
                      'longSeg',
                    ]}
                    rules={[
                      {
                        required: true,
                        message: 'O campo é obrigatório',
                      },
                    ]}
                  >
                    <NumberFormat
                      className='ant-col ant-form-item-control ant-form-item-control-input ant-input '
                      style={{ maxWidth: '100%', height: '24px' }}
                      value={12345}
                      displayType='input'
                      type={'text'}
                      decimalSeparator=','
                      decimalScale={1}
                      max={180}
                      maxLength={4}
                      min={-180}
                      autoComplete={'off'}
                      disabled
                      onKeyPress={(event: any) => {
                        let { value } = event.target;

                        if (!/[0-9]/.test(event.key) || !/./.test(event.key)) {
                          event.preventDefault();
                        }

                        value = value.replace(/(\d{2})/, '$1,');
                        value = value.replace(',,', ',');

                        event.currentTarget.value = value;
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>

        <br />
        <Row>
          <Col xs={24} lg={8}>
            <Button
              icon={<SyncOutlined />}
              style={{ width: '100%' }}
              type={'primary'}
              onClick={(e) => {
                //setFetching(true);
                onClickButtonUpdateCoords();
              }}
              disabled={props.formDisabled}
            >
              Atualizar Coordenadas
            </Button>
          </Col>
        </Row>
      </Spin>
    </>
  );
}
