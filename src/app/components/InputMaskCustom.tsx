import React from 'react';
import InputMask from 'react-input-mask';
import { Input } from 'antd';

export interface InputProps {
  mask: string;
  placeholder: string;
  disabled?: boolean;
  onChange?: (event: React.SyntheticEvent) => void;
  value?: string;
  onBlur?: (v: any) => any;
}

const InputComponentMask: React.FC<InputProps> = ({
  disabled,
  onChange,
  value,
  mask,
  onBlur,
  ...rest
}) => (
  <InputMask
    mask={mask}
    disabled={disabled}
    value={value}
    onChange={onChange}
    onBlur={onBlur}
  >
    <Input />
  </InputMask>
);

export default InputComponentMask;
