import {
  BookOutlined,
  CloudUploadOutlined,
  DesktopOutlined,
  DisconnectOutlined,
  HomeOutlined,
  LineChartOutlined,
  LogoutOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  MonitorOutlined,
  SettingOutlined,
  SyncOutlined,
  VerticalAlignBottomOutlined,
} from '@ant-design/icons';
import {
  Badge,
  Button,
  Col,
  Divider,
  Image,
  Layout,
  Menu,
  notification,
  Row,
  Typography,
} from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useEffect, useState } from 'react';
import { useNavigate, useLocation } from 'react-router';
import AuthorizationService from '../../../core/auth/Authorization.service';
import useBadgeState from '../../../core/hooks/useBadgeState';
import useOuterClick from '../../../core/hooks/useOuterClick';
import { CustomModal } from '../../components/CustomModal';
const { SubMenu } = Menu;
const { Sider } = Layout;

export default function DefaultLayoutSider() {
  const navigate = useNavigate();
  const location = useLocation();
  const { xs, sm, lg } = useBreakpoint();

  const [collapsed, setCollapsed] = useState<boolean>(false);

  const { quantidadeAcoesNaoSincronizadas } = useBadgeState();

  const [showModalInstallationVisible, setShowModalInstallationVisible] =
    useState<boolean>(false);

  const [showModalAtualizacao, setShowModalAtualizacao] =
    useState<boolean>(false);
  const [alreadyInstalled, setAlreadyInstalled] = useState(false);

  const [promptEvent, setPromptEvent] = useState<any>();

  const accessToken = AuthorizationService.getAccessToken();

  useEffect(() => {}, [accessToken]);

  useEffect(() => {
    window.addEventListener('beforeinstallprompt', function (e) {
      e.preventDefault();
      setPromptEvent(e);
    });
  }, []);

  useEffect(() => {
    const isStandalone = window.matchMedia(
      '(display-mode: standalone)'
    ).matches;
    //@ts-ignore
    if (navigator.standalone || isStandalone) {
      setAlreadyInstalled(true);
    } else setAlreadyInstalled(false);
  }, [setAlreadyInstalled]);

  useEffect(() => {}, [alreadyInstalled]);

  useEffect(() => {
    if (lg) setCollapsed(false);
    if (!lg) setCollapsed(true);
  }, [lg]);

  const handleOuterClick = () => {
    if (lg) return;
    if (!collapsed) setCollapsed(true);
  };

  const innerRef = useOuterClick((ev: any) => {
    handleOuterClick();
  });

  const trigger = () => {
    return (
      <div className='ant-layout-sider-trigger-custom'>
        {collapsed && (
          <MenuUnfoldOutlined
            style={{ fontSize: '32px', color: 'white' }}
            onClick={() => setCollapsed(!collapsed)}
          />
        )}

        {!collapsed && (
          <MenuFoldOutlined
            style={{ fontSize: '32px', color: 'white' }}
            onClick={() => setCollapsed(!collapsed)}
          />
        )}
      </div>
    );
  };

  return (
    <>
      {AuthorizationService.isAuthenticated() && (
        <div ref={innerRef} className='sider-wrapper'>
          {trigger()}

          <Sider
            width={250}
            collapsible
            collapsed={collapsed}
            collapsedWidth={`${xs || sm ? '0' : '80px'}`}
            defaultCollapsed={false}
            breakpoint={'lg'}
            trigger={null}
            style={{
              overflowY: 'initial',
              overflowX: 'hidden',
              height: '100vh',
              position: `${xs ? 'fixed' : 'sticky'}`,
              zIndex: '1',
              left: 0,
              top: 0,
              bottom: 0,
              paddingTop: 64,
            }}
          >
            <Menu
              mode='inline'
              selectedKeys={[location.pathname]}
              defaultOpenKeys={[location.pathname.split('/')[1]]}
              style={{ height: '100%', borderRight: 0 }}
            >
              <Menu.Item
                key='/home'
                onClick={() => {
                  if (!lg) setCollapsed(true);
                  navigate('/home');
                }}
                icon={<HomeOutlined />}
              >
                Home
              </Menu.Item>

              <Menu.Item
                key='/educacaosanitaria'
                onClick={() => {
                  if (!lg) setCollapsed(true);
                  //@ts-ignore
                  window.open('https://linktr.ee/educaindea', '_blank').focus();
                }}
                icon={<BookOutlined />}
              >
                Educaçao Sanitária
              </Menu.Item>

              <Menu.Item
                key='/gestaodemetas'
                onClick={() => {
                  if (!lg) setCollapsed(true);
                  //@ts-ignore
                  window
                    .open(
                      `http://cdsa:${encodeURIComponent(
                        '$4Iei1b7#SXc'
                      )}@pbi.mti.mt.gov.br/Reports/browse/INDEA/ANIMAL`,
                      '_blank'
                    )
                    .focus();
                }}
                icon={<LineChartOutlined />}
              >
                Gestão de metas
              </Menu.Item>

              <SubMenu key='visitas' icon={<MonitorOutlined />} title='FVER'>
                <Menu.Item
                  key='/visitas'
                  onClick={() => {
                    if (!lg) setCollapsed(true);
                    navigate('/visitas');
                  }}
                >
                  Lista
                </Menu.Item>
                <Menu.Item
                  key='/visitas/cadastro'
                  onClick={() => {
                    if (!lg) setCollapsed(true);
                    navigate(
                      `/visitas/cadastro${
                        !window.navigator.onLine ? '/local' : ''
                      }`
                    );
                  }}
                >
                  Cadastro
                </Menu.Item>
              </SubMenu>

              <Menu.Item
                key='/vigilancias'
                icon={<MonitorOutlined />}
                onClick={() => {
                  if (!lg) setCollapsed(true);
                  navigate('/vigilancias');
                }}
              >
                FVV
              </Menu.Item>
              <SubMenu
                key='formvins'
                icon={<MonitorOutlined />}
                title='Form VIN'
              >
                <Menu.Item
                  key='/formvins'
                  onClick={() => {
                    if (!lg) setCollapsed(true);
                    navigate('/formvins');
                  }}
                >
                  Lista
                </Menu.Item>
                <Menu.Item
                  key='/formvins/cadastro'
                  onClick={() => {
                    if (!lg) setCollapsed(true);
                    navigate('/formvins/cadastro');
                  }}
                >
                  Cadastro
                </Menu.Item>
              </SubMenu>

              <Divider style={{ margin: 0, marginTop: -5, padding: 0 }} />

              <Menu.Item
                key='/acoes-de-campo'
                onClick={() => {
                  if (!lg) setCollapsed(true);
                  navigate('/acoes-de-campo');
                }}
                icon={<CloudUploadOutlined />}
              >
                <Badge
                  count={quantidadeAcoesNaoSincronizadas}
                  overflowCount={99}
                  size='small'
                  showZero={false}
                  title={`Você possui ${quantidadeAcoesNaoSincronizadas} ações ainda não sincronizadas`}
                >
                  Ações de campo
                </Badge>
              </Menu.Item>

              <Menu.Item
                key='/offline'
                onClick={() => {
                  if (!lg) setCollapsed(true);
                  navigate('/offline');
                }}
                icon={<DisconnectOutlined />}
              >
                Sincronizar Dados
              </Menu.Item>

              <Divider style={{ margin: 0, marginTop: -5, padding: 0 }} />

              <SubMenu
                key='config'
                icon={<SettingOutlined />}
                title={'Configurações'}
              >
                {AuthorizationService.getUsuarioSIZ()?.userName ===
                  'leonardoassis' && (
                  <Menu.Item
                    key='/admin'
                    onClick={() => {
                      if (!lg) setCollapsed(true);
                      navigate('/admin');
                    }}
                    icon={<DesktopOutlined />}
                  >
                    Administração
                  </Menu.Item>
                )}
                <Menu.Item
                  key='/instalacao'
                  onClick={() => {
                    setShowModalInstallationVisible(true);
                  }}
                  icon={<VerticalAlignBottomOutlined />}
                >
                  Instalação
                </Menu.Item>

                <Menu.Item
                  key='/atualizacao'
                  onClick={() => {
                    setShowModalAtualizacao(true);
                  }}
                  icon={<SyncOutlined />}
                >
                  Atualizações
                </Menu.Item>
              </SubMenu>

              <SubMenu
                key='relatorios'
                icon={<MonitorOutlined />}
                title='Relatórios'
              >
                <Menu.Item
                  key='/relatorioFVER'
                  onClick={() => {
                    if (!lg) setCollapsed(true);
                    navigate('/relatorioFVER');
                  }}
                >
                  FVER
                </Menu.Item>
              </SubMenu>

              <Menu.Item
                key='/logout'
                onClick={() => {
                  AuthorizationService.logout();
                }}
                icon={<LogoutOutlined />}
              >
                Sair
              </Menu.Item>
            </Menu>

            <CustomModal
              centered
              open={showModalInstallationVisible}
              footer={null}
              closable={false}
              maskClosable={false}
              width={xs ? '100%' : '300px'}
            >
              <Typography.Paragraph>{alreadyInstalled}</Typography.Paragraph>

              <Row gutter={24} justify='start'>
                <Col xs={6}>
                  <Image height={'48px'} src={'/SIZ-128x128.png'} />
                </Col>

                <Col xs={18}>
                  <Typography.Title level={4}>Instalar SIZ</Typography.Title>
                  <Typography.Paragraph>Indea-MT</Typography.Paragraph>
                </Col>
              </Row>

              <Row hidden={alreadyInstalled || !promptEvent}>
                <Typography.Paragraph>
                  O SIZ mobile pode ser instalado como um app no seu dispositivo
                  para que seja acessado com maior agilidade e comodidade no
                  futuro.
                </Typography.Paragraph>

                <Typography.Paragraph>
                  Deseja continuar com a instalação?
                </Typography.Paragraph>
                <Divider />
                <Col xs={24}>
                  <Row justify='space-between'>
                    <Col xs={12} sm={8}>
                      <Button
                        style={{ width: '100%' }}
                        onClick={() => {
                          setShowModalInstallationVisible(false);
                        }}
                      >
                        Cancelar
                      </Button>
                    </Col>

                    <Col xs={12} sm={8}>
                      <Button
                        id='buttonInstallationPWA'
                        style={{ width: '100%' }}
                        type={'primary'}
                        onClick={() => {
                          setShowModalInstallationVisible(false);

                          if (!promptEvent) {
                            return;
                          }
                          promptEvent.prompt();
                          promptEvent.userChoice.then((choice: string) => {
                            if (choice === 'accepted') {
                              setPromptEvent(null);
                              notification.success({
                                message: 'SIZ instalado com sucesso',
                              });
                            }
                          });
                        }}
                      >
                        Instalar
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row hidden={!alreadyInstalled && promptEvent}>
                <Col xs={24}>
                  <Typography.Paragraph>
                    O SIZ mobile já está instalado neste dispositivo ou o seu
                    dispositivo não possui suporte a esta função
                  </Typography.Paragraph>
                </Col>
                <Divider />

                <Col xs={24}>
                  <Row justify='center'>
                    <Col xs={24} sm={8}>
                      <Button
                        style={{ width: '100%' }}
                        type={'primary'}
                        onClick={() => {
                          setShowModalInstallationVisible(false);
                        }}
                      >
                        Fechar
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </CustomModal>

            <CustomModal
              centered
              open={showModalAtualizacao}
              footer={null}
              closable={true}
              maskClosable={true}
              width={xs ? '100%' : '450px'}
              onCancel={() => setShowModalAtualizacao(false)}
            >
              <Row gutter={24} justify='start' align='bottom'>
                <Col xs={6}>
                  <Image height={'48px'} src={'/SIZ-128x128.png'} />
                </Col>

                <Col xs={18} style={{ textAlign: 'left' }}>
                  <Typography.Title level={4}>Atualizar</Typography.Title>
                </Col>
              </Row>
              <Divider style={{ marginTop: '6px', marginBottom: '6px' }} />

              <Row>
                <Col xs={24}>
                  <Typography.Paragraph style={{ textAlign: 'justify' }}>
                    O SIZ Mobile é capaz de se atualizar automaticament. Essa
                    atualização não é feita instantaneamente para não prejudicar
                    usuários ativos.
                  </Typography.Paragraph>
                  <Typography.Paragraph style={{ textAlign: 'justify' }}>
                    Porém, caso você deseje, pode forçar a atualização no seu
                    dispositivo clicando no botão abaixo
                  </Typography.Paragraph>
                </Col>
                <Divider style={{ marginTop: '-8px', marginBottom: '12px' }} />

                <Col xs={24}>
                  <Row justify='center'>
                    <Col span={24}>
                      <Button
                        style={{ width: '100%' }}
                        type={'primary'}
                        onClick={() => {
                          if ('serviceWorker' in navigator) {
                            navigator.serviceWorker
                              .getRegistration()
                              .then((registration) =>
                                registration?.waiting?.postMessage({
                                  type: 'SKIP_WAITING',
                                })
                              );
                          }
                          window.location.reload();
                        }}
                      >
                        Atualizar
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </CustomModal>
          </Sider>
        </div>
      )}
    </>
  );
}
