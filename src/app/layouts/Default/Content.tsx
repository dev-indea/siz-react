import { Layout, Row, Space } from 'antd';
import { useEffect } from 'react';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
const { Content } = Layout;

interface DefaultlayoutContentProps {
  children: React.ReactNode;
}
export default function DefaultlayoutContent(props: DefaultlayoutContentProps) {
  const { online } = useNavigatorStatus();

  useEffect(() => {}, [online]);

  return (
    <>
      <Content
        className='site-layout'
        style={{
          paddingBottom: '50px',
          zIndex: '0',
          width: '100%',
          marginTop: '60px',
          padding: 24,
        }}
      >
        {props.children}
      </Content>
    </>
  );
}
