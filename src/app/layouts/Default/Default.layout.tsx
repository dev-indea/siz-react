import { Layout, Spin } from 'antd';

import DefaultlayoutContent from './Content';
import DefaultLayoutFooter from './Footer';
import DefaultLayoutHeader from './Header';
import DefaultLayoutSider from './Sider';
import useLoadingPage from '../../../core/hooks/useLoadingPage';

interface DefaultLayoutProps {
  children: React.ReactNode;
}

export default function DefaultLayout(props: DefaultLayoutProps) {
  const { loading } = useLoadingPage();
  return (
    <>
      <Layout>
        {/* <Spin
          size='large'
          spinning={false}
          style={{
            position: 'fixed',
            top: '20%',
            zIndex: '1',
          }}
        > */}
        <DefaultLayoutHeader />

        <Layout hasSider>
          <DefaultLayoutSider />

          <Layout className='site-layout'>
            <DefaultlayoutContent>{props.children}</DefaultlayoutContent>

            <DefaultLayoutFooter />
          </Layout>
        </Layout>
        {/* </Spin> */}
      </Layout>
    </>
  );
}
