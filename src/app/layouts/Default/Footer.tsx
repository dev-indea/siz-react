import { Footer } from 'antd/lib/layout/layout';
import { ENVIRONMENT, VERSION } from '../../EnvironmentConfig';

export default function DefaultLayoutFooter() {
  return (
    <Footer style={{ color: 'white' }}>
      {ENVIRONMENT} - {VERSION}
    </Footer>
  );
}
