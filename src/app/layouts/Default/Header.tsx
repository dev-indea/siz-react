import {
  LogoutOutlined,
  NotificationOutlined,
  UserOutlined,
} from '@ant-design/icons';
import {
  Badge,
  Button,
  Drawer,
  Image,
  Layout,
  Menu,
  Row,
  Typography,
} from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import AuthorizationService from '../../../core/auth/Authorization.service';
import useBadgeState from '../../../core/hooks/useBadgeState';
const { Header } = Layout;

export default function DefaultLayoutHeader() {
  const { online } = useNavigatorStatus();
  const { xs } = useBreakpoint();
  const navigate = useNavigate();
  const { notificacoes } = useBadgeState();

  const [open, setOpen] = useState(false);

  return (
    <>
      <Header
        className='header'
        style={{
          position: 'fixed',
          top: 0,
          zIndex: 5,
          width: '100%',
          backgroundColor: `${!online ? '#f57878' : ''}`,
        }}
      >
        <Row
          align={'middle'}
          justify={'space-between'}
          style={{
            height: '64px',
            left: 85,
            right: 0,
            top: 0,
            position: 'fixed',
          }}
        >
          {(!window.navigator.onLine ||
            AuthorizationService.isAuthenticated()) && (
            <Row align={'middle'} justify={'start'}>
              <Image width={50} src='/SIZ-64x64.png' preview={false} />

              <Row
                align={'middle'}
                justify={'start'}
                style={{
                  paddingLeft: 6,
                }}
              >
                <div>
                  <Typography.Title
                    level={3}
                    style={{ padding: 0, margin: 0, color: 'white' }}
                  >
                    SIZ
                  </Typography.Title>

                  {!xs && (
                    <Typography.Title
                      level={5}
                      style={{
                        padding: 0,
                        margin: 0,
                        fontSize: '12px',
                        color: 'white',
                      }}
                    >
                      Sistema de Informações Zoossanitárias
                    </Typography.Title>
                  )}
                </div>
              </Row>
            </Row>
          )}

          <>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                minWidth: '45px',
              }}
            >
              <div>
                <Button
                  icon={<UserOutlined style={{ color: 'white' }} />}
                  type={'text'}
                  style={{
                    height: '100%',
                    color: 'white',
                    backgroundColor: 'transparent',
                  }}
                  onClick={() => {
                    setOpen(true);
                  }}
                >
                  {!xs
                    ? AuthorizationService.getUsuarioXVia()?.name.split(' ')[0]
                    : ''}
                </Button>
              </div>
            </div>
          </>
        </Row>
      </Header>

      <Drawer
        title={AuthorizationService.getUsuarioSIZ()?.nome}
        placement='right'
        onClose={() => {
          setOpen(false);
        }}
        open={open}
      >
        <Menu mode='inline' style={{ height: '100%', borderRight: 0 }}>
          <Menu.Item
            key='/usuario'
            icon={<UserOutlined />}
            onClick={() => {
              setOpen(false);
              navigate('/usuario');
            }}
          >
            Meus dados
          </Menu.Item>

          <Menu.Item
            key='/notificacoes'
            icon={<NotificationOutlined />}
            onClick={() => {
              setOpen(false);
              navigate('/notificacoes');
            }}
          >
            <Badge
              count={notificacoes}
              overflowCount={99}
              size='small'
              showZero={false}
              title={`Você tem ${notificacoes} novas notificações`}
            >
              Notificações
            </Badge>
          </Menu.Item>

          <Menu.Item
            key='/logout'
            onClick={() => {
              AuthorizationService.logout();
            }}
            icon={<LogoutOutlined />}
          >
            Sair
          </Menu.Item>
        </Menu>
      </Drawer>
    </>
  );
}
