import { useEffect } from 'react';
import { Route, useLocation, Routes } from 'react-router-dom';
import useAssinaturaBase64Image from '../core/hooks/useAssinaturaBase64Image';
import CreateFormVIN from './features/FormVIN/CreateFormVIN';
import FormVINEditView from './view/FormVINEditWrapper.view';
import FormVINList from './view/FormVINListWrapper.view';
import FormVINViewView from './view/FormVINViewWrapper.view';
import OfflineModeView from './view/OfflineMode.view';
import FVERCreateView from './view/FVERCreateWrapper.view';
import VisitaPropriedadeRuralEditView from './view/FVEREditWrapper.view';
import FVERList from './view/FVERListWrapper.view';
import FVERViewWrapper from './view/FVERViewWrapper.view';
import LandingView from './view/Landing.view';
import UsuarioView from './view/Usuario.view';
import VigilanciaVeterinariaList from './view/FVVListWrapper.view';
import VigilanciaVeterinariaCreateView from './view/FVVCreateWrapper.view';
import VigilanciaVeterinariaEditView from './view/FVVEditWrapper.view';
import VigilanciaVeterinariaViewView from './view/FVVViewWrapper.view';
import AcoesDeCampo from './view/AcoesDeCampo.view';
import NotificacoesSistema from './view/Notificacoes.view';
import FVERExternalPrintWrapper from './view/FVERExternalPrintWrapper.view';
import FVERReportGeralView from './view/FVERReportGeral.view';
import LoginView from './view/Login.view';
import SessionExpired from './components/SessionExpired';
import NotFoundPage from './components/NotFoundPage';
import { CustomRoute } from './components/CustomRoute';
import useStartUp from '../core/hooks/useStartUp';
import AdminPanel from './features/AdminPanel/AdminPanel';
import useLoadingPage from '../core/hooks/useLoadingPage';

export default function CustomRoutes() {
  const location = useLocation();
  const { init } = useStartUp();
  const { assinaturaBase64Image, setAssinaturaBase64Image } =
    useAssinaturaBase64Image();
  const { setLoading } = useLoadingPage();

  if ('launchQueue' in window) {
    //@ts-ignore
    window.launchQueue.setConsumer((launchParams: any) => {
      if (launchParams.targetURL) {
        init();
      }
    });
  }

  useEffect(() => {
    if (assinaturaBase64Image) setAssinaturaBase64Image(undefined);
    setLoading(false);
  }, [location]);

  return (
    <>
      <Routes>
        {/* Home */}
        <Route path={'/'} element={<CustomRoute component={LoginView} />} />
        <Route path={'/auth'} element={<CustomRoute component={LoginView} />} />
        s
        <Route
          path={'/home'}
          element={<CustomRoute isprivate={true} component={LandingView} />}
        />
        <Route
          path={'/usuario'}
          element={<CustomRoute isprivate={true} component={UsuarioView} />}
        />
        {/* FVER */}
        <Route
          path={'/visitas'}
          element={<CustomRoute isprivate={true} component={FVERList} />}
        />
        <Route
          path={'/visitas/cadastro/:local?'}
          element={<CustomRoute isprivate={true} component={FVERCreateView} />}
        />
        <Route
          path={`/visitas/edicao/:id/:local?`}
          element={
            <CustomRoute
              isprivate={true}
              component={VisitaPropriedadeRuralEditView}
            />
          }
        />
        <Route
          path={`/visitas/visualizar/:id`}
          element={<CustomRoute isprivate={true} component={FVERViewWrapper} />}
        />
        {/* Form VIN */}
        <Route
          path={'/formvins'}
          element={<CustomRoute isprivate={true} component={FormVINList} />}
        />
        <Route
          path={'/formvins/cadastro'}
          element={<CustomRoute isprivate={true} component={CreateFormVIN} />}
        />
        <Route
          path={`/formvins/edicao/:id/:local?`}
          element={<CustomRoute isprivate={true} component={FormVINEditView} />}
        />
        <Route
          path={`/formvins/visualizar/:id`}
          element={<CustomRoute isprivate={true} component={FormVINViewView} />}
        />
        {/* FVV */}
        <Route
          path={'/vigilancias'}
          element={
            <CustomRoute
              isprivate={true}
              component={VigilanciaVeterinariaList}
            />
          }
        />
        <Route
          path={'/vigilancias/cadastro'}
          element={
            <CustomRoute
              isprivate={true}
              component={VigilanciaVeterinariaCreateView}
            />
          }
        />
        <Route
          path={'/vigilancias/cadastro/:id/:local?'}
          element={
            <CustomRoute
              isprivate={true}
              component={VigilanciaVeterinariaCreateView}
            />
          }
        />
        <Route
          path={`/vigilancias/edicao/:id/:local?/:problem?`}
          element={
            <CustomRoute
              isprivate={true}
              component={VigilanciaVeterinariaEditView}
            />
          }
        />
        <Route
          path={`/vigilancias/visualizar/:id`}
          element={
            <CustomRoute
              isprivate={true}
              component={VigilanciaVeterinariaViewView}
            />
          }
        />
        {/* External */}
        <Route
          path={'/visualizarFVER/:codigoVerificador'}
          element={<CustomRoute component={FVERExternalPrintWrapper} />}
        />
        {/* Relatórios */}
        <Route
          path={'/relatorioFVER'}
          element={
            <CustomRoute isprivate={true} component={FVERReportGeralView} />
          }
        />
        <Route
          path={'/offline'}
          element={<CustomRoute isprivate={true} component={OfflineModeView} />}
        />
        <Route
          path={'/acoes-de-campo'}
          element={<CustomRoute isprivate={true} component={AcoesDeCampo} />}
        />
        <Route
          path={'/admin'}
          element={<CustomRoute isprivate={true} component={AdminPanel} />}
        />
        <Route
          path={'/notificacoes'}
          element={
            <CustomRoute isprivate={true} component={NotificacoesSistema} />
          }
        />
        <Route
          path={'/logout'}
          element={<CustomRoute isprivate={true} component={LoginView} />}
        />
        <Route
          path='*'
          element={<CustomRoute isprivate={true} component={NotFoundPage} />}
        />
      </Routes>
      {/* <AuthorizationVerify /> */}
      <SessionExpired />
    </>
  );
}
