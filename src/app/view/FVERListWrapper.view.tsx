import useLoadingPage from '../../core/hooks/useLoadingPage';
import usePageTitle from '../../core/hooks/usePageTitle';
import ErrorBoundary from '../components/ErrorBoundary';
import ListVisitas from '../features/FVER/ListFVER';

export default function FVERList() {
  usePageTitle('SIZ - Lista de Visitas a Propriedades Rurais');
  const { loading } = useLoadingPage();

  return (
    <>
      <ErrorBoundary component='a lista de FVERs'>
        {/* <Spin
          size='large'
          spinning={loading}
          style={{
            position: 'fixed',
            top: '20%',
            zIndex: '1',
          }}
        > */}
        <ListVisitas />
        {/* </Spin> */}
      </ErrorBoundary>
    </>
  );
}
