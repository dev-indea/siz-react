import { useEffect, useState } from 'react';
import { Col, Divider, Progress, Row, Space, Spin } from 'antd';
import useLoadingPage from '../../core/hooks/useLoadingPage';
import ErrorBoundary from '../components/ErrorBoundary';
import GlobalDataOffline from '../features/OfflineData/GlobalDataOffline';
import MunicipiosOfflineNew from '../features/OfflineData/MunicipiosOffline.new';

export default function OfflineMode() {
  const { loading } = useLoadingPage();

  const [space, setSpace] = useState<{ used: number; granted: number }>();
  const [erro, setErro] = useState(false);

  const bytesToSize = (bytes: number) => {
    const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return 'n/a';
    const i: number = parseInt(
      Math.floor(Math.log(bytes) / Math.log(1024)).toString()
    );
    if (i === 0) return `${bytes} ${sizes[i]}`;
    return `${(bytes / Math.pow(1024, i)).toFixed(1)} ${sizes[i]}`;
  };

  const getPercentage = (atual: number, total: number): number => {
    if (total === 0) return 100;
    if (atual === 0) return 0;
    return Number(((atual * 100) / total).toFixed(2));
  };

  useEffect(() => {
    try {
      //@ts-ignore
      navigator.webkitPersistentStorage.queryUsageAndQuota(
        //@ts-ignore
        function (usedBytes, grantedBytes) {
          setSpace({
            used: usedBytes,
            granted: grantedBytes,
          });
        },
        //@ts-ignore
        function (e) {
          console.log('Error', e);
          setErro(true);
        }
      );
    } catch {
      setErro(true);
    }
  }, []);

  return (
    <>
      <Divider orientation='left'>Dados armazenados</Divider>
      <Row gutter={24}>
        <>
          <Space size={24} direction={'vertical'} style={{ width: '100%' }}>
            <Col xs={24}>
              <ErrorBoundary component='dados dos municípios'>
                <Spin size='large' spinning={loading}>
                  <MunicipiosOfflineNew />
                </Spin>
              </ErrorBoundary>
            </Col>
            <Col xs={24}>
              <ErrorBoundary component='dados globais'>
                <Spin size='large' spinning={loading}>
                  <GlobalDataOffline />
                </Spin>
              </ErrorBoundary>
            </Col>
          </Space>
        </>
      </Row>

      {!erro && (
        <>
          <Divider orientation='left'>Uso de espaço em disco</Divider>

          <Row justify='center'>
            <Progress
              type='circle'
              strokeColor={{
                '0%': '#a1ff9a',
                '100%': '#ff0000',
              }}
              //@ts-ignore
              percent={getPercentage(space?.used, space?.granted)}
            />
          </Row>
        </>
      )}
    </>
  );
}
