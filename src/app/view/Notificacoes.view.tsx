import { CloseOutlined, DeleteOutlined } from '@ant-design/icons';
import {
  Button,
  Col,
  List,
  Modal,
  Row,
  Space,
  Table,
  Typography,
  notification,
} from 'antd';
import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import AuthorizationService from '../../core/auth/Authorization.service';
import { NotificacaoSistema } from '../../sdk/@types/NotificacaoSistema';
import NotificacaoSistemaService from '../../sdk/services/SIZ-API/NotificacaoSistema.service';
import { CustomModal } from '../components/CustomModal';

type NotificacoesDoUsuario = {
  lida: boolean;
} & NotificacaoSistema.Response;

export default function NotificacoesSistema() {
  const [notificacoesDoUsuario, setNotificacoesDoUsuario] =
    useState<NotificacoesDoUsuario[]>();
  const [fetching, setFetching] = useState(false);
  const [showModalNotificacao, setShowModalNotificacao] = useState(false);
  const [notificacaoModal, setNotificacaoModal] =
    useState<NotificacoesDoUsuario>();

  const markAsRead = useCallback(
    async (notificacaoSistema: NotificacaoSistema.Response) => {
      const usuarioSIZ = AuthorizationService.getUsuarioSIZ();
      if (usuarioSIZ) {
        return await NotificacaoSistemaService.markAsRead(
          usuarioSIZ.userName,
          notificacaoSistema.id
        )
          .then(() => console.log('sucesso'))
          .catch(() => console.log('erro'));
      }
    },
    []
  );

  const fetchNotificacoesLidas = async () => {
    const usuarioSIZ = AuthorizationService.getUsuarioSIZ();
    if (usuarioSIZ) {
      return await NotificacaoSistemaService.getByUsuario(usuarioSIZ.userName);
    }
  };

  const fetchTodasAsNotificacies = async () => {
    return await NotificacaoSistemaService.getAll();
  };

  const populateTableNotificacoes = useCallback(async () => {
    let notificacoesDoUsuario: NotificacoesDoUsuario[] = [];

    console.log('1');
    const todasAsNotificacoes = await fetchTodasAsNotificacies();
    console.log('2', todasAsNotificacoes);
    const notificacoesLidas = await fetchNotificacoesLidas();
    console.log('3', notificacoesLidas);

    todasAsNotificacoes.forEach((notificacao) => {
      if (
        notificacoesLidas?.find(
          (notificacaoLida) => notificacao.id === notificacaoLida.id
        )
      ) {
        notificacoesDoUsuario.push({
          lida: true,
          ...notificacao,
        });
      } else {
        notificacoesDoUsuario.push({
          lida: false,
          ...notificacao,
        });
      }

      console.log('4', notificacoesDoUsuario);
    });

    notificacoesDoUsuario.sort((n1, n2) => {
      if (n1.dataCadastro > n2.dataCadastro) return -1;
      if (n1.dataCadastro < n2.dataCadastro) return 1;

      return 0;
    });
    setNotificacoesDoUsuario(notificacoesDoUsuario);
  }, []);

  useEffect(() => {
    populateTableNotificacoes();
  }, [populateTableNotificacoes]);

  useEffect(() => {
    console.log('notificacoesDoUsuario', notificacoesDoUsuario);
  }, [notificacoesDoUsuario]);
  useEffect(() => {}, [fetching]);

  return (
    <>
      <Row>
        <Col span={24}>
          <Table<NotificacoesDoUsuario>
            dataSource={notificacoesDoUsuario}
            loading={fetching}
            rowKey={'id'}
            size={'small'}
            showHeader={false}
            title={() => {
              return (
                <Typography.Title level={5} style={{ color: 'white' }}>
                  Notificações
                </Typography.Title>
              );
            }}
            columns={[
              {
                dataIndex: 'titulo',
                title: 'Título',
                width: '50%',
                render(titulo, notificacao) {
                  if (notificacao.lida)
                    return (
                      <>
                        <span style={{ fontWeight: 'bold' }}>{titulo}</span>{' '}
                        <span style={{ color: 'red' }}>*</span>
                      </>
                    );
                  else return titulo;
                },
              },
              {
                dataIndex: 'dataCadastro',
                title: 'Data',
                render(date: string) {
                  return moment(date).format('DD/MM/YYYY');
                },
              },

              {
                dataIndex: 'lida',
                title: 'Lida',
                render(lida) {
                  return String(lida);
                },
              },
            ]}
            onRow={(record, index) => {
              return {
                onClick: (e) => {
                  setShowModalNotificacao(true);
                  setNotificacaoModal(record);
                },
              };
            }}
          />
        </Col>
      </Row>

      <CustomModal
        open={showModalNotificacao}
        title={notificacaoModal?.titulo}
        closable={false}
        maskClosable
        onCancel={() => setShowModalNotificacao(false)}
        footer={
          <Button
            style={{ width: '100%' }}
            type='default'
            icon={<CloseOutlined />}
            onClick={async () => {
              console.log('notificacaoModal?.lida', notificacaoModal?.lida);

              if (notificacaoModal?.lida) {
                //@ts-ignore
                await markAsRead(notificacaoModal).then(
                  populateTableNotificacoes
                );
              }
              setShowModalNotificacao(false);
            }}
          >
            Fechar
          </Button>
        }
      >
        {notificacaoModal && (
          <List
            size='small'
            dataSource={notificacaoModal.descricao.split('#')}
            renderItem={(item) => <List.Item>{item}</List.Item>}
          />
        )}
      </CustomModal>
    </>
  );
}
