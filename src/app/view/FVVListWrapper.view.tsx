import { Spin } from 'antd';
import useLoadingPage from '../../core/hooks/useLoadingPage';
import usePageTitle from '../../core/hooks/usePageTitle';
import ErrorBoundary from '../components/ErrorBoundary';
import ListFVV from '../features/FVV/ListFVV';

export default function VigilanciaVeterinariaList() {
  usePageTitle('SIZ - Lista de FVVs');
  const { loading } = useLoadingPage();

  return (
    <>
      <ErrorBoundary component='a lista de FVV'>
        <Spin
          size='large'
          spinning={loading}
          style={{
            position: 'fixed',
            top: '20%',
            zIndex: '1',
          }}
        >
          <ListFVV />
        </Spin>
      </ErrorBoundary>
    </>
  );
}
