import { Button, Space, notification } from 'antd';
import { useCallback, useEffect, useState } from 'react';
import AuthorizationService from '../../core/auth/Authorization.service';
import { LogoutOutlined } from '@ant-design/icons';
import ErrorDisplay from '../components/ErrorDisplay';
import LoginMT from '../features/Home/LoginMT';
import { useNavigate } from 'react-router';
import useUsuario from '../../core/hooks/useUsuario';
import LoginSIZ from '../features/Home/LoginSIZ';

export default function LoginView() {
  const navigate = useNavigate();
  const [showLoginSIZ, setShowLoginSIZ] = useState(false);
  const { fetchUsuarioByCpf, usuario } = useUsuario();

  const [loginEnded, setLoginEnded] = useState(false);

  const checkValidLoginAndFetchToken = useCallback(async () => {
    if (!AuthorizationService.isAuthenticated()) {
      const params = new Proxy(new URLSearchParams(window.location.search), {
        //@ts-ignore
        get: (searchParams, prop) => searchParams.get(prop),
      });

      //@ts-ignore
      const code = params.code;
      if (!code) {
        navigate('/');
        return;
      }

      AuthorizationService.setCodeVerifier(code);
      await AuthorizationService.getFirstAccessToken(code)
        .then((data) => data.json())
        .then(async (data) => {
          if (data.access_token) {
            AuthorizationService.setAccessToken(data.access_token);
          }
          if (data.refresh_token)
            AuthorizationService.setRefreshToken(data.refresh_token);
        })
        .then(async () => {
          await AuthorizationService.fetchUsuarioSVia();
        })
        .then(async () => {
          const cpf = AuthorizationService.getUsuarioXVia()?.cpf;

          if (cpf) {
            await fetchUsuarioByCpf(cpf)
              .then(() => {
                setLoginEnded(true);
              })
              .catch((e) => {
                setLoginEnded(true);
                notification.error({
                  message: 'Não foi possível localizar o usuário no SIZ',
                  description: 'Contate o administrador do sistema',
                });
              });
          }
        });
    }
  }, []);

  useEffect(() => {}, [showLoginSIZ]);

  useEffect(() => {
    const isInAuthorizationRoute = window.location.pathname === '/auth';

    if (isInAuthorizationRoute) {
      if (!AuthorizationService.getAccessToken()) {
        checkValidLoginAndFetchToken();
      } else {
        navigate('/home');
      }
    } else if (AuthorizationService.isAuthenticated()) {
      navigate('/home');
    }
  }, [checkValidLoginAndFetchToken, navigate]);

  useEffect(() => {
    if (loginEnded) {
      if (usuario) {
        AuthorizationService.setUsuarioSIZ(usuario);
        if (!usuario.vinculadoMTCidadao) {
          setShowLoginSIZ(true);
        } else {
          navigate('/home');
        }
      } else {
        localStorage.clear();
      }
    }
  }, [loginEnded, navigate]);

  const loginError = (message?: string) => {
    return (
      <div
        style={{
          padding: 24,
          border: '1px solid ',
        }}
      >
        <Space
          size={24}
          direction='vertical'
          align='center'
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}
        >
          <ErrorDisplay
            title='Erro ao realizar o login'
            message={message ? message : 'Tente novamente em alguns segundos'}
          />

          <Button
            key='/logout'
            type='primary'
            style={{ width: '150px' }}
            onClick={() => {
              AuthorizationService.logout();
            }}
            icon={<LogoutOutlined />}
          >
            Sair
          </Button>
        </Space>
      </div>
    );
  };

  return (
    <>
      {!showLoginSIZ && <LoginMT />}
      {showLoginSIZ && (
        <LoginSIZ
          usuarioSIZ={usuario}
          setUsuarioSIZ={AuthorizationService.setUsuarioSIZ}
          callback={() => setShowLoginSIZ(false)}
        />
      )}
    </>
  );
}
