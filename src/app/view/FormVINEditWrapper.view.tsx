import { useEffect } from 'react';
import useLoadingPage from '../../core/hooks/useLoadingPage';
import usePageTitle from '../../core/hooks/usePageTitle';
import ErrorBoundary from '../components/ErrorBoundary';
import EditFormVIN from '../features/FormVIN/EditFormVIN';

export default function FormVINEditView() {
  usePageTitle('Editando Form VIN');

  const { setLoading, setMode, setFirstOpening } = useLoadingPage();

  useEffect(() => {
    setLoading(true);
    setFirstOpening(true);
    setMode('EDIT');
  }, [setLoading, setMode, setFirstOpening]);

  return (
    <>
      <ErrorBoundary component='formulário'>
        <EditFormVIN />
      </ErrorBoundary>
    </>
  );
}
