import { useEffect, useState } from 'react';
import { Alert, Button, Col, Row, Space, Tag, Typography } from 'antd';
import FVERIDBService from '../../sdk/services/indexeddb/FVERIDB.service';
import generateHash from '../../core/functions/generateHash';
import FVVIDBService from '../../sdk/services/indexeddb/FVVIDB.service';
import { ENVIRONMENT, ENVIRONMENT_TYPE } from '../EnvironmentConfig';
import { StatusAssinatura } from '../../core/enums/StatusAssinatura';
import ListFVEROffline from '../features/FVER/ListFVEROffline';
import ListFVVOffline from '../features/FVV/ListFVVOffline';
import ErrorBoundary from '../components/ErrorBoundary';

export default function AcoesDeCampo() {
  const [backGroundSync, setBackGroundSync] = useState<boolean>(false);

  useEffect(() => {
    if (navigator.serviceWorker)
      navigator.serviceWorker.ready.then((swRegistration) => {
        //@ts-ignore
        if (swRegistration.sync) {
          setBackGroundSync(true);
        }
      });
  }, []);

  const generate = async (erro?: boolean) => {
    const baseFVER = {
      dataDaVisita: '2023-05-17T15:11:34.561Z',
      horaDaVisita: '',
      placaVeiculo: '434324324',
      tipoEstabelecimento: 'PROPRIEDADE',
      listChavePrincipalVisitaPropriedadeRural: [
        {
          motivoPrincipal: 'SIM',
          tipoChavePrincipalVisitaPropriedadeRural: {
            id: 1,
            nome: 'BEM ESTAR ANIMAL',
          },
          listChaveSecundariaVisitaPropriedadeRural: [
            {
              tipoChaveSecundariaVisitaPropriedadeRural: {
                id: 2,
                nome: 'CADASTRAMENTO OU ATUALIZAÇÃO',
              },
            },
            {
              tipoChaveSecundariaVisitaPropriedadeRural: {
                id: 30,
                nome: 'CONTROLE DE MOVIMENTAÇÃO DE ANIMAIS',
              },
            },
          ],
        },
      ],
      resumo: `Teste: ${Math.random().toString(36)}`,
      statusAssinatura: StatusAssinatura.keyOf(
        StatusAssinatura.ASSINADO_DIGITALMENTE
      ),
      /* assinatura:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjUAAAAZCAYAAADJ5N/8AAAAAXNSR0IArs4c6QAABepJREFUeF7tnT2u5EQUhc8ICSEQAgISEtgBEBAPCaSQEsES2AFLQBOSQULMEmABSLACYAX8ZJCAjuQjnbm4H+91M/O6uz5Lo+62q8quz56ZT7dulR/o8PaepLcl/SnpZUk/S/pt++5Pb/lMK7/XPh+bx284HYcgAAEIQAACEIDA8QQebFU/l/SspOcl/SXpfUlvHN/sjTUjO3vSE3GKDP1Q8hRhchk2CEAAAhCAAAQg8BgBS81Hkr6+QC4WnhYky06k6JftGAJ0gTeWS4YABCAAAQgcQ8BS86KkP46pfCF1Wnby3ZduKXL0JyJ0Id3hMiEAAQhAAAIQ2COQ4Scfe1XSC5LekfSKpM8kvbYAtuQJfSHpOUmvS/pxk518D4aXJDkKlAhQfh/KMVoAH12EAAQgAAEInAeBlpq9K3KCsDfn1+Q//+zzZ/b5eP9OW12292X/qRT2ZKL35Zr2ztM5PS73vaRnJL27RXG8762tYnMID39aeiw5LT05p9t3NKh/u1yfN3KUz0SOUu5UPtSHAAQgAAEILEPgv6RmGRAndtTisvfHzbbYRf56n79bjg5tlqAeIosURYRm4rXFyFuSrE/sGtUhAAEIQAACl0EAqTm/+2TJifxEirIvvzMzrSNI/u7hsL0tUaMc69yiKUl+Jmb586PEFUEAAhCAAAQGAaTmOh8JS09PyZ8RIvfaQ2s9DJihtj0x2pOiHkab0SSm31/nc0WvIAABCJw1AaTmrG/PvV3cHErLhUxZSrkWqK7bghQxyjOX4bMMk/29SVaStHPOTs6+KUfq3mBxYghAAAIQOA8CSM153IcVrmIOqbnPM5HcZSw7ESOXSeJ29n8i6dstZyjRpi+3Oh9LerTBfFPSd9tQ2sOa0ZZzpj0XZ/XrFZ5A+ggBCFw9AaTm6m/xMh1sEXKnM2PPwmIxSrJ1RCiRokhSZvJ9uklTyqduJ2a7bUeWHFWKJGXW3ZzmP6WJV4cs80jSUQhA4GkTQGqeNnHOd4kEZkSpRWZGoFqmMiyXfYlOdZ2Zm9RtW5x6VewWIgtVtghZPufrRi6ROdcMAQhA4M4EkJo7I6MCBJ4YgTn9PxIzc5c6t6hny2X/lDBfsIfjMt3/pwMvm53LA3RHe7hub7kA/1sylxl4YqBoGAIQgMAeAaSG5wIC6xKYSwLskZgLUHaZTgpPFOqm4z5mcfp1R6rmQpotSGnbZbLkQKJRc/jPZWfdde8wPYfAYgSQmsVuON2FwJkR6DWXDkWaOl9qSkxL10z4nrP4OhIVwWocnQ81VyaPLHXEaq8Nks7P7AHjctYigNSsdb/pLQQg8DiBQ+LTazhliK9zmrIvQ3GWKyeVf1Ovj/lw++0z+rvre6aeRc7lXdbtfCDpq+2yMmvPP/09Sxy4fK8sHgHsfCqS0Hm6lyeA1Cz/CAAAAhC4BwJTmnIJWdbAvy0yifwkImWxcRkLlJc2iFwlkpR63p93zyUa5vJ9HkemLE0dlcorW/xOuyyqOSNoM+J1D/g4JQT2CSA1PBkQgAAE1iDQchLRSaTHBDrHahLJDL5eEiFRrrQxo1czTyq/PauvtySwR5Z6xl9HpHr/GneMXt6ZAFJzZ2RUgAAEIACBWxKYyeSpNof9IlURn361y4xqtUy5XpLP03YkK0ODc1huT55ct5PQ0+4tu0mxcyGA1JzLneA6IAABCEDgFAJ7ojQlql/pEpG6jXh5qC6Ror2IUYbqpgxFqDofqgXK35mtd8pdH3WRmv8RJk1BAAIQgMASBDqXqSNJ7vyc0ddA5jIKPfzXkuX9WVsqQpRhu448zbWl0v4c0js0K+/qhAqpWeLvH52EAAQgAIErIBCZmsnbHZGaLxhO9Kjr9DIJPeQXsUp7ka7IT5Y9aJRZ5sDS5fJOMs+50t5c5Tzy1e/6m9Gso24XUnMUNipBAAIQgAAEliMQGTk0e6+lqKNSM1KU2Xxzdl/P2MsMPrfpd/J5CQSLj2f+uV6WRMhSCi7/EKlZ7pmkwxCAAAQgAIGrIPCvob9/AGS8VvjRxRBiAAAAAElFTkSuQmCC',
 */
      proprietario: {
        cpfCnpj: '00097154105',
        id: 1132395,
        nome: 'LAIR PEREIRA CARDOSO',
        email: null,
        endereco: {
          id: 1132395,
          telefone: '66992564758',
          tipoLogradouro: {
            id: 2,
            nome: 'RUA',
          },
          logradouro: 'RUA DOIS',
          bairro: 'COM. CENTRAL',
          complemento: null,
          referencia: null,
          municipio: {
            id: 5100250,
          },
        },
      },
      propriedade: {
        id: 1091945,
        nome: 'CHACARA SAO MATEUS',
        municipio: {
          id: 5100250,
          uf: {
            id: 51,
            nome: 'MATO GROSSO',
            sigla: 'MT',
          },
          nome: 'ALTA FLORESTA',
        },
        enderecoPropriedade: null,
        viaAcesso:
          'MT 320 - COMUNIDADE CENTRALZINHA - HIDROPONIA MAIS \r\nBRASIL- ANTES DA SEDE COM. CENTRAL 0,2KM. SITUA-SE A \r\n2,8KM DA MT 325 E 8,7KM DA MT 208',
        unidade: {
          nome: 'ULE ALTA FLORESTA',
        },
        coordenadaGeografica: {
          id: 264155,
          pessoa: {},
          orientacaoLatitude: 'S',
          orientacaoLongitude: 'O',
          latGrau: 9,
          latMin: 57,
          latSeg: 33.8,
          longGrau: 56,
          longMin: 5,
          longSeg: 21.1,
        },
      },
      setor: {
        id: 520,
        nome: 'SETOR 02',
      },
      exploracao: {
        id: 302830,
        codigo: '5100302830',
      },
      listServidores: [
        {
          id: 746699,
          pessoa: {
            id: 746699,
            tipoPessoa: 'F',
            cpfCnpj: '73013390115',
            nome: 'LEONARDO SOUZA DE ASSIS',
            inscricaoEstadual: null,
            apelido: null,
            codigo: null,
            email: null,
            endereco: {
              id: 746699,
              logradouro: '1',
              numero: '1',
              bairro: '1',
              complemento: null,
              referencia: null,
              cep: '1',
              municipio: {
                id: 5103403,
                nome: 'CUIABA',
                codgIBGE: '5103403',
                uf: {
                  id: 51,
                  nome: 'MATO GROSSO',
                  sigla: 'MT',
                },
              },
              telefone: null,
              tipoLogradouro: {
                id: 5,
                nome: 'ALAMEDA',
              },
            },
            situacaoCadastro: {
              id: 'A',
              nome: 'CADASTRO ATIVO',
            },
            coordenadaGeografica: null,
          },
          ule: {
            id: 1,
            nome: 'INSTITUTO DEFESA AGROPECUÁRIA DO ESTADO DE MATO GROSSO',
            sigla: 'INDEA',
            codigo: '5110000000',
            tipoUnidade: 'UC',
            uvl: null,
            urs: null,
            municipio: {
              id: 5103403,
              nome: 'CUIABA',
              codgIBGE: '5103403',
              uf: {
                id: 51,
                nome: 'MATO GROSSO',
                sigla: 'MT',
              },
            },
          },
          matricula: '249520',
        },
      ],
      status: 'NOVO',
    };

    const newFVER = {
      ...baseFVER,
      codigoVerificador: generateHash(baseFVER),
    };
    //@ts-ignore
    await FVERIDBService.insert(newFVER);

    const baseFVV = {
      emissaoDeFormIN: 'NAO',
      veterinario: {
        numeroConselho: '1686',
        id: 705,
        pessoa: {
          id: 705,
          tipoPessoa: 'F',
          cpfCnpj: '56961626149',
          nome: 'KELEN REGINA MALHADO DE SIQUEIRA',
          inscricaoEstadual: null,
          apelido: 'KELEN SIQUEIRA',
          codigo: null,
          email: 'ule_vgrande@indea.mt.gov.br',
          endereco: {
            id: 705,
            logradouro: 'CRISTOVÃO COLOMBO',
            numero: '0',
            bairro: 'JARDIM IMPERADOR',
            complemento: null,
            referencia: null,
            cep: '78056842',
            municipio: {
              id: 5103403,
              nome: 'CUIABA',
              codgIBGE: '5103403',
              uf: {
                id: 51,
                nome: 'MATO GROSSO',
                sigla: 'MT',
              },
            },
            telefone: '6536378576',
            tipoLogradouro: {
              id: 2,
              nome: 'RUA',
            },
          },
          situacaoCadastro: {
            id: 'A',
            nome: 'CADASTRO ATIVO',
          },
          coordenadaGeografica: null,
        },
        ule: {
          id: 17,
          nome: 'ULE VARZEA GRANDE',
          sigla: 'VGA',
          codigo: '5100000007',
          tipoUnidade: 'UL',
          uvl: 3,
          urs: {
            id: 2,
            nome: 'REGIONAL CUIABÁ',
            sigla: 'URS CBA',
            codigo: '5100100000',
            tipoUnidade: 'UR',
            uvl: null,
            urs: {
              id: 1,
              nome: 'INSTITUTO DEFESA AGROPECUÁRIA DO ESTADO DE MATO GROSSO',
              sigla: 'INDEA',
              codigo: '5110000000',
              tipoUnidade: 'UC',
              uvl: null,
              urs: null,
              municipio: {
                id: 5103403,
                nome: 'CUIABA',
                codgIBGE: '5103403',
                uf: {
                  id: 51,
                  nome: 'MATO GROSSO',
                  sigla: 'MT',
                },
              },
            },
            municipio: {
              id: 5103403,
              nome: 'CUIABA',
              codgIBGE: '5103403',
              uf: {
                id: 51,
                nome: 'MATO GROSSO',
                sigla: 'MT',
              },
            },
          },
          municipio: {
            id: 5108402,
            nome: 'VARZEA GRANDE',
            codgIBGE: '5108402',
            uf: {
              id: 51,
              nome: 'MATO GROSSO',
              sigla: 'MT',
            },
          },
        },
        matricula: '91676',
        proprietarioInstituicao: 'N',
        profissionalOficial: 'S',
        pesquisador: null,
        tipoEmitente: {
          id: 'VE',
          nome: 'VETERINÁRIO ESTADUAL',
        },
        conselho: {
          id: 'CRMV ',
          nome: 'CONSELHO REGIONAL DE MEDICINA VETERINÁRIA',
        },
      },
      dataVigilancia: '2023-05-17T15:11:34.561Z',
      exploracao: {
        id: 302830,
        codigo: '5100302830',
      },
      historicoDeSugadurasDeMorcegos: 'NAO',
      observacao: `Teste: ${Math.random().toString(36)}`,
      propriedade: {
        id: 1091945,
        nome: 'CHACARA SAO MATEUS',
        municipio: {
          id: 5100250,
          uf: {
            id: 51,
            nome: 'MATO GROSSO',
            sigla: 'MT',
          },
          nome: 'ALTA FLORESTA',
        },
        enderecoPropriedade: null,
        viaAcesso:
          'MT 320 - COMUNIDADE CENTRALZINHA - HIDROPONIA MAIS \r\nBRASIL- ANTES DA SEDE COM. CENTRAL 0,2KM. SITUA-SE A \r\n2,8KM DA MT 325 E 8,7KM DA MT 208',
        unidade: {
          nome: 'ULE ALTA FLORESTA',
        },
        coordenadaGeografica: {
          id: 264155,
          pessoa: {},
          orientacaoLatitude: 'S',
          orientacaoLongitude: 'O',
          latGrau: 9,
          latMin: 57,
          latSeg: 33.8,
          longGrau: 56,
          longMin: 5,
          longSeg: 21.1,
        },
      },
      proprietario: {
        cpfCnpj: '00097154105',
        id: 1132395,
        nome: 'LAIR PEREIRA CARDOSO',
        email: null,
        endereco: {
          id: 1132395,
          telefone: '66992564758',
          tipoLogradouro: {
            id: 2,
            nome: 'RUA',
          },
          logradouro: 'RUA DOIS',
          bairro: 'COM. CENTRAL',
          complemento: null,
          referencia: null,
          municipio: {
            id: 5100250,
          },
        },
      },
      setor: {
        id: 520,
        nome: 'SETOR 02',
      },
      vigilanciaAlimentosRuminantes: {
        presencaDeCamaDeAviario: 'NAO',
        utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes: 'NAO',
        pisciculturaComSistemaDeAlimentacaoABaseDeRacao: 'NAO',
        contaminacaoCruzadaDeRacoesDePeixeERuminante: 'NAO',
        colheitaDeAmostraDeAlimentosDeRuminantes: 'NAO',
        usoTesteRapido: 'NAO',
      },
      vigilanciaBovinos: {},
      vigilanciaBubalino: {},
      vigilanciaCaprino: {},
      vigilanciaOvino: {},
      vigilanciaPeixes: {},
      vigilanciaSuideos: {
        presencaDeSuinosAsselvajados: 'NAO',
        contatoDiretoDeSuinosDomesticosComAsselvajados: 'NAO',
      },
      vigilanciaAbelha: {},
      vigilanciaAsinino: {},
      vigilanciaEquino: {},
      vigilanciaMuar: {},
      visitaPropriedadeRural: newFVER,
    };

    const newFVV = {
      ...baseFVV,
      visitaPropriedadeRural: {
        ...baseFVV.visitaPropriedadeRural,
        id: -1,
        codigoVerificador: erro
          ? baseFVV.visitaPropriedadeRural.codigoVerificador + 'a'
          : baseFVV.visitaPropriedadeRural.codigoVerificador,
      },
      codigoVerificador: generateHash(baseFVER),
    };

    //@ts-ignore
    FVVIDBService.insert(newFVV);
    window.location.reload();
  };

  return (
    <>
      <Row gutter={[24, 24]}>
        <Space size={24} direction='vertical' style={{ width: '100%' }}>
          <Alert
            message={
              <span style={{ fontWeight: 'bold' }}>
                Sobre as ações de campo offline
              </span>
            }
            description={
              <>
                <Typography.Text>
                  Aqui listamos todos os registros que foram cadastrados no modo
                  offline.
                </Typography.Text>
                <Typography.Paragraph style={{ marginBottom: 0 }}>
                  Registros com o status{' '}
                  <Tag
                    style={{
                      backgroundColor: '#2baf57',
                      color: 'white',
                    }}
                  >
                    SINCRONIZADO
                  </Tag>
                  já estão salvos no servidor e podem ser deletados do
                  dispositivo sem perda de dados.
                </Typography.Paragraph>
              </>
            }
            type='info'
            showIcon
            closable={false}
          />
          {/* {!backGroundSync && (
            <Alert
              message='Sincronização automática indisponível neste dispositivo.'
              type='warning'
              showIcon
              closable={false}
            />
          )} */}

          {ENVIRONMENT !== ENVIRONMENT_TYPE.PROD && (
            <Row gutter={24}>
              <Col>
                <Button type='primary' onClick={() => generate()}>
                  Gerar massa de teste
                </Button>
              </Col>

              <Col>
                <Button type='primary' onClick={() => generate(true)}>
                  Gerar massa de teste com erros
                </Button>
              </Col>
            </Row>
          )}

          <ErrorBoundary component='a lista de FVERs offline'>
            <ListFVEROffline visible={true} />
          </ErrorBoundary>

          <ErrorBoundary component='a lista de FVVs offline'>
            <ListFVVOffline visible={true} />
          </ErrorBoundary>

          {/* <FormVINOffline visible={true} /> */}
        </Space>
      </Row>
    </>
  );
}
