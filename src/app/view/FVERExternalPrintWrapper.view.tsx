import usePageTitle from '../../core/hooks/usePageTitle';
import ErrorBoundary from '../components/ErrorBoundary';
import FVERExternalPrint from '../features/FVER/FVERExternalPrint';

export default function FVERExternalPrintWrapper() {
  usePageTitle('FVER');

  return (
    <>
      <ErrorBoundary component='a visita'>
        <FVERExternalPrint />
      </ErrorBoundary>
    </>
  );
}
