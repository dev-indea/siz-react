import useLoadingPage from '../../core/hooks/useLoadingPage';
import usePageTitle from '../../core/hooks/usePageTitle';
import ErrorBoundary from '../components/ErrorBoundary';

export default function FormVINCreate() {
  usePageTitle('Novo FVER');

  const { setFirstOpening, setMode } = useLoadingPage();

  setMode('CREATE');
  setFirstOpening(false);

  return (
    <>
      <ErrorBoundary component='formulário'>
        <FormVINCreate />
      </ErrorBoundary>
    </>
  );
}
