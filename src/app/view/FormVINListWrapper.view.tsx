import { Spin } from 'antd';
import useLoadingPage from '../../core/hooks/useLoadingPage';
import usePageTitle from '../../core/hooks/usePageTitle';
import ErrorBoundary from '../components/ErrorBoundary';

export default function FormVINList() {
  usePageTitle('SIZ - Lista de Form VINs');
  const { loading } = useLoadingPage();

  return (
    <>
      <ErrorBoundary component='a lista de Form VINs'>
        <Spin
          size='large'
          spinning={loading}
          style={{
            position: 'fixed',
            top: '20%',
            zIndex: '1',
          }}
        >
          <FormVINList />
        </Spin>
      </ErrorBoundary>
    </>
  );
}
