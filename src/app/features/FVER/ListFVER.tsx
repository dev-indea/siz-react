import {
  Button,
  Col,
  Descriptions,
  notification,
  Popconfirm,
  Row,
  Skeleton,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import { useCallback, useEffect, useState } from 'react';
import moment from 'moment';
import {
  DeleteOutlined,
  EditTwoTone,
  PlusOutlined,
  SearchOutlined,
  SyncOutlined,
} from '@ant-design/icons';
import { FVER } from '../../../sdk/@types';
import FVERService from '../../../sdk/services/SIZ-API/FVER.service';
import { Link, useNavigate } from 'react-router-dom';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import FVVSvg from '../../components/FVVSvg';
import { TipoEstabelecimento } from '../../../core/enums/TipoEstabelecimento';
import FVVService from '../../../sdk/services/SIZ-API/FVV.service';
import FilterPanelFVER from './FilterPanelFVER';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../core/store';
import * as FVERActions from '../../../core/store/FVER.slice';
import { CustomModal } from '../../components/CustomModal';

export default function ListFVER() {
  const dispatch = useDispatch();
  const [deleting, setDeleting] = useState(false);
  const { loading, setLoading, setFirstOpening } = useLoadingPage();
  const { xs } = useBreakpoint();
  const navigate = useNavigate();
  const [error, setError] = useState<Error>();
  const [modalSummaryVisible, setModalSummaryVisible] = useState(false);
  const [fverSummary, setFVERSummary] = useState<FVER.Summary>();

  const fvers = useSelector((state: RootState) => state.fver.fvers);
  const query = useSelector((state: RootState) => state.fver.query);
  const fetching = useSelector((state: RootState) => state.fver.fetching);

  if (!window.navigator.onLine)
    setError(
      new Error('Não é possível visualizar a lista de FVER no modo offline')
    );

  if (error) throw error;

  useEffect(() => {
    setFirstOpening(true);
  }, []);

  useEffect(() => {}, [fvers]);

  const deleteFVER = useCallback(
    async (a1) => {
      if (a1) {
        setDeleting(true);
        return await FVERService.remove(a1)
          .then(() => {
            setDeleting(false);
            notification.success({
              message: 'FVER removido com sucesso',
            });
            dispatch(FVERActions.filter(query));
          })
          .catch(() => {
            setDeleting(false);
            notification.error({
              message: 'Não foi possível remover o FVER',
              description: 'Verifique se há algum FVV vinculado a este FVER',
            });
          });
      }
    },
    [dispatch, query]
  );

  return (
    <>
      <FilterPanelFVER />
      <br />
      <Row>
        <Col span={24}>
          {fetching && <Skeleton active />}

          {!fetching && (
            <>
              <Table<FVER.Summary>
                rowKey={'id'}
                dataSource={fvers?.content}
                size={'small'}
                loading={deleting || fetching}
                pagination={{
                  current: query.page ? query.page + 1 : 1,
                  onChange: (page, pageSize) => {
                    dispatch(
                      FVERActions.setQuery({
                        ...query,
                        page: page - 1,
                        size: pageSize,
                      })
                    );
                  },
                  total: fvers?.totalElements,
                  pageSize: query.size,
                }}
                title={() => {
                  return (
                    <Row justify={'space-between'}>
                      <Typography.Title level={5} style={{ color: 'white' }}>
                        FVERs
                      </Typography.Title>

                      <Space>
                        <Col>
                          <Link to={'/visitas/cadastro'}>
                            <Button
                              icon={<PlusOutlined />}
                              type={'primary'}
                              title={'Novo'}
                            />
                          </Link>
                        </Col>

                        <Col>
                          <Button
                            icon={<SyncOutlined />}
                            type={'primary'}
                            title={'Atualizar'}
                            onClick={async () => {
                              setLoading(true);
                              try {
                                dispatch(FVERActions.filter(query));
                              } finally {
                                setLoading(false);
                              }
                            }}
                          />
                        </Col>
                      </Space>
                    </Row>
                  );
                }}
                columns={[
                  {
                    dataIndex: 'numero',
                    title: 'Número',
                    width: 175,
                    responsive: ['xs', 'sm'],
                    render(numero) {
                      return (
                        <span style={{ fontWeight: 'bold' }}>{numero}</span>
                      );
                    },
                  },
                  {
                    dataIndex: 'dataDaVisita',
                    title: 'Data',
                    width: 100,
                    responsive: ['xs', 'sm'],
                    render(data: FVER.Summary['dataDaVisita']) {
                      return moment(new Date(data)).format('DD/MM/YYYY');
                    },
                  },
                  {
                    dataIndex: 'tipoEstabelecimento',
                    title: 'Tipo',
                    width: 150,
                    responsive: ['md'],
                    render(tipo: FVER.Summary['tipoEstabelecimento']) {
                      return (
                        <Tag>
                          {TipoEstabelecimento.valueOf(tipo).toString()}
                        </Tag>
                      );
                    },
                  },
                  {
                    dataIndex: 'nomeEstabelecimento',
                    title: 'Nome',
                    responsive: ['sm'],
                    render(nome: FVER.Summary['nomeEstabelecimento']) {
                      return <Tooltip title={nome}>{nome}</Tooltip>;
                    },
                  },
                  {
                    dataIndex: 'id',
                    title: '',
                    responsive: ['xs', 'sm'],
                    width: 130,
                    align: 'right',
                    render(id: number, fver: FVER.Summary) {
                      return (
                        <Space>
                          <Link to={`/visitas/visualizar/${id}`}>
                            <Button
                              icon={<SearchOutlined />}
                              size={'small'}
                              type={'ghost'}
                            />
                          </Link>

                          <Button
                            icon={<FVVSvg />}
                            size={'small'}
                            type={'ghost'}
                            onClick={async () => {
                              if (fver.tipoEstabelecimento !== 'PROPRIEDADE') {
                                notification.warn({
                                  message: `Não é possível incluir FVV para ${fver.tipoEstabelecimento}`,
                                });
                                return;
                              }

                              await FVVService.getByFVER(id).then(
                                (idVigilancia) => {
                                  if (idVigilancia)
                                    navigate(
                                      `/vigilancias/edicao/${idVigilancia}`
                                    );
                                  else navigate(`/vigilancias/cadastro/${id}`);
                                }
                              );
                            }}
                          />

                          <Popconfirm
                            title={'Deseja remover o FVER?'}
                            onConfirm={() => {
                              deleteFVER(id);
                            }}
                          >
                            <Button
                              icon={<DeleteOutlined />}
                              size={'small'}
                              type={'ghost'}
                              danger
                            />
                          </Popconfirm>
                        </Space>
                      );
                    },
                  },
                ]}
                expandable={{
                  expandedRowRender: (record) => (
                    <Typography
                      style={{
                        color: '#969494',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        display: '-webkit-box',
                        WebkitLineClamp: 2,
                        lineClamp: 2,
                        WebkitBoxOrient: 'vertical',
                      }}
                    >
                      {record.resumo}
                    </Typography>
                  ),
                  defaultExpandAllRows: true,
                  showExpandColumn: false,
                }}
                onRow={(record, index) => {
                  return {
                    onDoubleClick: (e) => {
                      if (!xs) return;

                      setModalSummaryVisible(true);
                      setFVERSummary(record);
                    },
                  };
                }}
              ></Table>
              <CustomModal
                centered
                open={modalSummaryVisible}
                title={`FVER nº ${fverSummary?.numero}`}
                closable={false}
                onCancel={() => {
                  setModalSummaryVisible(false);
                  setFVERSummary(undefined);
                }}
                maskClosable={true}
                keyboard={true}
                footer={
                  <Button
                    type='primary'
                    onClick={() => {
                      setModalSummaryVisible(false);
                      setFVERSummary(undefined);
                    }}
                  >
                    Fechar
                  </Button>
                }
              >
                {fverSummary && (
                  <Descriptions size={'small'}>
                    <Descriptions.Item label={'Número'}>
                      {fverSummary.numero}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Data'}>
                      {moment(new Date(fverSummary.dataDaVisita)).format(
                        'DD/MM/YYYY'
                      )}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Tipo'}>
                      <Tag>
                        {TipoEstabelecimento.valueOf(
                          fverSummary.tipoEstabelecimento
                        ).toString()}
                      </Tag>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Nome'}>
                      <Tooltip title={fverSummary.nomeEstabelecimento}>
                        {fverSummary.nomeEstabelecimento}
                      </Tooltip>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Resumo'}>
                      <Tooltip title={fverSummary.resumo}>
                        <Typography.Text
                          ellipsis={true}
                          style={{ maxWidth: '100%' }}
                        >
                          {fverSummary.resumo}
                        </Typography.Text>
                      </Tooltip>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Ações'}>
                      <Space>
                        <Link to={`/visitas/edicao/${fverSummary.id}`}>
                          <Button
                            icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                            size={'small'}
                            type={'ghost'}
                          />
                        </Link>
                        <Popconfirm
                          title={'Deseja remover o FVER?'}
                          onConfirm={() => {
                            try {
                              deleteFVER(fverSummary.id)?.then((result) => {
                                notification.success({
                                  message: 'FVER removido com sucesso',
                                });
                              });
                            } catch (e) {
                              throw e;
                            }
                          }}
                        >
                          <Button
                            icon={<DeleteOutlined />}
                            size={'small'}
                            type={'ghost'}
                            danger
                          />
                        </Popconfirm>
                      </Space>
                    </Descriptions.Item>
                  </Descriptions>
                )}
              </CustomModal>
            </>
          )}
        </Col>
      </Row>
    </>
  );
}
