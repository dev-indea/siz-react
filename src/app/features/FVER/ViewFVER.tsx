import { Skeleton, Spin, Typography } from 'antd';
import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import useFVER from '../../../core/hooks/useFVER';
import FormFVER from './FormFVER';

export default function ViewFVER() {
  const { loading } = useLoadingPage();

  const [error, setError] = useState<Error>();

  const params = useParams<{ id: string }>();

  const { fetchFVERById, fver } = useFVER();

  useEffect(() => {
    if (!isNaN(Number(params.id))) {
      fetchFVERById(Number(params.id)).catch((e) => {
        setError(e);
      });
    }
  }, [fetchFVERById, params]);

  if (error) throw error;

  const prepareData = useCallback((fver) => {
    const dataDaVisita = moment(fver.dataDaVisita);

    return {
      ...fver,
      dataDaVisita: dataDaVisita,
      horaDaVisita:
        (dataDaVisita.get('hour') + '').padStart(2, '0') +
        ':' +
        (dataDaVisita.get('minute') + '').padStart(2, '0'),
    };
  }, []);

  return (
    <>
      {!fver && <Skeleton active />}

      {fver && (
        <>
          <Typography.Title level={2}>
            FVER
            {fver ? ` nº ${fver?.numero}` : ' offline'}
          </Typography.Title>

          <Spin
            size='large'
            spinning={loading}
            style={{
              position: 'fixed',
              top: '20%',
              zIndex: '1',
            }}
          >
            <FormFVER fver={prepareData(fver)} />
          </Spin>
        </>
      )}
    </>
  );
}
