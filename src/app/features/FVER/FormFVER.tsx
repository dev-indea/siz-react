import {
  CheckCircleOutlined,
  PlusOutlined,
  CloseOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import {
  DatePicker,
  Form,
  Row,
  Col,
  Divider,
  Select,
  Button,
  Table,
  Typography,
  Input,
  notification,
  Tag,
  Space,
  Descriptions,
  Switch,
  Popconfirm,
  Tooltip,
  Card,
  Radio,
  Popover,
} from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import confirm from 'antd/lib/modal/confirm';
import moment, { Moment } from 'moment';
import { useCallback, useEffect, useState } from 'react';
import useTipoChavePrincipalFVER from '../../../core/hooks/useTipoChavePrincipalFVER';
import useTipoChaveSecundariaFVER from '../../../core/hooks/useTipoChaveSecundariaFVER';
import {
  ChavePrincipalFVER,
  Servidor,
  TipoChaveSecundariaFVER,
  FVER,
} from '../../../sdk/@types';
import PropriedadePanel from '../../components/PropriedadePanel';
import AbatedouroPanel from '../../components/AbatedouroPanel';
import RecintoPanel from '../../components/RecintoPanel';
import SignatureWrapper from '../../components/SignatureWrapper';
import useFVER from '../../../core/hooks/useFVER';
import useAssinaturaBase64Image from '../../../core/hooks/useAssinaturaBase64Image';
import { Link, useNavigate, useParams } from 'react-router-dom';
import FVERIDBService from '../../../sdk/services/indexeddb/FVERIDB.service';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import ServidoresPanel from '../../components/ServidoresPanel';
import InputMask from 'react-input-mask';
import ServidorIDBService from '../../../sdk/services/indexeddb/ServidorIDB.service';
import AuthorizationService from '../../../core/auth/Authorization.service';
import scrollFieldToTop from '../../../core/functions/scrollFieldToTop';
import { StatusAssinatura } from '../../../core/enums/StatusAssinatura';
import FVERService from '../../../sdk/services/SIZ-API/FVER.service';
import { CustomModal } from '../../components/CustomModal';
import generateHash from '../../../core/functions/generateHash';
import Service from '../../../sdk/services/SIZ-API/Service';

const { TextArea } = Input;

export type FormFVERType = {
  dataDaVisita?: Moment;
  horaDaVisita?: string;
  modalTipoChavePrimaria?: {
    id: number;
    nome: string;
  };
  modalListaTipoChaveSecundaria?: TipoChaveSecundariaFVER.Summary[];
  quantidadeApreensoes?: number;
  quantidadeDestruicoes?: number;
  servidorBusca?: Servidor.Summary;
} & Omit<FVER.Input, 'dataDaVisita'>;

type FormFVERProps = {
  fver?: FormFVERType;
};

export default function FormFVER(props: FormFVERProps) {
  const [statusAssinaturaState, setStatusAssinaturaState] = useState();

  const [error, setError] = useState<Error>();

  const params = useParams<{ id: string; local?: string }>();
  const { insert, updateFVEROffline } = useFVER();
  const [form] = Form.useForm<FormFVERType>();
  const { xs } = useBreakpoint();
  const navigate = useNavigate();
  const [formDisabled, setFormDisabled] = useState<boolean>(false);

  const { mode, setLoading } = useLoadingPage();

  const { assinaturaBase64Image, setAssinaturaBase64Image } =
    useAssinaturaBase64Image();

  const [showAbatedouroPanel, setShowAbatedouroPanel] =
    useState<boolean>(false);
  const [showPropriedadePanel, setShowPropriedadePanel] =
    useState<boolean>(false);
  const [showRecintoPanel, setShowRecintoPanel] = useState<boolean>(false);

  const {
    tipoChavePrincipalFVER,
    fetchActive: fetchActiveTipoChavePrincipalFVER,
  } = useTipoChavePrincipalFVER();

  const {
    tipoChaveSecundariaFVER,
    fetchActive: fetchActiveTipoChaveSecundariaFVER,
  } = useTipoChaveSecundariaFVER();

  const [showModalProgramas, setShowModalProgramas] = useState<boolean>(false);
  const [showModalFinalizacao, setShowModalFinalizacao] =
    useState<boolean>(false);

  const [dadosFVERModalFinalizacao, setDadosFVERModalFinalizacao] = useState<{
    id?: string;
    numero?: string;
    fver?: FVER.Input;
    local?: boolean;
    problema_rede?: boolean;
  }>({});

  const [dataSourceTableChavesPrimarias, setDatasourceTableChavesPrimarias] =
    useState<FormFVERType['listChavePrincipalVisitaPropriedadeRural']>([]);

  const [listaServidores, setListaServidores] = useState<Servidor.Summary[]>(
    []
  );

  const listaStatusAssinatura = StatusAssinatura.keys();

  useEffect(() => {
    setLoading(false);
  }, []);

  useEffect(() => {}, [dadosFVERModalFinalizacao]);

  useEffect(() => {
    switchPanel(form.getFieldValue('tipoEstabelecimento'));

    if (props.fver?.listServidores)
      setListaServidores(props.fver?.listServidores);
    if (props.fver?.listChavePrincipalVisitaPropriedadeRural) {
      setDatasourceTableChavesPrimarias(
        props.fver?.listChavePrincipalVisitaPropriedadeRural
      );
    }
    if (params.local) setLoading(false);

    if (props.fver?.assinatura) setAssinaturaBase64Image(props.fver.assinatura);
  }, [props.fver, params, setAssinaturaBase64Image, setLoading]);

  useEffect(() => {
    setFormDisabled(mode === 'VIEW' ? true : false);

    if (mode !== 'CREATE')
      setStatusAssinaturaState(form.getFieldValue('statusAssinatura'));
  }, [mode, form]);

  useEffect(() => {
    const insertLoggedUser = async () => {
      if (mode === 'CREATE') {
        if (AuthorizationService.getUsuarioSIZ()) {
          await ServidorIDBService.getByCpf(
            //@ts-ignore
            AuthorizationService.getUsuarioSIZ()?.cpf.replace(/\D/g, '')
          ).then((servidor) => {
            if (servidor) {
              const newLista = listaServidores.concat(servidor.payload);
              setListaServidores(newLista);
            }
          });
        }
      }
    };

    insertLoggedUser();
  }, []);

  useEffect(() => {
    fetchActiveTipoChavePrincipalFVER();
    fetchActiveTipoChaveSecundariaFVER();
  }, [fetchActiveTipoChavePrincipalFVER, fetchActiveTipoChaveSecundariaFVER]);

  const saveFVERLocal = useCallback(
    async (fver: FVER.Input, problema_rede?: boolean) => {
      console.log('5');
      if (params.id) {
        console.log('6');
        await updateFVEROffline(Number(params.id), fver)
          .then((visita) => {
            setDadosFVERModalFinalizacao({
              id: params.id,
              numero: '',
              fver: fver,
              local: true,
            });

            console.log('7');
            setShowModalFinalizacao(true);
          })
          .catch((e) => {
            notification.error({
              message: 'Não foi possível atualizar o FVER no dispositivo',
              description: e.message,
            });
          });
      } else {
        console.log('8');
        fver.codigoVerificador = generateHash(fver);
        await FVERIDBService.insert(fver)
          .then((visita) => {
            setDadosFVERModalFinalizacao({
              id: visita.toString(),
              fver: fver,
              local: true,
              problema_rede: problema_rede,
            });
            console.log('9');

            setShowModalFinalizacao(true);
          })
          .catch((e) => {
            notification.error({
              message: 'Não foi possível salvar o FVER no dispositivo',
              description: e.message,
            });
            setLoading(false);
          })
          .finally(() => {
            setLoading(false);
          });
      }
    },
    [params, setLoading, updateFVEROffline]
  );

  const saveFVEROnline = useCallback(
    async (fver: FVER.Input) => {
      if (!fver.id) {
        fver.codigoVerificador = generateHash(fver);
      }
      await FVERService.add(fver)
        .then((visita) => {
          console.log('1');

          setDadosFVERModalFinalizacao({
            //@ts-ignore
            id: visita.id,
            //@ts-ignore
            numero: visita.numero,
            fver: visita,
          });
          console.log('2');
          setShowModalFinalizacao(true);
        })
        .catch(async (e) => {
          console.log('3', e.message);
          console.log('4', JSON.stringify(e));

          if (
            e.message === Service.NETWORK_ERROR ||
            e.message.includes('Serviço indisponível')
          ) {
            if (!fver.id) {
              await saveFVERLocal(fver, true);
            }
          }

          console.log('10');
          setLoading(false);
        })
        .finally(() => {
          setLoading(false);
        });
    },
    [params, saveFVERLocal, setLoading]
  );

  const handleFormSubmit = useCallback(
    async (formParam: FormFVERType) => {
      setLoading(true);
      const lista: FVER.Input['listChavePrincipalVisitaPropriedadeRural'] =
        dataSourceTableChavesPrimarias.map((chave) => {
          return {
            motivoPrincipal: chave.motivoPrincipal,
            tipoChavePrincipalVisitaPropriedadeRural: {
              id: chave.tipoChavePrincipalVisitaPropriedadeRural.id,
              nome: chave.tipoChavePrincipalVisitaPropriedadeRural.nome,
            },

            listChaveSecundariaVisitaPropriedadeRural:
              chave.listChaveSecundariaVisitaPropriedadeRural,
            quantidadeApreensoes: form.getFieldValue('quantidadeApreensoes'),
            quantidadeDestruicoes: form.getFieldValue('quantidadeDestruicoes'),
          };
        });

      const data = form.getFieldValue('dataDaVisita');
      const hora: string = form.getFieldValue('horaDaVisita');
      const dataMoment = data as Moment;

      dataMoment.set('hour', Number(hora.split(':')[0]));
      dataMoment.set('minute', Number(hora.split(':')[1]));

      const fverDTO: FVER.Input = {
        ...formParam,
        dataDaVisita: JSON.stringify(dataMoment.toDate()).replaceAll('"', ''),
        //@ts-ignore
        horaDaVisita: '',
        listChavePrincipalVisitaPropriedadeRural: lista,
        listServidores: listaServidores,
        statusAssinatura: statusAssinaturaState,
        assinatura: assinaturaBase64Image,
      };

      if (window.navigator.onLine) {
        if (params.local) saveFVERLocal(fverDTO);
        else saveFVEROnline(fverDTO);
      } else {
        if (!fverDTO.id) saveFVERLocal(fverDTO);
        else saveFVEROnline(fverDTO);
      }
      return false;
    },
    [
      dataSourceTableChavesPrimarias,
      form,
      listaServidores,
      assinaturaBase64Image,
      params,
      saveFVERLocal,
      saveFVEROnline,
      setLoading,
      statusAssinaturaState,
    ]
  );

  const handleSelectChavePrimariaChange = (value: any) => {
    let obj: FormFVERType['modalTipoChavePrimaria'] = JSON.parse(value);

    if (obj)
      form.setFieldsValue({
        modalTipoChavePrimaria: { id: obj.id, nome: obj.nome },
      });
  };

  const handleSelectChaveSecundariaChange = (list: any[]) => {
    let listaParseada: any[] = [];

    list.forEach((obj, i) => {
      listaParseada = listaParseada.concat(JSON.parse(obj));
    });

    form.setFieldsValue({
      modalListaTipoChaveSecundaria: listaParseada,
    });
  };

  const cleanModalProgramasEAtividades = useCallback(() => {
    form.setFieldsValue({
      modalTipoChavePrimaria: undefined,
      modalListaTipoChaveSecundaria: undefined,
      quantidadeApreensoes: undefined,
      quantidadeDestruicoes: undefined,
    });
  }, [form]);

  const handleAddPrograma = useCallback(() => {
    const tipoChaveSendoInserida: ChavePrincipalFVER.Input['tipoChavePrincipalVisitaPropriedadeRural'] =
      form.getFieldValue('modalTipoChavePrimaria');

    const arrayDeChavesIguaisAChaveQueEstaSendoInserida =
      dataSourceTableChavesPrimarias.filter((chave) => {
        return (
          chave.tipoChavePrincipalVisitaPropriedadeRural.id ===
          tipoChaveSendoInserida.id
        );
      });

    if (arrayDeChavesIguaisAChaveQueEstaSendoInserida.length > 0) {
      notification.warn({
        message: 'Esta chave principal já foi incluída',
        description: 'Selecione outra chave principal',
      });
      return;
    }

    const listaTipoChaveSecundaria: TipoChaveSecundariaFVER.Summary[] =
      form.getFieldValue('modalListaTipoChaveSecundaria');

    let list = listaTipoChaveSecundaria?.map((s) => {
      return { tipoChaveSecundariaVisitaPropriedadeRural: s };
    });

    const novaChaveASerIncluida: typeof dataSourceTableChavesPrimarias = [
      {
        motivoPrincipal:
          dataSourceTableChavesPrimarias.length > 0 ? 'NAO' : 'SIM',
        tipoChavePrincipalVisitaPropriedadeRural: {
          id: tipoChaveSendoInserida.id,
          nome: tipoChaveSendoInserida.nome,
        },
        listChaveSecundariaVisitaPropriedadeRural: list,
        quantidadeApreensoes: form.getFieldValue('quantidadeApreensoes'),
        quantidadeDestruicoes: form.getFieldValue('quantidadeDestruicoes'),
      },
    ];

    setDatasourceTableChavesPrimarias(
      dataSourceTableChavesPrimarias.concat(novaChaveASerIncluida)
    );
    setShowModalProgramas(false);
    cleanModalProgramasEAtividades();
    form.validateFields(['tableChavesPrimarias']);
    notification.success({
      message: 'Programa e atividades incluído com sucesso',
    });
  }, [dataSourceTableChavesPrimarias, form, cleanModalProgramasEAtividades]);

  const switchPanel = useCallback(
    (value: FVER.Input['tipoEstabelecimento']) => {
      if (value === 'ABATEDOURO') {
        setShowAbatedouroPanel(true);
        setShowPropriedadePanel(false);
        setShowRecintoPanel(false);
      } else if (value === 'PROPRIEDADE') {
        setShowAbatedouroPanel(false);
        setShowPropriedadePanel(true);
        setShowRecintoPanel(false);
      } else if (value === 'RECINTO') {
        setShowAbatedouroPanel(false);
        setShowPropriedadePanel(false);
        setShowRecintoPanel(true);
      }

      /*
      form.setFieldsValue({
        propriedade: undefined,
        abatedouro: undefined,
        recinto: undefined,
        proprietario: undefined,
      });
      */
    },
    []
  );

  const onChangeSwitchMotivoPrincipal = useCallback(
    (value, chavePrincipal: ChavePrincipalFVER.Input) => {
      setDatasourceTableChavesPrimarias(
        dataSourceTableChavesPrimarias.map((chave) => {
          if (
            chave.tipoChavePrincipalVisitaPropriedadeRural.id ===
            chavePrincipal.tipoChavePrincipalVisitaPropriedadeRural.id
          ) {
            if (dataSourceTableChavesPrimarias.length > 1) {
              if (chave.motivoPrincipal === 'SIM') {
                notification.warn({
                  message: 'Não foi possível desmarcar o item',
                  description:
                    'Pelo menos um programa deve ser o motivo principal da visita',
                });
              } else chave.motivoPrincipal = value ? 'SIM' : 'NAO';
            } else if (chave.motivoPrincipal === 'SIM') {
              notification.warn({
                message: 'Não foi possível desmarcar o item',
                description:
                  'Pelo menos um programa deve ser o motivo principal da visita',
              });
            } else {
              chave.motivoPrincipal = 'SIM';
            }
          } else if (value) {
            chave.motivoPrincipal = 'NAO';
          }

          return chave;
        })
      );
    },
    [dataSourceTableChavesPrimarias]
  );

  const chavePrimariaTableActions = (
    chavePrimaria: ChavePrincipalFVER.Input
  ) => {
    return (
      <Row align={'middle'} justify={xs ? 'start' : 'center'} gutter={12}>
        <Popconfirm
          disabled={chavePrimaria.motivoPrincipal === 'SIM'}
          title={'Deseja remover a chave primária?'}
          onConfirm={() => {
            setDatasourceTableChavesPrimarias(
              dataSourceTableChavesPrimarias.filter(
                (chave) =>
                  chave.tipoChavePrincipalVisitaPropriedadeRural.id !==
                  chavePrimaria.tipoChavePrincipalVisitaPropriedadeRural.id
              )
            );
            notification.success({
              message: `Chave ${chavePrimaria.tipoChavePrincipalVisitaPropriedadeRural.nome} removida`,
            });
          }}
        >
          <Button
            icon={<DeleteOutlined />}
            danger
            disabled={chavePrimaria.motivoPrincipal === 'SIM' || formDisabled}
            type={'ghost'}
            title={'Remover chave primária'}
          />
        </Popconfirm>
      </Row>
    );
  };

  return (
    <>
      <fieldset disabled={formDisabled}>
        <Form<FormFVERType>
          layout={'vertical'}
          form={form}
          size={!xs ? 'small' : 'large'}
          onFinish={handleFormSubmit}
          autoComplete={'off'}
          onFinishFailed={() => {
            notification.error({
              message: 'Alguns campos obrigatórios não foram preenchidos',
              description: 'Por favor revise o formulário',
            });
          }}
          initialValues={props.fver}
        >
          <Row gutter={24}>
            <Form.Item name={'id'} hidden>
              <Input disabled hidden />
            </Form.Item>

            <Form.Item name={'codigoVerificador'} hidden>
              <Input disabled hidden />
            </Form.Item>
            <Form.Item name={'numero'} hidden>
              <Input disabled hidden />
            </Form.Item>
            <Col xs={15} lg={5}>
              <Form.Item
                label={'Data da visita'}
                name={'dataDaVisita'}
                rules={[{ required: true, message: 'O campo é obrigatório' }]}
              >
                <DatePicker
                  onClick={() => {
                    scrollFieldToTop('dataDaVisita');
                  }}
                  style={{ width: '100%' }}
                  format={'DD/MM/YYYY'}
                  disabledDate={(date) => date.isAfter(moment().toDate())}
                  disabled={formDisabled}
                  inputReadOnly
                />
              </Form.Item>
            </Col>
            <Col xs={9} lg={3}>
              <Form.Item
                label={'Hora'}
                name={'horaDaVisita'}
                rules={[{ required: true, message: 'O campo é obrigatório' }]}
              >
                <InputMask
                  onClick={() => {
                    scrollFieldToTop('horaDaVisita');
                  }}
                  disabled={formDisabled}
                  mask={[/^([0-2])/, /([0-9])/, ':', /[0-5]/, /[0-9]/]}
                >
                  <Input inputMode='numeric' autoComplete='off' />
                </InputMask>
              </Form.Item>
            </Col>
            <Col xs={24} lg={0}>
              <br />
            </Col>
            <Col xs={24} lg={8}>
              <Form.Item
                label={'Placa do veículo utilizado na visita'}
                name={'placaVeiculo'}
                rules={[
                  {
                    required: true,
                    message: 'O campo é obrigatório',
                  },
                ]}
              >
                <Input
                  autoComplete='off'
                  onClick={() => {
                    scrollFieldToTop('placaVeiculo');
                  }}
                />
              </Form.Item>
            </Col>

            <Col xs={24} lg={8}>
              <Form.Item
                label={'Tipo estabelecimento visitado'}
                name={'tipoEstabelecimento'}
                rules={[
                  {
                    required: true,
                    message: 'O campo é obrigatorio',
                  },
                ]}
              >
                <Select
                  onClick={() => {
                    scrollFieldToTop('tipoEstabelecimento');
                  }}
                  placeholder={'Selecione o tipo do estabelecimento'}
                  onChange={switchPanel}
                  disabled={formDisabled || mode === 'EDIT'}
                  placement={'bottomLeft'}
                >
                  <Select.Option key={'ABATEDOURO'} value={'ABATEDOURO'}>
                    Abatedouro
                  </Select.Option>
                  <Select.Option key={'PROPRIEDADE'} value={'PROPRIEDADE'}>
                    Estabelecimento Rural
                  </Select.Option>
                  <Select.Option key={'RECINTO'} value={'RECINTO'}>
                    Recinto
                  </Select.Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>

          {showAbatedouroPanel && (
            <AbatedouroPanel form={form} formDisabled={formDisabled} />
          )}
          {showPropriedadePanel && (
            <PropriedadePanel
              form={form}
              formDisabled={formDisabled}
              showHeader
            />
          )}
          {showRecintoPanel && (
            <RecintoPanel form={form} formDisabled={formDisabled} />
          )}

          <Divider orientation='left'>Dados da Visita</Divider>

          <Row gutter={24}>
            <Space direction={'vertical'} style={{ width: '100%' }}>
              <Col xs={0} lg={0}>
                <Form.Item
                  name={'listChavePrincipalVisitaPropriedadeRural'}
                  hidden
                >
                  <Input hidden />
                </Form.Item>
              </Col>

              <Col xs={24} lg={24}>
                <Form.Item
                  label={'Resumo das atividades realizadas'}
                  name={'resumo'}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <TextArea
                    showCount
                    rows={5}
                    maxLength={2000}
                    onClick={() => {
                      scrollFieldToTop('resumo');
                    }}
                  />
                </Form.Item>
              </Col>

              <CustomModal
                centered
                open={showModalProgramas}
                title={'Cadastro de programas e atividades'}
                onCancel={() => {
                  setShowModalProgramas(false);
                  cleanModalProgramasEAtividades();
                }}
                width={750}
                footer={
                  <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Button
                      style={{ width: '50%' }}
                      danger
                      icon={<CloseOutlined />}
                      onClick={() => {
                        setShowModalProgramas(false);
                        cleanModalProgramasEAtividades();
                      }}
                    >
                      Cancelar
                    </Button>
                    <Button
                      style={{ width: '50%' }}
                      type={'primary'}
                      icon={<CheckCircleOutlined />}
                      onClick={() => {
                        form.validateFields([['modalTipoChavePrimaria', 'id']]);
                        form.validateFields(['modalListaTipoChaveSecundaria']);

                        const tipoChavePrimaria = form.getFieldValue(
                          'modalTipoChavePrimaria'
                        );

                        const listaTipoChaveSecundaria = form.getFieldValue(
                          'modalListaTipoChaveSecundaria'
                        );

                        if (!tipoChavePrimaria || !listaTipoChaveSecundaria)
                          return;
                        handleAddPrograma();
                      }}
                    >
                      Salvar
                    </Button>
                  </div>
                }
                maskClosable={false}
                destroyOnClose
              >
                <Form layout={'vertical'} form={form} autoComplete={'off'}>
                  <Row gutter={24}>
                    <Col xs={24} lg={24}>
                      <Form.Item
                        label={'Chave Principal'}
                        name={['modalTipoChavePrimaria', 'id']}
                        rules={[
                          {
                            required: true,
                            message: 'O campo é obrigatório',
                          },
                        ]}
                        valuePropName={'modaltipochaveprimaria'}
                      >
                        <Select
                          placeholder={'Selecione uma chave'}
                          onChange={handleSelectChavePrimariaChange}
                          placement={'bottomLeft'}
                        >
                          {tipoChavePrincipalFVER?.map((tipo) => (
                            <Select.Option
                              key={tipo.nome}
                              value={JSON.stringify(tipo)}
                            >
                              {tipo.nome}
                            </Select.Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>

                    <Col xs={0} lg={0}>
                      <Form.Item
                        name={['modalTipoChavePrimaria', 'nome']}
                        hidden
                      >
                        <Input />
                      </Form.Item>
                    </Col>

                    <Col xs={24} lg={24}>
                      <Form.Item
                        label={'Chave Secundária'}
                        name={'modalListaTipoChaveSecundaria'}
                        valuePropName={'checked'}
                        rules={[
                          {
                            required: true,
                            message: 'O campo é obrigatório',
                          },
                        ]}
                      >
                        <Select
                          mode='multiple'
                          placeholder={'Selecione as chaves secundárias'}
                          onChange={handleSelectChaveSecundariaChange}
                          showSearch={false}
                          placement={'bottomLeft'}
                        >
                          {tipoChaveSecundariaFVER?.map((tipo) => (
                            <Select.Option
                              key={tipo.nome}
                              value={JSON.stringify(tipo)}
                            >
                              {tipo.nome}
                            </Select.Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>

                    <Col xs={24} lg={8}>
                      <Form.Item
                        label={'Quantidade de apreensões'}
                        name={'modalQuantidadeApreensoes'}
                      >
                        <Input
                          inputMode='numeric'
                          autoComplete='off'
                          onKeyPress={(event: any) => {
                            if (!/[0-9]/.test(event.key)) {
                              event.preventDefault();
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>

                    <Col xs={24} lg={8}>
                      <Form.Item
                        label={'Quantidade de destruições'}
                        name={'modalQuantidadeDestruicoes'}
                      >
                        <Input
                          inputMode='numeric'
                          autoComplete='off'
                          onKeyPress={(event: any) => {
                            if (!/[0-9]/.test(event.key)) {
                              event.preventDefault();
                            }
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </CustomModal>

              <Col xs={24} sm={24}>
                <Form.Item
                  validateTrigger={'onSubmit'}
                  name='tableChavesPrimarias'
                  rules={[
                    {
                      validator(_, value) {
                        if (dataSourceTableChavesPrimarias.length < 1)
                          return Promise.reject(
                            new Error(
                              'Deve ser informado pelo menos um programa'
                            )
                          );
                        return Promise.resolve();
                      },
                    },
                  ]}
                >
                  <Table<ChavePrincipalFVER.Input>
                    size={'small'}
                    dataSource={dataSourceTableChavesPrimarias}
                    rowKey={(record) =>
                      record.tipoChavePrincipalVisitaPropriedadeRural.nome
                    }
                    title={() => {
                      return (
                        <Row justify={'space-between'}>
                          <Typography.Title
                            level={5}
                            style={{ color: 'white' }}
                          >
                            Programas e Atividades
                          </Typography.Title>
                          <Button
                            icon={<PlusOutlined />}
                            type={'primary'}
                            onClick={() => {
                              setShowModalProgramas(true);

                              scrollFieldToTop('tableChavesPrimarias');
                            }}
                            disabled={formDisabled}
                          />
                        </Row>
                      );
                    }}
                    columns={[
                      {
                        dataIndex: [
                          'tipoChavePrincipalVisitaPropriedadeRural',
                          'id',
                        ],
                        responsive: ['xs'],
                        render(_, chavePrimaria) {
                          return (
                            <Descriptions size={'small'} column={1} bordered>
                              <Descriptions.Item label={'Chave principal'}>
                                {
                                  chavePrimaria
                                    .tipoChavePrincipalVisitaPropriedadeRural
                                    .nome
                                }
                              </Descriptions.Item>
                              <Descriptions.Item label={'Motivo principal?'}>
                                <Switch
                                  disabled={formDisabled}
                                  defaultChecked={false}
                                  checked={
                                    chavePrimaria.motivoPrincipal === 'SIM'
                                      ? true
                                      : false
                                  }
                                  onChange={(value) =>
                                    onChangeSwitchMotivoPrincipal(
                                      value,
                                      chavePrimaria
                                    )
                                  }
                                />
                              </Descriptions.Item>
                              <Descriptions.Item label={'Chaves secundárias'}>
                                {chavePrimaria.listChaveSecundariaVisitaPropriedadeRural?.map(
                                  (chave) => (
                                    <Tooltip
                                      title={
                                        chave
                                          .tipoChaveSecundariaVisitaPropriedadeRural
                                          ?.nome
                                      }
                                    >
                                      <Tag
                                        style={{
                                          maxWidth: '160px',
                                          textOverflow: 'ellipsis',
                                          whiteSpace: 'nowrap',
                                          overflow: 'hidden',
                                        }}
                                        key={
                                          chave
                                            .tipoChaveSecundariaVisitaPropriedadeRural
                                            ?.id
                                        }
                                      >
                                        {
                                          chave
                                            .tipoChaveSecundariaVisitaPropriedadeRural
                                            ?.nome
                                        }
                                      </Tag>
                                    </Tooltip>
                                  )
                                )}
                              </Descriptions.Item>

                              <Descriptions.Item label='Ações'>
                                {chavePrimariaTableActions(chavePrimaria)}
                              </Descriptions.Item>
                            </Descriptions>
                          );
                        },
                      },
                      {
                        dataIndex: [
                          'tipoChavePrincipalVisitaPropriedadeRural',
                          'nome',
                        ],
                        title: 'Chave primária',
                        responsive: ['sm'],
                      },
                      {
                        dataIndex: 'motivoPrincipal',
                        title: 'Motivo Principal?',
                        responsive: ['sm'],
                        render(
                          motivoPrincipal: ChavePrincipalFVER.Input['motivoPrincipal'],
                          chave: ChavePrincipalFVER.Input
                        ) {
                          return (
                            <Switch
                              disabled={formDisabled}
                              checked={motivoPrincipal === 'SIM' ? true : false}
                              onChange={(value) =>
                                onChangeSwitchMotivoPrincipal(value, chave)
                              }
                            />
                          );
                        },
                      },

                      {
                        dataIndex: 'listChaveSecundariaVisitaPropriedadeRural',
                        title: 'Chaves secundárias',
                        responsive: ['sm'],
                        width: 550,
                        render(
                          chaves: FormFVERType['listChavePrincipalVisitaPropriedadeRural'][0]['listChaveSecundariaVisitaPropriedadeRural']
                        ) {
                          return chaves?.map((chave) => (
                            <Tag
                              key={
                                chave.tipoChaveSecundariaVisitaPropriedadeRural
                                  ?.id
                              }
                            >
                              {
                                chave.tipoChaveSecundariaVisitaPropriedadeRural
                                  ?.nome
                              }
                            </Tag>
                          ));
                        },
                      },
                      {
                        dataIndex: 'id',
                        title: 'Ações',
                        responsive: ['sm'],
                        width: 60,
                        render(id, chavePrimaria) {
                          return chavePrimariaTableActions(chavePrimaria);
                        },
                      },
                    ]}
                  />
                </Form.Item>
              </Col>

              <ServidoresPanel
                form={form}
                formDisabled={formDisabled}
                listaServidores={listaServidores}
                setListaServidores={setListaServidores}
              />

              <Divider orientation='left'>Assinatura do responsável</Divider>

              <Col xs={24} lg={24}>
                <Form.Item
                  label={'Como foi realizada a assinatura?'}
                  name={'statusAssinatura'}
                  valuePropName={'checked'}
                  rules={[{ required: true, message: 'O campo é obrigatório' }]}
                >
                  <Radio.Group
                    value={statusAssinaturaState}
                    defaultValue={form.getFieldValue('statusAssinatura')}
                    onChange={(e) => {
                      setStatusAssinaturaState(e.target.value);
                    }}
                  >
                    <Space direction='vertical'>
                      {listaStatusAssinatura?.map((statusAssinatura) => (
                        <Radio key={statusAssinatura} value={statusAssinatura}>
                          {StatusAssinatura.valueOf(statusAssinatura)}
                        </Radio>
                      ))}
                    </Space>
                  </Radio.Group>
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  validateTrigger={'onSubmit'}
                  name='assinatura'
                  rules={[
                    {
                      validator(_, value) {
                        if (
                          statusAssinaturaState ===
                          StatusAssinatura.keyOf(
                            StatusAssinatura.ASSINADO_DIGITALMENTE
                          )
                        ) {
                          if (!assinaturaBase64Image)
                            return Promise.reject(
                              new Error(
                                'Deve ser informada a assinatura do produtor'
                              )
                            );
                          return Promise.resolve();
                        } else {
                          return Promise.resolve();
                        }
                      },
                    },
                  ]}
                >
                  <SignatureWrapper
                    disabled={formDisabled}
                    onSaveSignature={() => {
                      form.validateFields(['assinatura']);
                    }}
                    onClearSignature={() => {
                      form.setFieldsValue({ assinatura: undefined });
                    }}
                  />
                </Form.Item>
              </Col>
            </Space>
          </Row>

          <Divider />

          <Row justify={'end'} gutter={24}>
            <Col xs={12} lg={6}>
              <Button
                style={{ width: '100%' }}
                onClick={() => {
                  if (formDisabled)
                    navigate(`${window.navigator.onLine ? '/visitas' : '/'}`);
                  else
                    confirm({
                      title: 'Deseja cancelar o cadastro?',
                      content:
                        'Ao confirmar, todos os dados digitados serão perdidos e você será redirecionado para a lista de Visitas',
                      onOk: () => navigate('/'),
                    });
                }}
              >
                {formDisabled ? 'Voltar' : 'Cancelar'}
              </Button>
            </Col>
            {!formDisabled && (
              <Col xs={12} lg={6}>
                <Button
                  style={{ width: '100%' }}
                  htmlType={'submit'}
                  type={'primary'}
                >
                  Salvar
                </Button>
              </Col>
            )}
            {formDisabled && (
              <Col xs={12} lg={6}>
                <Button
                  style={{ width: '100%' }}
                  htmlType={'submit'}
                  type={'primary'}
                  onClick={() => {
                    navigate(`/visitas/edicao/${params.id}`);
                  }}
                >
                  Editar
                </Button>
              </Col>
            )}
          </Row>
        </Form>
      </fieldset>

      <CustomModal
        centered
        open={showModalFinalizacao}
        width={750}
        footer={null}
        destroyOnClose
        closable={false}
        title={'FVER salvo com sucesso'}
      >
        <Card
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          bordered={false}
        >
          <Row
            gutter={24}
            style={{
              display: 'flex',
              flexDirection: 'row',
              textAlign: 'center',
            }}
          >
            {dadosFVERModalFinalizacao.problema_rede && (
              <Col xs={24} sm={24}>
                <Typography.Paragraph>
                  Houve um problema enquanto tentamos nos comunicar com o
                  servidor.
                </Typography.Paragraph>
              </Col>
            )}

            <Col xs={24} sm={24}>
              <Typography.Paragraph>
                {dadosFVERModalFinalizacao.local
                  ? 'O FVER foi salvo no dispositivo, e está disponível para visualização na tela Ações de campo.'
                  : 'O FVER foi salvo no servidor e não necessita sincronização.'}
              </Typography.Paragraph>
            </Col>

            <Col xs={24} sm={24}>
              <Typography.Paragraph style={{ fontWeight: 'bold' }}>
                {!dadosFVERModalFinalizacao.local
                  ? dadosFVERModalFinalizacao?.numero
                  : ''}
              </Typography.Paragraph>
            </Col>
          </Row>

          <Divider />

          <Row
            align='middle'
            style={{
              justifyContent: 'center',
            }}
            gutter={24}
          >
            <Col>
              <Link to={'/'}>
                <Button type={'primary'}> Voltar </Button>
              </Link>
            </Col>

            {!dadosFVERModalFinalizacao.local && (
              <Col>
                <Link
                  to={`/visitas/visualizar/${dadosFVERModalFinalizacao?.id}`}
                >
                  <Button type={'primary'}> Visualizar </Button>
                </Link>
              </Col>
            )}

            {dadosFVERModalFinalizacao?.fver?.tipoEstabelecimento ===
              'PROPRIEDADE' && (
              <Col>
                {dadosFVERModalFinalizacao.local && (
                  <Popover
                    open={
                      dadosFVERModalFinalizacao?.fver.tipoEstabelecimento !==
                      'PROPRIEDADE'
                    }
                  >
                    <Link
                      to={`/vigilancias/cadastro/${dadosFVERModalFinalizacao?.id}/local`}
                    >
                      <Button
                        type={'primary'}
                        disabled={
                          dadosFVERModalFinalizacao?.fver
                            .tipoEstabelecimento !== 'PROPRIEDADE'
                        }
                      >
                        Iniciar FVV
                      </Button>
                    </Link>
                  </Popover>
                )}

                {!dadosFVERModalFinalizacao.local && (
                  <Link
                    to={`/vigilancias/cadastro/${dadosFVERModalFinalizacao?.id}`}
                  >
                    <Button type={'primary'}> Iniciar FVV </Button>
                  </Link>
                )}
              </Col>
            )}
          </Row>
        </Card>
      </CustomModal>
    </>
  );
}
