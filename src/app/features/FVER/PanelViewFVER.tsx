import {
  Col,
  FormInstance,
  Row,
  Typography,
  Form,
  Input,
  Collapse,
} from 'antd';
import PropriedadePanel from '../../components/PropriedadePanel';

interface PanelViewFVERProps {
  form: FormInstance;
  formDisabled: boolean;
}

export default function PanelViewFVER(props: PanelViewFVERProps) {
  return (
    <>
      <Typography.Title level={5}>Dados do FVER</Typography.Title>

      <Row gutter={24} align={'bottom'}>
        <Col xs={0} lg={0}>
          <Form.Item
            label={'Id'}
            name={['visitaPropriedadeRural', 'id']}
            hidden
          >
            <Input disabled />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Número'}
            name={['visitaPropriedadeRural', 'numero']}
          >
            <Input disabled />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Tipo Estabelecimento'}
            name={['visitaPropriedadeRural', 'tipoEstabelecimento']}
          >
            {/* <div className='ant-form-item-control-input-content'>
              <Tag
                style={{
                  width: '100%',
                  minHeight: '24px',
                  color: 'rgba(0, 0, 0, 0.25)',
                  backgroundColor: '#f5f5f5',
                  borderColor: 'rgba(39, 64, 96, 0.15)',
                  boxShadow: 'none',
                  cursor: 'not-allowed',
                  opacity: '1',
                }}
              >
                {TipoEstabelecimento.valueOf(
                  props.form.getFieldValue([
                    'visitaPropriedadeRural',
                    'tipoEstabelecimento',
                  ])
                ).toString()}
              </Tag>
            </div> */}
            <Input disabled />
          </Form.Item>
        </Col>
        <Col xs={24} lg={8}>
          <Form.Item
            label={'Data da visita'}
            name={['visitaPropriedadeRural', 'dataDaVisita']}
          >
            <Input disabled />
          </Form.Item>
        </Col>

        <Col xs={24} lg={24}>
          <Form.Item
            label={'Estabelecimento'}
            name={['visitaPropriedadeRural', 'nomeEstabelecimento']}
          >
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>

      <Row>
        <Collapse style={{ width: '100%' }}>
          <Collapse.Panel key={'1'} header={'Dados do estabelecimento rural'}>
            <fieldset disabled={true}>
              <PropriedadePanel form={props.form} formDisabled={true} />
            </fieldset>
          </Collapse.Panel>
        </Collapse>
      </Row>
    </>
  );
}
