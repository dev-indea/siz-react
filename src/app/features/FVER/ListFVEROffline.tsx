import {
  CloseOutlined,
  CloudUploadOutlined,
  DeleteOutlined,
  EditTwoTone,
  ExclamationCircleTwoTone,
  EyeTwoTone,
} from '@ant-design/icons';
import {
  Alert,
  Button,
  Col,
  Descriptions,
  Modal,
  notification,
  Popconfirm,
  Row,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import { Link, useNavigate } from 'react-router-dom';
import syncFVERs from '../../../core/functions/syncFVERs';
import { FVER } from '../../../sdk/@types';
import { SYNC_VISITA_DONE } from '../../../sdk/@types/ServiceWorker.types';
import { ServiceIDBPayloadInput } from '../../../sdk/services/indexeddb/ServiceIDB';
import FVERIDBService from '../../../sdk/services/indexeddb/FVERIDB.service';
import syncFVVs from '../../../core/functions/syncFVVs';
import useSyncingState from '../../../core/hooks/useSyncingState';
import { TipoEstabelecimento } from '../../../core/enums/TipoEstabelecimento';
import FVVService from '../../../sdk/services/SIZ-API/FVV.service';
import FVVIDBService from '../../../sdk/services/indexeddb/FVVIDB.service';
import TroubleshootingButton from '../../components/TroubleshootingButton';
import FVVSvg from '../../components/FVVSvg';
import { CustomModal } from '../../components/CustomModal';
import useFVER from '../../../core/hooks/useFVER';

interface ListFVEROfflineProps {
  visible?: boolean;
}

export default function ListFVEROffline(props: ListFVEROfflineProps) {
  const [syncing, setSyncing] = useState(false);
  const [listaFVERs, setListaFVERs] = useState<ServiceIDBPayloadInput[]>();
  const navigate = useNavigate();
  const [error, setError] = useState<Error>();
  const { online } = useNavigatorStatus();
  const [fetching, setFetching] = useState(false);
  const { updateFVEROffline } = useFVER();
  const [syncFVERDone, setSyncFVERDone] = useState<string | undefined>(
    undefined
  );
  const [showModalMotivoErro, setShowModalMotivoErro] =
    useState<boolean>(false);
  const [showModalDeleteFVERs, setShowModalDeleteFVERs] =
    useState<boolean>(false);
  const [motivoErro, setMotivoErro] = useState<string>();

  const { fver, fvv, setFver, setFvv } = useSyncingState();

  if (navigator.serviceWorker)
    navigator.serviceWorker.onmessage = (event) => {
      if (event.data && event.data.type === SYNC_VISITA_DONE) {
        fetchFVERsOffline();
        setSyncFVERDone(event.data.message);
      }
    };

  const fixFVERsOffline = async (lista: ServiceIDBPayloadInput[]) => {
    let needFix = false;

    lista.forEach((fver) => {
      console.log('fver.payload.codgVerificador', fver.payload.codgVerificador);

      if (fver.payload.codgVerificador !== undefined) needFix = true;
    });

    console.log('needFix', needFix);
    if (needFix) {
      lista.forEach(async (fver) => {
        if (!fver.payload.codgVerificador) {
          if (fver.status !== 'SINCRONIZADO') {
            await updateFVEROffline(Number(fver.id), fver.payload);
          } else {
          }
        }
      });
    }

    return needFix;
  };

  const fetchFVERsOffline = useCallback(async () => {
    setFetching(true);

    /* await FVERIDBService.getAllFromInputTable().then(async (lista) => {
      await fixFVERsOffline(lista)
        .then((success: boolean) => {
          if (success)
            notification.info({
              message:
                'Foram encontrados erros nos FVERs e corrigidos automaticamente',
            });
        })
        .catch((e) =>
          notification.warn({
            message: 'Encontramos erros em alguns FVERs',
            description: 'Contate o administrador do sistema',
          })
        );
    }); */

    await FVERIDBService.getAllFromInputTable()
      .then(async (fvers) => {
        let quantidadeFVERsSincronizados = 0;
        fvers.forEach((fver) => {
          if (fver.status === 'SINCRONIZADO') quantidadeFVERsSincronizados++;
        });

        if (quantidadeFVERsSincronizados >= 10) setShowModalDeleteFVERs(true);

        setListaFVERs(fvers);
        setFetching(false);
      })
      .catch((e) => {
        if (e.code === 8) return;
        else {
          setError(e);
          throw e;
        }
      });
  }, []);

  const deleteFVER = useCallback(
    async (fver) => {
      setSyncing(true);
      setFver(true);
      setFvv(true);
      if (fver.id) {
        await FVERIDBService.delete(fver.id)
          .then(async () => {
            if (fver.status !== 'SINCRONIZADO') {
              await FVVIDBService.getByCodigoVerificador(
                fver.payload.codigoVerificador
              ).then(async (vigilancia) => {
                if (vigilancia)
                  //@ts-ignore
                  await FVVIDBService.delete(vigilancia.id);
                await fetchFVERsOffline();
              });
            }
          })
          .finally(() => {
            setSyncing(false);
            setFver(false);
            setFvv(false);
            navigate(`/acoes-de-campo`);
          });
      }
    },
    [fetchFVERsOffline, setFver, setFvv, navigate]
  );

  const deleteSynced = useCallback(async () => {
    const listFVERsSynced = listaFVERs?.filter(
      (fver) => fver.status === 'SINCRONIZADO'
    );

    const listFVERsNotSynced = listaFVERs?.filter(
      (fver) => fver.status !== 'SINCRONIZADO'
    );

    if (listFVERsSynced)
      for (const fver of listFVERsSynced) {
        //@ts-ignore
        await FVERIDBService.delete(fver.id);
      }

    setListaFVERs(listFVERsNotSynced);
    notification.success({
      message: 'Todas os FVERs sincronizados foram removidos',
    });
  }, [listaFVERs]);

  useEffect(() => {}, [syncFVERDone]);

  useEffect(() => {
    const fetchFVERsAsync = async () => {
      await fetchFVERsOffline();
    };
    fetchFVERsAsync();
  }, [fetchFVERsOffline]);

  useEffect(() => {
    setFetching(false);
  }, [listaFVERs]);

  useEffect(() => {}, [fver, fvv]);

  if (error) throw error;

  return (
    <>
      <Row>
        <Space size={4} direction='vertical' style={{ width: '100%' }}>
          <Col hidden={!syncFVERDone} xs={24}>
            <Alert
              message={syncFVERDone}
              type='info'
              showIcon
              closable={false}
            ></Alert>
          </Col>
          <Col span={24}>
            <Table
              dataSource={listaFVERs}
              size={'small'}
              rowKey={'id'}
              loading={fver}
              title={() => {
                return (
                  <Row justify={'space-between'}>
                    <Typography.Title level={5} style={{ color: 'white' }}>
                      FVERs
                    </Typography.Title>

                    <Space>
                      <Popconfirm
                        title={'Deseja remover todos os sincronizados??'}
                        onConfirm={deleteSynced}
                      >
                        <Button
                          icon={<DeleteOutlined />}
                          size={'small'}
                          type={'primary'}
                          danger
                        />
                      </Popconfirm>
                      <Popconfirm
                        title={'Confirma a sincronização?'}
                        disabled={
                          !listaFVERs || listaFVERs.length === 0 || !online
                        }
                        onConfirm={async () => {
                          setSyncing(true);
                          setFver(true);
                          setFvv(true);
                          await syncFVERs(true)
                            .then(async (message) => {
                              notification.success({
                                message: message,
                              });
                            })
                            .then(async () => {
                              await syncFVVs(true).finally(() => setFvv(false));
                            })
                            .then(async () => {
                              await fetchFVERsOffline();
                            })
                            .catch(async (e) => {
                              await fetchFVERsOffline();
                              notification.error({
                                message: 'Não foi possível sincronizar',
                                description: e,
                              });
                            })
                            .finally(() => {
                              setSyncing(false);
                              setFver(false);
                              setFvv(false);
                              navigate(`/acoes-de-campo`);
                            });
                        }}
                      >
                        <Button
                          icon={<CloudUploadOutlined />}
                          type={'primary'}
                          disabled={
                            !listaFVERs || listaFVERs.length === 0 || !online
                          }
                        />
                      </Popconfirm>

                      <TroubleshootingButton
                        component='FVER'
                        data={listaFVERs}
                      />
                    </Space>
                  </Row>
                );
              }}
              columns={[
                {
                  dataIndex: 'id',
                  title: 'FVERs',
                  responsive: ['xs'],
                  render(id: FVER.Summary['id'], fver) {
                    return (
                      <Descriptions size={'small'}>
                        <Descriptions.Item label={'Data'}>
                          {moment(fver.payload.dataDaVisita).format(
                            'DD/MM/YYYY HH:mm'
                          )}
                        </Descriptions.Item>
                        <Descriptions.Item label={'Tipo'}>
                          <Tag>
                            {TipoEstabelecimento.valueOf(
                              fver.payload.tipoEstabelecimento
                            ).toString()}
                          </Tag>
                        </Descriptions.Item>
                        <Descriptions.Item label={'Estabelecimento'}>
                          {fver.payload.tipoEstabelecimento ===
                            'ABATEDOURO' && (
                            <Tooltip
                              title={fver.payload.abatedouro.pessoa.nome}
                            >
                              {fver.payload.abatedouro.pessoa.nome}
                            </Tooltip>
                          )}
                          {fver.payload.tipoEstabelecimento ===
                            'PROPRIEDADE' && (
                            <Tooltip title={fver.payload.propriedade.nome}>
                              {fver.payload.propriedade.nome}
                            </Tooltip>
                          )}
                          {fver.payload.tipoEstabelecimento === 'RECINTO' && (
                            <Tooltip title={fver.payload.recinto.pessoa.nome}>
                              {fver.payload.recinto.pessoa.nome}
                            </Tooltip>
                          )}
                        </Descriptions.Item>
                        <Descriptions.Item label={'Resumo'}>
                          <Tooltip title={fver.payload.resumo}>
                            <Typography.Text
                              ellipsis={true}
                              style={{ maxWidth: '100%' }}
                            >
                              {fver.payload.resumo}
                            </Typography.Text>
                          </Tooltip>
                        </Descriptions.Item>

                        <Descriptions.Item label={'Ações'}>
                          <Space>
                            {fver.status !== 'SINCRONIZADO' && (
                              <Link to={`/visitas/edicao/${id}/${'local'}`}>
                                <Button
                                  icon={
                                    <EditTwoTone twoToneColor={'#84aee6'} />
                                  }
                                  size={'small'}
                                  type={'ghost'}
                                />
                              </Link>
                            )}

                            {fver.status !== 'SINCRONIZADO' && (
                              <Link
                                to={`/vigilancias/cadastro/${id}/${'local'}`}
                              >
                                <Button
                                  icon={<FVVSvg />}
                                  size={'small'}
                                  type={'ghost'}
                                />
                              </Link>
                            )}

                            <Button
                              icon={<DeleteOutlined />}
                              size={'small'}
                              type={'ghost'}
                              danger
                              onClick={() => {
                                Modal.confirm({
                                  content: (
                                    <>
                                      <Typography.Paragraph>
                                        Deseja proseguir?
                                      </Typography.Paragraph>
                                      <Typography.Paragraph>
                                        Caso haja um FVV não sincronizado e
                                        vinculado a este FVER, ele também será
                                        removido
                                      </Typography.Paragraph>
                                    </>
                                  ),
                                  onOk: async () => {
                                    try {
                                      await deleteFVER(fver);
                                      notification.success({
                                        message: 'FVER removido com sucesso',
                                      });
                                    } catch (e) {
                                      throw e;
                                    }
                                  },
                                });
                              }}
                            />
                          </Space>
                        </Descriptions.Item>
                      </Descriptions>
                    );
                  },
                } /* 
                  {
                    dataIndex: ['id'],
                    title: 'Id',
                    responsive: ['sm'],
                    width: 50,
                  }, */,
                {
                  dataIndex: ['payload', 'numero'],
                  title: 'Número',
                  width: 215,
                  responsive: ['xs', 'sm'],
                  render(numero, row) {
                    return (
                      <>
                        <Row gutter={4}>
                          <Col>
                            <span style={{ fontWeight: 'bold' }}>{numero}</span>
                          </Col>
                          <Col>
                            {numero && (
                              <Link
                                to={`/visitas/visualizar/${row.payload.id}`}
                              >
                                <Button
                                  icon={<EyeTwoTone twoToneColor={'#09f'} />}
                                  size={'small'}
                                  type={'ghost'}
                                />
                              </Link>
                            )}
                          </Col>
                        </Row>
                      </>
                    );
                  },
                },
                {
                  dataIndex: ['payload', 'dataDaVisita'],
                  title: 'Data da Visita',
                  responsive: ['sm'],
                  width: 145,
                  render(data: FVER.Summary['dataDaVisita']) {
                    return moment(data).format('DD/MM/YYYY HH:mm');
                  },
                },

                {
                  dataIndex: ['payload', 'tipoEstabelecimento'],
                  title: 'Tipo',
                  width: 150,
                  responsive: ['md'],
                  render(tipo: FVER.Summary['tipoEstabelecimento']) {
                    return (
                      <Tag>{TipoEstabelecimento.valueOf(tipo).toString()}</Tag>
                    );
                  },
                },
                {
                  dataIndex: ['payload', 'nomeEstabelecimento'],
                  title: 'Estabelecimento',
                  responsive: ['sm'],
                  render(nome: FVER.Summary['nomeEstabelecimento'], row) {
                    if (row.payload.tipoEstabelecimento === 'ABATEDOURO')
                      return (
                        <Tooltip title={row.payload.abatedouro.pessoa.nome}>
                          {row.payload.abatedouro.pessoa.nome}
                        </Tooltip>
                      );
                    else if (row.payload.tipoEstabelecimento === 'PROPRIEDADE')
                      return (
                        <Tooltip title={row.payload.propriedade.nome}>
                          {row.payload.propriedade.nome}
                        </Tooltip>
                      );
                    else if (row.payload.tipoEstabelecimento === 'RECINTO')
                      return (
                        <Tooltip title={row.payload.recinto.pessoa.nome}>
                          {row.payload.recinto.pessoa.nome}
                        </Tooltip>
                      );
                  },
                },
                {
                  dataIndex: ['status'],
                  title: 'Status',
                  width: 110,
                  responsive: ['md'],
                  render(status: ServiceIDBPayloadInput['status'], record) {
                    return (
                      <>
                        {status === 'NOVO' && (
                          <Tag
                            style={{
                              backgroundColor: '#09f',
                              color: 'white',
                            }}
                          >
                            {status}
                          </Tag>
                        )}{' '}
                        {status === 'SINCRONIZADO' && (
                          <Tag
                            style={{
                              backgroundColor: '#2baf57',
                              color: 'white',
                            }}
                          >
                            {status}
                          </Tag>
                        )}{' '}
                        {status === 'ERRO' && (
                          <Space style={{ textAlign: 'center' }}>
                            <Tag
                              style={{
                                backgroundColor: '#ff4e4e',
                                color: 'white',
                              }}
                            >
                              {status}
                            </Tag>
                            <Button
                              type='text'
                              danger
                              icon={
                                <ExclamationCircleTwoTone
                                  twoToneColor={'#d11b1b'}
                                />
                              }
                              onClick={() => {
                                setShowModalMotivoErro(true);
                                setMotivoErro(record.motivoErro);
                              }}
                            />
                          </Space>
                        )}
                      </>
                    );
                  },
                },
                {
                  dataIndex: 'id',
                  title: 'Ações',
                  responsive: ['sm'],
                  width: 100,
                  align: 'right',
                  render(id, fver) {
                    return (
                      <>
                        <Space>
                          {fver.status !== 'SINCRONIZADO' && (
                            <Link to={`/visitas/edicao/${id}/${'local'}`}>
                              <Button
                                icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                                size={'small'}
                                type={'ghost'}
                              />
                            </Link>
                          )}

                          {fver.status !== 'SINCRONIZADO' && (
                            <Button
                              icon={<FVVSvg />}
                              size={'small'}
                              type={'ghost'}
                              onClick={() => {
                                if (
                                  fver.payload.tipoEstabelecimento !==
                                  'PROPRIEDADE'
                                ) {
                                  notification.warn({
                                    message: `Não é possível incluir FVV para ${fver.payload.tipoEstabelecimento}`,
                                  });
                                  return;
                                }

                                if (fver.payload.id) {
                                  FVVService.getByFVER(fver.payload.id)
                                    .then((id) => {
                                      if (id)
                                        navigate(`/vigilancias/edicao/${id}`);
                                      else
                                        navigate(
                                          `/vigilancias/cadastro/${id}/${'local'}`
                                        );
                                    })
                                    .catch(setError);
                                }
                                //:TODO buscar fvv offline
                                else
                                  navigate(
                                    `/vigilancias/cadastro/${id}/${'local'}`
                                  );
                              }}
                            />
                          )}

                          <Button
                            icon={<DeleteOutlined />}
                            size={'small'}
                            type={'ghost'}
                            danger
                            onClick={() => {
                              Modal.confirm({
                                content: (
                                  <>
                                    <Typography.Paragraph>
                                      Deseja proseguir?
                                    </Typography.Paragraph>
                                    <Typography.Paragraph>
                                      Caso haja um FVV não sincronizado e
                                      vinculado a este FVER, ele também será
                                      removido
                                    </Typography.Paragraph>
                                  </>
                                ),
                                onOk: async () => {
                                  try {
                                    await deleteFVER(fver);
                                    notification.success({
                                      message: 'FVER removido com sucesso',
                                    });
                                  } catch (e) {
                                    throw e;
                                  }
                                },
                              });
                            }}
                          />
                        </Space>
                      </>
                    );
                  },
                },
              ]}
              expandable={{
                expandedRowRender: (record) => 'Tsest',
                defaultExpandAllRows: true,
                showExpandColumn: false,
              }}
            />
          </Col>
        </Space>
      </Row>

      <CustomModal
        open={showModalMotivoErro}
        title={'Motivo'}
        footer={
          <Button
            style={{ width: '100%' }}
            type='primary'
            icon={<CloseOutlined />}
            onClick={() => {
              setShowModalMotivoErro(false);
              setMotivoErro(undefined);
            }}
          >
            Fechar
          </Button>
        }
      >
        <Typography.Title level={3} style={{ textAlign: 'center' }}>
          {motivoErro}
        </Typography.Title>
      </CustomModal>

      <CustomModal
        open={showModalDeleteFVERs}
        title={'Aviso'}
        footer={
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Button
              style={{ width: '100%' }}
              type='default'
              icon={<CloseOutlined />}
              onClick={() => {
                setShowModalDeleteFVERs(false);
              }}
            >
              Fechar
            </Button>

            <Button
              style={{ width: '100%' }}
              type='primary'
              icon={<CloseOutlined />}
              onClick={async () => {
                await deleteSynced().finally(() =>
                  setShowModalDeleteFVERs(false)
                );
              }}
            >
              Remover
            </Button>
          </div>
        }
      >
        <Typography.Paragraph>
          Notamos que você ainda está mantendo no seu dispositivo muitos FVERs
          que já foram sincronizados.
        </Typography.Paragraph>

        <Typography.Paragraph>
          Deseja removê-los? Essa ação não irá afetar os FVERs no banco de
          dados.
        </Typography.Paragraph>
      </CustomModal>
    </>
  );
}
