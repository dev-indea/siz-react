import {
  Button,
  Col,
  Descriptions,
  Modal,
  notification,
  Popconfirm,
  Row,
  Skeleton,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import { useCallback, useEffect, useState } from 'react';
import moment from 'moment';
import {
  DeleteOutlined,
  EditTwoTone,
  PlusOutlined,
  SearchOutlined,
  SyncOutlined,
} from '@ant-design/icons';
import useFormVIN from '../../../core/hooks/useFormVIN';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { FormVIN } from '../../../sdk/@types';
import FormVINService from '../../../sdk/services/SIZ-API/FormVIN.service';
import { Link } from 'react-router-dom';
import { CustomModal } from '../../components/CustomModal';

export default function ListFormVINs() {
  const { query, formVINs, fetchFormVINs, setQuery } = useFormVIN();
  const [fetching, setFetching] = useState(true);
  const [deleting, setDeleting] = useState(false);
  const { loading, setLoading } = useLoadingPage();
  const { xs } = useBreakpoint();

  const [modalSummaryVisible, setModalSummaryVisible] = useState(false);
  const [formVINSummary, setFormVINSummary] = useState<FormVIN.Summary>();

  const [error, setError] = useState<Error>();

  useEffect(() => {
    setFetching(true);
  }, []);

  useEffect(() => {
    if (window.navigator.onLine) {
      setFetching(true);
      fetchFormVINs().catch((e) => {
        setError(e);
      });
    }
  }, [fetchFormVINs, query]);

  if (!window.navigator.onLine)
    throw new Error(
      'Não é possível visualizar a lista de Form VIN no modo offline'
    );
  if (error) throw error;

  useEffect(() => {
    if (formVINs) setFetching(false);
  }, [formVINs]);

  const deleteFormVIN = useCallback(async (a1) => {
    if (a1) {
      setDeleting(true);
      return await FormVINService.remove(a1)
        .then(() => {
          setDeleting(false);
          fetchFormVINs();
        })
        .catch(() => {
          setDeleting(false);
        });
    }
  }, []);

  return (
    <>
      <Row>
        <Col span={24}>
          {fetching && <Skeleton active />}

          {!fetching && (
            <>
              <Table<FormVIN.Summary>
                rowKey={'id'}
                dataSource={formVINs?.content}
                pagination={{
                  current: query.page ? query.page + 1 : 1,
                  onChange: (page, pageSize) => {
                    setQuery({ page: page - 1, size: pageSize });
                  },
                  total: formVINs?.totalElements,
                  pageSize: query.size,
                }}
                title={() => {
                  return (
                    <Row justify={'space-between'}>
                      <Typography.Title level={5} style={{ color: 'white' }}>
                        Form VINs
                      </Typography.Title>

                      <Space>
                        <Col>
                          <Link to={'/formvins/cadastro'}>
                            <Button
                              icon={<PlusOutlined />}
                              type={'primary'}
                              title={'Novo'}
                            />
                          </Link>
                        </Col>

                        <Col>
                          <Button
                            icon={<SyncOutlined spin={loading} />}
                            type={'primary'}
                            title={'Atualizar'}
                            onClick={async () => {
                              setLoading(true);
                              try {
                                await fetchFormVINs()
                                  .catch((e) => {
                                    setLoading(false);
                                    setError(e);
                                  })
                                  .finally(() => {
                                    setLoading(false);
                                  });
                              } finally {
                                setLoading(false);
                              }
                            }}
                          />
                        </Col>
                      </Space>
                    </Row>
                  );
                }}
                size={'small'}
                loading={deleting || fetching}
                columns={[
                  {
                    dataIndex: 'formINNumero',
                    title: 'Form IN',
                    width: 120,
                    responsive: ['xs', 'sm'],
                  },
                  {
                    dataIndex: 'numeroInspecao',
                    title: 'Inspeção',
                    width: 80,
                    responsive: ['xs', 'sm'],
                  },
                  {
                    dataIndex: 'dataInspecao',
                    title: 'Data',
                    width: 100,
                    responsive: ['xs', 'sm'],
                    render(data: FormVIN.Summary['dataInspecao']) {
                      return data
                        ? moment(new Date(data)).format('DD/MM/YYYY')
                        : undefined;
                    },
                  },
                  {
                    dataIndex: 'tipoEstabelecimento',
                    title: 'Tipo',
                    width: 110,
                    responsive: ['md'],
                    render(tipo: FormVIN.Summary['tipoEstabelecimento']) {
                      return <Tag>{tipo}</Tag>;
                    },
                  },
                  {
                    dataIndex: 'nomeEstabelecimento',
                    title: 'Nome',
                    responsive: ['sm'],
                    render(nome: FormVIN.Summary['nomeEstabelecimento']) {
                      return <Tooltip title={nome}>{nome}</Tooltip>;
                    },
                  },
                  {
                    dataIndex: 'id',
                    title: 'Ações',
                    responsive: ['xs', 'sm'],
                    width: 100,
                    align: 'right',
                    render(id: number) {
                      return (
                        <Space>
                          <Link to={`/formvins/edicao/${id}`}>
                            <Button
                              icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                              size={'small'}
                              type={'ghost'}
                            />
                          </Link>
                          <Link to={`/formvins/visualizar/${id}`}>
                            <Button
                              icon={<SearchOutlined />}
                              size={'small'}
                              type={'ghost'}
                            />
                          </Link>
                          <Popconfirm
                            title={'Deseja remover a formVIN?'}
                            onConfirm={() => {
                              deleteFormVIN(id)
                                ?.then((result) => {
                                  notification.success({
                                    message: 'FormVIN removida com sucesso',
                                  });
                                })
                                .catch((e) => {
                                  notification.success({
                                    message:
                                      'Ocorreu um erro ao remover o Form VIN',
                                    description: e.message,
                                  });
                                });
                            }}
                          >
                            <Button
                              icon={<DeleteOutlined />}
                              size={'small'}
                              type={'ghost'}
                              danger
                            />
                          </Popconfirm>
                        </Space>
                      );
                    },
                  },
                ]}
                expandable={{
                  expandedRowRender: (record) => (
                    <Typography
                      style={{
                        color: '#969494',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        display: '-webkit-box',
                        WebkitLineClamp: 2,
                        lineClamp: 2,
                        WebkitBoxOrient: 'vertical',
                      }}
                    >
                      {record.observacoes}
                    </Typography>
                  ),
                  defaultExpandAllRows: true,
                  showExpandColumn: false,
                }}
                onRow={(record, index) => {
                  return {
                    onClick: (e) => {
                      if (!xs) return;

                      setModalSummaryVisible(true);
                      setFormVINSummary(record);
                    },
                  };
                }}
              ></Table>
              <CustomModal
                centered
                open={modalSummaryVisible}
                title={`Form VIN nº ${formVINSummary?.formINNumero}/${
                  formVINSummary?.numeroInspecao
                    ? formVINSummary?.numeroInspecao
                    : 's/n'
                }`}
                closable={false}
                onCancel={() => {
                  setModalSummaryVisible(false);
                  setFormVINSummary(undefined);
                }}
                maskClosable={true}
                keyboard={true}
                footer={
                  <Button
                    type='primary'
                    onDoubleClick={() => {
                      setModalSummaryVisible(false);
                      setFormVINSummary(undefined);
                    }}
                  >
                    Fechar
                  </Button>
                }
              >
                {formVINSummary && (
                  <Descriptions size={'small'}>
                    <Descriptions.Item label={'Form IN'}>
                      {formVINSummary.formINNumero}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Número Inspeção'}>
                      {formVINSummary.numeroInspecao}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Data'}>
                      {moment(formVINSummary.dataInspecao).format('DD/MM/YYYY')}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Tipo'}>
                      <Tag>{formVINSummary.tipoEstabelecimento}</Tag>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Nome'}>
                      <Tooltip title={formVINSummary.nomeEstabelecimento}>
                        {formVINSummary.nomeEstabelecimento}
                      </Tooltip>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Observações'}>
                      <Tooltip title={formVINSummary.observacoes}>
                        <Typography.Text
                          ellipsis={true}
                          style={{ maxWidth: '100%' }}
                        >
                          {formVINSummary.observacoes}
                        </Typography.Text>
                      </Tooltip>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Ações'}>
                      <Space>
                        <Link to={`/formvins/edicao/${formVINSummary.id}`}>
                          <Button
                            icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                            size={'small'}
                            type={'ghost'}
                          />
                        </Link>
                        <Popconfirm
                          title={'Deseja remover a formVIN?'}
                          onConfirm={() => {
                            try {
                              deleteFormVIN(formVINSummary.id)?.then(
                                (result) => {
                                  notification.success({
                                    message: 'FormVIN removida com sucesso',
                                  });
                                }
                              );
                            } catch (e) {
                              throw e;
                            }
                          }}
                        >
                          <Button
                            icon={<DeleteOutlined />}
                            size={'small'}
                            type={'ghost'}
                            danger
                          />
                        </Popconfirm>
                      </Space>
                    </Descriptions.Item>
                  </Descriptions>
                )}
              </CustomModal>
            </>
          )}
        </Col>
      </Row>
    </>
  );
}
