import {
  CloudUploadOutlined,
  DeleteOutlined,
  EditTwoTone,
} from '@ant-design/icons';
import {
  Alert,
  Button,
  Col,
  Descriptions,
  notification,
  Popconfirm,
  Row,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import { Link, useNavigate } from 'react-router-dom';
import syncFormVINs from '../../../core/functions/syncFormVINs';
import { FormVIN } from '../../../sdk/@types';
import { SYNC_FORM_VIN_DONE } from '../../../sdk/@types/ServiceWorker.types';
import FormVINIDBService from '../../../sdk/services/indexeddb/FormVINIDB.service';
import { ServiceIDBPayload } from '../../../sdk/services/indexeddb/ServiceIDB';
import TroubleshootingButton from '../../components/TroubleshootingButton';

interface ListFormVINOfflineProps {
  visible?: boolean;
}
export default function ListFormVINOffline(props: ListFormVINOfflineProps) {
  const [syncing, setSyncing] = useState(false);
  const [listaFormVINs, setListaFormVINs] = useState<ServiceIDBPayload[]>();
  const navigate = useNavigate();
  const { online } = useNavigatorStatus();
  const [fetching, setFetching] = useState(false);
  const [syncFormVINDone, setSyncFormVINDone] = useState<string | undefined>(
    undefined
  );

  if (navigator.serviceWorker)
    navigator.serviceWorker.onmessage = (event) => {
      if (event.data && event.data.type === SYNC_FORM_VIN_DONE) {
        fetchLocalFormVINs();
        setSyncFormVINDone(event.data.message);
      }
    };

  const fetchLocalFormVINs = useCallback(async () => {
    setFetching(true);

    return await FormVINIDBService.getAll()
      .then((lista) => {
        setListaFormVINs(lista);
        setFetching(false);
      })
      .catch((e) => {
        if (e.code === 8) return;
        else throw e;
      });
  }, []);

  const deleteFormVIN = useCallback((a1) => {
    if (a1) FormVINIDBService.delete(a1).then(fetchLocalFormVINs);
  }, []);

  useEffect(() => {}, [syncFormVINDone]);

  useEffect(() => {
    fetchLocalFormVINs();
  }, [fetchLocalFormVINs]);

  useEffect(() => {
    setFetching(false);
  }, [listaFormVINs]);

  return (
    <>
      <Row>
        <Space size={4} direction='vertical' style={{ width: '100%' }}>
          <Col hidden={!syncFormVINDone} xs={24}>
            <Alert
              message={syncFormVINDone}
              type='info'
              showIcon
              closable={false}
            ></Alert>
          </Col>

          <Col span={24}>
            <Table
              dataSource={listaFormVINs}
              size={'small'}
              rowKey={'id'}
              loading={syncing}
              title={() => {
                return (
                  <Row justify={'space-between'}>
                    <Typography.Title level={5} style={{ color: 'white' }}>
                      Form VINs
                    </Typography.Title>
                    <Space>
                      <Popconfirm
                        title={'Confirma a sincronização?'}
                        disabled={
                          !listaFormVINs ||
                          listaFormVINs.length === 0 ||
                          !online
                        }
                        onConfirm={async () => {
                          setSyncing(true);
                          await syncFormVINs(true)
                            .then(async (message) => {
                              notification.success({
                                message: message,
                              });
                            })
                            .then(async () => {
                              navigate(`/`);
                              await fetchLocalFormVINs();
                            })
                            .catch((e) => {
                              notification.error({
                                message: 'Não foi possível sincronizar',
                                description: e.message,
                              });
                            })
                            .finally(() => {
                              setSyncing(false);
                            });
                        }}
                      >
                        <Button
                          icon={<CloudUploadOutlined />}
                          type={'primary'}
                          disabled={
                            !listaFormVINs ||
                            listaFormVINs.length === 0 ||
                            !online
                          }
                        />
                      </Popconfirm>
                    </Space>
                  </Row>
                );
              }}
              columns={[
                {
                  dataIndex: 'id',
                  title: 'FormVINs',
                  responsive: ['xs'],
                  render(id: FormVIN.Summary['id'], formVIN) {
                    return (
                      <Descriptions size={'small'}>
                        <Descriptions.Item label={'Form IN'}>
                          formVIN
                          {formVIN.payload.formIN.numero}
                        </Descriptions.Item>
                        <Descriptions.Item label={'Data'}>
                          {moment(formVIN.payload.dataInspecao).format(
                            'DD/MM/YYYY'
                          )}
                        </Descriptions.Item>
                        <Descriptions.Item label={'Tipo'}>
                          <Tag>{formVIN.payload.tipoEstabelecimento}</Tag>
                        </Descriptions.Item>
                        <Descriptions.Item label={'Estabelecimento'}>
                          <Tag>{formVIN.payload.nomeEstabelecimento}</Tag>
                        </Descriptions.Item>
                        <Descriptions.Item label={'Ações'}>
                          <Space>
                            <Link to={`/formvins/edicao/${id}/${'local'}`}>
                              <Button
                                icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                                size={'small'}
                                type={'ghost'}
                              />
                            </Link>

                            <Popconfirm
                              title={'Deseja remover o Form VIN?'}
                              onConfirm={() => {
                                try {
                                  deleteFormVIN(id);
                                  notification.success({
                                    message: 'Form VIN removido com sucesso',
                                  });
                                } catch (e) {
                                  throw e;
                                }
                              }}
                            >
                              <Button
                                icon={<DeleteOutlined />}
                                size={'small'}
                                type={'ghost'}
                                danger
                              />
                            </Popconfirm>
                          </Space>
                        </Descriptions.Item>
                      </Descriptions>
                    );
                  },
                },
                {
                  dataIndex: ['payload', 'formIN', 'numero'],
                  title: 'Form IN',
                  width: 120,
                  responsive: ['sm'],
                },
                {
                  dataIndex: ['payload', 'dataDaInspecao'],
                  title: 'Data',
                  width: 100,
                  responsive: ['sm'],
                  render(data: FormVIN.Summary['dataInspecao']) {
                    return moment(data).format('DD/MM/YYYY');
                  },
                },
                {
                  dataIndex: ['payload', 'tipoEstabelecimento'],
                  title: 'Tipo',
                  width: 110,
                  responsive: ['md'],
                  render(tipo: FormVIN.Summary['tipoEstabelecimento']) {
                    return <Tag>{tipo}</Tag>;
                  },
                },
                {
                  dataIndex: ['payload', 'nomeEstabelecimento'],
                  title: 'Estabelecimento',
                  responsive: ['sm'],
                  render(
                    nome: FormVIN.Summary['nomeEstabelecimento'],
                    formVIN
                  ) {
                    if (formVIN.payload.tipoEstabelecimento === 'ABATEDOURO')
                      return (
                        <Tooltip title={formVIN.payload.abatedouro.pessoa.nome}>
                          {formVIN.payload.abatedouro.pessoa.nome}
                        </Tooltip>
                      );
                    else if (
                      formVIN.payload.tipoEstabelecimento === 'PROPRIEDADE'
                    )
                      return (
                        <Tooltip title={formVIN.payload.propriedade.nome}>
                          {formVIN.payload.propriedade.nome}
                        </Tooltip>
                      );
                  },
                },
                {
                  dataIndex: 'id',
                  title: 'Ações',
                  responsive: ['sm'],
                  width: 60,
                  render(id, row) {
                    return (
                      <>
                        <Space>
                          <Link to={`/formvins/edicao/${id}/${'local'}`}>
                            <Button
                              icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                              size={'small'}
                              type={'ghost'}
                            />
                          </Link>

                          <Popconfirm
                            title={'Deseja remover o Form VIN?'}
                            onConfirm={() => {
                              try {
                                deleteFormVIN(id);
                                notification.success({
                                  message: 'Form VIN removida com sucesso',
                                });
                              } catch (e) {
                                throw e;
                              }
                            }}
                          >
                            <Button
                              icon={<DeleteOutlined />}
                              size={'small'}
                              type={'ghost'}
                              danger
                            />
                          </Popconfirm>
                        </Space>
                      </>
                    );
                  },
                },
              ]}
            />
          </Col>
        </Space>
      </Row>
    </>
  );
}
