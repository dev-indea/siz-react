import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons';
import {
  Button,
  Col,
  Divider,
  Form,
  Modal,
  notification,
  Row,
  Select,
  Space,
  Table,
  Tag,
  Typography,
} from 'antd';
import { useCallback, useEffect, useState } from 'react';
import useMunicipio from '../../../core/hooks/useMunicipio';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import usePropriedade from '../../../core/hooks/usePropriedade';
import { Municipio } from '../../../sdk/@types/Municipio';
import MunicipioIDBService from '../../../sdk/services/indexeddb/MunicipioIDB.service';
import moment, { Moment } from 'moment';
import { ServiceIDBPayload } from '../../../sdk/services/indexeddb/ServiceIDB';
import confirm from 'antd/lib/modal/confirm';
import { useForm } from 'antd/lib/form/Form';
import PropriedadeIDBService from '../../../sdk/services/indexeddb/PropriedadeIDB.service';
import useSetor from '../../../core/hooks/useSetor';
import SetorIDBService from '../../../sdk/services/indexeddb/SetorIDB.service';
import useAbatedouro from '../../../core/hooks/useAbatedouro';
import AbatedouroIDBService from '../../../sdk/services/indexeddb/AbatedouroIDB.service';
import useRecinto from '../../../core/hooks/useRecinto';
import RecintoIDBService from '../../../sdk/services/indexeddb/RecintoIDB.service';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import { SYNC_MUNICIPIO_DONE } from '../../../sdk/@types/ServiceWorker.types';
import { CustomModal } from '../../components/CustomModal';

type ForMunicipiosOfflineType = {
  municipio: Municipio.Detailed;
};
export default function MunicipiosOffline() {
  const [form] = useForm<ForMunicipiosOfflineType>();
  const [
    showModalCadastroMunicipioOffline,
    setShowModalCadastroMunicipioOffline,
  ] = useState(false);
  const { online } = useNavigatorStatus();

  const { setLoading } = useLoadingPage();

  const { listaMunicipio, fetchMunicipiosByUf } = useMunicipio();
  const { listaPropriedade, fetchPropriedadesByMunicipio } = usePropriedade();
  const { listaAbatedouros, fetchAbatedourosByMunicipio } = useAbatedouro();
  const { listaRecintos, fetchRecintosByMunicipio } = useRecinto();
  const { listaSetor, fetchSetoresByMunicipio } = useSetor();

  const [municipioSelecionado, setMunicipioSelecionado] =
    useState<Municipio.Detailed>();
  const [listMunicipios, setListMunicipios] = useState<ServiceIDBPayload[]>([]);

  const [ressincronizando, setRessincronizando] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);
  const [savingComplete, setSavingComplete] = useState<boolean>(false);

  const [fetchingSetor, setFetchingSetor] = useState<boolean>(false);
  const [fetchingPropriedade, setFetchingPropriedade] =
    useState<boolean>(false);
  const [fetchingAbatedouro, setFetchingAbatedouro] = useState<boolean>(false);
  const [fetchingRecinto, setFetchingRecinto] = useState<boolean>(false);

  const [erroNoFetchSetor, setErroNoFetchSetor] = useState<boolean>(false);
  const [erroNoFetchPropriedade, setErroNoFetchPropriedade] =
    useState<boolean>(false);
  const [erroNoFetchAbatedouro, setErroNoFetchAbatedouro] =
    useState<boolean>(false);
  const [erroNoFetchRecinto, setErroNoFetchRecinto] = useState<boolean>(false);

  const [error, setError] = useState<Error>();

  const handleSelectMunicipioChange = (value: any) => {
    setMunicipioSelecionado(JSON.parse(value));
  };

  const [dadosSincronizados, setDadosSincronizados] = useState<
    ServiceIDBPayload[]
  >([]);

  const populateDadosSyncVisita = useCallback(async () => {
    try {
      setDadosSincronizados([]);
      await MunicipioIDBService.getAll().then(setListMunicipios);
    } catch (e) {
      setError(
        new Error(
          'Erro ao buscar dados offline. Contate o administrador do sistema.'
        )
      );
    }
  }, []);

  useEffect(() => {
    populateDadosSyncVisita();

    fetchMunicipiosByUf('mt').catch((e) => {
      if (online) setError(new Error(e.message));
    });
  }, [populateDadosSyncVisita, fetchMunicipiosByUf]);

  useEffect(() => {
    if (ressincronizando) {
      form.setFieldsValue({
        municipio: municipioSelecionado,
      });
      setShowModalCadastroMunicipioOffline(true);
      sincronizarMunicipio();
    }
  }, [municipioSelecionado]);

  useEffect(() => {
    let helper = dadosSincronizados;
    listMunicipios.forEach((municipio) => {
      helper = helper.concat(municipio);
    });

    setDadosSincronizados(helper);
  }, [listMunicipios]);

  useEffect(() => {
    if (
      saving &&
      !fetchingPropriedade &&
      !fetchingSetor &&
      !fetchingAbatedouro &&
      !fetchingRecinto
    ) {
      setSavingComplete(true);

      if (!sincronizacaoPossuiErros()) {
        //@ts-ignore
        MunicipioIDBService.add(municipioSelecionado);
        populateDadosSyncVisita();
        notification.success({
          message: `Sincronização do município ${municipioSelecionado?.nome} concluída com sucesso`,
        });
      } else {
        notification.error({
          message: `Sincronização do município ${municipioSelecionado?.nome} foi finalizada com erros`,
          description: 'Um ou mais registros não foram salvos',
        });

        //@ts-ignore
        //        removerMunicipio(municipioSelecionado?.id);
      }
      navigator.serviceWorker?.controller?.postMessage(SYNC_MUNICIPIO_DONE);
    }
  }, [fetchingPropriedade, fetchingSetor, fetchingAbatedouro, fetchingRecinto]);

  useEffect(() => {
    try {
      if (listaPropriedade)
        PropriedadeIDBService.addAllPropriedade(listaPropriedade);
    } catch (error) {
    } finally {
      setFetchingPropriedade(false);
    }
  }, [listaPropriedade]);

  useEffect(() => {
    try {
      if (listaAbatedouros)
        AbatedouroIDBService.addAllAbatedouro(listaAbatedouros);
    } catch (error) {
    } finally {
      setFetchingAbatedouro(false);
    }
  }, [listaAbatedouros]);

  useEffect(() => {
    try {
      if (listaRecintos) RecintoIDBService.addAllRecinto(listaRecintos);
    } catch (error) {
    } finally {
      setFetchingRecinto(false);
    }
  }, [listaRecintos]);

  useEffect(() => {
    try {
      if (listaSetor) SetorIDBService.addAllSetor(listaSetor);
    } catch (error) {
    } finally {
      setFetchingSetor(false);
    }
  }, [listaSetor]);

  const sincronizacaoPossuiErros = () => {
    return (
      erroNoFetchAbatedouro ||
      erroNoFetchPropriedade ||
      erroNoFetchRecinto ||
      erroNoFetchSetor
    );
  };

  const removerMunicipios = useCallback(async () => {
    setLoading(true);

    return await Promise.all(
      dadosSincronizados.map(async (row) => {
        return await MunicipioIDBService.delete(row.payload.id);
      })
    )
      .then(() => {
        setDadosSincronizados([]);
        notification.success({ message: 'Dados removidos com sucesso' });
        setLoading(false);
      })
      .catch((e) => {
        notification.error({
          message:
            'Houve um erro ao remover os dados. Contate o administrador do sistema',
        });
        setLoading(false);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [dadosSincronizados]);

  const sincronizarMunicipio = useCallback(async () => {
    setSaving(true);
    setFetchingPropriedade(true);
    setFetchingAbatedouro(true);
    setFetchingRecinto(true);
    setFetchingSetor(true);

    if (municipioSelecionado && municipioSelecionado.codgIBGE)
      await MunicipioIDBService.delete(municipioSelecionado.codgIBGE);

    try {
      if (municipioSelecionado) {
        fetchPropriedades();
        fetchAbatedouros();
        fetchRecintos();
        fetchSetores();
      }
    } catch (e) {
      setErroNoFetchAbatedouro(true);
      setFetchingAbatedouro(false);
      setErroNoFetchPropriedade(true);
      setFetchingPropriedade(false);
      setErroNoFetchRecinto(true);
      setFetchingRecinto(false);
      setErroNoFetchSetor(true);
      setFetchingSetor(false);

      setSavingComplete(true);
    } finally {
    }
  }, [municipioSelecionado, ressincronizando]);

  const clearModal = () => {
    setFetchingPropriedade(false);
    setFetchingAbatedouro(false);
    setFetchingRecinto(false);
    setFetchingSetor(false);
    setErroNoFetchPropriedade(false);
    setErroNoFetchAbatedouro(false);
    setErroNoFetchRecinto(false);
    setErroNoFetchSetor(false);

    setSaving(false);
    setSavingComplete(false);
    setMunicipioSelecionado(undefined);
    setRessincronizando(false);
    form.setFieldsValue({
      municipio: undefined,
    });

    setShowModalCadastroMunicipioOffline(false);
  };

  const removerMunicipio = useCallback(
    async (idMunicipio: string) => {
      setLoading(true);
      MunicipioIDBService.delete(idMunicipio)
        .then(() => {
          const helper = dadosSincronizados;
          setDadosSincronizados(
            helper.filter((payload) => payload.payload.codgIBGE !== idMunicipio)
          );

          notification.success({ message: 'Dados removidos com sucesso' });
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
        })
        .finally(() => {
          setLoading(false);
        });
    },
    [dadosSincronizados, setLoading]
  );

  const fetchPropriedades = () => {
    try {
      fetchPropriedadesByMunicipio('' + municipioSelecionado?.codgIBGE).catch(
        (e) => {
          setErroNoFetchPropriedade(true);
          setFetchingPropriedade(false);
        }
      );
    } finally {
    }

    //throw new Error('ERRO DOIDO');
  };

  const fetchAbatedouros = () => {
    try {
      fetchAbatedourosByMunicipio('' + municipioSelecionado?.codgIBGE).catch(
        (e) => {
          setErroNoFetchAbatedouro(true);
          setFetchingAbatedouro(false);
        }
      );
    } finally {
    }
  };

  const fetchRecintos = () => {
    try {
      fetchRecintosByMunicipio('' + municipioSelecionado?.codgIBGE).catch(
        (e) => {
          setErroNoFetchRecinto(true);
          setFetchingRecinto(false);
        }
      );
    } finally {
    }
  };

  const fetchSetores = () => {
    try {
      fetchSetoresByMunicipio('' + municipioSelecionado?.codgIBGE, true).catch(
        (e) => {
          setErroNoFetchSetor(true);
          setFetchingSetor(false);
        }
      );
    } finally {
    }
  };

  if (error) throw error;

  return (
    <>
      <Row>
        <Col span={24}>
          <Table<ServiceIDBPayload>
            dataSource={dadosSincronizados}
            rowKey={'id'}
            size={'small'}
            title={() => {
              return (
                <Row justify={'space-between'}>
                  <Typography.Title level={5} style={{ color: 'white' }}>
                    Municípios
                  </Typography.Title>

                  {
                    <Col>
                      <Space>
                        <Button
                          icon={<DeleteOutlined />}
                          type={'primary'}
                          danger
                          title={'Remover todos'}
                          disabled={
                            !dadosSincronizados ||
                            dadosSincronizados.length === 0
                          }
                          onClick={() => {
                            confirm({
                              title:
                                'Deseja remover todos os dados dos municípios?',
                              onOk: () => {
                                removerMunicipios();
                              },
                            });
                          }}
                        />

                        <Button
                          icon={<PlusOutlined />}
                          type={'primary'}
                          title={`${
                            !online
                              ? 'Sem conexão com a internet. Não é possível sincronizar'
                              : error !== undefined
                              ? 'Erro ao tentar se conectar com o servidor. Não é possível sincronizar'
                              : 'Adicionar município para sincronizar'
                          }`}
                          disabled={!online || error !== undefined}
                          onClick={() => {
                            setShowModalCadastroMunicipioOffline(true);
                          }}
                        />
                      </Space>
                    </Col>
                  }
                </Row>
              );
            }}
            columns={[
              {
                dataIndex: ['payload', 'nome'],
                title: 'Nome',
                width: '50%',
              },
              {
                dataIndex: 'date',
                title: 'Data de sincronização',
                render(date: string) {
                  return moment(date).format('DD/MM/YYYY');
                },
              },

              {
                dataIndex: 'date',
                title: 'Status',
                render(date: string) {
                  const daysFromSync = moment(date).diff(
                    moment().toDate(),
                    'days'
                  );
                  return (
                    <Tag color={daysFromSync < -1 ? 'red' : 'green'}>
                      {daysFromSync < -1 ? 'EXPIRADO' : 'VÁLIDO'}
                    </Tag>
                  );
                },
              },

              {
                dataIndex: ['payload', 'id'],
                title: 'Ações',
                width: 100,
                render(id, row) {
                  return (
                    <>
                      <Button
                        icon={<DeleteOutlined />}
                        danger
                        title='Remover dados'
                        size='small'
                        type='ghost'
                        onClick={() => {
                          confirm({
                            title: `Deseja remover os dados do município ${row.payload.nome}`,
                            onOk: () => {
                              removerMunicipio(row.payload.codgIBGE);
                            },
                          });
                        }}
                      />
                      <Button
                        icon={<SyncOutlined />}
                        title='Atualizar'
                        size='small'
                        type='ghost'
                        onClick={() => {
                          setRessincronizando(true);
                          setMunicipioSelecionado(row.payload);
                        }}
                      />
                    </>
                  );
                },
              },
            ]}
          />
        </Col>
      </Row>

      <CustomModal
        centered
        open={showModalCadastroMunicipioOffline}
        title={'Escolha um município para sincronizar'}
        onCancel={() => {
          setShowModalCadastroMunicipioOffline(false);
        }}
        footer={null}
        destroyOnClose
        closable={false}
        maskClosable={false}
        keyboard={!saving}
      >
        <Form<ForMunicipiosOfflineType>
          layout='vertical'
          onFinish={sincronizarMunicipio}
          form={form}
        >
          <Row>
            <Col xs={24} lg={24}>
              <Form.Item
                label='Município'
                name={['municipio', 'nome']}
                rules={[{ required: true, message: 'O campo é obrigatório' }]}
              >
                <Select
                  style={{ width: '100%' }}
                  showSearch
                  placeholder='Selecione um município'
                  optionFilterProp='children'
                  onChange={handleSelectMunicipioChange}
                  disabled={saving}
                  filterOption={(input, option) => {
                    if (option && option.children)
                      return (
                        option.children
                          .toString()
                          .toLowerCase()
                          .normalize('NFD')
                          .replace(/[\u0300-\u036f]/g, '')
                          .indexOf(input.toLowerCase()) >= 0
                      );

                    return false;
                  }}
                >
                  {listaMunicipio?.map((municipio) => (
                    <Select.Option
                      key={municipio.codgIBGE}
                      value={JSON.stringify(municipio)}
                    >
                      {municipio.nome}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>

            {saving && (
              <>
                {!savingComplete && (
                  <div
                    style={{
                      backgroundColor: '#ee5757',
                      width: '100%',
                      marginBottom: '10px',
                    }}
                  >
                    <Row justify='space-around' align='middle'>
                      <Typography.Paragraph
                        style={{
                          color: 'white',
                          textAlign: 'center',
                        }}
                      >
                        Este processo pode levar algum tempo. Não feche o seu
                        navegador.
                      </Typography.Paragraph>
                      <Typography.Paragraph
                        style={{
                          color: 'white',
                          textAlign: 'center',
                        }}
                      >
                        Avisaremos quando estiver concluído
                      </Typography.Paragraph>
                    </Row>
                  </div>
                )}

                {savingComplete && !sincronizacaoPossuiErros() && (
                  <div
                    style={{
                      backgroundColor: '#68c555',
                      width: '100%',
                      marginBottom: '10px',
                    }}
                  >
                    <Row justify='space-around' align='middle'>
                      <Typography.Paragraph
                        style={{
                          color: 'white',
                          textAlign: 'center',
                        }}
                      >
                        Sincronização concluída com sucesso
                      </Typography.Paragraph>
                    </Row>
                  </div>
                )}

                {savingComplete && sincronizacaoPossuiErros() && (
                  <div
                    style={{
                      backgroundColor: '#ee5757',
                      width: '100%',
                      marginBottom: '10px',
                    }}
                  >
                    <Row justify='space-around' align='middle'>
                      <Typography.Paragraph
                        style={{
                          color: 'white',
                          textAlign: 'center',
                        }}
                      >
                        Sincronização finalizada com erros.
                        <br />
                        Não foi possível realizar o download dos dados. Contate
                        o administrador do sistema
                      </Typography.Paragraph>
                    </Row>
                  </div>
                )}

                <Col span={24}>
                  <Typography.Paragraph>
                    Setores . . .{' '}
                    {fetchingSetor && <SyncOutlined spin size={48} />}
                    {!fetchingSetor && !erroNoFetchSetor && (
                      <CheckCircleOutlined style={{ color: '#44b858' }} />
                    )}
                    {erroNoFetchSetor && (
                      <CloseCircleOutlined style={{ color: '#ff0000' }} />
                    )}
                  </Typography.Paragraph>

                  <Typography.Paragraph>
                    Propriedades . . .{' '}
                    {fetchingPropriedade && <SyncOutlined spin size={48} />}
                    {!fetchingPropriedade && !erroNoFetchPropriedade && (
                      <CheckCircleOutlined style={{ color: '#44b858' }} />
                    )}
                    {erroNoFetchPropriedade && (
                      <CloseCircleOutlined style={{ color: '#ff0000' }} />
                    )}
                  </Typography.Paragraph>

                  <Typography.Paragraph>
                    Abatedouros . . .{' '}
                    {fetchingAbatedouro && <SyncOutlined spin size={48} />}
                    {!fetchingAbatedouro && !erroNoFetchAbatedouro && (
                      <CheckCircleOutlined style={{ color: '#44b858' }} />
                    )}
                    {erroNoFetchAbatedouro && (
                      <CloseCircleOutlined style={{ color: '#ff0000' }} />
                    )}
                  </Typography.Paragraph>

                  <Typography.Paragraph>
                    Recintos . . .{' '}
                    {fetchingRecinto && <SyncOutlined spin size={48} />}
                    {!fetchingRecinto && !erroNoFetchRecinto && (
                      <CheckCircleOutlined style={{ color: '#44b858' }} />
                    )}
                    {erroNoFetchRecinto && (
                      <CloseCircleOutlined style={{ color: '#ff0000' }} />
                    )}
                  </Typography.Paragraph>
                </Col>
              </>
            )}
          </Row>

          <Divider />
          <Row gutter={24} justify={'end'}>
            {(!saving || savingComplete) && (
              <Col xs={12} lg={12}>
                <Button
                  style={{ width: '100%' }}
                  danger
                  icon={<CloseOutlined />}
                  onClick={clearModal}
                >
                  {savingComplete ? 'Fechar' : 'Cancelar'}
                </Button>
              </Col>
            )}

            {!saving && (
              <Col xs={12} lg={12}>
                <Button
                  style={{ width: '100%' }}
                  type={'primary'}
                  htmlType='submit'
                  icon={<CheckCircleOutlined />}
                  loading={saving}
                >
                  Salvar
                </Button>
              </Col>
            )}
          </Row>
        </Form>
      </CustomModal>
    </>
  );
}
