import {
  CheckCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons';
import {
  Alert,
  Button,
  Col,
  Divider,
  Form,
  notification,
  Progress,
  Row,
  Select,
  Space,
  Spin,
  Table,
  Tag,
  Typography,
} from 'antd';
import { useCallback, useEffect, useState } from 'react';
import useMunicipio from '../../../core/hooks/useMunicipio';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import { Municipio } from '../../../sdk/@types/Municipio';
import MunicipioIDBService from '../../../sdk/services/indexeddb/MunicipioIDB.service';
import moment from 'moment';
import { ServiceIDBPayload } from '../../../sdk/services/indexeddb/ServiceIDB';
import confirm from 'antd/lib/modal/confirm';
import { useForm } from 'antd/lib/form/Form';
import PropriedadeIDBService from '../../../sdk/services/indexeddb/PropriedadeIDB.service';
import SetorIDBService from '../../../sdk/services/indexeddb/SetorIDB.service';
import AbatedouroIDBService from '../../../sdk/services/indexeddb/AbatedouroIDB.service';
import RecintoIDBService from '../../../sdk/services/indexeddb/RecintoIDB.service';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import PropriedadeService from '../../../sdk/services/SIZ-API/Propriedade.service';
import { Abatedouro, Propriedade, Recinto, Setor } from '../../../sdk/@types';
import AbatedouroService from '../../../sdk/services/SIZ-API/Abatedouro.service';
import SetorService from '../../../sdk/services/SIZ-API/Setor.service';
import RecintoService from '../../../sdk/services/SIZ-API/Recinto.service';
import {
  SYNC_MUNICIPIO_DONE,
  SYNC_MUNICIPIO_DONE_WITH_ERROR,
} from '../../../sdk/@types/ServiceWorker.types';
import { CustomModal } from '../../components/CustomModal';
import scrollFieldToTop from '../../../core/functions/scrollFieldToTop';

type ForMunicipiosOfflineType = {
  municipio: Municipio.Detailed;
};
export default function MunicipiosOffline() {
  const [form] = useForm<ForMunicipiosOfflineType>();
  const [
    showModalCadastroMunicipioOffline,
    setShowModalCadastroMunicipioOffline,
  ] = useState(false);

  const [showDetailError, setShowDetailError] = useState(false);
  const { online } = useNavigatorStatus();

  const { setLoading } = useLoadingPage();

  const { listaMunicipio, fetchMunicipiosByUf } = useMunicipio();

  const [saving, setSaving] = useState<boolean>(false);
  const [savingComplete, setSavingComplete] = useState<boolean>(false);

  const [removendoMunicipio, setRemovendoMunicipio] = useState<boolean>(false);

  const [fetchingAbatedouro, setFetchingAbatedouro] = useState<boolean>(false);

  const [fetchingEstabelecimentoRural, setFetchingEstabelecimentoRural] =
    useState<boolean>(false);
  const [fetchingRecinto, setFetchingRecinto] = useState<boolean>(false);
  const [fetchingSetor, setFetchingSetor] = useState<boolean>(false);

  const [erroSalvandoAbatedouro, setErroSalvandoAbatedouro] =
    useState<boolean>(false);
  const [
    erroSalvandoEstabelecimentoRural,
    setErroSalvandoEstabelecimentoRural,
  ] = useState<boolean>(false);
  const [erroSalvandoRecinto, setErroSalvandoRecinto] =
    useState<boolean>(false);
  const [erroSalvandoSetor, setErroSalvandoSetor] = useState<boolean>(false);

  const [abatedouroSincronizado, setAbatedouroSincronizado] =
    useState<boolean>(false);
  const [
    estabelecimentoRuralSincronizado,
    setEstabelecimentoRuralSincronizado,
  ] = useState<boolean>(false);

  const [recintoSincronizado, setRecintoSincronizado] =
    useState<boolean>(false);
  const [setorSincronizado, setSetorSincronizado] = useState<boolean>(false);

  const [error, setError] = useState<Error>();

  const [municipioSelecionado, setMunicipioSelecionado] =
    useState<Municipio.Detailed>();
  const [municipiosSincronizados, setMunicipiosSincronizados] = useState<
    ServiceIDBPayload[]
  >([]);

  const handleSelectMunicipioChange = (value: any) => {
    setMunicipioSelecionado(JSON.parse(value));
  };

  const [totalAbatedouro, setTotalAbatedouro] = useState(0);
  const [totalEstabelecimentoRural, setTotalEstabelecimentoRural] = useState(0);
  const [totalRecinto, setTotalRecinto] = useState(0);
  const [totalSetor, setTotalSetor] = useState(0);

  const [contadorAbatedouro, setContadorAbatedouro] = useState(0);
  const [contadorEstabelecimentoRural, setContadorEstabelecimentoRural] =
    useState(0);
  const [contadorRecinto, setContadorRecinto] = useState(0);
  const [contadorSetor, setContadorSetor] = useState(0);

  const fetchMunicipiosSincronizados = useCallback(async () => {
    try {
      setMunicipiosSincronizados([]);
      await MunicipioIDBService.getAll().then(setMunicipiosSincronizados);
    } catch (e) {
      setError(
        new Error(
          'Erro ao buscar dados offline. Contate o administrador do sistema.'
        )
      );
    }
  }, []);

  const getPercentage = (atual: number, total: number): number => {
    if (removendoMunicipio) return 0;
    if (total === 0) return 100;
    return Number(((atual * 100) / total).toFixed(2));
  };

  const fetchAbatedouro = useCallback(async (codgIBGE: string) => {
    setFetchingAbatedouro(true);
    let erroSalvandoAbatedouroForProgress = false;
    await AbatedouroService.countByMunicipio(codgIBGE)
      .then(async (total) => {
        if (total === 0) {
          setAbatedouroSincronizado(true);
          setFetchingAbatedouro(false);
          return;
        }
        setTotalAbatedouro(total);

        await AbatedouroService.getByMunicipio(codgIBGE)
          .then(async (lista: Abatedouro.Summary[]) => {
            setFetchingAbatedouro(false);
            let contador = 1;

            await Promise.all([
              ...lista.map((abatedouro) =>
                AbatedouroIDBService.add(abatedouro).then(() => {
                  if (!erroSalvandoAbatedouroForProgress)
                    setContadorAbatedouro(contador++);
                })
              ),
            ]);
          })
          .catch((e) => {
            erroSalvandoAbatedouroForProgress = true;
            setErroSalvandoAbatedouro(true);
            setFetchingAbatedouro(false);
          });
      })
      .catch((e) => {
        setFetchingAbatedouro(false);
      });
  }, []);

  const fetchEstabelecimentoRural = useCallback(async (codgIBGE: string) => {
    setFetchingEstabelecimentoRural(true);
    await PropriedadeService.countByMunicipio(codgIBGE)
      .then(async (total) => {
        setTotalEstabelecimentoRural(total);
        let erroSalvandoEstabelecimentoRuralForProgress = false;
        await PropriedadeService.getByMunicipio(codgIBGE)
          .then(async (lista: Propriedade.Summary[]) => {
            setFetchingEstabelecimentoRural(false);
            let contador = 1;

            await Promise.all([
              ...lista.map((estabelecimentoRural) =>
                PropriedadeIDBService.add(estabelecimentoRural).then(() => {
                  if (!erroSalvandoEstabelecimentoRuralForProgress)
                    setContadorEstabelecimentoRural(contador++);
                })
              ),
            ]);
          })
          .catch((e) => {
            erroSalvandoEstabelecimentoRuralForProgress = true;
            setErroSalvandoEstabelecimentoRural(true);
            setFetchingEstabelecimentoRural(false);
            throw e;
          });
      })
      .catch((e) => {
        setFetchingEstabelecimentoRural(false);

        throw e;
      });
  }, []);

  const fetchRecinto = useCallback(async (codgIBGE: string) => {
    setFetchingRecinto(true);
    let erroSalvandoRecintoForProgress = false;
    await RecintoService.countByMunicipio(codgIBGE)
      .then(async (total) => {
        if (total === 0) {
          setRecintoSincronizado(true);
          setFetchingRecinto(false);
          return;
        }
        setTotalRecinto(total);

        await RecintoService.getByMunicipio(codgIBGE)
          .then(async (lista: Recinto.Summary[]) => {
            setFetchingRecinto(false);
            let contador = 1;

            await Promise.all([
              ...lista.map((recinto) =>
                RecintoIDBService.add(recinto).then(() => {
                  if (!erroSalvandoRecintoForProgress)
                    setContadorRecinto(contador++);
                })
              ),
            ]);
          })
          .catch((e) => {
            erroSalvandoRecintoForProgress = true;
            setErroSalvandoRecinto(true);
            setFetchingRecinto(false);
          });
      })
      .catch((e) => {
        setFetchingRecinto(false);
      });
  }, []);

  const fetchSetor = useCallback(async (codgIBGE: string) => {
    setFetchingSetor(true);
    let erroSalvandoSetorForProgress = false;
    await SetorService.countByMunicipio(codgIBGE)
      .then(async (total) => {
        if (total === 0) {
          setSetorSincronizado(true);
          setFetchingSetor(false);
          return;
        }
        setTotalSetor(total);

        await SetorService.getByMunicipio(codgIBGE)
          .then(async (lista: Setor.Summary[]) => {
            setFetchingSetor(false);
            let contador = 1;

            await Promise.all([
              ...lista.map((setor) =>
                SetorIDBService.add(setor).then(() => {
                  if (!erroSalvandoSetorForProgress)
                    setContadorSetor(contador++);
                })
              ),
            ]);
          })
          .catch((e) => {
            erroSalvandoSetorForProgress = true;
            setErroSalvandoSetor(true);
            setFetchingSetor(false);
          });
      })
      .catch((e) => {
        setFetchingSetor(false);
      });
  }, []);

  useEffect(() => {}, [
    contadorAbatedouro,
    erroSalvandoAbatedouro,
    fetchingAbatedouro,
    totalAbatedouro,
    abatedouroSincronizado,
  ]);

  useEffect(() => {}, [
    contadorEstabelecimentoRural,
    erroSalvandoEstabelecimentoRural,
    fetchingEstabelecimentoRural,
    totalEstabelecimentoRural,
    estabelecimentoRuralSincronizado,
  ]);

  useEffect(() => {}, [
    contadorRecinto,
    erroSalvandoRecinto,
    fetchingRecinto,
    totalRecinto,
    recintoSincronizado,
  ]);

  useEffect(() => {}, [
    contadorSetor,
    erroSalvandoSetor,
    fetchingSetor,
    totalSetor,
    setorSincronizado,
  ]);

  useEffect(() => {}, [
    contadorAbatedouro,
    erroSalvandoAbatedouro,
    fetchingAbatedouro,
    totalAbatedouro,
    abatedouroSincronizado,
  ]);

  useEffect(() => {}, [saving, savingComplete]);

  useEffect(() => {
    fetchMunicipiosSincronizados();
  }, [fetchMunicipiosSincronizados]);

  useEffect(() => {
    fetchMunicipiosByUf('mt').catch((e) => {
      setError(new Error(e.message));
    });
  }, [fetchMunicipiosByUf]);

  const limparDadosDeControle = () => {
    setShowModalCadastroMunicipioOffline(false);
    setSaving(false);
    setSavingComplete(false);
    setRemovendoMunicipio(false);
    setMunicipioSelecionado(undefined);
    form.setFieldValue('municipio', undefined);

    setContadorAbatedouro(0);
    setErroSalvandoAbatedouro(false);
    setFetchingAbatedouro(false);
    setTotalAbatedouro(0);
    setAbatedouroSincronizado(false);

    setContadorEstabelecimentoRural(0);
    setErroSalvandoEstabelecimentoRural(false);
    setFetchingEstabelecimentoRural(false);
    setTotalEstabelecimentoRural(0);
    setEstabelecimentoRuralSincronizado(false);

    setContadorRecinto(0);
    setErroSalvandoRecinto(false);
    setFetchingRecinto(false);
    setTotalRecinto(0);
    setRecintoSincronizado(false);

    setContadorSetor(0);
    setErroSalvandoSetor(false);
    setFetchingSetor(false);
    setTotalSetor(0);
    setSetorSincronizado(false);
  };

  const sincronizarMunicipio = useCallback(async () => {
    setSaving(true);

    if (municipioSelecionado && municipioSelecionado?.codgIBGE) {
      setRemovendoMunicipio(true);
      await MunicipioIDBService.delete(municipioSelecionado?.codgIBGE)
        .then(() => setRemovendoMunicipio(false))
        .catch((e) => {
          setRemovendoMunicipio(false);
          throw e;
        });
      await MunicipioIDBService.add(municipioSelecionado);

      await Promise.all([
        fetchAbatedouro(municipioSelecionado?.codgIBGE).then(() => {
          setAbatedouroSincronizado(true);
        }),
        fetchEstabelecimentoRural(municipioSelecionado?.codgIBGE).then(() => {
          setEstabelecimentoRuralSincronizado(true);
        }),
        fetchRecinto(municipioSelecionado?.codgIBGE).then(() => {
          setRecintoSincronizado(true);
        }),
        fetchSetor(municipioSelecionado?.codgIBGE).then(() => {
          setSetorSincronizado(true);
        }),
      ])
        .then(async () => {
          setSavingComplete(true);
          fetchMunicipiosSincronizados();
          navigator.serviceWorker?.controller?.postMessage(SYNC_MUNICIPIO_DONE);
        })
        .catch(async (e) => {
          setSavingComplete(true);
          setError(e);
          if (municipioSelecionado?.codgIBGE)
            MunicipioIDBService.delete(municipioSelecionado?.codgIBGE);
          navigator.serviceWorker?.controller?.postMessage(
            SYNC_MUNICIPIO_DONE_WITH_ERROR
          );
        });
    } else {
      notification.warn({
        message: 'Nenhum município selecionado!',
      });
    }
  }, [
    fetchAbatedouro,
    fetchEstabelecimentoRural,
    fetchMunicipiosSincronizados,
    fetchRecinto,
    fetchSetor,
    municipioSelecionado,
  ]);

  const removerMunicipios = useCallback(async () => {
    setLoading(true);

    MunicipioIDBService.deleteAll()
      .then(() => {
        setMunicipiosSincronizados([]);
        notification.success({ message: 'Dados removidos com sucesso' });
        setLoading(false);
      })
      .catch((e) => {
        notification.error({
          message:
            'Houve um erro ao remover os dados. Contate o administrador do sistema',
        });
        setLoading(false);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [setLoading]);

  const removerMunicipio = useCallback(
    async (idMunicipio: string) => {
      setLoading(true);
      MunicipioIDBService.delete(idMunicipio)
        .then(() => {
          const helper = municipiosSincronizados;
          setMunicipiosSincronizados(
            helper.filter((payload) => payload.payload.codgIBGE !== idMunicipio)
          );

          notification.success({ message: 'Dados removidos com sucesso' });
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
        })
        .finally(() => {
          setLoading(false);
        });
    },
    [municipiosSincronizados, setLoading]
  );

  return (
    <>
      <Row>
        <Col span={24}>
          <Table<ServiceIDBPayload>
            dataSource={municipiosSincronizados}
            rowKey={'id'}
            size={'small'}
            title={() => {
              return (
                <Row justify={'space-between'}>
                  <Typography.Title level={5} style={{ color: 'white' }}>
                    Municípios
                  </Typography.Title>

                  <Col>
                    <Space>
                      <Button
                        icon={<DeleteOutlined />}
                        type={'primary'}
                        danger
                        title={'Remover todos'}
                        disabled={
                          !municipiosSincronizados ||
                          municipiosSincronizados.length === 0
                        }
                        onClick={() => {
                          confirm({
                            title: 'Deseja remover todos os municípios?',
                            onOk: () => {
                              removerMunicipios();
                            },
                          });
                        }}
                      />

                      <Button
                        icon={<PlusOutlined />}
                        type={'primary'}
                        title={`${
                          !online
                            ? 'Sem conexão com a internet. Não é possível sincronizar'
                            : 'Adicionar'
                        }`}
                        disabled={!online}
                        onClick={() => {
                          setShowModalCadastroMunicipioOffline(true);
                        }}
                      />
                    </Space>
                  </Col>
                </Row>
              );
            }}
            columns={[
              {
                dataIndex: ['payload', 'nome'],
                title: 'Nome',
                width: '50%',
              },
              {
                dataIndex: 'date',
                title: 'Data de sincronização',
                render(date: string) {
                  return moment(date).format('DD/MM/YYYY');
                },
              },

              {
                dataIndex: 'date',
                title: 'Status',
                render(date: string) {
                  const daysFromSync = moment(date).diff(
                    moment().toDate(),
                    'days'
                  );
                  return (
                    <Tag color={daysFromSync < -1 ? 'red' : 'green'}>
                      {daysFromSync < -1 ? 'EXPIRADO' : 'VÁLIDO'}
                    </Tag>
                  );
                },
              },

              {
                dataIndex: ['payload', 'id'],
                title: 'Ações',
                width: 100,
                render(id, row) {
                  return (
                    <>
                      <Button
                        icon={<DeleteOutlined />}
                        danger
                        title='Remover dados'
                        size='small'
                        type='ghost'
                        onClick={() => {
                          confirm({
                            title: `Deseja remover os dados do município ${row.payload.nome}`,
                            onOk: () => {
                              removerMunicipio(row.payload.codgIBGE);
                            },
                          });
                        }}
                      />
                      <Button
                        icon={<SyncOutlined />}
                        title='Atualizar'
                        size='small'
                        type='ghost'
                        onClick={() => {
                          //:todo
                        }}
                      />
                    </>
                  );
                },
              },
            ]}
          />
        </Col>
      </Row>

      <CustomModal
        centered
        open={showModalCadastroMunicipioOffline}
        title={
          saving
            ? `Sincronizando ${municipioSelecionado?.nome}`
            : 'Escolha um município para sincronizar'
        }
        onCancel={() => {
          setShowModalCadastroMunicipioOffline(false);
        }}
        footer={null}
        destroyOnClose
        closable={false}
        maskClosable={false}
        keyboard={!saving}
      >
        <Form<ForMunicipiosOfflineType>
          layout='vertical'
          onFinish={sincronizarMunicipio}
          form={form}
        >
          <Row>
            {!saving && (
              <Col xs={24} lg={24}>
                <Form.Item
                  label='Município'
                  name={['municipio', 'nome']}
                  rules={[{ required: true, message: 'O campo é obrigatório' }]}
                >
                  <Select
                    autoFocus
                    showAction={['focus']}
                    style={{ width: '100%' }}
                    showSearch
                    placeholder='Selecione um município'
                    optionFilterProp='children'
                    onChange={handleSelectMunicipioChange}
                    disabled={saving}
                    onClick={() => {
                      scrollFieldToTop('municipio_nome');
                    }}
                    filterOption={(input, option) => {
                      if (option && option.children)
                        return (
                          option.children
                            .toString()
                            .toLowerCase()
                            .normalize('NFD')
                            .replace(/[\u0300-\u036f]/g, '')
                            .indexOf(input.toLowerCase()) >= 0
                        );

                      return false;
                    }}
                  >
                    {listaMunicipio?.map((municipio) => (
                      <Select.Option
                        key={municipio.codgIBGE}
                        value={JSON.stringify(municipio)}
                      >
                        {municipio.nome}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
            )}
          </Row>

          {(erroSalvandoAbatedouro ||
            erroSalvandoEstabelecimentoRural ||
            erroSalvandoRecinto ||
            erroSalvandoSetor) && (
            <Alert
              message={
                <span style={{ fontWeight: 'bold' }}>
                  Não foi possível finalizar a sincronização.
                </span>
              }
              description={showDetailError ? error?.message : null}
              type='error'
              action={
                <Button
                  size='small'
                  danger
                  onClick={() => {
                    if (showDetailError) setShowDetailError(false);
                    else setShowDetailError(true);
                  }}
                >
                  {!showDetailError ? 'Detalhes' : 'Fechar'}
                </Button>
              }
            />
          )}

          {saving && (
            <Spin
              spinning={removendoMunicipio}
              size='small'
              tip={'Removendo dados desatualizados...'}
              style={{
                width: '100%',
              }}
            >
              <>
                <Typography.Text>Abatedouros</Typography.Text>
                <Spin
                  spinning={fetchingAbatedouro}
                  size='small'
                  tip={'Buscando...'}
                  style={{
                    width: '100%',
                  }}
                >
                  <Progress
                    percent={
                      fetchingAbatedouro
                        ? 0
                        : getPercentage(contadorAbatedouro, totalAbatedouro)
                    }
                    status={
                      erroSalvandoAbatedouro
                        ? 'exception'
                        : abatedouroSincronizado
                        ? 'success'
                        : 'active'
                    }
                  />
                </Spin>
              </>

              <>
                <Typography.Text>Estabelecimentos Rurais</Typography.Text>
                <Spin
                  spinning={fetchingEstabelecimentoRural}
                  size='small'
                  tip={'Buscando...'}
                  style={{
                    width: '100%',
                  }}
                >
                  <Progress
                    percent={
                      fetchingEstabelecimentoRural
                        ? 0
                        : getPercentage(
                            contadorEstabelecimentoRural,
                            totalEstabelecimentoRural
                          )
                    }
                    status={
                      erroSalvandoEstabelecimentoRural
                        ? 'exception'
                        : estabelecimentoRuralSincronizado
                        ? 'success'
                        : 'active'
                    }
                  />
                </Spin>
              </>

              <>
                <Typography.Text>Recintos</Typography.Text>
                <Spin
                  spinning={fetchingRecinto}
                  size='small'
                  tip={'Buscando...'}
                  style={{
                    width: '100%',
                  }}
                >
                  <Progress
                    percent={
                      fetchingRecinto
                        ? 0
                        : getPercentage(contadorRecinto, totalRecinto)
                    }
                    status={
                      erroSalvandoRecinto
                        ? 'exception'
                        : setorSincronizado
                        ? 'success'
                        : 'active'
                    }
                  />
                </Spin>
              </>

              <>
                <Typography.Text>Setores</Typography.Text>
                <Spin
                  spinning={fetchingSetor}
                  size='small'
                  tip={'Buscando...'}
                  style={{
                    width: '100%',
                  }}
                >
                  <Progress
                    percent={
                      fetchingSetor
                        ? 0
                        : getPercentage(contadorSetor, totalSetor)
                    }
                    status={
                      erroSalvandoSetor
                        ? 'exception'
                        : setorSincronizado
                        ? 'success'
                        : 'active'
                    }
                  />
                </Spin>
              </>
            </Spin>
          )}

          <Divider />
          <Row gutter={24} justify={'end'}>
            {(!saving || savingComplete) && (
              <Col xs={12} lg={12}>
                <Button
                  style={{ width: '100%' }}
                  danger
                  icon={<CloseOutlined />}
                  onClick={() => {
                    limparDadosDeControle();
                    setShowModalCadastroMunicipioOffline(false);
                  }}
                >
                  {savingComplete ? 'Fechar' : 'Cancelar'}
                </Button>
              </Col>
            )}

            {!saving && (
              <Col xs={12} lg={12}>
                <Button
                  style={{ width: '100%' }}
                  type={'primary'}
                  htmlType='submit'
                  icon={<CheckCircleOutlined />}
                  loading={saving}
                >
                  Sincronizar
                </Button>
              </Col>
            )}
          </Row>
        </Form>
      </CustomModal>
    </>
  );
}
