import {
  Button,
  Col,
  Divider,
  Modal,
  notification,
  Row,
  Spin,
  Typography,
} from 'antd';
import { useEffect, useState } from 'react';
import usePageTitle from '../../../core/hooks/usePageTitle';
import AuthorizationService from '../../../core/auth/Authorization.service';
import {
  initializeIndexedDB_v1,
  InitializerStatus,
  persistOfflineGlobalData,
} from '../../../sdk/services/indexeddb/Initializer';
import { useNavigate } from 'react-router-dom';
import { QuestionCircleOutlined, UserOutlined } from '@ant-design/icons';
import useOfflinePersistState from '../../../core/hooks/useOfflinePersistState';
import useBadgeState from '../../../core/hooks/useBadgeState';

import FVERIDBService from '../../../sdk/services/indexeddb/FVERIDB.service';
import FormVINIDBService from '../../../sdk/services/indexeddb/FormVINIDB.service';
import FVVIDBService from '../../../sdk/services/indexeddb/FVVIDB.service';
import ErrorDisplay from '../../components/ErrorDisplay';
import { CustomModal } from '../../components/CustomModal';

export default function Home() {
  usePageTitle('SIZ - Início');

  const { done, setDone, setSyncing } = useOfflinePersistState();
  const { setQuantidadeAcoesNaoSincronizadas } = useBadgeState();
  const [showModalInfo, setShowModalInfo] = useState(false);
  const [modalInfo, setModalInfo] = useState<{
    title: string;
    info: string;
    to: string;
  }>();

  const [fetching, setFetching] = useState(false);
  const navigate = useNavigate();

  const [screen, setScreen] = useState<'HOME' | 'FORM_INVESTIGACAO'>('HOME');

  if ('virtualKeyboard' in navigator) {
    //@ts-ignore
    navigator.virtualKeyboard.overlaysContent = true;
  }

  useEffect(() => {
    const initDatabse = async () => {
      setFetching(true);

      setSyncing(true);
      await initializeIndexedDB_v1()
        .then(async () => {
          await persistOfflineGlobalData()
            .then((result) => {
              const statusSincronizacao: InitializerStatus = result[0];
              if (statusSincronizacao === InitializerStatus.ERROR)
                notification.error({
                  message:
                    'Houve um erro durante a sincronização de dados globais',
                });

              if (statusSincronizacao === InitializerStatus.UPDATED)
                notification.success({
                  message:
                    'Sincronização de dados globais realizada com sucesso',
                });

              setDone(true);
              setSyncing(false);
              setFetching(false);
            })
            .finally(() => {
              setFetching(false);
              setDone(true);
              setSyncing(false);
            })
            .catch((e) => {
              notification.error({
                message: 'Encontramos um erro ao atualizar os dados',
                description: 'Contate o administrador do sistema.',
              });
              setFetching(false);
              setDone(true);
              setSyncing(false);
            });
        })
        .finally(() => {
          setFetching(false);
          setDone(true);
          setSyncing(false);
        })
        .catch((e) => {
          setFetching(false);
          setDone(true);
          setSyncing(false);
        });
    };

    if (!fetching) initDatabse();
  }, []);

  useEffect(() => {
    const countOfflineInputData = async () => {
      let totalFVER = 0;
      let totalFVV = 0;
      let totalFormVIN = 0;
      //@ts-ignore
      await FVERIDBService.count().then((value) => (totalFVER = value));
      //@ts-ignore
      await FVVIDBService.count().then((value: any) => (totalFVV = value));
      //@ts-ignore
      await FormVINIDBService.count().then(
        (value: any) => (totalFormVIN = value)
      );
      setQuantidadeAcoesNaoSincronizadas(totalFVER + totalFVV + totalFormVIN);
    };

    if (done) {
      countOfflineInputData();
    }
  });

  useEffect(() => {}, [fetching]);

  useEffect(() => {}, [screen]);
  interface BotaoProps {
    to: string;
    title: string;
    info: string;
    icon?: React.ReactNode;
    emDesemvolvimento?: boolean;
    external?: boolean;
    onClick?: () => any;
  }

  const Botao: React.FC<BotaoProps> = (props) => {
    return (
      <div
        style={{
          width: '100%',
          height: '100px',
          position: 'relative',
        }}
      >
        <div
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            zIndex: 1,
            padding: '5px',
          }}
        >
          <QuestionCircleOutlined
            //@ts-ignore
            style={{
              fontSize: '24px',
              color: '#ffffff',
              padding: '5px',
            }}
            onClick={() => {
              setShowModalInfo(true);
              setModalInfo({
                ...props,
              });
            }}
          />
        </div>
        <div style={{ width: '100%', height: '100%' }}>
          <Button
            type='primary'
            style={{ width: '100%', height: '100px' }}
            icon={props.icon}
            onClick={(e) => {
              if (props.onClick) {
                props.onClick();
                e.preventDefault();
                return;
              }

              if (props.emDesemvolvimento) {
                Modal.info({
                  content: (
                    <>
                      <Typography.Title
                        level={3}
                        style={{ textAlign: 'center' }}
                      >
                        Em desenvolvimento
                      </Typography.Title>
                      <Divider style={{ marginBottom: '-15px' }} />
                    </>
                  ),
                  icon: null,
                });
              } else {
                if (props.external) {
                  //@ts-ignore
                  window.open(props.to, '_blank').focus();
                } else {
                  navigate(props.to);
                }
              }
              e.preventDefault();
            }}
          >
            {props.title}
          </Button>
        </div>
      </div>
    );
  };

  return (
    <>
      <Spin
        size='large'
        spinning={fetching}
        tip={'Estamos atualizando os dados do app'}
        style={{
          position: 'fixed',
          top: '20%',
          zIndex: '1',
        }}
      >
        {screen === 'HOME' && (
          <Row gutter={[8, 8]}>
            <Col md={8} xs={24}>
              {Botao({
                to: `/visitas/cadastro${
                  !window.navigator.onLine ? '/local' : ''
                }`,
                title: 'Novo FVER',
                info: 'Neste botão você preencherá o Formulário de Visita a Estabelecimento Rural e, na sequência, o Formulário de Vigilância Veterinária (caso seja veterinário).',
              })}
            </Col>
            <Col md={8} xs={24}>
              {Botao({
                to: '/visitas',
                title: 'Novo FVV',
                info: 'Neste botão você preencherá o Formulário de Vigilância Veterinária a partir de uma visita realizada anteriormente. Não é possível inserir o Formulário de Vigilância Veterinária sem haver, antes, um Formulário de Visita a Estabelecimento Rural.',
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: '#',
                title: 'Atualização de dados cadastrais',
                info: 'Neste botão você realizará a atualização cadastral de estabelecimento rural, exploração pecuária, produtor rural e espécies animais (exceto bovino e bubalino).',
                emDesemvolvimento: true,
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: '/',
                title: 'Formulários de investigação',
                info: 'Neste botão você terá acesso aos formulários oficiais para atendimento à suspeita de doença de notificação obrigatória.',
                onClick: () => {
                  setScreen('FORM_INVESTIGACAO');
                },
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: `http://cdsa:${encodeURIComponent(
                  '$4Iei1b7#SXc'
                )}@pbi.mti.mt.gov.br/Reports/browse/INDEA/ANIMAL`,
                title: 'Gestão de metas',
                info: 'Neste botão você terá acesso ao andamento das metas de sua unidade podendo acompanhar mais de perto o seu progresso.',
                external: true,
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: '#',
                title: 'Manuais técnicos',
                info: 'Neste botão você terá acesso, para consulta, aos manuais de procedimentos publicados pela Coordenadoria de Defesa Sanitária Animal, MAP e OIE.',
                emDesemvolvimento: true,
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: 'https://linktr.ee/educaindea',
                title: 'Educação Sanitária',
                info: 'Neste botão você terá acesso ao repositório de Educação Sanitária.',
                external: true,
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: '/offline',
                title: 'Sincronizar dados',
                info: 'Neste botão você poderá armazenar localmente (offline) os dados de produtores rurais, estabelecimentos rurais e explorações pecuárias. O funcionamento offline depende do envio e sincronização desses dados.',
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: '/acoes-de-campo',
                title: 'Ações de campo',
                info: 'Neste botão você fará o envio, para o SIZ WEB, dos dados inseridos de forma offline. Somente após este envio a ação de campo será registrada.',
              })}
            </Col>

            <Col md={8} xs={24}>
              {Botao({
                to: '/usuario',
                title:
                  '' +
                  AuthorizationService.getUsuarioXVia()?.name.split(' ')[0],
                info: 'Neste botão você poderá visualizar o seu perfil',
                icon: (
                  <UserOutlined style={{ color: 'white', fontSize: '20px' }} />
                ),
              })}
            </Col>
          </Row>
        )}

        {screen === 'FORM_INVESTIGACAO' && (
          <>
            <Row gutter={[8, 8]}>
              <Col md={8} xs={24}>
                {Botao({
                  to: '/formvins/cadastro',
                  title: 'Novo FormVIN',
                  info: 'Novo FormVIN',
                })}
              </Col>
            </Row>

            <br />
            <Row>
              <Col xs={24}>
                <Button
                  type='primary'
                  style={{ width: '100%' }}
                  onClick={() => setScreen('HOME')}
                >
                  Voltar
                </Button>
              </Col>
            </Row>
          </>
        )}
      </Spin>

      <CustomModal
        open={false}
        closable={false}
        footer={
          <Button
            type='primary'
            style={{ width: '100%', height: '60px' }}
            onClick={() => {
              window.location.reload();
            }}
          >
            Tentar novamente
          </Button>
        }
      >
        <div
          style={{
            padding: 24,
            border: '1px solid ',
            textAlign: 'center',
          }}
        >
          <ErrorDisplay
            title='Não conseguimos conectar com o servidor. Tente novamente mais tarde'
            message=' '
          />
        </div>
      </CustomModal>

      <CustomModal
        open={showModalInfo}
        closable={false}
        bodyStyle={{ padding: 0 }}
        title={modalInfo?.title}
        footer={
          <Row gutter={12}>
            <Col span={12}>
              <Button
                type='default'
                style={{ width: '100%' }}
                onClick={() => {
                  setShowModalInfo(false);
                }}
              >
                Fechar
              </Button>
            </Col>
            <Col span={12}>
              <Button
                type='primary'
                style={{ width: '100%' }}
                onClick={() => {
                  setShowModalInfo(false);
                  if (modalInfo?.to) navigate(modalInfo?.to);
                }}
              >
                {modalInfo?.to && modalInfo?.to !== '#' && 'Ir'}
                {(!modalInfo?.to || modalInfo?.to === '#') &&
                  'Em desenvolvimento'}
              </Button>
            </Col>
          </Row>
        }
      >
        <Typography.Paragraph style={{ textAlign: 'justify' }}>
          {modalInfo?.info}
        </Typography.Paragraph>
      </CustomModal>
    </>
  );
}
