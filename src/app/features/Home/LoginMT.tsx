import { LoginOutlined, SearchOutlined } from '@ant-design/icons';
import {
  Button,
  Col,
  Collapse,
  Form,
  Image,
  Input,
  Row,
  Space,
  Typography,
} from 'antd';
import { useCallback } from 'react';
import AuthorizationService from '../../../core/auth/Authorization.service';
import { useNavigate } from 'react-router-dom';

export default function LoginMT() {
  const navigate = useNavigate();

  const handleFormSubmit = useCallback(
    (formValues) => {
      navigate(`/visualizarFVER/${formValues.codgVerificador}`);
    },
    [navigate]
  );

  return (
    <>
      <div className={'loginPanel'}>
        <Row align={'middle'} style={{ textAlign: 'center', width: '100%' }}>
          <Space size={8} direction='vertical' style={{ width: '100%' }}>
            <Col xs={24}>
              <Image width={128} src='/SIZ-128x128.png' preview={false} />
            </Col>

            <Col xs={24}>
              <Typography.Title level={1}>SIZ</Typography.Title>
            </Col>
            <Col xs={24}>
              <Typography.Title level={5} style={{ marginTop: '-25px' }}>
                Sistema de Informações Zoossanitárias
              </Typography.Title>
            </Col>

            <Col xs={24}>
              <Button
                type='primary'
                style={{ width: '100%' }}
                icon={<LoginOutlined />}
                onClick={() => {
                  window.location.replace(AuthorizationService.URL_LOGIN);
                }}
              >
                Entrar
              </Button>
            </Col>
            <Col>
              <Collapse
                defaultActiveKey={'0'}
                style={{ width: '100%', backgroundColor: '#e0e4e8' }}
              >
                <Collapse.Panel
                  header={
                    <>
                      <SearchOutlined />
                      <span style={{ fontWeight: 'bold' }}>
                        {' '}
                        Consultar Visita
                      </span>
                    </>
                  }
                  key={1}
                  showArrow={false}
                >
                  <Form
                    layout={'vertical'}
                    size={'small'}
                    onFinish={handleFormSubmit}
                  >
                    <Form.Item
                      label={'Código de autenticação'}
                      name={'codgVerificador'}
                      rules={[
                        {
                          required: true,
                          message: 'O campo é obrigatório',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Button
                      type='primary'
                      style={{ width: '100%' }}
                      htmlType='submit'
                      icon={<SearchOutlined />}
                    >
                      Consultar
                    </Button>
                  </Form>
                </Collapse.Panel>
              </Collapse>
            </Col>
          </Space>
        </Row>
      </div>
    </>
  );
}
