import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import FVERService from '../../../sdk/services/SIZ-API/FVER.service';
import { FVER } from '../../../sdk/@types';
import {
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Spin,
  Typography,
} from 'antd';
import generateHash from '../../../core/functions/generateHash';
import FVVIDBService from '../../../sdk/services/indexeddb/FVVIDB.service';
import FVERIDBService from '../../../sdk/services/indexeddb/FVERIDB.service';

export default function AdminPanel() {
  const [
    listFVERsWithNullCodgVerificador,
    setListFVERsWithNullCodgVerificador,
  ] = useState<FVER.Detailed[]>();

  const [loading, setLoading] = useState(false);

  const fetchFVERsWithNullCodgVerificador = useCallback(async () => {
    const dataCadastro = moment().set('month', 4).set('date', 15);
    return await FVERService.getByDataCadastro(
      dataCadastro.format('DD/MM/YYYY')
    );
  }, []);

  const fixFVERsWithNullCodgVerificador = useCallback(async () => {
    if (
      listFVERsWithNullCodgVerificador &&
      listFVERsWithNullCodgVerificador.length > 0
    ) {
      let fverFixed: FVER.Input;
      return listFVERsWithNullCodgVerificador.forEach((fver) => {
        return FVERService.getById(fver.id).then((fverInput) => {
          fverFixed = {
            ...fverInput,
            codigoVerificador: generateHash(fverInput),
          };
          return FVERService.add(fverFixed);
        });
      });
    }
  }, [listFVERsWithNullCodgVerificador]);

  useEffect(() => {
    setLoading(true);
    fetchFVERsWithNullCodgVerificador().then((lista) => {
      setLoading(false);
      setListFVERsWithNullCodgVerificador(lista);
    });
  }, [fetchFVERsWithNullCodgVerificador]);

  useEffect(() => {}, [loading, listFVERsWithNullCodgVerificador]);

  const inserirJson = (formValues: any) => {
    if (formValues.fvv) {
      const fvvs: any[] = JSON.parse(formValues.fvv);
      if (fvvs) {
        fvvs.forEach((e) => {
          FVVIDBService.insert(e.payload);
        });
      }
    }

    if (formValues.fver) {
      const fvers: any[] = JSON.parse(formValues.fver);
      if (fvers) {
        fvers.forEach((e) => {
          FVERIDBService.insert(e.payload);
        });
      }
    }
  };

  return (
    <>
      <Spin
        size='large'
        spinning={loading}
        style={{
          position: 'fixed',
          top: '20%',
          zIndex: '1',
        }}
      >
        <Typography.Title level={3}>Painel de Administração</Typography.Title>
        <Divider />

        {listFVERsWithNullCodgVerificador && (
          <Card title={'FVERs'}>
            <Row gutter={12} align='middle' justify='space-between'>
              <Typography.Text>
                Encontrados {listFVERsWithNullCodgVerificador.length} FVERs sem
                código verificador
              </Typography.Text>
              <Button
                type='primary'
                onClick={() => {
                  setLoading(true);
                  fixFVERsWithNullCodgVerificador()
                    .then(() => {
                      setLoading(false);
                    })
                    .catch((e) => {
                      console.log('ERRO FIX', e);
                      setLoading(false);
                    });
                }}
              >
                Corrigir
              </Button>
            </Row>
          </Card>
        )}
      </Spin>
      <br />

      <Card title={'Inserir JSON'}>
        <Form size='small' layout='vertical' onFinish={inserirJson}>
          <Row gutter={12}>
            <Col span={24}>
              <Form.Item label={'FVV'} name={'fvv'}>
                <Input.TextArea />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item label={'FVER'} name={'fver'}>
                <Input.TextArea />
              </Form.Item>
            </Col>
          </Row>

          <Row justify={'end'} gutter={12}>
            <Button type='primary' htmlType='submit'>
              Inserir
            </Button>
          </Row>
        </Form>
      </Card>
    </>
  );
}
