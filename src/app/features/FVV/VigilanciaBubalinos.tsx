import {
  Card,
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Row,
  Switch,
  Tooltip,
  Typography,
} from 'antd';
import SintomasObservadosPanel from '../../components/SintomasObservadosPanel';
import { SintomasObservados } from '../../../core/enums/SintomasObservados';

type VigilanciaBubalinosProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaBubalinos(props: VigilanciaBubalinosProps) {
  const triggerRecountTotaExaminadosBubalinos = () => {
    const _00_12_Femeas: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados00_04_F',
    ]);
    const _00_12_Machos: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados00_04_M',
    ]);
    const _05_12_Femeas: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados05_12_F',
    ]);
    const _05_12_Machos: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados05_12_M',
    ]);
    const _13_24_Femeas: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados13_24_F',
    ]);
    const _13_24_Machos: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados13_24_M',
    ]);
    const _25_36_Femeas: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados25_36_F',
    ]);
    const _25_36_Machos: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionados25_36_M',
    ]);
    const _36_AcimaFemeas: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionadosAcima36_F',
    ]);
    const _36_AcimaMachos: number = props.form.getFieldValue([
      'vigilanciaBubalino',
      'quantidadeInspecionadosAcima36_M',
    ]);

    const dados = [
      _00_12_Femeas,
      _00_12_Machos,
      _05_12_Femeas,
      _05_12_Machos,
      _13_24_Femeas,
      _13_24_Machos,
      _25_36_Femeas,
      _25_36_Machos,
      _36_AcimaFemeas,
      _36_AcimaMachos,
    ];

    let totalExaminados: number = 0;

    dados.forEach((qtdade) => {
      if (!isNaN(Number(qtdade))) {
        totalExaminados = Number(totalExaminados) + Number(qtdade);

        props.form.setFieldValue(
          ['vigilanciaBubalino', 'quantidadeExaminados'],
          totalExaminados
        );
      }
    });
  };

  return (
    <>
      <Typography.Title level={5}>Bubalinos examinados</Typography.Title>

      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='00-04'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados00_04_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados00_04_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='05-12'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados05_12_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados05_12_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='13-24'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados13_24_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados13_24_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='25-36'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados25_36_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionados25_36_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Acima de 36'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionadosAcima36_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaBubalino',
                    'quantidadeInspecionadosAcima36_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosBubalinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Total'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Examinados'}
                  name={['vigilanciaBubalino', 'quantidadeExaminados']}
                >
                  <Input disabled />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Tooltip
                  title={
                    'Preencher com todos os animais observados, inclusive os examinados'
                  }
                >
                  <Form.Item
                    label={'Vistoriados'}
                    name={['vigilanciaBubalino', 'quantidadeVistoriados']}
                  >
                    <Input
                      inputMode='numeric'
                      maxLength={4}
                      autoComplete='off'
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Tooltip>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row gutter={24}>
        <SintomasObservadosPanel
          listaSintomasObservados={SintomasObservados.bufalos()}
          pathToCoordenada={'vigilanciaBubalino'}
          form={props.form}
        />

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há suspeita clínica?'}
            name={['vigilanciaBubalino', 'haSuspeitaClinica']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={16}>
          <Form.Item
            label={'Suspeita clinica'}
            name={['vigilanciaBubalino', 'suspeitaClinica']}
          >
            <Input autoComplete='off' />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}
