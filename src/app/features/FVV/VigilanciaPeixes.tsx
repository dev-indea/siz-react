import {
  CheckCircleOutlined,
  CloseOutlined,
  DeleteOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import {
  Button,
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Modal,
  notification,
  Popconfirm,
  Row,
  Select,
  Switch,
  Table,
  Tooltip,
  Typography,
} from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useCallback, useEffect, useState } from 'react';
import { Especie } from '../../../core/enums/Especie';
import { VigilanciaPeixesEspecie } from '../../../sdk/@types/VigilanciaPeixesEspecie';
import SintomasObservadosPanel from '../../components/SintomasObservadosPanel';
import { SintomasObservados } from '../../../core/enums/SintomasObservados';
import { CustomModal } from '../../components/CustomModal';

type VigilanciaPeixesProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaPeixesPanel(props: VigilanciaPeixesProps) {
  const { xs } = useBreakpoint();

  const listaEspeciePeixes = Especie.peixes();

  const [dataSourceVigilanciaPeixes, setDatasourceVigilanciaPeixes] = useState<
    VigilanciaPeixesEspecie.Input[]
  >([]);

  const [showModalVigilanciaPeixes, setShowModalVigilanciaPeixes] =
    useState<boolean>(false);

  useEffect(() => {
    setDatasourceVigilanciaPeixes(
      props.form.getFieldValue(['vigilanciaPeixes', 'vigilanciaPeixesEspecies'])
    );
  }, [props.form]);

  const clearModalVigilanciaPeixes = useCallback(() => {
    props.form.setFieldsValue({ modalVigilanciaPeixes: undefined });
  }, [props.form]);

  const vigilanciaPeixesTableActions = (
    vigilanciaPeixesToBeRemoved: VigilanciaPeixesEspecie.Input
  ) => {
    return (
      <Row align={'middle'} justify={xs ? 'start' : 'center'} gutter={12}>
        <Popconfirm
          disabled={props.formDisabled}
          title={'Deseja remover a espécie?'}
          onConfirm={() => {
            setDatasourceVigilanciaPeixes(
              dataSourceVigilanciaPeixes.filter(
                (vigilanciaPeixes) =>
                  vigilanciaPeixes.especie !==
                  vigilanciaPeixesToBeRemoved.especie
              )
            );
            notification.success({
              message: `Espécie ${Especie.valueOf(
                vigilanciaPeixesToBeRemoved.especie
              )} removida`,
            });
          }}
        >
          <Button
            icon={<DeleteOutlined />}
            danger
            type={'ghost'}
            title={'Remover espécie'}
            disabled={props.formDisabled}
          />
        </Popconfirm>
      </Row>
    );
  };

  const handleAddVigilanciaPeixes = useCallback(() => {
    const investigacaoEpidemiologica_modal: VigilanciaPeixesEspecie.Input =
      props.form.getFieldValue('modalVigilanciaPeixes');

    let newDataSource;
    if (!dataSourceVigilanciaPeixes) {
      newDataSource = [investigacaoEpidemiologica_modal];
    } else {
      newDataSource = dataSourceVigilanciaPeixes.concat(
        investigacaoEpidemiologica_modal
      );
    }

    setDatasourceVigilanciaPeixes(newDataSource);
    props.form.setFieldsValue({
      vigilanciaPeixes: { vigilanciaPeixesEspecies: newDataSource },
    });

    setShowModalVigilanciaPeixes(false);
    clearModalVigilanciaPeixes();
    notification.success({
      message: 'Vigilância de peixes incluída com sucesso',
    });
  }, [dataSourceVigilanciaPeixes]);
  return (
    <>
      <Col xs={24} sm={24}>
        <Form.Item
          validateTrigger={'onSubmit'}
          name='tableInvestigacaoEpidemiologicas'
        >
          <Table<VigilanciaPeixesEspecie.Input>
            size={'small'}
            dataSource={dataSourceVigilanciaPeixes}
            rowKey={(record) => record.especie}
            title={() => {
              return (
                <Row justify={'space-between'}>
                  <Typography.Title level={5} style={{ color: 'white' }}>
                    Vigilância de Peixes
                  </Typography.Title>
                  <Button
                    icon={<PlusOutlined />}
                    type={'primary'}
                    onClick={() => {
                      setShowModalVigilanciaPeixes(true);
                    }}
                    disabled={props.formDisabled}
                  />
                </Row>
              );
            }}
            columns={[
              {
                dataIndex: 'especie',
                title: 'Epécie',
                responsive: ['sm'],
                render(especie: string) {
                  return Especie.valueOf(especie);
                },
              },

              {
                dataIndex: 'quantidadeVistoriados',
                title: 'Vistoriados',
                responsive: ['sm'],
              },

              {
                dataIndex: 'quantidadeInspecionados',
                title: 'Inspecionados',
                responsive: ['sm'],
              },

              {
                dataIndex: 'id',
                title: 'Ações',
                responsive: ['sm'],
                width: 60,
                render(id, vigilanciaPeixes) {
                  return vigilanciaPeixesTableActions(vigilanciaPeixes);
                },
              },
            ]}
          />
        </Form.Item>
      </Col>

      <Divider />
      <Row gutter={24}>
        <SintomasObservadosPanel
          listaSintomasObservados={SintomasObservados.peixes()}
          pathToCoordenada={'vigilanciaPeixes'}
          form={props.form}
        />

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há suspeita clínica?'}
            name={['vigilanciaPeixes', 'haSuspeitaClinica']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={16}>
          <Form.Item
            label={'Suspeita clinica'}
            name={['vigilanciaPeixes', 'suspeitaClinica']}
          >
            <Input autoComplete='off' />
          </Form.Item>
        </Col>
      </Row>

      <CustomModal
        centered
        open={showModalVigilanciaPeixes}
        title={'Cadastro de Vigilância de Peixes'}
        onCancel={() => {
          setShowModalVigilanciaPeixes(false);
          clearModalVigilanciaPeixes();
        }}
        width={750}
        footer={null}
        maskClosable={false}
        destroyOnClose
      >
        <Form layout={'vertical'} form={props.form} autoComplete={'off'}>
          <Row gutter={24}>
            <Col xs={24} lg={24}>
              <Form.Item
                label={'Espécie'}
                name={['modalVigilanciaPeixes', 'especie']}
                rules={[
                  {
                    required: true,
                    message: 'O campo é obrigatório',
                  },
                ]}
              >
                <Select
                  placeholder={'Selecione uma espécie'}
                  placement={'bottomLeft'}
                >
                  {listaEspeciePeixes?.map((especie) => (
                    <Select.Option key={especie} value={especie}>
                      {Especie.valueOf(especie)}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>

            <Col xs={24} lg={12}>
              <Form.Item
                label={'Examinados'}
                name={['modalVigilanciaPeixes', 'quantidadeInspecionados']}
                rules={[
                  {
                    required: true,
                    message: 'O campo é obrigatório',
                  },
                ]}
              >
                <Input
                  inputMode='numeric'
                  autoComplete='off'
                  maxLength={5}
                  onKeyPress={(event: any) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                />
              </Form.Item>
            </Col>

            <Col xs={24} lg={12}>
              <Tooltip
                title={
                  'Preencher com todos os animais observados, inclusive os examinados'
                }
              >
                <Form.Item
                  label={'Vistoriados'}
                  name={['modalVigilanciaPeixes', 'quantidadeVistoriados']}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                  />
                </Form.Item>
              </Tooltip>
            </Col>
          </Row>

          <Divider />
          <Row gutter={24} justify={'end'}>
            <Col xs={12} lg={6}>
              <Button
                style={{ width: '100%' }}
                danger
                icon={<CloseOutlined />}
                onClick={() => {
                  setShowModalVigilanciaPeixes(false);
                  clearModalVigilanciaPeixes();
                }}
              >
                Cancelar
              </Button>
            </Col>
            <Col xs={12} lg={6}>
              <Button
                style={{ width: '100%' }}
                type={'primary'}
                icon={<CheckCircleOutlined />}
                onClick={() => {
                  props.form
                    .validateFields([
                      ['modalVigilanciaPeixes', 'especie'],
                      ['modalVigilanciaPeixes', 'quantidadeVistoriados'],
                      ['modalVigilanciaPeixes', 'quantidadeInspecionados'],
                    ])
                    .then(() => {
                      handleAddVigilanciaPeixes();
                    })
                    .catch((e) => {
                      return;
                    });
                }}
              >
                Salvar
              </Button>
            </Col>
          </Row>
        </Form>
      </CustomModal>
    </>
  );
}
