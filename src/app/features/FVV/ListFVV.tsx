import {
  Button,
  Col,
  Descriptions,
  Modal,
  notification,
  Popconfirm,
  Row,
  Skeleton,
  Space,
  Table,
  Tooltip,
  Typography,
} from 'antd';
import { useCallback, useEffect, useState } from 'react';
import moment from 'moment';
import {
  DeleteOutlined,
  EditTwoTone,
  EyeTwoTone,
  SearchOutlined,
  SyncOutlined,
} from '@ant-design/icons';
import { FVV } from '../../../sdk/@types';

import { Link } from 'react-router-dom';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import FilterPanelFVV from './FilterPanelFVV';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../core/store';
import * as FVVActions from '../../../core/store/FVV.slice';
import FVVService from '../../../sdk/services/SIZ-API/FVV.service';
import { CustomModal } from '../../components/CustomModal';

export default function ListFVV() {
  const dispatch = useDispatch();
  const [deleting, setDeleting] = useState(false);
  const { loading, setFirstOpening, setLoading } = useLoadingPage();
  const { xs } = useBreakpoint();

  const [modalSummaryVisible, setModalSummaryVisible] = useState(false);
  const [fvvSummary, setFVVSummary] = useState<FVV.Summary>();

  const [error, setError] = useState<Error>();

  const fvvs = useSelector((state: RootState) => state.fvv.fvvs);
  const query = useSelector((state: RootState) => state.fvv.query);
  const fetching = useSelector((state: RootState) => state.fvv.fetching);

  if (!window.navigator.onLine)
    throw new Error('Não é possível visualizar a lista de FVV no modo offline');

  if (error) throw error;

  useEffect(() => {
    setFirstOpening(true);
  }, []);

  useEffect(() => {}, [fvvs]);

  const deleteFVV = useCallback(
    async (id) => {
      if (id) {
        setDeleting(true);
        return await FVVService.remove(id)
          .then(() => {
            setDeleting(false);
            notification.success({
              message: 'FVV removido com sucesso',
            });
            dispatch(FVVActions.filter(query));
          })
          .catch((e) => {
            setDeleting(false);
            notification.error({
              message: 'Não foi possível remover o FVV',
              description: e.message,
            });
          });
      }
    },
    [dispatch, query]
  );

  return (
    <>
      <FilterPanelFVV />
      <br />

      <Row>
        <Col span={24}>
          {fetching && <Skeleton active />}

          {!fetching && (
            <>
              <Table<FVV.Summary>
                rowKey={'id'}
                dataSource={fvvs?.content}
                size={'small'}
                loading={deleting || fetching}
                pagination={{
                  current: query.page ? query.page + 1 : 1,
                  onChange: (page, pageSize) => {
                    dispatch(
                      FVVActions.setQuery({
                        ...query,
                        page: page - 1,
                        size: pageSize,
                      })
                    );
                  },
                  total: fvvs?.totalElements,
                  pageSize: query.size,
                }}
                title={() => {
                  return (
                    <Row justify={'space-between'}>
                      <Typography.Title level={5} style={{ color: 'white' }}>
                        FVVs
                      </Typography.Title>

                      <Space>
                        <Col>
                          <Button
                            icon={<SyncOutlined />}
                            type={'primary'}
                            title={'Atualizar'}
                            onClick={async () => {
                              setLoading(true);
                              try {
                                dispatch(FVVActions.filter(query));
                              } finally {
                                setLoading(false);
                              }
                            }}
                          />
                        </Col>
                      </Space>
                    </Row>
                  );
                }}
                columns={[
                  {
                    dataIndex: 'numero',
                    title: 'Número',
                    width: 175,
                    responsive: ['xs', 'sm'],
                    render(numero) {
                      return (
                        <span style={{ fontWeight: 'bold' }}>{numero}</span>
                      );
                    },
                  },
                  {
                    dataIndex: 'dataVigilancia',
                    title: 'Data',
                    width: 100,
                    responsive: ['xs', 'sm'],
                    render(data: FVV.Summary['dataVigilancia']) {
                      return moment(new Date(data)).format('DD/MM/YYYY');
                    },
                  },
                  {
                    dataIndex: 'visitaPropriedadeRural',
                    title: 'FVER',
                    responsive: ['sm'],
                    width: 210,
                    render(fver: FVV.Summary['visitaPropriedadeRural']) {
                      if (fver)
                        return (
                          <>
                            <Row gutter={12} align={'middle'}>
                              <Col>
                                <Tooltip title={fver.numero}>
                                  {fver.numero}
                                </Tooltip>
                              </Col>
                              <Col>
                                <Link to={`/visitas/visualizar/${fver.id}`}>
                                  <Button
                                    icon={<EyeTwoTone twoToneColor={'#09f'} />}
                                    size={'small'}
                                    type={'ghost'}
                                  />
                                </Link>
                              </Col>
                            </Row>
                          </>
                        );
                      else return undefined;
                    },
                  },
                  {
                    dataIndex: 'nomeEstabelecimento',
                    title: 'Estabelecimento',
                    responsive: ['sm'],
                    render(nome: FVV.Summary['nomeEstabelecimento']) {
                      return <Tooltip title={nome}>{nome}</Tooltip>;
                    },
                  },
                  {
                    dataIndex: 'id',
                    title: '',
                    responsive: ['xs', 'sm'],
                    width: 100,
                    align: 'right',
                    render(id: number) {
                      return (
                        <Space>
                          <Link to={`/vigilancias/edicao/${id}`}>
                            <Button
                              icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                              size={'small'}
                              type={'ghost'}
                            />
                          </Link>
                          <Link to={`/vigilancias/visualizar/${id}`}>
                            <Button
                              icon={<SearchOutlined />}
                              size={'small'}
                              type={'ghost'}
                            />
                          </Link>
                          <Popconfirm
                            title={'Deseja remover o FVV?'}
                            onConfirm={() => {
                              deleteFVV(id);
                            }}
                          >
                            <Button
                              icon={<DeleteOutlined />}
                              size={'small'}
                              type={'ghost'}
                              danger
                            />
                          </Popconfirm>
                        </Space>
                      );
                    },
                  },
                ]}
                expandable={{
                  expandedRowRender: (record) => (
                    <Typography
                      style={{
                        color: '#969494',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        display: '-webkit-box',
                        WebkitLineClamp: 2,
                        lineClamp: 2,
                        WebkitBoxOrient: 'vertical',
                      }}
                    >
                      {record.nomeVeterinario}
                    </Typography>
                  ),
                  defaultExpandAllRows: true,
                  showExpandColumn: false,
                }}
                onRow={(record, index) => {
                  return {
                    onDoubleClick: (e) => {
                      if (!xs) return;

                      setModalSummaryVisible(true);
                      setFVVSummary(record);
                    },
                  };
                }}
              ></Table>
              <CustomModal
                centered
                open={modalSummaryVisible}
                title={`FVV nº ${fvvSummary?.numero}`}
                closable={false}
                onCancel={() => {
                  setModalSummaryVisible(false);
                  setFVVSummary(undefined);
                }}
                maskClosable={true}
                keyboard={true}
                footer={
                  <Button
                    type='primary'
                    onClick={() => {
                      setModalSummaryVisible(false);
                      setFVVSummary(undefined);
                    }}
                  >
                    Fechar
                  </Button>
                }
              >
                {fvvSummary && (
                  <Descriptions size={'small'}>
                    <Descriptions.Item label={'Número'}>
                      {fvvSummary.numero}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Data'}>
                      {moment(new Date(fvvSummary.dataVigilancia)).format(
                        'DD/MM/YYYY'
                      )}
                    </Descriptions.Item>
                    <Descriptions.Item label={'Nome'}>
                      <Tooltip title={fvvSummary.nomeEstabelecimento}>
                        {fvvSummary.nomeEstabelecimento}
                      </Tooltip>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Ações'}>
                      <Space>
                        <Link to={`/vigilancias/edicao/${fvvSummary.id}`}>
                          <Button
                            icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                            size={'small'}
                            type={'ghost'}
                          />
                        </Link>
                        <Popconfirm
                          title={'Deseja remover o FVV?'}
                          onConfirm={() => {
                            try {
                              deleteFVV(fvvSummary.id)?.then((result) => {
                                notification.success({
                                  message: 'FVV removido com sucesso',
                                });
                              });
                            } catch (e) {
                              throw e;
                            }
                          }}
                        >
                          <Button
                            icon={<DeleteOutlined />}
                            size={'small'}
                            type={'ghost'}
                            danger
                          />
                        </Popconfirm>
                      </Space>
                    </Descriptions.Item>
                  </Descriptions>
                )}
              </CustomModal>
            </>
          )}
        </Col>
      </Row>
    </>
  );
}
