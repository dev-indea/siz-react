import {
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Row,
  Switch,
  Typography,
} from 'antd';

type VigilanciaHistoricoMordeduraProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaHistoricoMordedura(
  props: VigilanciaHistoricoMordeduraProps
) {
  return (
    <>
      <Typography.Title level={5}>
        Histórico de mordedura de morcegos na propriedade
      </Typography.Title>
      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há histórico de mordedura?'}
            name={'historicoDeSugadurasDeMorcegos'}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>
      </Row>

      <Typography.Title level={5}>Informe as quantidades</Typography.Title>
      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8}>
          <Form.Item label={'Bovinos'} name={'quantidadeSugadurasEmBovinos'}>
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Bubalinos'}
            name={'quantidadeSugadurasEmBubalinos'}
          >
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item label={'Aves'} name={'quantidadeSugadurasEmAves'}>
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item label={'Suideos'} name={'quantidadeSugadurasEmSuideos'}>
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item label={'Equideos'} name={'quantidadeSugadurasEmEquideos'}>
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item label={'Caprinos'} name={'quantidadeSugadurasEmCaprinos'}>
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}
