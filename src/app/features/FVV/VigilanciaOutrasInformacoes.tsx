import { Col, Form, FormInstance, Row, Switch } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import VeterinarioPanel from '../../components/VeterinarioPanel';

type VigilanciaOutrasInformacoesProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaOutrasInformacoes(
  props: VigilanciaOutrasInformacoesProps
) {
  return (
    <>
      <Row gutter={24}>
        <Col xs={24} lg={24}>
          <Form.Item label={'Observação'} name={'observacao'}>
            <TextArea showCount rows={5} maxLength={4000} />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Houve emissão de Form IN?'}
            name={'emissaoDeFormIN'}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>
      </Row>

      <VeterinarioPanel
        form={props.form}
        formDisabled={props.formDisabled ? true : false}
      />
    </>
  );
}
