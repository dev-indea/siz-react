import {
  Card,
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Row,
  Switch,
  Tooltip,
  Typography,
} from 'antd';
import SintomasObservadosPanel from '../../components/SintomasObservadosPanel';
import { SintomasObservados } from '../../../core/enums/SintomasObservados';

type VigilanciaAsininosProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaAsininos(props: VigilanciaAsininosProps) {
  const triggerRecountTotaExaminadosAsininos = () => {
    const ate6Meses: number = props.form.getFieldValue([
      'vigilanciaAsinino',
      'quantidadeInspecionadosAbaixo06Meses',
    ]);
    const acima6Meses: number = props.form.getFieldValue([
      'vigilanciaAsinino',
      'quantidadeInspecionadosAcima06Meses',
    ]);
    const dados = [ate6Meses, acima6Meses];

    let totalExaminados: number = 0;

    dados.forEach((qtdade) => {
      if (!isNaN(Number(qtdade))) {
        totalExaminados = Number(totalExaminados) + Number(qtdade);

        props.form.setFieldValue(
          ['vigilanciaAsinino', 'quantidadeExaminados'],
          totalExaminados
        );
      }
    });
  };

  return (
    <>
      <Typography.Title level={5}>Asininos examinados</Typography.Title>

      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8}>
          <Form.Item
            label={'Até 6 meses'}
            name={['vigilanciaAsinino', 'quantidadeInspecionadosAbaixo06Meses']}
          >
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onBlur={(event: any) => {
                triggerRecountTotaExaminadosAsininos();
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Acima de 6 meses'}
            name={['vigilanciaAsinino', 'quantidadeInspecionadosAcima06Meses']}
          >
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onBlur={(event: any) => {
                triggerRecountTotaExaminadosAsininos();
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Examinados'}
            name={['vigilanciaAsinino', 'quantidadeExaminados']}
          >
            <Input disabled />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Tooltip
            title={
              'Preencher com todos os animais observados, inclusive os examinados'
            }
          >
            <Form.Item
              label={'Vistoriados'}
              name={['vigilanciaAsinino', 'quantidadeVistoriados']}
            >
              <Input
                inputMode='numeric'
                maxLength={4}
                autoComplete='off'
                onKeyPress={(event: any) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
              />
            </Form.Item>
          </Tooltip>
        </Col>
      </Row>

      <Row gutter={24}>
        <SintomasObservadosPanel
          listaSintomasObservados={SintomasObservados.asininos()}
          pathToCoordenada={'vigilanciaAsinino'}
          form={props.form}
        />

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há suspeita clínica?'}
            name={['vigilanciaAsinino', 'haSuspeitaClinica']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={16}>
          <Form.Item
            label={'Suspeita clinica'}
            name={['vigilanciaAsinino', 'suspeitaClinica']}
          >
            <Input autoComplete='off' />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}
