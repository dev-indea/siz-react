import {
  CloseOutlined,
  CloudUploadOutlined,
  DeleteOutlined,
  EditTwoTone,
  ExclamationCircleTwoTone,
  EyeTwoTone,
} from '@ant-design/icons';
import {
  Alert,
  Button,
  Col,
  Descriptions,
  notification,
  Popconfirm,
  Popover,
  Row,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { FVV } from '../../../sdk/@types';
import { SYNC_VIGILANCIA_DONE } from '../../../sdk/@types/ServiceWorker.types';
import { ServiceIDBPayloadInput } from '../../../sdk/services/indexeddb/ServiceIDB';
import FVVIDBService from '../../../sdk/services/indexeddb/FVVIDB.service';
import useSyncingState from '../../../core/hooks/useSyncingState';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import syncFVVs from '../../../core/functions/syncFVVs';
import FVERService from '../../../sdk/services/SIZ-API/FVER.service';
import FVERIDBService from '../../../sdk/services/indexeddb/FVERIDB.service';
import TroubleshootingButton from '../../components/TroubleshootingButton';
import { CustomModal } from '../../components/CustomModal';

interface ListFVVOfflineProps {
  visible?: boolean;
}

export type FVVComErro = {
  erro: boolean;
} & ServiceIDBPayloadInput;

export default function ListFVVOffline(props: ListFVVOfflineProps) {
  const [syncing, setSyncing] = useState(false);
  const navigate = useNavigate();
  const [error, setError] = useState<Error>();
  const { online } = useNavigatorStatus();

  const [listaFVVsComErro, setlistaFVVsComErro] = useState<FVVComErro[]>();
  const [listaFVVs, setlistaFVVs] = useState<ServiceIDBPayloadInput[]>();

  const [fetching, setFetching] = useState(false);
  const [syncFVVDone, setSyncFVVDone] = useState<string | undefined>(undefined);

  const [showModalMotivoErro, setShowModalMotivoErro] =
    useState<boolean>(false);
  const [showModalDeleteFVVs, setShowModalDeleteFVV] = useState<boolean>(false);
  const [motivoErro, setMotivoErro] = useState<string>();

  const { fvv, setFvv } = useSyncingState();
  const [deleteOpen, setDeleteOpen] = useState(false);

  if (navigator.serviceWorker)
    navigator.serviceWorker.onmessage = (event) => {
      if (event.data && event.data.type === SYNC_VIGILANCIA_DONE) {
        fetchListFVVsOffline();
        setSyncFVVDone(event.data.message);
      }
    };

  const fetchListFVVsOffline = useCallback(async () => {
    await FVVIDBService.getAll()
      .then(async (listFVVOffline) => {
        setlistaFVVs(listaFVVs);

        let newList = [];
        let newFVV;
        let quantidadeFVVsSincronizados = 0;
        let quantidadeFVVsdeRecinto = 0;

        for (const fvv of listFVVOffline) {
          if (fvv.status === 'SINCRONIZADO') quantidadeFVVsSincronizados++;

          if (
            fvv.payload.visitaPropriedadeRural.tipoEstabelecimento !==
            'PROPRIEDADE'
          ) {
            quantidadeFVVsdeRecinto++;
            if (fvv.id) {
              await FVVIDBService.delete(fvv.id);
            }
          } else {
            if (!fvv.payload.visitaPropriedadeRural.codigoVerificador) {
              newFVV = { ...fvv, erro: true };
            } else {
              newFVV =
                await FVERIDBService.getAllFromInputTableOnlyPayload().then(
                  async (lista) => {
                    const fverLocal = lista.filter(
                      (fver) =>
                        fver.codigoVerificador ===
                        fvv.payload.visitaPropriedadeRural.codigoVerificador
                    );
                    if (fverLocal.length > 0) return { ...fvv, erro: false };
                    else {
                      if (window.navigator.onLine) {
                        return await FVERService.getByCodigoVerificador(
                          fvv.payload.visitaPropriedadeRural.codigoVerificador
                          //'bd69c284cefd8b9b2e5ba145be5e65f67750df07'
                        )
                          .then(async (fver) => {
                            if (fver) {
                              if (fver.content && fver.content?.length > 0) {
                                return { ...fvv, erro: false };
                              } else return { ...fvv, erro: true };
                            } else return { ...fvv, erro: true };
                          })
                          .catch((e) => ({ ...fvv, erro: true }));
                      } else {
                        return { ...fvv, erro: true };
                      }
                    }
                  }
                );
            }

            newList.push(newFVV);
          }
        }

        if (quantidadeFVVsSincronizados >= 10) {
          setShowModalDeleteFVV(true);
        }
        if (quantidadeFVVsdeRecinto > 0) {
          notification.info({
            message:
              'Identificamos FVVs criados a partir de Recintos ou Abatedouros e eles foram excluídos automaticamente',
            description:
              'Qualquer dúvida sobre este procedimento, por favor consulte a CDSA',
          });
        }
        return newList;
      })
      .then(async (lista) => {
        setlistaFVVsComErro(lista);
        setFetching(false);
      })
      .catch((e) => {
        notification.error({
          message: 'Houve um erro ao exibir os FVV',
          description: `Motivo: ${e.message}`,
        });
        setlistaFVVsComErro(undefined);
      })
      .finally(() => setFetching(false));
  }, [listaFVVs]);

  const deleteFVV = useCallback(
    (a1) => {
      if (a1) FVVIDBService.delete(a1).then(fetchListFVVsOffline);
    },
    [fetchListFVVsOffline]
  );

  const deleteSynced = useCallback(async () => {
    const listFVVsSynced = listaFVVsComErro?.filter(
      (fvv) => fvv.status === 'SINCRONIZADO'
    );

    const listFVVsNotSynced = listaFVVsComErro?.filter(
      (fvv) => fvv.status !== 'SINCRONIZADO'
    );

    if (listFVVsSynced)
      for (const fvv of listFVVsSynced) {
        //@ts-ignore
        await FVVIDBService.delete(fvv.id);
      }

    setlistaFVVsComErro(listFVVsNotSynced);
    setDeleteOpen(false);
    notification.success({
      message: 'Todas os FVVs sincronizados foram removidos',
    });
  }, [listaFVVsComErro]);

  useEffect(() => {}, [syncFVVDone]);

  useEffect(() => {
    setFetching(false);

    if (listaFVVsComErro)
      for (const fvv of listaFVVsComErro) {
        if (fvv.status === 'SINCRONIZADO') setDeleteOpen(true);
      }
  }, [listaFVVsComErro]);

  useEffect(() => {
    fetchListFVVsOffline();
  }, [fetchListFVVsOffline, fvv]);

  if (error) throw error;

  return (
    <>
      <Row>
        <Space size={4} direction='vertical' style={{ width: '100%' }}>
          <Col hidden={!syncFVVDone} xs={24}>
            <Alert
              message={syncFVVDone}
              type='info'
              showIcon
              closable={false}
            ></Alert>
          </Col>
          <Col span={24}>
            <Table
              dataSource={listaFVVsComErro}
              size={'small'}
              rowKey={'id'}
              loading={fvv || fetching}
              title={() => {
                return (
                  <Row justify={'space-between'}>
                    <Typography.Title level={5} style={{ color: 'white' }}>
                      FVVs
                    </Typography.Title>

                    <Space>
                      <Popover
                        arrowPointAtCenter={false}
                        content={
                          <div
                            style={{ maxWidth: '350px', textAlign: 'justify' }}
                          >
                            <Typography.Text>
                              Os dados das visitas com o status{' '}
                              <Tag
                                style={{
                                  backgroundColor: '#2baf57',
                                  color: 'white',
                                }}
                              >
                                SINCRONIZADO
                              </Tag>{' '}
                              já se encontram no servidor. Esta operação irá
                              remover apenas os registros no dispositivo, sem
                              perda de dados das atividades realizadas.
                            </Typography.Text>
                          </div>
                        }
                        title={
                          <Row justify='space-between'>
                            <span style={{ fontWeight: 'bold' }}>
                              Remover FVVs sincronizados
                            </span>
                            <Button
                              icon={<CloseOutlined />}
                              size={'small'}
                              type={'text'}
                              onClick={() => {
                                setDeleteOpen(false);
                              }}
                            />
                          </Row>
                        }
                        open={false}
                      >
                        <Popconfirm
                          title={'Deseja remover todos os sincronizados??'}
                          onConfirm={deleteSynced}
                        >
                          <Button
                            icon={<DeleteOutlined />}
                            size={'small'}
                            type={'primary'}
                            danger
                          />
                        </Popconfirm>
                      </Popover>

                      <Popconfirm
                        title={'Confirma a sincronização?'}
                        disabled={
                          !listaFVVsComErro ||
                          listaFVVsComErro.length === 0 ||
                          !online
                        }
                        onConfirm={async () => {
                          setSyncing(true);
                          setFvv(true);
                          await syncFVVs(true)
                            .then(async (message) => {
                              notification.success({
                                message: message,
                              });
                              await fetchListFVVsOffline();
                            })
                            .catch((e) => {
                              notification.error({
                                message: 'Não foi possível sincronizar',
                                description: e.message,
                              });
                            })
                            .finally(() => {
                              setSyncing(false);
                              setFvv(false);
                              navigate(`/acoes-de-campo`);
                            });
                        }}
                      >
                        <Button
                          icon={<CloudUploadOutlined />}
                          type={'primary'}
                          disabled={
                            !listaFVVsComErro ||
                            listaFVVsComErro.length === 0 ||
                            !online
                          }
                        />
                      </Popconfirm>

                      <TroubleshootingButton
                        component='FVV'
                        data={listaFVVsComErro ? listaFVVsComErro : listaFVVs}
                      />
                    </Space>
                  </Row>
                );
              }}
              columns={[
                {
                  dataIndex: 'id',
                  title: 'FVVs',
                  responsive: ['xs'],
                  render(id: FVV.Summary['id'], fvv) {
                    return (
                      <Descriptions size={'small'}>
                        <Descriptions.Item label={'Data'}>
                          {moment(fvv.payload.dataVigilancia).format(
                            'DD/MM/YYYY HH:mm'
                          )}
                        </Descriptions.Item>
                        <Descriptions.Item label={'Estabelecimento'}>
                          <Tooltip title={fvv.payload.propriedade.nome}>
                            {fvv.payload.propriedade.nome}
                          </Tooltip>
                        </Descriptions.Item>
                        <Descriptions.Item label={'Ações'}>
                          <Space>
                            {fvv.status !== 'SINCRONIZADO' && (
                              <Link to={`/vigilancias/edicao/${id}/${'local'}`}>
                                <Button
                                  icon={
                                    <EditTwoTone twoToneColor={'#84aee6'} />
                                  }
                                  size={'small'}
                                  type={'ghost'}
                                />
                              </Link>
                            )}

                            <Popconfirm
                              title={'Deseja remover o FVV?'}
                              onConfirm={() => {
                                try {
                                  deleteFVV(id);
                                  notification.success({
                                    message: 'FVV removido com sucesso',
                                  });
                                } catch (e) {
                                  throw e;
                                }
                              }}
                            >
                              <Button
                                icon={<DeleteOutlined />}
                                size={'small'}
                                type={'ghost'}
                                danger
                              />
                            </Popconfirm>
                          </Space>
                        </Descriptions.Item>
                      </Descriptions>
                    );
                  },
                } /* 
                {
                  dataIndex: ['id'],
                  title: 'ID',
                  width: 50,
                  responsive: ['xs', 'sm'],
                }, */,
                {
                  dataIndex: ['payload', 'numero'],
                  title: 'Número',
                  width: 235,
                  responsive: ['xs', 'sm'],
                  render(numero, row) {
                    return (
                      <>
                        <Row gutter={4}>
                          <Col>
                            <span style={{ fontWeight: 'bold' }}>{numero}</span>
                          </Col>
                          <Col>
                            {numero && (
                              <Link
                                to={`/vigilancias/visualizar/${row.payload.id}`}
                              >
                                <Button
                                  icon={<EyeTwoTone twoToneColor={'#09f'} />}
                                  size={'small'}
                                  type={'ghost'}
                                />
                              </Link>
                            )}
                          </Col>
                        </Row>
                      </>
                    );
                  },
                },
                {
                  dataIndex: ['payload', 'dataVigilancia'],
                  title: 'Data',
                  responsive: ['sm'],
                  width: 150,
                  render(data: FVV.Summary['dataVigilancia']) {
                    return moment(data).format('DD/MM/YYYY HH:mm');
                  },
                },
                {
                  dataIndex: ['payload', 'visitaPropriedadeRural'],
                  title: 'FVER',
                  responsive: ['sm'],
                  width: 215,
                  render(fver: FVV.Summary['visitaPropriedadeRural']) {
                    if (fver)
                      return (
                        <Tooltip title={fver.numero}>{fver.numero}</Tooltip>
                      );
                    else return undefined;
                  },
                },
                {
                  dataIndex: ['payload', 'nomeEstabelecimento'],
                  title: 'Estabelecimento',
                  responsive: ['sm'],
                  render(nome: FVV.Summary['nomeEstabelecimento'], row) {
                    return (
                      <Tooltip title={row.payload.propriedade.nome}>
                        {row.payload.propriedade.nome}
                      </Tooltip>
                    );
                  },
                },
                {
                  dataIndex: ['status'],
                  title: 'Status',
                  width: 110,
                  responsive: ['md'],
                  render(status: ServiceIDBPayloadInput['status'], record) {
                    return (
                      <>
                        {status === 'NOVO' && (
                          <Tag
                            style={{
                              backgroundColor: '#09f',
                              color: 'white',
                            }}
                          >
                            {status}
                          </Tag>
                        )}{' '}
                        {status === 'SINCRONIZADO' && (
                          <Tag
                            style={{
                              backgroundColor: '#2baf57',
                              color: 'white',
                            }}
                          >
                            {status}
                          </Tag>
                        )}{' '}
                        {status === 'ERRO' && (
                          <Space style={{ textAlign: 'center' }}>
                            <Tag
                              style={{
                                backgroundColor: '#ff4e4e',
                                color: 'white',
                              }}
                            >
                              {status}
                            </Tag>
                            <Button
                              type='text'
                              danger
                              icon={
                                <ExclamationCircleTwoTone
                                  twoToneColor={'#d11b1b'}
                                />
                              }
                              onClick={() => {
                                setShowModalMotivoErro(true);
                                setMotivoErro(record.motivoErro);
                              }}
                            />
                          </Space>
                        )}
                      </>
                    );
                  },
                },
                {
                  dataIndex: 'id',
                  title: 'Ações',
                  responsive: ['sm'],
                  width: 100,
                  align: 'right',
                  render(id, fvv) {
                    return (
                      <>
                        <Space>
                          {fvv.erro && (
                            <Popover
                              arrowPointAtCenter={false}
                              content={'Encontramos um problema com esse FVV. '}
                              open={false}
                              placement={'topRight'}
                            >
                              <Button
                                icon={
                                  <ExclamationCircleTwoTone
                                    twoToneColor={'#d11b1b'}
                                    spin
                                  />
                                }
                                size={'large'}
                                type={'ghost'}
                                onClick={() => {
                                  navigate(
                                    `/vigilancias/edicao/${id}/${'local'}/${'problem'}`
                                  );
                                }}
                              />
                            </Popover>
                          )}

                          {fvv.status !== 'SINCRONIZADO' && !fvv.erro && (
                            <Link to={`/vigilancias/edicao/${id}/${'local'}`}>
                              <Button
                                icon={<EditTwoTone twoToneColor={'#84aee6'} />}
                                size={'small'}
                                type={'ghost'}
                              />
                            </Link>
                          )}

                          <Popconfirm
                            title={'Deseja remover o FVV?'}
                            onConfirm={() => {
                              try {
                                deleteFVV(id);
                                notification.success({
                                  message: 'FVV removida com sucesso',
                                });
                              } catch (e) {
                                throw e;
                              }
                            }}
                          >
                            <Button
                              icon={<DeleteOutlined />}
                              size={'small'}
                              type={'ghost'}
                              danger
                            />
                          </Popconfirm>
                        </Space>
                      </>
                    );
                  },
                },
              ]}
              expandable={{
                expandedRowRender: (record) => (
                  <Typography
                    style={{
                      color: '#969494',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                      display: '-webkit-box',
                      WebkitLineClamp: 2,
                      lineClamp: 2,
                      WebkitBoxOrient: 'vertical',
                    }}
                  >
                    {record.payload.nomeVeterinario}
                  </Typography>
                ),
                defaultExpandAllRows: true,
                showExpandColumn: false,
              }}
            />
          </Col>
        </Space>
      </Row>

      <CustomModal
        open={showModalMotivoErro}
        title={'Motivo'}
        footer={
          <Button
            style={{ width: '100%' }}
            type='primary'
            icon={<CloseOutlined />}
            onClick={() => {
              setShowModalMotivoErro(false);
              setMotivoErro(undefined);
            }}
          >
            Fechar
          </Button>
        }
      >
        <Typography.Title level={3} style={{ textAlign: 'center' }}>
          {motivoErro}
        </Typography.Title>
      </CustomModal>

      <CustomModal
        open={showModalDeleteFVVs}
        title={'Aviso'}
        footer={
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Button
              style={{ width: '100%' }}
              type='default'
              icon={<CloseOutlined />}
              onClick={() => {
                setShowModalDeleteFVV(false);
              }}
            >
              Fechar
            </Button>

            <Button
              style={{ width: '100%' }}
              type='primary'
              icon={<CloseOutlined />}
              onClick={async () => {
                await deleteSynced().finally(() =>
                  setShowModalDeleteFVV(false)
                );
              }}
            >
              Remover
            </Button>
          </div>
        }
      >
        <Typography.Paragraph>
          Notamos que você ainda está mantendo no seu dispositivo muitos FVVs
          que já foram sincronizados.
        </Typography.Paragraph>

        <Typography.Paragraph>
          Deseja removê-los? Essa ação não irá afetar os FVVs no banco de dados.
        </Typography.Paragraph>
      </CustomModal>
    </>
  );
}
