import { useEffect } from 'react';
import {
  Card,
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Row,
  Select,
  Space,
  Switch,
  Typography,
} from 'antd';
import { FaseExploracaoBovinosEBubalinos } from '../../../core/enums/FaseExploracaoBovinosEBubalinos';
import { IdadeRuminantesAlimentadosComRacao } from '../../../core/enums/IdadeRuminantesAlimentadosComRacao';
import { FinalidadeExploracaoBovinosEBubalinos } from '../../../core/enums/FinalidadeExploracaoBovinosEBubalinos';
import { SistemaDeCriacaoDeRuminantes } from '../../../core/enums/SistemaDeCriacaoDeRuminantes';
import { TipoAlimentacaoRuminantes } from '../../../core/enums/TipoAlimentacaoRuminantes';
import { EpocaDoAnoSuplementacao } from '../../../core/enums/EpocaDoAnoSuplementacao';
import { TipoExploracaoAvicolaSistemaIndustrial } from '../../../core/enums/TipoExploracaoAvicolaSistemaIndustrial';
import { TipoExploracaoSuinosSistemaIndustrial } from '../../../core/enums/TipoExploracaoSuinosSistemaIndustrial';
import { AtivaPassiva } from '../../../core/enums/AtivaPassiva';
import { ResultadoTesteRapido } from '../../../core/enums/ResultadoTesteRapido';
import { ChavePrincipalFVER } from '../../../sdk/@types';
import TextArea from 'antd/lib/input/TextArea';

type VigilanciaInvestigacaoDeAlimentosProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaInvestigacaoDeAlimentos(
  props: VigilanciaInvestigacaoDeAlimentosProps
) {
  const ID_PREVENÇÃO_E_CONTROLE_DAS_ENCEFALOPATIAS_ESPONGIFORMES_TRANSMISSÍVEIS: number = 6;

  const listaFaseExploracaoBovinosEBubalinos =
    FaseExploracaoBovinosEBubalinos.keys();
  const listaIdadeRuminantesAlimentadosComRacaoBovinos =
    IdadeRuminantesAlimentadosComRacao.idadeRuminantesAlimentadosComRacao_Bovinos();
  const listaIdadeRuminantesAlimentadosComRacaoOvinosCaprinos =
    IdadeRuminantesAlimentadosComRacao.idadeRuminantesAlimentadosComRacao_Ovinos_Caprinos();
  const listaFinalidadeExploracaoBovinosEBubalinos =
    FinalidadeExploracaoBovinosEBubalinos.keys();
  const listaSistemaDeCriacaoDeRuminantes = SistemaDeCriacaoDeRuminantes.keys();
  const listaTipoAlimentacaoRuminantes = TipoAlimentacaoRuminantes.keys();
  const listaEpocaAnoSuplementacao = EpocaDoAnoSuplementacao.keys();
  const listaTipoExploracaoAvicolaSistemaIndustrial =
    TipoExploracaoAvicolaSistemaIndustrial.keys();
  const listaTipoExploracaoSuinosSistemaIndustrial =
    TipoExploracaoSuinosSistemaIndustrial.keys();
  const listaAtivaPassiva = AtivaPassiva.keys();
  const listaResultadoTesteRapido = ResultadoTesteRapido.keys();

  useEffect(() => {
    props.form.setFieldValue(
      ['vigilanciaAlimentosRuminantes', 'presencaDeCamaDeAviario'],
      props.form.getFieldValue([
        'vigilanciaAlimentosRuminantes',
        'presencaDeCamaDeAviario',
      ]) === 'SIM'
        ? true
        : false
    );

    props.form.setFieldValue(
      [
        'vigilanciaAlimentosRuminantes',
        'utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes',
      ],
      props.form.getFieldValue([
        'vigilanciaAlimentosRuminantes',
        'utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes',
      ]) === 'SIM'
        ? true
        : false
    );

    props.form.setFieldValue(
      [
        'vigilanciaAlimentosRuminantes',
        'pisciculturaComSistemaDeAlimentacaoABaseDeRacao',
      ],
      props.form.getFieldValue([
        'vigilanciaAlimentosRuminantes',
        'pisciculturaComSistemaDeAlimentacaoABaseDeRacao',
      ]) === 'SIM'
        ? true
        : false
    );

    props.form.setFieldValue(
      [
        'vigilanciaAlimentosRuminantes',
        'contaminacaoCruzadaDeRacoesDePeixeERuminante',
      ],
      props.form.getFieldValue([
        'vigilanciaAlimentosRuminantes',
        'contaminacaoCruzadaDeRacoesDePeixeERuminante',
      ]) === 'SIM'
        ? true
        : false
    );

    props.form.setFieldValue(
      [
        'vigilanciaAlimentosRuminantes',
        'colheitaDeAmostraDeAlimentosDeRuminantes',
      ],
      props.form.getFieldValue([
        'vigilanciaAlimentosRuminantes',
        'colheitaDeAmostraDeAlimentosDeRuminantes',
      ]) === 'SIM'
        ? true
        : false
    );

    props.form.setFieldValue(
      ['vigilanciaAlimentosRuminantes', 'usoTesteRapido'],
      props.form.getFieldValue([
        'vigilanciaAlimentosRuminantes',
        'usoTesteRapido',
      ]) === 'SIM'
        ? true
        : false
    );
  }, [props.form]);
  const isInvestigacaoDeAlimentosLiberada = () => {
    const chavesVisita: ChavePrincipalFVER.Input[] = props.form.getFieldValue([
      'visitaPropriedadeRural',
      'listChavePrincipalVisitaPropriedadeRural',
    ]);

    let result = false;
    chavesVisita.forEach((chave) => {
      if (
        Number(chave.tipoChavePrincipalVisitaPropriedadeRural.id) ===
        ID_PREVENÇÃO_E_CONTROLE_DAS_ENCEFALOPATIAS_ESPONGIFORMES_TRANSMISSÍVEIS
      )
        result = true;
    });

    return result;
  };

  const handleSelectFaseCriacao = (list: any[]) => {
    props.form.setFieldsValue({
      vigilanciaAlimentosRuminantes: {
        sistemaDeCriacaoDeRuminantes2: list,
      },
    });
  };

  const handleSelectIdadeRuminantesAlimentadosComRacaoBovinos = (
    list: any[]
  ) => {
    props.form.setFieldsValue({
      vigilanciaAlimentosRuminantes: {
        idadeBovinosAlimentadosComRacao: list,
      },
    });
  };

  const handleSelectIdadeRuminantesAlimentadosComRacaoOvinosCaprinos = (
    list: any[]
  ) => {
    props.form.setFieldsValue({
      vigilanciaAlimentosRuminantes: {
        idadeOvinosCaprinosAlimentadosComRacao: list,
      },
    });
  };

  const handleSelectTipoAlimentacao = (list: any[]) => {
    props.form.setFieldsValue({
      vigilanciaAlimentosRuminantes: {
        tipoAlimentacaoRuminantesMobile: list,
      },
    });
  };

  const handleSelectEpocaDoAnoQueAconteceASuplementacaoMobile = (
    list: any[]
  ) => {
    props.form.setFieldsValue({
      vigilanciaAlimentosRuminantes: {
        epocaDoAnoQueAconteceASuplementacaoMobile: list,
      },
    });
  };

  return (
    <>
      <Typography.Title level={5}>
        Investigação de Alimentos Ruminantes
      </Typography.Title>
      <Divider />
      <fieldset disabled={props.formDisabled}>
        {isInvestigacaoDeAlimentosLiberada() && (
          <>
            <Row gutter={24}>
              <Form.Item
                label={'id'}
                name={['vigilanciaAlimentosRuminantes', 'id']}
                hidden
              >
                <Input />
              </Form.Item>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Finalidade exploração ruminantes'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'finalidadeExploracaoBovinosEBubalinos',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    placeholder={'Selecione uma finalidade'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaFinalidadeExploracaoBovinosEBubalinos?.map(
                      (finalidade) => (
                        <Select.Option key={finalidade} value={finalidade}>
                          {FinalidadeExploracaoBovinosEBubalinos.valueOf(
                            finalidade
                          )}
                        </Select.Option>
                      )
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Sistema de produção de ruminantes'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'sistemaDeCriacaoDeRuminantes',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    placeholder={'Selecione um sistema'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaSistemaDeCriacaoDeRuminantes?.map((finalidade) => (
                      <Select.Option key={finalidade} value={finalidade}>
                        {SistemaDeCriacaoDeRuminantes.valueOf(finalidade)}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Fase da produção'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'sistemaDeCriacaoDeRuminantes2',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    mode='multiple'
                    placeholder={'Selecione uma opção'}
                    onChange={handleSelectFaseCriacao}
                    showSearch={false}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                    maxTagCount={'responsive'}
                    style={{
                      inlineSize: 250,
                      height: 'auto',
                      wordWrap: 'break-word',
                      width: '100%',
                    }}
                  >
                    {listaFaseExploracaoBovinosEBubalinos?.map((finalidade) => (
                      <Select.Option key={finalidade} value={finalidade}>
                        {FaseExploracaoBovinosEBubalinos.valueOf(finalidade)}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Outro'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'outroSistemaDeCriacaoDeRuminante',
                  ]}
                >
                  <Input autoComplete='off' />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Idade dos bovinos alimentados com ração'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'idadeBovinosAlimentadosComRacao',
                  ]}
                >
                  <Select
                    mode='multiple'
                    placeholder={'Selecione uma opção'}
                    onChange={
                      handleSelectIdadeRuminantesAlimentadosComRacaoBovinos
                    }
                    showSearch={false}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                    maxTagCount={'responsive'}
                    style={{
                      inlineSize: 250,
                      height: 'auto',
                      wordWrap: 'break-word',
                      width: '100%',
                    }}
                  >
                    {listaIdadeRuminantesAlimentadosComRacaoBovinos?.map(
                      (idade) => (
                        <Select.Option key={idade} value={idade}>
                          {IdadeRuminantesAlimentadosComRacao.valueOf(idade)}
                        </Select.Option>
                      )
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Idade dos ovinos/caprinos alimentados com ração'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'idadeOvinosCaprinosAlimentadosComRacao',
                  ]}
                >
                  <Select
                    mode='multiple'
                    placeholder={'Selecione uma opção'}
                    onChange={
                      handleSelectIdadeRuminantesAlimentadosComRacaoOvinosCaprinos
                    }
                    showSearch={false}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                    maxTagCount={'responsive'}
                    style={{
                      inlineSize: 250,
                      height: 'auto',
                      wordWrap: 'break-word',
                      width: '100%',
                    }}
                  >
                    {listaIdadeRuminantesAlimentadosComRacaoOvinosCaprinos?.map(
                      (idade) => (
                        <Select.Option key={idade} value={idade}>
                          {IdadeRuminantesAlimentadosComRacao.valueOf(idade)}
                        </Select.Option>
                      )
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Space direction='vertical' style={{ width: '100%' }}>
                <Col xs={24} lg={24}>
                  <Card title='Nº total de ruminantes'>
                    <Row gutter={24}>
                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Bovinos'}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeBovinos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Caprinos'}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeCaprinos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Ovinos'}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeOvinos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Outros '}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeOutraEspecieRuminante',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>

                <Col xs={24} lg={24}>
                  <Card title='Nº total de ruminantes expostos'>
                    <Row gutter={24}>
                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Bovinos'}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeBovinosExpostos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Caprinos'}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeCaprinosExpostos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Ovinos'}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeOvinosExpostos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>

                      <Col xs={24} lg={6}>
                        <Form.Item
                          label={'Outros '}
                          name={[
                            'vigilanciaAlimentosRuminantes',
                            'quantidadeOutraEspecieRuminanteExpostos',
                          ]}
                        >
                          <Input
                            inputMode='numeric'
                            autoComplete='off'
                            maxLength={5}
                            onKeyPress={(event: any) => {
                              if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Space>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Tipo de alimentação'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'tipoAlimentacaoRuminantesMobile',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    placeholder={'Selecione'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaTipoAlimentacaoRuminantes?.map((finalidade) => (
                      <Select.Option key={finalidade} value={finalidade}>
                        {TipoAlimentacaoRuminantes.valueOf(finalidade)}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Época que acontece suplementação'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'epocaDoAnoQueAconteceASuplementacaoMobile',
                  ]}
                >
                  <Select
                    placeholder={'Selecione'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaEpocaAnoSuplementacao?.map((finalidade) => (
                      <Select.Option key={finalidade} value={finalidade}>
                        {EpocaDoAnoSuplementacao.valueOf(finalidade)}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Há criação avícola em sistema industrial?'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'tipoExploracaoAvicolaSistemaIndustrial',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    placeholder={'Selecione uma opção'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaTipoExploracaoAvicolaSistemaIndustrial?.map(
                      (tipoExploracaoAvicola) => (
                        <Select.Option
                          key={tipoExploracaoAvicola}
                          value={tipoExploracaoAvicola}
                        >
                          {TipoExploracaoAvicolaSistemaIndustrial.valueOf(
                            tipoExploracaoAvicola
                          )}
                        </Select.Option>
                      )
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Outro'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'outroTipoExploracaoAvicolaSistemaIndustrial',
                  ]}
                >
                  <Input autoComplete='off' />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Presença de cama de aviário'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'presencaDeCamaDeAviario',
                  ]}
                  valuePropName={'checked'}
                >
                  <Switch
                    defaultChecked={false}
                    size={'default'}
                    disabled={props.formDisabled}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={24}>
                <Form.Item
                  label={'Observação sobre cama de aviário'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'observacaoSobreCamaDeAviario',
                  ]}
                >
                  <TextArea showCount rows={5} maxLength={200} />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Utilização da cama de aviário na alimentação'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes',
                  ]}
                  valuePropName={'checked'}
                >
                  <Switch
                    defaultChecked={false}
                    size={'default'}
                    disabled={props.formDisabled}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Há criação de suínos em sistema industrial?'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'tipoExploracaoSuinosSistemaIndustrial',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    placeholder={'Selecione uma opção'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaTipoExploracaoSuinosSistemaIndustrial?.map(
                      (tipoExploracaoSuinos) => (
                        <Select.Option
                          key={tipoExploracaoSuinos}
                          value={tipoExploracaoSuinos}
                        >
                          {TipoExploracaoSuinosSistemaIndustrial.valueOf(
                            tipoExploracaoSuinos
                          )}
                        </Select.Option>
                      )
                    )}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={24}>
                <Form.Item
                  label={
                    'Observação sobre criação de suínos em sistema industrial'
                  }
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'observacaoTipoExploracaoSuinosSistemaIndustrial',
                  ]}
                >
                  <TextArea showCount rows={5} maxLength={200} />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={
                    'Psicultura com sistema de alimentação à base de ração'
                  }
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'pisciculturaComSistemaDeAlimentacaoABaseDeRacao',
                  ]}
                  valuePropName={'checked'}
                >
                  <Switch
                    defaultChecked={false}
                    size={'default'}
                    disabled={props.formDisabled}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Indícios de contaminação cruzada peixe x ração'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'contaminacaoCruzadaDeRacoesDePeixeERuminante',
                  ]}
                  valuePropName={'checked'}
                >
                  <Switch
                    defaultChecked={false}
                    size={'default'}
                    disabled={props.formDisabled}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Colheita de amostra de alimentos'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'colheitaDeAmostraDeAlimentosDeRuminantes',
                  ]}
                  valuePropName={'checked'}
                >
                  <Switch
                    defaultChecked={false}
                    size={'default'}
                    disabled={props.formDisabled}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Tipo de vigilância'}
                  name={['vigilanciaAlimentosRuminantes', 'tipoFiscalizacao']}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <Select
                    placeholder={'Selecione uma opção'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaAtivaPassiva?.map((tipo) => (
                      <Select.Option key={tipo} value={tipo}>
                        {AtivaPassiva.valueOf(tipo)}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Nº da denúncia'}
                  name={['vigilanciaAlimentosRuminantes', 'numeroDenuncia']}
                >
                  <Input
                    inputMode='numeric'
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Uso de teste rápido'}
                  name={['vigilanciaAlimentosRuminantes', 'usoTesteRapido']}
                  valuePropName={'checked'}
                >
                  <Switch
                    defaultChecked={false}
                    size={'default'}
                    disabled={props.formDisabled}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={8}>
                <Form.Item
                  label={'Resultado do teste rápido'}
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'resultadoTesteRapido',
                  ]}
                >
                  <Select
                    placeholder={'Selecione uma opção'}
                    placement={'bottomLeft'}
                    disabled={props.formDisabled}
                  >
                    {listaResultadoTesteRapido?.map((tipo) => (
                      <Select.Option key={tipo} value={tipo}>
                        {ResultadoTesteRapido.valueOf(tipo)}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={24} lg={24}>
                <Form.Item
                  label={
                    'Descrição do sistema de armazenamento e elaboração de rações'
                  }
                  name={[
                    'vigilanciaAlimentosRuminantes',
                    'descricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes',
                  ]}
                  rules={[
                    {
                      required: true,
                      message: 'O campo é obrigatório',
                    },
                  ]}
                >
                  <TextArea showCount rows={5} maxLength={2000} />
                </Form.Item>
              </Col>

              <Col xs={24} lg={24}>
                <Form.Item
                  label={'Outras observações'}
                  name={['vigilanciaAlimentosRuminantes', 'outrasObservacoes']}
                >
                  <TextArea showCount rows={5} maxLength={2000} />
                </Form.Item>
              </Col>
            </Row>
          </>
        )}

        {!isInvestigacaoDeAlimentosLiberada() && (
          <div
            style={{
              padding: 24,
              border: '1px solid ',
              textAlign: 'center',
            }}
          >
            <Typography.Title level={5}>
              Para informar dados da investigação de alimentos para ruminantes
              se faz necessário informar a chave primária:
              <span style={{ fontWeight: 'bold' }}>
                {' '}
                PREVENÇÃO E CONTROLE DAS ENCEFALOPATIAS ESPONGIFORMES
                TRANSMISSÍVEIS
              </span>
            </Typography.Title>
          </div>
        )}
      </fieldset>
    </>
  );
}
