import { useEffect, useState } from 'react';
import {
  Card,
  Col,
  Divider,
  Form,
  FormInstance,
  Image,
  Input,
  Row,
  Select,
  Switch,
  Tooltip,
  Typography,
} from 'antd';
import { TipoDestinoCadaveres } from '../../../core/enums/TipoDestinoCadaveres';
import SintomasObservadosPanel from '../../components/SintomasObservadosPanel';
import { SintomasObservados } from '../../../core/enums/SintomasObservados';
import { InfoCircleOutlined } from '@ant-design/icons';

type VigilanciaSuideosProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaSuideos(props: VigilanciaSuideosProps) {
  const [showModalImagemSuinoAsselvajado, setShowModalImagemSuinoAsselvajado] =
    useState(false);
  const listaTipoDestinoCadaveres = TipoDestinoCadaveres.keys();

  const triggerRecountTotaExaminadosSuideos = () => {
    const _00_30_Femeas: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionadosAte_30_F',
    ]);
    const _00_30_Machos: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionadosAte_30_M',
    ]);
    const _31_60_Femeas: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionados31_60_F',
    ]);
    const _31_60_Machos: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionados31_60_M',
    ]);
    const _61_180_Femeas: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionados61_180_F',
    ]);
    const _61_180_Machos: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionados61_180_M',
    ]);
    const _180_AcimaFemeas: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionadosAcima180_F',
    ]);
    const _180_AcimaMachos: number = props.form.getFieldValue([
      'vigilanciaSuideos',
      'quantidadeInspecionadosAcima180_M',
    ]);

    const dados = [
      _00_30_Femeas,
      _00_30_Machos,
      _31_60_Femeas,
      _31_60_Machos,
      _61_180_Femeas,
      _61_180_Machos,
      _180_AcimaFemeas,
      _180_AcimaMachos,
    ];

    let totalExaminados: number = 0;

    dados.forEach((qtdade) => {
      if (!isNaN(Number(qtdade))) {
        totalExaminados = Number(totalExaminados) + Number(qtdade);

        props.form.setFieldValue(
          ['vigilanciaSuideos', 'quantidadeExaminados'],
          totalExaminados
        );
      }
    });
  };

  useEffect(() => {
    props.form.setFieldValue(
      ['vigilanciaSuideos', 'presencaDeSuinosAsselvajados'],
      props.form.getFieldValue([
        'vigilanciaSuideos',
        'presencaDeSuinosAsselvajados',
      ]) === 'SIM'
        ? true
        : false
    );

    props.form.setFieldValue(
      ['vigilanciaSuideos', 'contatoDiretoDeSuinosDomesticosComAsselvajados'],
      props.form.getFieldValue([
        'vigilanciaSuideos',
        'contatoDiretoDeSuinosDomesticosComAsselvajados',
      ]) === 'SIM'
        ? true
        : false
    );
  }, [props.form]);
  return (
    <>
      <Typography.Title level={5}>Suídeos examinados</Typography.Title>
      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Até 30'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaSuideos',
                    'quantidadeInspecionadosAte_30_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaSuideos',
                    'quantidadeInspecionadosAte_30_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='31 a 60 dias'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={['vigilanciaSuideos', 'quantidadeInspecionados31_60_F']}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={['vigilanciaSuideos', 'quantidadeInspecionados31_60_M']}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='61 a 180 dias'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaSuideos',
                    'quantidadeInspecionados61_180_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaSuideos',
                    'quantidadeInspecionados61_180_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Acima de 180 dias'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaSuideos',
                    'quantidadeInspecionadosAcima180_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaSuideos',
                    'quantidadeInspecionadosAcima180_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={6}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosSuideos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Total'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Examinados'}
                  name={['vigilanciaSuideos', 'quantidadeExaminados']}
                >
                  <Input disabled />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Tooltip
                  title={
                    'Preencher com todos os animais observados, inclusive os examinados'
                  }
                >
                  <Form.Item
                    label={'Vistoriados'}
                    name={['vigilanciaSuideos', 'quantidadeVistoriados']}
                  >
                    <Input
                      inputMode='numeric'
                      maxLength={6}
                      autoComplete='off'
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Tooltip>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row gutter={24}>
        <SintomasObservadosPanel
          listaSintomasObservados={SintomasObservados.suinos()}
          pathToCoordenada={'vigilanciaSuideos'}
          form={props.form}
        />

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há suspeita clínica?'}
            name={['vigilanciaSuideos', 'haSuspeitaClinica']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={16}>
          <Form.Item
            label={'Suspeita clinica'}
            name={['vigilanciaSuideos', 'suspeitaClinica']}
          >
            <Input autoComplete='off' />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={24}>
        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há presença de suínos asselvajados na propriedade?'}
            name={['vigilanciaSuideos', 'presencaDeSuinosAsselvajados']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={
              <div>
                Há contato direto dos suínos domésticos com suínos asselvajados?{' '}
                <InfoCircleOutlined
                  style={{
                    color: '#337ab7',
                    zIndex: 5,
                    position: 'absolute',
                    padding: '5px',
                  }}
                  onClick={(e) => {
                    e.preventDefault();
                    setShowModalImagemSuinoAsselvajado(true);
                  }}
                />
              </div>
            }
            name={[
              'vigilanciaSuideos',
              'contatoDiretoDeSuinosDomesticosComAsselvajados',
            ]}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Destino dos cadáveres'}
            name={['vigilanciaSuideos', 'tipoDestinoCadaveres']}
          >
            <Select
              placeholder={'Selecione um destino'}
              placement={'bottomLeft'}
              disabled={props.formDisabled}
            >
              {listaTipoDestinoCadaveres?.map((destino) => (
                <Select.Option key={destino} value={destino}>
                  {TipoDestinoCadaveres.valueOf(destino)}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>

      {/* <CustomModal
        open={showModalImagemSuinoAsselvajado}
        title={'Suíno asselvajado'}
        closable={true}
        maskClosable={true}
        onCancel={() => setShowModalImagemSuinoAsselvajado(false)}
        footer={
          <Button
            type='default'
            onClick={() => {
              setShowModalImagemSuinoAsselvajado(false);
            }}
            style={{ width: '100%' }}
          >
            Fechar
          </Button>
        }
      > */}
      <Image
        style={{ display: 'none' }}
        fallback='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
        preview={{
          visible: showModalImagemSuinoAsselvajado,
          src: '/DiferenciacaoSuideos.png',
          onVisibleChange: (value) => {
            setShowModalImagemSuinoAsselvajado(false);
          },
        }}
      />
      {/* </CustomModal> */}
    </>
  );
}
