import {
  Affix,
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Drawer,
  Form,
  Input,
  Modal,
  notification,
  Row,
  Space,
  Steps,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { useCallback, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import useLoadingPage from '../../../core/hooks/useLoadingPage';
import { FVV } from '../../../sdk/@types/FVV';
import {
  InvestigacaoEpidemiologica,
  VigilanciaAves,
  VigilanciaPeixes,
  FVER,
} from '../../../sdk/@types';
import moment from 'moment';
import useFVER from '../../../core/hooks/useFVER';
import useFVV from '../../../core/hooks/useFVV';
import useNavigatorStatus from '../../../core/hooks/useNavigatorStatus';
import { useDispatch, useSelector } from 'react-redux';
import * as FVERActions from '../../../core/store/FVER.slice';
import { RootState } from '../../../core/store';
import { TipoEstabelecimento } from '../../../core/enums/TipoEstabelecimento';
import { CloseOutlined, SelectOutlined } from '@ant-design/icons';
import FVERService from '../../../sdk/services/SIZ-API/FVER.service';
import AuthorizationService from '../../../core/auth/Authorization.service';
import VeterinarioIDBService from '../../../sdk/services/indexeddb/VeterinarioIDB.service';
import VigilanciaInvestigacaoDeAlimentos from './VigilanciaInvestigacaoDeAlimentos';
import VigilanciaBovinos from './VigilanciaBovinos';
import VigilanciaBubalinos from './VigilanciaBubalinos';
import VigilanciaCaprinos from './VigilanciaCaprinos';
import VigilanciaOvinos from './VigilanciaOvinos';
import VigilanciaSuideos from './VigilanciaSuideos';
import VigilanciaAvesPanel from './VigilanciaAves';
import VigilanciaPeixesPanel from './VigilanciaPeixes';
import VigilanciaEquinos from './VigilanciaEquinos';
import VigilanciaAsininos from './VigilanciaAsininos';
import VigilanciaMuares from './VigilanciaMuares';
import VigilanciaAbelhas from './VigilanciaAbelhas';
import VigilanciaHistoricoMordedura from './VigilanciaHistoricoMordedura';
import VigilanciaInvestigacaoEpidemiologica from './VigilanciaInvestigacaoEpidemiologica';
import VigilanciaOutrasInformacoes from './VigilanciaOutrasInformacoes';
import PanelViewFVER from '../FVER/PanelViewFVER';
import FilterPanelFVER from '../FVER/FilterPanelFVER';
import FVVService from '../../../sdk/services/SIZ-API/FVV.service';
import { CustomModal } from '../../components/CustomModal';

export type FormFVVType = {
  modalInvestigacaoEpimiologica?: InvestigacaoEpidemiologica.Input;
  modalVigilanciaAves?: VigilanciaAves.Input;
  modalVigilanciaPeixes?: VigilanciaPeixes.Input;
} & FVV.Input;

type FormFVVProps = {
  fvv?: FormFVVType;
  fverOffline?: FVER.Input;
};

export default function FormFVV(props: FormFVVProps) {
  const { online } = useNavigatorStatus();
  const dispatch = useDispatch();

  const fvers = useSelector((state: RootState) => state.fver.fvers);
  const query = useSelector((state: RootState) => state.fver.query);
  const fetching = useSelector((state: RootState) => state.fver.fetching);

  const [error, setError] = useState<Error>();

  const params = useParams<{ id: string; local?: string; problem?: string }>();
  const [form] = Form.useForm<FormFVVType>();
  const { xs, lg } = useBreakpoint();
  const navigate = useNavigate();
  const [formDisabled, setFormDisabled] = useState<boolean>(false);
  const { mode, firstOpening, setFirstOpening, setLoading } = useLoadingPage();

  const { insert, updateFVVOffline } = useFVV();
  const { fetchFVERById, fver: visita } = useFVER();

  const [showModalFinalizacao, setShowModalFinalizacao] =
    useState<boolean>(false);
  const [fverNaoEncontrado, setFVERNaoEncontrado] = useState(false);

  const [dadosFVVModalFinalizacao, setDadosFVVModalFinalizacao] = useState<{
    id?: string;
    numero: string;
  }>();

  type StateStep = {
    step: number;
    state: boolean;
  };

  const [stateStep_0, setStateStep_0] = useState<StateStep>({
    step: 0,
    state: false,
  });
  const [stateStep_1, setStateStep_1] = useState<StateStep>({
    step: 1,
    state: false,
  });
  const [stateStep_2, setStateStep_2] = useState<StateStep>({
    step: 2,
    state: false,
  });
  const [stateStep_3, setStateStep_3] = useState<StateStep>({
    step: 3,
    state: false,
  });
  const [stateStep_4, setStateStep_4] = useState<StateStep>({
    step: 4,
    state: false,
  });
  const [stateStep_5, setStateStep_5] = useState<StateStep>({
    step: 5,
    state: false,
  });
  const [stateStep_6, setStateStep_6] = useState<StateStep>({
    step: 6,
    state: false,
  });
  const [stateStep_7, setStateStep_7] = useState<StateStep>({
    step: 7,
    state: false,
  });
  const [stateStep_8, setStateStep_8] = useState<StateStep>({
    step: 8,
    state: false,
  });
  const [stateStep_9, setStateStep_9] = useState<StateStep>({
    step: 9,
    state: false,
  });
  const [stateStep_10, setStateStep_10] = useState<StateStep>({
    step: 10,
    state: false,
  });
  const [stateStep_11, setStateStep_11] = useState<StateStep>({
    step: 11,
    state: false,
  });
  const [stateStep_12, setStateStep_12] = useState<StateStep>({
    step: 12,
    state: false,
  });
  const [stateStep_13, setStateStep_13] = useState<StateStep>({
    step: 13,
    state: false,
  });
  const [stateStep_14, setStateStep_14] = useState<StateStep>({
    step: 14,
    state: false,
  });
  const [stateStep_15, setStateStep_15] = useState<StateStep>({
    step: 15,
    state: false,
  });

  useEffect(() => {}, [online]);

  useEffect(() => {
    setFirstOpening(true);
  }, []);

  useEffect(() => {
    if (!params.local)
      if (mode === 'EDIT' || mode === 'VIEW') {
        fetchFVERById(
          //@ts-ignore
          props.fvv?.visitaPropriedadeRural?.id
        ).catch((e) => {
          setError(new Error(e.message));
        });
      }
  }, [mode]);

  useEffect(() => {
    if (params.local) {
      setLoading(false);
      if (firstOpening)
        if (params.problem) {
          setFVERNaoEncontrado(true);
          setLoading(false);
        }
    }

    form.setFieldValue(
      'emissaoDeFormIN',
      form.getFieldValue('emissaoDeFormIN') === 'SIM' ? true : false
    );

    form.setFieldValue(
      'historicoDeSugadurasDeMorcegos',
      form.getFieldValue('historicoDeSugadurasDeMorcegos') === 'SIM'
        ? true
        : false
    );
  }, [params, props.fvv, setLoading, form]);

  useEffect(() => {
    setFormDisabled(mode === 'VIEW' ? true : false);

    if (props.fverOffline) {
      //@ts-ignore
      form.setFieldsValue({ visitaPropriedadeRural: props.fverOffline });

      form.setFieldsValue({
        //@ts-ignore
        dataVigilancia: moment(props.fverOffline.dataDaVisita),
        visitaPropriedadeRural: {
          ...props.fverOffline,
          dataDaVisita: moment(props.fverOffline.dataDaVisita).format(
            'DD/MM/YYYY'
          ),
          //@ts-ignore
          nomeEstabelecimento:
            props.fverOffline.tipoEstabelecimento === 'PROPRIEDADE'
              ? props.fverOffline.propriedade?.nome
              : props.fverOffline.tipoEstabelecimento === 'ABATEDOURO'
              ? props.fverOffline.abatedouro?.pessoa.nome
              : props.fverOffline.recinto?.pessoa.nome,
          //@ts-ignore
          codigoEstabelecimento:
            props.fverOffline.tipoEstabelecimento === 'PROPRIEDADE'
              ? props.fverOffline.propriedade?.id
              : props.fverOffline.tipoEstabelecimento === 'ABATEDOURO'
              ? props.fverOffline.abatedouro?.id
              : props.fverOffline.recinto?.id,
          numero: props.fverOffline.numero ? props.fverOffline.numero : '-',
          id: props.fverOffline.id ? props.fverOffline.id : -1,
          local: moment(props.fverOffline.dataDaVisita)
            .format('DD/MM/YYYY')
            .concat('-')
            .concat(props.fverOffline.tipoEstabelecimento)
            .concat(' - ')
            .concat(props.fverOffline.propriedade?.nome + ''),
        },
        //@ts-ignore
        proprietario: props.fverOffline.proprietario,
        propriedade: props.fverOffline.propriedade,
        setor: props.fverOffline.setor,
        exploracao: props.fverOffline.exploracao,
      });
      setLoading(false);
    } else {
      if (!params.local)
        if (mode === 'CREATE') {
          //procurar visita on line
          fetchFVERById(Number(params.id))
            .catch((e) => {
              setError(new Error(e.message));
            })
            .finally(() => {
              setLoading(false);
            });
        }
    }

    if (mode === 'EDIT' || mode === 'VIEW') {
      setStateStep_0({
        step: 0,
        state: true,
      });
      setStateStep_1({
        step: 1,
        state: true,
      });
      setStateStep_2({
        step: 2,
        state: true,
      });
      setStateStep_3({
        step: 3,
        state: true,
      });
      setStateStep_4({
        step: 4,
        state: true,
      });
      setStateStep_5({
        step: 5,
        state: true,
      });
      setStateStep_6({
        step: 6,
        state: true,
      });
      setStateStep_7({
        step: 7,
        state: true,
      });
      setStateStep_8({
        step: 8,
        state: true,
      });
      setStateStep_9({
        step: 9,
        state: true,
      });
      setStateStep_10({
        step: 10,
        state: true,
      });
      setStateStep_11({
        step: 11,
        state: true,
      });
      setStateStep_12({
        step: 12,
        state: true,
      });
      setStateStep_13({
        step: 13,
        state: true,
      });
      setStateStep_14({
        step: 14,
        state: true,
      });
      setStateStep_15({
        step: 15,
        state: true,
      });
    }
  }, [fetchFVERById, mode, params]);

  useEffect(() => {
    if (visita) {
      //@ts-ignore
      form.setFieldsValue({ visitaPropriedadeRural: visita });

      form.setFieldsValue({
        //@ts-ignore
        dataVigilancia: moment(visita.dataDaVisita),
        visitaPropriedadeRural: {
          ...visita,
          dataDaVisita: moment(visita.dataDaVisita).format('DD/MM/YYYY'),
          //@ts-ignore
          nomeEstabelecimento:
            visita.tipoEstabelecimento === 'PROPRIEDADE'
              ? visita.propriedade?.nome
              : visita.tipoEstabelecimento === 'ABATEDOURO'
              ? visita.abatedouro?.pessoa.nome
              : visita.recinto?.pessoa.nome,
          //@ts-ignore
          codigoEstabelecimento:
            visita.tipoEstabelecimento === 'PROPRIEDADE'
              ? visita.propriedade?.id
              : visita.tipoEstabelecimento === 'ABATEDOURO'
              ? visita.abatedouro?.id
              : visita.recinto?.id,
          numero: visita.numero ? visita.numero : '-',
          id: visita.id ? visita.id : -1,
          local: moment(visita.dataDaVisita)
            .format('DD/MM/YYYY')
            .concat('-')
            .concat(visita.tipoEstabelecimento)
            .concat(' - ')
            .concat(visita.propriedade?.nome + ''),
        },
        //@ts-ignore
        proprietario: visita.proprietario,
        propriedade: visita.propriedade,
        setor: visita.setor,
        exploracao: visita.exploracao,
      });
      setLoading(false);
    }
  }, [setLoading, visita, form]);

  useEffect(() => {
    const insertLoggedUser = async () => {
      if (mode === 'CREATE') {
        if (AuthorizationService.getUsuarioSIZ()) {
          await VeterinarioIDBService.getByCpf(
            //@ts-ignore
            AuthorizationService.getUsuarioSIZ()?.cpf.replace(/\D/g, '')
          ).then((veterinario) => {
            if (veterinario) {
              form.setFieldsValue({
                veterinario: veterinario.payload,
              });
            }
          });
        }
      }
    };

    insertLoggedUser();
  }, []);

  const handleFormSubmit = useCallback(
    async (formParam: FormFVVType) => {
      form.validateFields().then(() => {
        setStateStep_15({
          step: 15,
          state: true,
        });
      });

      if (
        stateStep_0.state &&
        stateStep_1.state &&
        stateStep_2.state &&
        stateStep_3.state &&
        stateStep_4.state &&
        stateStep_5.state &&
        stateStep_6.state &&
        stateStep_7.state &&
        stateStep_8.state &&
        stateStep_9.state &&
        stateStep_10.state &&
        stateStep_11.state &&
        stateStep_12.state &&
        stateStep_13.state &&
        stateStep_14.state
      ) {
      } else {
        notification.error({
          message: 'Algumas etapas não foram finalizadas',
          description: 'Por favor revise o formulário',
        });
        return;
      }

      /**
       * Por algum motivo (suspeito que seja por causa do form
       * englobando todos os steps), os valores do form não estão
       * armazenados no formParam, apenas no form.
       * Então foi preciso copiar manualmente cada campo
       */

      const data = form.getFieldValue('dataVigilancia');
      const investigacoesEpidemiologicas: InvestigacaoEpidemiologica.Input[] =
        form.getFieldValue('investigacoesEpidemiologicas');

      let investigacoesEpidemiologicasFormatado;
      if (investigacoesEpidemiologicas)
        investigacoesEpidemiologicasFormatado =
          investigacoesEpidemiologicas.map(
            (investigacao: InvestigacaoEpidemiologica.Input) => {
              return {
                ...investigacao,
                data: JSON.stringify(investigacao.data).replaceAll('"', ''),
              };
            }
          );
      else investigacoesEpidemiologicasFormatado = undefined;

      const fvvDTO: FVV.Input = {
        ...formParam,
        id: form.getFieldValue('id'),
        dataVigilancia: JSON.stringify(data).replaceAll('"', ''),
        emissaoDeFormIN:
          form.getFieldValue('emissaoDeFormIN') === true ? 'SIM' : 'NAO',
        exploracao: form.getFieldValue('exploracao'),
        historicoDeSugadurasDeMorcegos: form.getFieldValue(
          'historicoDeSugadurasDeMorcegos'
        )
          ? 'SIM'
          : 'NAO',
        investigacoesEpidemiologicas: investigacoesEpidemiologicasFormatado,
        motivosVigilanciaVeterinaria: form.getFieldValue(
          'motivosVigilanciaVeterinaria'
        ),
        numero: form.getFieldValue('numero'),
        observacao: form.getFieldValue('observacao'),
        pontosDeRisco: form.getFieldValue('pontosDeRisco'),
        propriedade: form.getFieldValue('propriedade'),
        proprietario: form.getFieldValue('proprietario'),
        quantidadeSugadurasEmAves: form.getFieldValue(
          'quantidadeSugadurasEmAves'
        ),
        quantidadeSugadurasEmBovinos: form.getFieldValue(
          'quantidadeSugadurasEmBovinos'
        ),
        quantidadeSugadurasEmBubalinos: form.getFieldValue(
          'quantidadeSugadurasEmBubalinos'
        ),
        quantidadeSugadurasEmCaprinos: form.getFieldValue(
          'quantidadeSugadurasEmCaprinos'
        ),
        quantidadeSugadurasEmEquideos: form.getFieldValue(
          'quantidadeSugadurasEmEquideos'
        ),
        quantidadeSugadurasEmSuideos: form.getFieldValue(
          'quantidadeSugadurasEmSuideos'
        ),
        setor: form.getFieldValue('setor'),
        tipoEstabelecimento: form.getFieldValue('tipoEstabelecimento'),
        veterinario: form.getFieldValue('veterinario'),
        //ok
        vigilanciaAlimentosRuminantes: {
          ...form.getFieldValue('vigilanciaAlimentosRuminantes'),
          presencaDeCamaDeAviario:
            form.getFieldValue([
              'vigilanciaAlimentosRuminantes',
              'presencaDeCamaDeAviario',
            ]) === true
              ? 'SIM'
              : 'NAO',
          utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes:
            form.getFieldValue([
              'vigilanciaAlimentosRuminantes',
              'utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes',
            ]) === true
              ? 'SIM'
              : 'NAO',
          pisciculturaComSistemaDeAlimentacaoABaseDeRacao:
            form.getFieldValue([
              'vigilanciaAlimentosRuminantes',
              'pisciculturaComSistemaDeAlimentacaoABaseDeRacao',
            ]) === true
              ? 'SIM'
              : 'NAO',
          contaminacaoCruzadaDeRacoesDePeixeERuminante:
            form.getFieldValue([
              'vigilanciaAlimentosRuminantes',
              'contaminacaoCruzadaDeRacoesDePeixeERuminante',
            ]) === true
              ? 'SIM'
              : 'NAO',
          colheitaDeAmostraDeAlimentosDeRuminantes:
            form.getFieldValue([
              'vigilanciaAlimentosRuminantes',
              'colheitaDeAmostraDeAlimentosDeRuminantes',
            ]) === true
              ? 'SIM'
              : 'NAO',
          usoTesteRapido:
            form.getFieldValue([
              'vigilanciaAlimentosRuminantes',
              'usoTesteRapido',
            ]) === true
              ? 'SIM'
              : 'NAO',
        },
        //ok
        vigilanciaAves: form.getFieldValue('vigilanciaAves'),

        //ok
        vigilanciaBovinos: {
          ...form.getFieldValue('vigilanciaBovinos'),
        },
        //ok
        vigilanciaBubalino: {
          ...form.getFieldValue('vigilanciaBubalino'),
        },
        //ok
        vigilanciaCaprino: {
          ...form.getFieldValue('vigilanciaCaprino'),
        },
        vigilanciaOutrasEspecies: form.getFieldValue(
          'vigilanciaOutrasEspecies'
        ),
        //ok
        vigilanciaOvino: {
          ...form.getFieldValue('vigilanciaOvino'),
        },
        //ok
        vigilanciaPeixes: {
          ...form.getFieldValue('vigilanciaPeixes'),
        },
        //ok
        vigilanciaSuideos: {
          ...form.getFieldValue('vigilanciaSuideos'),
          presencaDeSuinosAsselvajados:
            form.getFieldValue([
              'vigilanciaSuideos',
              'presencaDeSuinosAsselvajados',
            ]) === true
              ? 'SIM'
              : 'NAO',
          contatoDiretoDeSuinosDomesticosComAsselvajados:
            form.getFieldValue([
              'vigilanciaSuideos',
              'contatoDiretoDeSuinosDomesticosComAsselvajados',
            ]) === true
              ? 'SIM'
              : 'NAO',
        },
        //ok
        vigilanciaAbelha: {
          ...form.getFieldValue('vigilanciaAbelha'),
        },
        //ok
        vigilanciaAsinino: {
          ...form.getFieldValue('vigilanciaAsinino'),
        },
        //ok
        vigilanciaEquino: {
          ...form.getFieldValue('vigilanciaEquino'),
        },
        //ok
        vigilanciaMuar: {
          ...form.getFieldValue('vigilanciaMuar'),
        },
        visitaPropriedadeRural: {
          ...form.getFieldValue('visitaPropriedadeRural'),
          dataDaVisita: undefined,
        },
      };

      //edita localmente se for local
      if (params.local) {
        updateFVVOffline(Number(params.id), fvvDTO)
          .then((fvv) => {
            setDadosFVVModalFinalizacao({
              id: params.id,
              numero: '',
            });

            setShowModalFinalizacao(true);
          })
          .catch(console.log);
      } else {
        setLoading(true);
        await FVVService.save(fvvDTO)
          .then((fvv) => {
            setDadosFVVModalFinalizacao({
              //@ts-ignore
              id: fvv.id,
              //@ts-ignore
              numero: fvv.numero,
            });

            setLoading(false);
            setShowModalFinalizacao(true);
          })
          .catch((e) => {
            setLoading(false);
            notification.error({
              message: 'Houve um erro ao salvar os dados',
              description: e.message,
            });
          })
          .finally(() => {
            setLoading(false);
          });
      }
    },
    [
      stateStep_0,
      stateStep_1,
      stateStep_2,
      stateStep_3,
      stateStep_4,
      stateStep_5,
      stateStep_6,
      stateStep_7,
      stateStep_8,
      stateStep_9,
      stateStep_10,
      stateStep_11,
      stateStep_12,
      stateStep_13,
      stateStep_14,
      stateStep_15,
    ]
  );

  /* Controle Steps */
  const { Step } = Steps;
  const [current, setCurrent] = useState(0);
  const next = () => {
    setCurrent(current + 1);
  };
  const prev = () => {
    setCurrent(current - 1);
  };
  enum content {
    VISITA,
    INVESTIGACAO_DE_ALIMENTOS_RUMINANTES,
    BOVINOS,
    BUBALINOS,
    CAPRINOS,
    OVINOS,
    SUIDEOS,
    AVES,
    PEIXES,
    EQUINOS,
    ASININOS,
    MUARES,
    ABELHAS,
    HISTORICO_MORDEDURA,
    INVESTIGACAO_EPIDEMIOLOGICA,
    OUTRAS_INFORMACOES,
  }
  const steps = [
    {
      title: 'Visita',
      content: content.VISITA,
    },
    {
      title: 'Investigação de alimentos',
      content: content.INVESTIGACAO_DE_ALIMENTOS_RUMINANTES,
    },
    {
      title: 'Bovinos',
      content: content.BOVINOS,
    },
    {
      title: 'Bubalinos',
      content: content.BUBALINOS,
    },
    {
      title: 'Caprinos',
      content: content.CAPRINOS,
    },
    {
      title: 'Ovinos',
      content: content.OVINOS,
    },
    {
      title: 'Suídeos',
      content: content.SUIDEOS,
    },
    {
      title: 'Aves',
      content: content.AVES,
    },
    {
      title: 'Peixes',
      content: content.PEIXES,
    },
    {
      title: 'Equinos',
      content: content.EQUINOS,
    },
    {
      title: 'Asininos',
      content: content.ASININOS,
    },
    {
      title: 'Muares',
      content: content.MUARES,
    },
    {
      title: 'Abelhas',
      content: content.ABELHAS,
    },
    {
      title: 'Histórico de mordedura de morcegos',
      content: content.HISTORICO_MORDEDURA,
    },
    {
      title: 'Investigação epidemiológica',
      content: content.INVESTIGACAO_EPIDEMIOLOGICA,
    },
    {
      title: 'Outras informações',
      content: content.OUTRAS_INFORMACOES,
    },
  ];

  const getStatus = (atual: any) => {
    let state = false;

    if (atual === 0) {
      state = stateStep_0.state;
    } else if (atual === 1) {
      state = stateStep_1.state;
    } else if (atual === 2) {
      state = stateStep_2.state;
    } else if (atual === 3) {
      state = stateStep_3.state;
    } else if (atual === 4) {
      state = stateStep_4.state;
    } else if (atual === 5) {
      state = stateStep_5.state;
    } else if (atual === 6) {
      state = stateStep_6.state;
    } else if (atual === 7) {
      state = stateStep_7.state;
    } else if (atual === 8) {
      state = stateStep_8.state;
    } else if (atual === 9) {
      state = stateStep_9.state;
    } else if (atual === 10) {
      state = stateStep_10.state;
    } else if (atual === 11) {
      state = stateStep_11.state;
    } else if (atual === 12) {
      state = stateStep_12.state;
    } else if (atual === 13) {
      state = stateStep_13.state;
    } else if (atual === 14) {
      state = stateStep_14.state;
    } else if (atual === 15) {
      state = stateStep_15.state;
    }

    let v: 'process' | 'wait' | 'finish' | 'error' = 'wait';
    if (current === atual) v = 'process';
    else if (state) v = 'finish';
    else if (current > atual) v = 'error';
    else v = 'wait';

    return v;
  };

  const stepsMenu = (
    direction: 'vertical' | 'horizontal',
    labelPlacement: 'vertical' | 'horizontal'
  ) => {
    return (
      <Steps
        current={current}
        direction={direction}
        labelPlacement={labelPlacement}
        onChange={(value: number) => {
          if (value < current) {
            setCurrent(value);
          } else {
            form
              .validateFields()
              .then(() => {
                if (current === stateStep_0.step)
                  setStateStep_0({ step: 0, state: true });
                else if (current === stateStep_1.step)
                  setStateStep_1({ step: 1, state: true });
                else if (current === stateStep_2.step)
                  setStateStep_2({ step: 2, state: true });
                else if (current === stateStep_3.step)
                  setStateStep_3({ step: 3, state: true });
                else if (current === stateStep_4.step)
                  setStateStep_4({ step: 4, state: true });
                else if (current === stateStep_5.step)
                  setStateStep_5({ step: 5, state: true });
                else if (current === stateStep_6.step)
                  setStateStep_6({ step: 6, state: true });
                else if (current === stateStep_7.step)
                  setStateStep_7({ step: 7, state: true });
                else if (current === stateStep_8.step)
                  setStateStep_8({ step: 8, state: true });
                else if (current === stateStep_9.step)
                  setStateStep_9({ step: 9, state: true });
                else if (current === stateStep_10.step)
                  setStateStep_10({ step: 10, state: true });
                else if (current === stateStep_11.step)
                  setStateStep_11({ step: 11, state: true });
                else if (current === stateStep_12.step)
                  setStateStep_12({ step: 12, state: true });
                else if (current === stateStep_13.step)
                  setStateStep_13({ step: 13, state: true });
                else if (current === stateStep_14.step)
                  setStateStep_14({ step: 14, state: true });
                else if (current === stateStep_15.step)
                  setStateStep_15({ step: 15, state: true });

                setCurrent(value);
                window.location.replace('#');
              })
              .catch(() => {
                notification.error({
                  message: 'Alguns campos obrigatórios não foram preenchidos',
                  description: 'Por favor revise o formulário',
                });
              });
          }
        }}
        style={{ paddingBottom: '24px' }}
      >
        {steps.map((item, atual) => (
          /* atual > current - 5 &&
            atual < current + 5 && */
          <Step
            key={item.title}
            title={item.title}
            status={getStatus(atual)}
            onClick={() => {
              onClose();
            }}
          />
        ))}
      </Steps>
    );
  };
  const [open, setOpen] = useState<boolean>(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Form<FormFVVType>
        layout={'vertical'}
        form={form}
        size={!xs ? 'small' : 'large'}
        onFinish={handleFormSubmit}
        autoComplete={'off'}
        onFinishFailed={() => {
          notification.error({
            message: 'Alguns campos obrigatórios não foram preenchidos',
            description: 'Por favor revise o formulário',
          });
        }}
        initialValues={props.fvv}
      >
        {lg && stepsMenu('horizontal', 'vertical')}
        {!lg && (
          <Affix offsetTop={80}>
            <Button
              className='top'
              type='primary'
              onClick={() => {
                showDrawer();
              }}
              block
            >
              Ir para etapa
            </Button>
          </Affix>
        )}
        <Divider />

        <Drawer
          title='Etapas'
          placement='right'
          onClose={onClose}
          visible={open}
        >
          {stepsMenu('vertical', 'horizontal')}
        </Drawer>

        <div id='top' className='top' />
        <div>
          <fieldset disabled={formDisabled}>
            {steps[current].content === content.VISITA && (
              <>
                <Row gutter={24}>
                  <Form.Item name={'id'} hidden>
                    <Input disabled hidden />
                  </Form.Item>
                  <Form.Item name={'codigoVerificador'} hidden>
                    <Input disabled hidden />
                  </Form.Item>
                  <Col xs={24} lg={8}>
                    <Form.Item label={'Número'} name={['numero']}>
                      <Input disabled />
                    </Form.Item>
                  </Col>

                  <Col xs={24} lg={8}>
                    <Form.Item label={'Data'} name={['dataVigilancia']}>
                      <DatePicker
                        disabled
                        format={'DD/MM/YYYY'}
                        style={{ width: '100%' }}
                      />
                    </Form.Item>
                  </Col>
                </Row>

                <PanelViewFVER form={form} formDisabled={false} />
              </>
            )}

            {steps[current].content ===
              content.INVESTIGACAO_DE_ALIMENTOS_RUMINANTES && (
              <VigilanciaInvestigacaoDeAlimentos
                form={form}
                formDisabled={formDisabled}
              />
            )}

            {steps[current].content === content.BOVINOS && (
              <VigilanciaBovinos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.BUBALINOS && (
              <VigilanciaBubalinos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.CAPRINOS && (
              <VigilanciaCaprinos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.OVINOS && (
              <VigilanciaOvinos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.SUIDEOS && (
              <VigilanciaSuideos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.AVES && (
              <VigilanciaAvesPanel form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.PEIXES && (
              <VigilanciaPeixesPanel form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.EQUINOS && (
              <VigilanciaEquinos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.ASININOS && (
              <VigilanciaAsininos form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.MUARES && (
              <VigilanciaMuares form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.ABELHAS && (
              <VigilanciaAbelhas form={form} formDisabled={formDisabled} />
            )}

            {steps[current].content === content.HISTORICO_MORDEDURA && (
              <VigilanciaHistoricoMordedura
                form={form}
                formDisabled={formDisabled}
              />
            )}

            {steps[current].content === content.INVESTIGACAO_EPIDEMIOLOGICA && (
              <VigilanciaInvestigacaoEpidemiologica
                form={form}
                formDisabled={formDisabled}
              />
            )}

            {steps[current].content === content.OUTRAS_INFORMACOES && (
              <VigilanciaOutrasInformacoes
                form={form}
                formDisabled={formDisabled}
              />
            )}
          </fieldset>
        </div>
        <Divider />

        <Row justify={'end'} gutter={24}>
          {current > 0 && (
            <Col xs={12}>
              <Button
                style={{ width: '100%', height: '40px' }}
                onClick={() => {
                  prev();
                  window.location.replace('#');
                }}
              >
                Anterior
              </Button>
            </Col>
          )}
          {current < steps.length - 1 && (
            <Col xs={12}>
              <Button
                type='primary'
                style={{ width: '100%', height: '40px' }}
                onClick={() => {
                  form
                    .validateFields()
                    .then(() => {
                      if (current === stateStep_0.step)
                        setStateStep_0({ step: 0, state: true });
                      else if (current === stateStep_1.step)
                        setStateStep_1({ step: 1, state: true });
                      else if (current === stateStep_2.step)
                        setStateStep_2({ step: 2, state: true });
                      else if (current === stateStep_3.step)
                        setStateStep_3({ step: 3, state: true });
                      else if (current === stateStep_4.step)
                        setStateStep_4({ step: 4, state: true });
                      else if (current === stateStep_5.step)
                        setStateStep_5({ step: 5, state: true });
                      else if (current === stateStep_6.step)
                        setStateStep_6({ step: 6, state: true });
                      else if (current === stateStep_7.step)
                        setStateStep_7({ step: 7, state: true });
                      else if (current === stateStep_8.step)
                        setStateStep_8({ step: 8, state: true });
                      else if (current === stateStep_9.step)
                        setStateStep_9({ step: 9, state: true });
                      else if (current === stateStep_10.step)
                        setStateStep_10({ step: 10, state: true });
                      else if (current === stateStep_11.step)
                        setStateStep_11({ step: 11, state: true });
                      else if (current === stateStep_12.step)
                        setStateStep_12({ step: 12, state: true });
                      else if (current === stateStep_13.step)
                        setStateStep_13({ step: 13, state: true });
                      else if (current === stateStep_14.step)
                        setStateStep_14({ step: 14, state: true });
                      else if (current === stateStep_15.step)
                        setStateStep_15({ step: 15, state: true });

                      next();
                      window.location.replace('#');
                    })
                    .catch(() => {
                      notification.error({
                        message:
                          'Alguns campos obrigatórios não foram preenchidos',
                        description: 'Por favor revise o formulário',
                      });
                    });
                }}
              >
                Próximo
              </Button>
            </Col>
          )}
          {current === steps.length - 1 && !formDisabled && (
            <Col xs={12}>
              <Button
                type='primary'
                htmlType={'submit'}
                block
                style={{ height: '40px' }}
              >
                Salvar
              </Button>
            </Col>
          )}
          {formDisabled && current === steps.length - 1 && (
            <Col xs={12} lg={12}>
              <Button
                style={{ width: '100%', height: '40px' }}
                htmlType={'submit'}
                type={'primary'}
                onClick={() => {
                  navigate(`/vigilancias/edicao/${params.id}`);
                }}
              >
                Editar
              </Button>
            </Col>
          )}
        </Row>
      </Form>

      <CustomModal
        centered
        open={showModalFinalizacao}
        width={750}
        footer={null}
        destroyOnClose
        closable={false}
      >
        <Card
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          bordered={false}
        >
          <Row
            gutter={24}
            style={{
              display: 'flex',
              flexDirection: 'row',
              textAlign: 'center',
            }}
          >
            <Col xs={24} sm={24}>
              <Typography.Paragraph>
                {window.navigator.onLine
                  ? 'FVV salvo com sucesso'
                  : 'FVV salvo localmente'}
              </Typography.Paragraph>
            </Col>
            <Col xs={24} sm={24}>
              <Typography.Paragraph style={{ fontWeight: 'bold' }}>
                {window.navigator.onLine
                  ? dadosFVVModalFinalizacao?.numero
                  : 'Ela será enviada para o servidor quando houver conexão novamente'}
              </Typography.Paragraph>
            </Col>
          </Row>

          <Row
            align='middle'
            style={{
              justifyContent: 'center',
            }}
            gutter={24}
          >
            <Col>
              {!params.local && (
                <Link to={`${window.navigator.onLine ? '/vigilancias' : '/'}`}>
                  <Button type={'primary'}> Voltar </Button>
                </Link>
              )}

              {params.local && (
                <Link to={'/acoes-de-campo'}>
                  <Button type={'primary'}> Voltar </Button>
                </Link>
              )}
            </Col>
            {window.navigator.onLine && !params.local && (
              <Col>
                <Link
                  to={`/vigilancias/visualizar/${dadosFVVModalFinalizacao?.id}`}
                >
                  <Button type={'primary'}> Visualizar </Button>
                </Link>
              </Col>
            )}
          </Row>
        </Card>
      </CustomModal>

      <CustomModal
        open={fverNaoEncontrado}
        closable={true}
        maskClosable={false}
        keyboard={false}
        closeIcon={<CloseOutlined style={{ color: 'white' }} />}
        bodyStyle={{ padding: 0 }}
        footer={null}
        width={'100%'}
        onCancel={() => {
          Modal.confirm({
            content: 'Deseja sair sem vincular o FVER?',
            onOk: () => {
              navigate('/acoes-de-campo');
            },
          });
        }}
      >
        <div
          style={{
            width: '100%',
            height: '60px',
            boxShadow: '0 0 20px -10px rgba(@text-color, 0.5)',
            backgroundColor: '#337ab7',
          }}
        >
          <Typography.Title
            level={4}
            className='centeredElement'
            style={{
              color: 'white',
              margin: 'auto',
              paddingTop: '12px',
              width: '100%',
              textAlign: 'center',
            }}
          >
            FVER não encontrado
          </Typography.Title>
        </div>

        <div
          style={{
            marginTop: '15px',
            marginLeft: '25px',
            marginRight: '25px',
          }}
        >
          <Typography.Paragraph style={{ textAlign: 'justify' }}>
            Devido a um erro de versões anteriores, este FVV perdeu a referência
            ao seu FVER. Por causa disso, será necessário selecionar um novo
            FVER.
          </Typography.Paragraph>

          <FilterPanelFVER open />

          <br />
          <Table<FVER.Summary>
            rowKey={'id'}
            dataSource={fvers?.content}
            size={'small'}
            loading={fetching}
            pagination={{
              current: query.page ? query.page + 1 : 1,
              onChange: (page, pageSize) => {
                dispatch(
                  FVERActions.setQuery({ page: page - 1, size: pageSize })
                );
              },
              total: fvers?.totalElements,
              pageSize: query.size,
            }}
            columns={[
              {
                dataIndex: 'numero',
                title: 'Número',
                width: 175,
                responsive: ['xs', 'sm'],
                render(numero) {
                  return <span style={{ fontWeight: 'bold' }}>{numero}</span>;
                },
              },
              {
                dataIndex: 'dataDaVisita',
                title: 'Data',
                width: 100,
                responsive: ['xs', 'sm'],
                render(data: FVER.Summary['dataDaVisita']) {
                  return moment(new Date(data)).format('DD/MM/YYYY');
                },
              },
              {
                dataIndex: 'tipoEstabelecimento',
                title: 'Tipo',
                width: 150,
                responsive: ['md'],
                render(tipo: FVER.Summary['tipoEstabelecimento']) {
                  return (
                    <Tag>{TipoEstabelecimento.valueOf(tipo).toString()}</Tag>
                  );
                },
              },
              {
                dataIndex: 'nomeEstabelecimento',
                title: 'Nome',
                responsive: ['sm'],
                render(nome: FVER.Summary['nomeEstabelecimento']) {
                  return <Tooltip title={nome}>{nome}</Tooltip>;
                },
              },
              {
                dataIndex: 'id',
                title: '',
                responsive: ['xs', 'sm'],
                width: 130,
                align: 'right',
                render(idVisita: number, fverSelected) {
                  return (
                    <Space>
                      <Button
                        icon={<SelectOutlined style={{ color: 'green' }} />}
                        size={'small'}
                        type={'ghost'}
                        onClick={() => {
                          Modal.confirm({
                            title: (
                              <>
                                Deseja selecionar o FVER nº{' '}
                                {fverSelected.numero}
                              </>
                            ),
                            content: (
                              <Typography.Paragraph>
                                Esta ação não pode ser desfeita.
                              </Typography.Paragraph>
                            ),
                            onOk: async () => {
                              await FVERService.getById(fverSelected.id)
                                .then((fver) => {
                                  form.setFieldValue(
                                    'visitaPropriedadeRural',
                                    fver
                                  );
                                  setFVERNaoEncontrado(false);
                                  notification.success({
                                    message: (
                                      <>
                                        FVER nº {fver.numero} vinculado ao FVV
                                      </>
                                    ),
                                  });
                                })
                                .catch((e) => {
                                  notification.error({
                                    message: 'Erro ao buscar o FVER',
                                  });
                                });
                            },
                          });
                        }}
                      />
                    </Space>
                  );
                },
              },
            ]}
          ></Table>
        </div>
        <br />
      </CustomModal>
    </>
  );
}
