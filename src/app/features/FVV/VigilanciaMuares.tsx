import {
  Card,
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Row,
  Switch,
  Tooltip,
  Typography,
} from 'antd';
import SintomasObservadosPanel from '../../components/SintomasObservadosPanel';
import { SintomasObservados } from '../../../core/enums/SintomasObservados';

type VigilanciaMuaresProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaMuares(props: VigilanciaMuaresProps) {
  const triggerRecountTotaExaminadosMuares = () => {
    const ate6Meses: number = props.form.getFieldValue([
      'vigilanciaMuar',
      'quantidadeInspecionadosAbaixo06Meses',
    ]);
    const acima6Meses: number = props.form.getFieldValue([
      'vigilanciaMuar',
      'quantidadeInspecionadosAcima06Meses',
    ]);
    const dados = [ate6Meses, acima6Meses];

    let totalExaminados: number = 0;

    dados.forEach((qtdade) => {
      if (!isNaN(Number(qtdade))) {
        totalExaminados = Number(totalExaminados) + Number(qtdade);

        props.form.setFieldValue(
          ['vigilanciaMuar', 'quantidadeExaminados'],
          totalExaminados
        );
      }
    });
  };

  return (
    <>
      <Typography.Title level={5}>Muares examinados</Typography.Title>

      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8}>
          <Form.Item
            label={'Até 6 meses'}
            name={['vigilanciaMuar', 'quantidadeInspecionadosAbaixo06Meses']}
          >
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onBlur={(event: any) => {
                triggerRecountTotaExaminadosMuares();
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Acima de 6 meses'}
            name={['vigilanciaMuar', 'quantidadeInspecionadosAcima06Meses']}
          >
            <Input
              inputMode='numeric'
              autoComplete='off'
              maxLength={5}
              onKeyPress={(event: any) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onBlur={(event: any) => {
                triggerRecountTotaExaminadosMuares();
              }}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Examinados'}
            name={['vigilanciaMuar', 'quantidadeExaminados']}
          >
            <Input disabled />
          </Form.Item>
        </Col>

        <Col xs={24} lg={8}>
          <Tooltip
            title={
              'Preencher com todos os animais observados, inclusive os examinados'
            }
          >
            <Form.Item
              label={'Vistoriados'}
              name={['vigilanciaMuar', 'quantidadeVistoriados']}
            >
              <Input
                inputMode='numeric'
                maxLength={4}
                autoComplete='off'
                onKeyPress={(event: any) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
              />
            </Form.Item>
          </Tooltip>
        </Col>
      </Row>
      <Row gutter={24}>
        <SintomasObservadosPanel
          listaSintomasObservados={SintomasObservados.asininos()}
          pathToCoordenada={'vigilanciaMuares'}
          form={props.form}
        />

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há suspeita clínica?'}
            name={['vigilanciaMuares', 'haSuspeitaClinica']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={16}>
          <Form.Item
            label={'Suspeita clinica'}
            name={['vigilanciaMuares', 'suspeitaClinica']}
          >
            <Input autoComplete='off' />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}
