import {
  Card,
  Col,
  Divider,
  Form,
  FormInstance,
  Input,
  Row,
  Switch,
  Tooltip,
  Typography,
} from 'antd';
import SintomasObservadosPanel from '../../components/SintomasObservadosPanel';
import { SintomasObservados } from '../../../core/enums/SintomasObservados';

type VigilanciaCaprinosProps = {
  form: FormInstance;
  formDisabled?: boolean;
};

export default function VigilanciaCaprinos(props: VigilanciaCaprinosProps) {
  const triggerRecountTotaExaminadosCaprinos = () => {
    const _00_12_Femeas: number = props.form.getFieldValue([
      'vigilanciaCaprino',
      'quantidadeInspecionadosAte_12_F',
    ]);
    const _00_12_Machos: number = props.form.getFieldValue([
      'vigilanciaCaprino',
      'quantidadeInspecionadosAte_12_M',
    ]);
    const _12_Acima_Femeas: number = props.form.getFieldValue([
      'vigilanciaCaprino',
      'quantidadeInspecionadosAcima_12_F',
    ]);
    const _12_Acima_Machos: number = props.form.getFieldValue([
      'vigilanciaCaprino',
      'quantidadeInspecionadosAcima_12_M',
    ]);

    const dados = [
      _00_12_Femeas,
      _00_12_Machos,
      _12_Acima_Femeas,
      _12_Acima_Machos,
    ];

    let totalExaminados: number = 0;

    dados.forEach((qtdade) => {
      if (!isNaN(Number(qtdade))) {
        totalExaminados = Number(totalExaminados) + Number(qtdade);

        props.form.setFieldValue(
          ['vigilanciaCaprino', 'quantidadeExaminados'],
          totalExaminados
        );
      }
    });
  };

  return (
    <>
      <Typography.Title level={5}>Caprinos examinados</Typography.Title>

      <Divider />

      <Row gutter={24}>
        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Até 12 meses'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaCaprino',
                    'quantidadeInspecionadosAte_12_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosCaprinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaCaprino',
                    'quantidadeInspecionadosAte_12_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosCaprinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Acima de 12 meses'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Fêmeas'}
                  name={[
                    'vigilanciaCaprino',
                    'quantidadeInspecionadosAcima_12_F',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosCaprinos();
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Machos'}
                  name={[
                    'vigilanciaCaprino',
                    'quantidadeInspecionadosAcima_12_M',
                  ]}
                >
                  <Input
                    inputMode='numeric'
                    maxLength={4}
                    autoComplete='off'
                    onKeyPress={(event: any) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onBlur={(event: any) => {
                      triggerRecountTotaExaminadosCaprinos();
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>

        <Col xs={24} lg={8} style={{ marginBottom: '24px' }}>
          <Card title='Total'>
            <Row gutter={24}>
              <Col xs={24} lg={12}>
                <Form.Item
                  label={'Examinados'}
                  name={['vigilanciaCaprino', 'quantidadeExaminados']}
                >
                  <Input disabled />
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Tooltip
                  title={
                    'Preencher com todos os animais observados, inclusive os examinados'
                  }
                >
                  <Form.Item
                    label={'Vistoriados'}
                    name={['vigilanciaCaprino', 'quantidadeVistoriados']}
                  >
                    <Input
                      inputMode='numeric'
                      maxLength={4}
                      autoComplete='off'
                      onKeyPress={(event: any) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  </Form.Item>
                </Tooltip>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row gutter={24}>
        <SintomasObservadosPanel
          listaSintomasObservados={SintomasObservados.caprinos()}
          pathToCoordenada={'vigilanciaCaprino'}
          form={props.form}
        />

        <Col xs={24} lg={8}>
          <Form.Item
            label={'Há suspeita clínica?'}
            name={['vigilanciaCaprino', 'haSuspeitaClinica']}
            valuePropName={'checked'}
          >
            <Switch
              defaultChecked={false}
              size={'default'}
              disabled={props.formDisabled}
            />
          </Form.Item>
        </Col>

        <Col xs={24} lg={16}>
          <Form.Item
            label={'Suspeita clinica'}
            name={['vigilanciaCaprino', 'suspeitaClinica']}
          >
            <Input autoComplete='off' />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}
